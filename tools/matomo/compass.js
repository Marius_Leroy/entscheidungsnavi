document.addEventListener('DOMContentLoaded', () => {
  if (!window.dtMatomoSiteId) return;

  var _paq = (window._paq = window._paq || []);
  _paq.push(['enableLinkTracking']);

  var u = '/tunnel/';
  _paq.push(['setTrackerUrl', u + 'compass.php']);
  _paq.push(['setSiteId', window.dtMatomoSiteId]);
  var d = document,
    g = d.createElement('script'),
    s = d.getElementsByTagName('script')[0];
  g.async = true;
  g.src = u + 'compass.js';
  s.parentNode.insertBefore(g, s);
});
