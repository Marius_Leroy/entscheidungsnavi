#!/bin/bash

mongoimport --host mongo --db navi --collection users --jsonArray --file /users.json
mongoimport --host mongo --db navi --collection projects --jsonArray --file /projects.json
mongoimport --host mongo --db navi --collection teams --jsonArray --file /teams.json
mongoimport --host mongo --db navi --collection quickstarttags --jsonArray --file /quickstarttags.json
mongoimport --host mongo --db navi --collection quickstartprojects --jsonArray --file /quickstartprojects.json
mongoimport --host mongo --db navi --collection quickstartvalues --jsonArray --file /quickstartvalues.json
mongoimport --host mongo --db navi --collection events --jsonArray --file /events.json
mongoimport --host mongo --db navi --collection eventregistrations --jsonArray --file /eventregistrations.json
