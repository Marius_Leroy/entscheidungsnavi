import { QuickstartProject } from '@entscheidungsnavi/api-types';
import { IsNotMongoId } from '@entscheidungsnavi/tools';
import { Type } from 'class-transformer';
import { IsBoolean, IsDate, IsDefined, IsMongoId, IsNotEmpty, IsOptional, Validate } from 'class-validator';

export class QuickstartProjectDto implements QuickstartProject {
  @IsMongoId()
  id: string;

  @IsNotEmpty()
  name: string;

  @IsOptional()
  data?: any;

  @IsDefined()
  @IsMongoId({ each: true })
  tags: string[];

  @IsDefined()
  @IsBoolean()
  visible = false;

  @IsOptional()
  @Validate(IsNotMongoId)
  @IsNotEmpty()
  shareToken?: string;

  @IsNotEmpty()
  shareUrl: string;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}

export class QuickstartProjectWithDataDto extends QuickstartProjectDto {
  override data: any;
}
