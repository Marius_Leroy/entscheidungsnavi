import {
  GroupedQuickstartObjective,
  GroupedQuickstartObjectiveList,
  GroupedQuickstartObjectiveWithStats,
  GroupedQuickstartObjectiveListWithStats,
} from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDefined, IsInt, IsMongoId, IsNumber, IsString, ValidateNested } from 'class-validator';
import { QuickstartObjectiveDto, QuickstartObjectiveWithStatsDto } from './quickstart-objective.dto';

export class GroupedQuickstartObjectiveDto implements GroupedQuickstartObjective {
  @IsString()
  name: string;

  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartObjectiveDto)
  items: QuickstartObjectiveDto[];
}

export class GroupedQuickstartObjectiveListDto implements GroupedQuickstartObjectiveList {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => GroupedQuickstartObjectiveDto)
  items: GroupedQuickstartObjectiveDto[];

  @IsDefined()
  @IsMongoId({ each: true })
  activeTags: string[];

  @IsInt()
  count: number;
}

export class GroupedQuickstartObjectiveWithStatsDto extends GroupedQuickstartObjectiveDto implements GroupedQuickstartObjectiveWithStats {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartObjectiveWithStatsDto)
  override items: QuickstartObjectiveWithStatsDto[];

  @IsNumber()
  accumulatedScore: number;
}

export class GroupedQuickstartObjectiveListWithStatsDto implements GroupedQuickstartObjectiveListWithStats {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => GroupedQuickstartObjectiveWithStatsDto)
  items: GroupedQuickstartObjectiveWithStatsDto[];

  @IsDefined()
  @IsMongoId({ each: true })
  activeTags: string[];

  @IsInt()
  count: number;
}
