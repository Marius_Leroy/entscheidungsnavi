import { QuickstartValue, QuickstartValueWithStats } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDate, IsDefined, IsInt, IsMongoId, IsNumber, ValidateNested } from 'class-validator';
import { LocalizedStringDto } from '../../common/localized-string.dto';

export class QuickstartValueDto implements QuickstartValue {
  @IsMongoId()
  id: string;

  @IsDefined()
  @ValidateNested()
  @Type(() => LocalizedStringDto)
  name: LocalizedStringDto;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;

  @IsDate()
  @Type(() => Date)
  updatedAt: Date;
}

export class QuickstartValueListDto {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartValueDto)
  list: QuickstartValueDto[];
}

export class QuickstartValueWithStatsDto extends QuickstartValueDto implements QuickstartValueWithStats {
  @IsInt()
  countInTop5: number;

  @IsInt()
  countAssigned: number;

  @IsInt()
  countTracked: number;

  @IsNumber()
  accumulatedScore: number;
}

export class QuickstartValueWithStatsListDto {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => QuickstartValueWithStatsDto)
  list: QuickstartValueWithStatsDto[];
}
