import { Type } from 'class-transformer';
import { IsDate, IsMongoId, IsString } from 'class-validator';
import { TeamComment as ApiTeamComment } from '@entscheidungsnavi/api-types';

export class TeamCommentDto implements ApiTeamComment {
  @IsMongoId()
  id: string;

  @IsString()
  objectId: string;

  @IsMongoId()
  author: string;

  @IsString()
  content: string;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;
}
