import { TeamInviteInfo } from '@entscheidungsnavi/api-types';
import { IsString } from 'class-validator';

export class TeamInviteInfoDto implements TeamInviteInfo {
  @IsString()
  teamName: string;

  @IsString()
  inviterName: string;
}
