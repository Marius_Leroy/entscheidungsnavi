import { Type } from 'class-transformer';
import { IsDate, IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Team as ApiTeam } from '@entscheidungsnavi/api-types';
import { TeamMemberDto } from './team-member.dto';
import { TeamInviteDto } from './team-invite.dto';

export class TeamDto implements ApiTeam {
  @IsString()
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @Type(() => TeamMemberDto)
  @ValidateNested({ each: true })
  members: readonly TeamMemberDto[];

  @Type(() => TeamInviteDto)
  @ValidateNested({ each: true })
  pendingInvites: TeamInviteDto[];

  @IsString()
  @IsNotEmpty()
  editor: string;

  @IsString()
  @IsNotEmpty()
  owner: string;

  @IsString()
  whiteboard: string;

  @IsDate()
  @Type(() => Date)
  createdAt: Date;
}
