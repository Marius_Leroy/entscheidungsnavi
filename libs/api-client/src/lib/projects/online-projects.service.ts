import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { Observable, map } from 'rxjs';
import { ProjectUpdate } from '@entscheidungsnavi/api-types';
import { ProjectListDto } from './dto/project-list.dto';
import { ProjectDto, ProjectWithDataDto } from './dto/project.dto';
import { ProjectHistoryEntryListDto } from './dto/project-history-entry.dto';

@Injectable({
  providedIn: 'root',
})
export class OnlineProjectsService {
  constructor(private http: HttpClient) {}

  getProjectList() {
    return this.http.get<unknown>('/api/projects').pipe(
      map(list => ({ list })),
      transformAndValidate(ProjectListDto),
      logError()
    );
  }

  public getProject(id: string) {
    return this.http.get<unknown>(`/api/projects/${id}`).pipe(transformAndValidate(ProjectWithDataDto), logError());
  }

  getHistory(projectId: string) {
    return this.http.get<unknown>(`/api/projects/${projectId}/history`).pipe(
      map(list => ({ list })),
      transformAndValidate(ProjectHistoryEntryListDto),
      logError()
    );
  }

  getDataFromHistory(projectId: string, historyId: string) {
    return this.http.get(`/api/projects/${projectId}/history/${historyId}/data`, {
      responseType: 'text',
    });
  }

  public addProject(name: string, projectString: string) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http
      .post<unknown>('/api/projects', { name, data: projectString }, { headers })
      .pipe(transformAndValidate(ProjectDto), logError());
  }

  public renameProject(id: string, name: string) {
    return this.updateProject(id, { name });
  }

  public updateProject(id: string, body: ProjectUpdate) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.patch<unknown>(`/api/projects/${id}`, body, { headers }).pipe(transformAndValidate(ProjectDto), logError());
  }

  public deleteProject(id: string) {
    return this.http.delete<void>(`/api/projects/${id}`).pipe(logError());
  }

  public setShare(projectId: string, share: true): Observable<string>;
  public setShare(projectId: string, share: false): Observable<void>;
  public setShare(projectId: string, share: boolean): Observable<string | void> {
    if (share) {
      return this.http.post(`/api/projects/${projectId}/share`, null, { responseType: 'text' }).pipe(logError());
    } else {
      return this.http.delete<void>(`/api/projects/${projectId}/share`).pipe(logError());
    }
  }

  public getProjectFromShareToken(token: string) {
    return this.http.get<unknown>(`/api/shared/${token}`).pipe(transformAndValidate(ProjectWithDataDto), logError());
  }
}
