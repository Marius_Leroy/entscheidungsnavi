import { ProjectHistoryEntry } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsBoolean, IsDate, IsDefined, IsMongoId, ValidateNested } from 'class-validator';

export class ProjectHistoryEntryDto implements ProjectHistoryEntry {
  @IsMongoId()
  id: string;

  @IsBoolean()
  resolvesConflict: boolean;

  @IsDate()
  @Type(() => Date)
  versionTimestamp: Date;
}

export class ProjectHistoryEntryListDto {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => ProjectHistoryEntryDto)
  list: ProjectHistoryEntryDto[];
}
