import { Type } from 'class-transformer';
import { IsDefined, ValidateNested } from 'class-validator';
import { AbstractSearchableList } from '../../common/abstract-searchable-list';
import { ProjectDto } from './project.dto';

export class ProjectListDto extends AbstractSearchableList<ProjectDto> {
  @IsDefined()
  @ValidateNested({ each: true })
  @Type(() => ProjectDto)
  list: ProjectDto[];
}
