import { UserList } from '@entscheidungsnavi/api-types';
import { Type } from 'class-transformer';
import { IsDefined, IsNumber } from 'class-validator';
import { UserDto } from '../../auth/dto/user.dto';

export class UserListDto implements UserList {
  @IsDefined()
  @Type(() => UserDto)
  items: UserDto[];

  @IsNumber()
  count: number;
}
