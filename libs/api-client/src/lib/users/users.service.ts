import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PaginationParams, Role, UserFilter, UserSort } from '@entscheidungsnavi/api-types';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { map } from 'rxjs';
import { UserDto } from '../auth/dto/user.dto';
import { ProjectListDto } from '../projects/dto/project-list.dto';
import { UserListDto } from './dto/user-list.dto';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  getUsers(filter: UserFilter, sort: UserSort, pagination: PaginationParams) {
    return this.http
      .get<unknown>('/api/users', { params: { ...filter, ...sort, ...pagination } })
      .pipe(transformAndValidate(UserListDto), logError());
  }

  getUser(userId: string) {
    return this.http.get<unknown>(`/api/users/${userId}`).pipe(transformAndValidate(UserDto), logError());
  }

  updateRoles(userId: string, roles: Role[]) {
    return this.http.patch<void>(`/api/users/${userId}`, { roles }).pipe(logError());
  }

  deleteUser(userId: string) {
    return this.http.delete<void>(`/api/users/${userId}`).pipe(logError());
  }

  getUserProjects(userId: string) {
    return this.http.get<unknown>(`/api/users/${userId}/projects`).pipe(
      map(list => ({ list })),
      transformAndValidate(ProjectListDto),
      logError()
    );
  }
}
