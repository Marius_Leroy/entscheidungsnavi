import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { logError, cachedSwitchAll, transformAndValidate } from '@entscheidungsnavi/tools';
import { EMPTY, Subject, combineLatest, map, startWith } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { EventRegistrationDto } from './dto/event-registration.dto';

@Injectable({
  providedIn: 'root',
})
export class EventsService {
  private refreshEvents$ = new Subject<void>();

  readonly events$ = combineLatest([this.authService.user$, this.refreshEvents$.pipe(startWith(null))]).pipe(
    map(([user]) => (user != null ? this.fetchEvents() : EMPTY)),
    cachedSwitchAll()
  );

  constructor(private http: HttpClient, private authService: AuthService) {}

  refreshEvents() {
    this.refreshEvents$.next();
    return this.events$;
  }

  private fetchEvents() {
    return this.http.get<unknown[]>(`/api/user/event-registrations`).pipe(transformAndValidate(EventRegistrationDto), logError());
  }

  joinEvent(code: string) {
    return this.http
      .post<void>(`/api/user/event-registrations`, {
        code,
      })
      .pipe(logError());
  }

  submitSubmission(registrationId: string) {
    return this.http.post<void>(`/api/user/event-registrations/${registrationId}/submit`, {}).pipe(logError());
  }

  updateSubmission(
    registrationId: string,
    update: Partial<Pick<EventRegistrationDto, 'freeText' | 'questionnaireResponses' | 'projectData'>>
  ) {
    return this.http
      .patch<unknown>(`/api/user/event-registrations/${registrationId}`, update)
      .pipe(transformAndValidate(EventRegistrationDto), logError());
  }

  leaveEvent(registrationId: string) {
    return this.http.delete<void>(`/api/user/event-registrations/${registrationId}`).pipe(logError());
  }
}
