import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KlugFinishProject } from '@entscheidungsnavi/api-types';
import { logError, transformAndValidate } from '@entscheidungsnavi/tools';
import { KlugProjectDto } from './dto/klug-project.dto';

@Injectable({
  providedIn: 'root',
})
export class KlugService {
  constructor(private http: HttpClient) {}

  getKlugProject(token: string) {
    return this.http.get<unknown>(`/api/klug/projects/${token}`).pipe(transformAndValidate(KlugProjectDto), logError());
  }

  updateKlugProject(token: string, update: { data: string }) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http
      .patch<unknown>(`/api/klug/projects/${token}`, update, { headers })
      .pipe(transformAndValidate(KlugProjectDto), logError());
  }

  generateUnofficialProject() {
    return this.http.post<unknown>('/api/klug/projects', {}).pipe(transformAndValidate(KlugProjectDto), logError());
  }

  finishProject(token: string, body: KlugFinishProject) {
    return this.http.post<void>(`/api/klug/projects/${token}/finish`, body);
  }
}
