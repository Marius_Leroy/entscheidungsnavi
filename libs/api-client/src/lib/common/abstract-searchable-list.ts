import escapeStringRegexp from 'escape-string-regexp';

interface ListItem {
  id: string;
  name: string;
}

export abstract class AbstractSearchableList<T extends ListItem> {
  abstract list: T[];

  search(text: string): T[] {
    const regexp = new RegExp(escapeStringRegexp(text), 'i');
    return this.list && this.list.filter(entry => regexp.test(entry.name));
  }

  remove(id: string) {
    const index = this.list.findIndex(p => p.id === id);
    if (index >= 0) {
      this.list.splice(index, 1);
    }
  }

  getMap(): Map<string, T> {
    return new Map(this.list.map((entry: T): [string, T] => [entry.id, entry]));
  }
}
