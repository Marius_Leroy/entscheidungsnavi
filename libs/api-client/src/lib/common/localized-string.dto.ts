import { LocalizedString } from '@entscheidungsnavi/api-types';
import { IsString } from 'class-validator';

export class LocalizedStringDto implements LocalizedString {
  @IsString()
  en: string;

  @IsString()
  de: string;
}
