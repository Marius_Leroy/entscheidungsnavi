import { Objective, ObjectiveType, IndicatorObjectiveData, Indicator, NumericalObjectiveData, VerbalObjectiveData } from '../classes';
import { validateValue } from './validations';

describe('validateValue', () => {
  let obj1: Objective, obj2: Objective, obj3: Objective, obj4: Objective, obj5: Objective;
  beforeEach(() => {
    obj1 = new Objective('', new NumericalObjectiveData(0, 10), undefined, undefined, ObjectiveType.Numerical);
    obj2 = new Objective('', new NumericalObjectiveData(40, -10), undefined, undefined, ObjectiveType.Numerical);
    obj3 = new Objective('', undefined, new VerbalObjectiveData(['1', '2', '3']), undefined, ObjectiveType.Verbal);
    obj4 = new Objective(
      '',
      undefined,
      undefined,
      new IndicatorObjectiveData([new Indicator('', 0, 10), new Indicator('', 40, -10)]),
      ObjectiveType.Indicator
    );
    obj5 = new Objective(
      '',
      undefined,
      undefined,
      new IndicatorObjectiveData([new Indicator('', 1, 10), new Indicator('', 40, -10)]),
      ObjectiveType.Indicator
    );
  });

  it('detects out of range on numerical', () => {
    expect(validateValue([[-1]], obj1)[0]).toBe(false);
    expect(validateValue([[0]], obj1)[0]).toBe(true);
    expect(validateValue([[5]], obj1)[0]).toBe(true);
    expect(validateValue([[10]], obj1)[0]).toBe(true);
    expect(validateValue([[11]], obj1)[0]).toBe(false);

    expect(validateValue([[-11]], obj2)[0]).toBe(false);
    expect(validateValue([[-10]], obj2)[0]).toBe(true);
    expect(validateValue([[0]], obj2)[0]).toBe(true);
    expect(validateValue([[40]], obj2)[0]).toBe(true);
    expect(validateValue([[41]], obj2)[0]).toBe(false);
  });

  it('detects out of range on indicator', () => {
    expect(validateValue([[-1], [-11]], obj4)[0]).toBe(false);
    expect(validateValue([[-1], [-10]], obj4)[0]).toBe(false);
    expect(validateValue([[0], [-11]], obj4)[0]).toBe(false);
    expect(validateValue([[0], [-10]], obj4)[0]).toBe(true);
    expect(validateValue([[5], [20]], obj4)[0]).toBe(true);
    expect(validateValue([[10], [40]], obj4)[0]).toBe(true);
    expect(validateValue([[11], [40]], obj4)[0]).toBe(false);
    expect(validateValue([[10], [41]], obj4)[0]).toBe(false);
    expect(validateValue([[11], [41]], obj4)[0]).toBe(false);
  });

  it('detects out of range on indicator [value = 0, aggregate in range]', () => {
    expect(validateValue([[0], [0]], obj5)[0]).toBe(false);
  });

  it('detects out of range on verbal', () => {
    expect(validateValue([[0]], obj3)[0]).toBe(false);
    expect(validateValue([[1]], obj3)[0]).toBe(true);
    expect(validateValue([[3]], obj3)[0]).toBe(true);
    expect(validateValue([[4]], obj3)[0]).toBe(false);
  });
});
