import { BehaviorSubject, Subject } from 'rxjs';
import { isRichTextEmpty } from '@entscheidungsnavi/tools';
import { pick } from 'lodash';
import {
  Alternative,
  DecisionQuality,
  DecisionStatement,
  ErrorMsg,
  Indicator,
  NotePage,
  NumericalObjectiveData,
  Objective,
  ObjectiveType,
  Outcome,
  ProjectMode,
  SavedValues,
  TimeRecording,
  UserdefinedInfluenceFactor,
  UserdefinedState,
  UtilityFunction,
  PredefinedInfluenceFactor,
} from './classes';
import * as validations from './validations';
import { Weights } from './classes/weights';
import { HintAlternatives } from './steps/hint-alternatives';
import { CommonData } from './export/properties';
import { NaviStep } from './steps/navi-step';
import { ObjectiveAspects, ObjectiveElement } from './steps/hint-aspects';
import { getAlternativeUtilitiesMeta, getUtilityMatrixMeta, getWeightedUtilityMatrixMeta } from './calculation';

export type AttachedObjectiveData = 'ideas' | 'outcomes' | 'weights' | 'utility' | 'scale' | 'comment';

export class DecisionData implements CommonData {
  objectiveAdded$ = new Subject<number>();
  objectiveRemoved$ = new Subject<number>();

  decisionProblem: string;
  authorName: string;

  projectMode$ = new BehaviorSubject<ProjectMode>('educational');
  get projectMode() {
    return this.projectMode$.value;
  }
  set projectMode(newMode: ProjectMode) {
    if (newMode !== this.projectMode) {
      this.projectMode$.next(newMode);
    }
  }

  decisionStatement: DecisionStatement;
  objectiveAspects: ObjectiveAspects;
  objectiveHierarchyMainElement: ObjectiveElement;
  objectives: Objective[];
  hintAlternatives: HintAlternatives;
  alternatives: Alternative[];
  outcomes: Outcome[][]; // [#alternatives][#objectives]
  influenceFactors: UserdefinedInfluenceFactor[];
  weights: Weights;
  decisionQuality: DecisionQuality;

  /**
   * We save one explanation for each step of the entscheidungsnavi.
   */
  stepExplanations: Record<NaviStep, string>;
  projectNotes: string;

  savedValues: SavedValues;
  timeRecording: TimeRecording;
  resultSubstepProgress: number; // Indicates which results substep is currently active (or null if none)
  lastUrl: string;

  // Field that can be used to add additional data to the project that will be saved / loaded.
  extraData: { [key: string]: unknown };

  constructor(public version = '0.0.0') {
    this.reset(version);
  }

  reset(version = '0.0.0') {
    this.decisionQuality = new DecisionQuality();
    this.decisionStatement = new DecisionStatement();
    this.decisionProblem = '';
    this.objectiveAspects = new ObjectiveAspects(this);
    this.objectiveHierarchyMainElement = new ObjectiveElement();
    this.objectives = [];
    this.weights = new Weights();
    this.alternatives = [];
    this.influenceFactors = [];
    this.projectNotes = '';
    this.outcomes = [];
    this.stepExplanations = {
      decisionStatement: '',
      objectives: '',
      alternatives: '',
      impactModel: '',
      results: '',
      finishProject: '',
    };
    this.hintAlternatives = new HintAlternatives(this);
    this.version = version;
    this.lastUrl = null;
    this.resultSubstepProgress = null;
    this.savedValues = new SavedValues();
    this.timeRecording = new TimeRecording();
    NotePage.resetCounters();
    this.extraData = {};
    this.projectMode = 'educational';
  }

  isEducational() {
    return this.projectMode === 'educational';
  }

  isProfessional() {
    return this.projectMode === 'professional';
  }

  isStarter() {
    return this.projectMode === 'starter';
  }

  addObjective(objective?: Objective, position = this.objectives.length) {
    if (objective === undefined) {
      objective = new Objective();
    }

    this.objectives.splice(position, 0, objective); // insert objective at the given position
    this.weights.addObjective(position);
    this.hintAlternatives.addObjective(position);

    // add the new outcome objects for the new objective
    this.outcomes.forEach(row => row.splice(position, 0, new Outcome(null, objective)));

    this.objectiveAdded$.next(position);
  }

  moveObjective(from: number, to: number) {
    this.objectives.splice(to, 0, ...this.objectives.splice(from, 1));
    this.weights.moveObjective(from, to);
    this.outcomes.forEach(row => row.splice(to, 0, ...row.splice(from, 1)));
  }

  removeObjective(position: number) {
    this.objectives.splice(position, 1);
    this.weights.removeObjective(position);
    this.hintAlternatives.removeObjective(position);
    this.outcomes.forEach(e => e.splice(position, 1));

    this.objectiveRemoved$.next(position);
  }

  /**
   * Changes the type of an objective.
   *
   * @returns true when a manual tradeoff for this objective was deleted as part of this process
   */
  changeObjectiveType(position: number, newType: ObjectiveType) {
    // Only change when the newType is in fact different
    if (this.objectives[position].objectiveType === newType) return;

    // We change the type of the objective...
    const objective = this.objectives[position];
    objective.objectiveType = newType;

    // ...but also have to update the value entries of the outcomes
    this.outcomes.forEach(ocForAlternative => {
      ocForAlternative[position].initializeValues(objective);
    });

    return this.onUtilityFunctionChange(position);
  }

  /**
   * Called whenever a utility function changes. Deletes manual tradeoffs if required.
   *
   * @param objectivePosition - The position of the objective whose utility function was changed
   * @returns true when a manual tradeoff was deleted
   */
  onUtilityFunctionChange(objectivePosition: number) {
    if (this.weights.manualTradeoffs[objectivePosition]) {
      this.weights.manualTradeoffs[objectivePosition] = null;
      return true;
    }

    return false;
  }

  addAlternative({
    alternative = new Alternative(1),
    position = this.alternatives.length,
  }: { alternative?: Alternative; position?: number } = {}): Alternative {
    if (this.hintAlternatives.screws && alternative.screwConfiguration.length !== this.hintAlternatives.screws.length)
      alternative.screwConfiguration = new Array(this.hintAlternatives.screws.length).fill(null);

    this.alternatives.splice(position, 0, alternative);
    this.outcomes.splice(
      position,
      0,
      this.objectives.map(obj => new Outcome(null, obj))
    );
    return alternative;
  }

  moveAlternative(from: number, to: number) {
    this.alternatives.splice(to, 0, ...this.alternatives.splice(from, 1));
    this.outcomes.splice(to, 0, ...this.outcomes.splice(from, 1));
  }

  removeAlternative(position: number) {
    this.alternatives.splice(position, 1);
    this.outcomes.splice(position, 1);
  }

  addInfluenceFactor(inf = new UserdefinedInfluenceFactor(), position = this.influenceFactors.length) {
    this.influenceFactors.splice(position, 0, inf);
    this.restoreInfluenceFactorIDs();
    return inf;
  }

  removeInfluenceFactor(position: number) {
    // reset all outcomes that depend on that uncertainty factor
    this.outcomes.forEach(row => {
      row?.forEach(outcome => {
        if (outcome?.influenceFactor === this.influenceFactors[position]) {
          outcome.setInfluenceFactor(undefined);
        }
      });
    });
    this.influenceFactors.splice(position, 1);
    this.restoreInfluenceFactorIDs();
  }

  moveInfluenceFactor(from: number, to: number) {
    this.influenceFactors.splice(to, 0, ...this.influenceFactors.splice(from, 1));
    this.restoreInfluenceFactorIDs();
  }

  // reset uncertainty factor IDs to their index
  restoreInfluenceFactorIDs() {
    this.influenceFactors.forEach((uf: UserdefinedInfluenceFactor, idx: number) => (uf.id = idx));
  }

  addState(uf: UserdefinedInfluenceFactor, state?: UserdefinedState, position?: number) {
    if (position == null) position = uf.states.length;

    uf.states.splice(position, 0, state);
    this.outcomes.forEach(e =>
      e.forEach(state => {
        if (state.influenceFactor === uf) {
          state.addUfState(position);
        }
      })
    );
  }

  removeState(uf: UserdefinedInfluenceFactor, position: number) {
    if (uf != null && position != null) {
      uf.states.splice(position, 1);
      this.outcomes.forEach(e =>
        e.forEach(state => {
          if (state.influenceFactor === uf) {
            state.removeUfState(position);
          }
        })
      );
    }
  }

  removeAllStates(uf: UserdefinedInfluenceFactor) {
    const length = uf.states.length;
    for (let i = 0; i < length; i++) {
      this.removeState(uf, 0);
    }
  }

  addObjectiveOption(objectivePosition: number, optionPosition?: number, optionName = '') {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].verbalData.addOption(optionPosition, optionName);
    }
  }

  removeObjectiveOption(objectivePosition: number, optionPosition: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].verbalData.removeOption(optionPosition);
      this.outcomes.forEach(e => (e[objectivePosition].processed = false));
    }
  }

  addObjectiveIndicator(objectivePosition: number, indicatorPosition?: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      const objInd = this.objectives[objectivePosition].indicatorData;
      // Default to adding it to the end of the list
      if (indicatorPosition == null) {
        indicatorPosition = objInd.indicators.length;
      }
      objInd.addIndicator(indicatorPosition, new Indicator());
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].addObjectiveIndicator(indicatorPosition);
      });
    }
  }

  removeObjectiveIndicator(objectivePosition: number, indicatorPosition: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].indicatorData.removeIndicator(indicatorPosition);
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].removeObjectiveIndicator(indicatorPosition);
      });
    }
  }

  moveObjectiveIndicator(objectivePosition: number, indicatorFromPosition: number, indicatorToPosition: number) {
    if (objectivePosition >= 0 && objectivePosition < this.objectives.length) {
      this.objectives[objectivePosition].indicatorData.moveIndicator(indicatorFromPosition, indicatorToPosition);
      this.outcomes.forEach(ocForAlternative => {
        ocForAlternative[objectivePosition].moveObjectiveIndicator(indicatorFromPosition, indicatorToPosition);
      });
    }
  }

  validateName(): [boolean, ErrorMsg[]] {
    return validations.validateName(this.decisionProblem);
  }

  validateDecisionStatement(): [boolean, ErrorMsg[]] {
    return validations.validateDecisionStatement(this.decisionStatement);
  }

  validateObjectives(): [boolean, ErrorMsg[]] {
    return validations.validateObjectives(this.objectives);
  }

  validateObjectiveScales(): [boolean, ErrorMsg[]] {
    return validations.validateObjectiveScales(this.objectives);
  }

  validateAlternatives(): [boolean, ErrorMsg[]] {
    return validations.validateAlternatives(this.alternatives);
  }

  validateUtilityFunctions(): [boolean, ErrorMsg[], number] {
    return validations.validateUtilityFunctions(this.objectives);
  }

  validateTradeoffObjective(): [boolean, ErrorMsg[]] {
    return validations.validateTradeoffObjective(this);
  }

  validateWeights(): [boolean, ErrorMsg[]] {
    return validations.validateWeights(this.weights.preliminaryWeights);
  }

  validateOutcomes(): [boolean, ErrorMsg[]] {
    return validations.validateOutcomes(this);
  }

  validateInfluenceFactors(): [boolean, ErrorMsg[], number] {
    return validations.validateInfluenceFactors(this.influenceFactors);
  }

  getUtilityMatrix() {
    return getUtilityMatrixMeta(this.outcomes, this.objectives);
  }

  getWeightedUtilityMatrix() {
    return getWeightedUtilityMatrixMeta(this.outcomes, this.objectives, this.weights);
  }

  /**
   * Compute utilities for every alternative.
   *
   * @returns utility for every alternative
   */
  getAlternativeUtilities() {
    return getAlternativeUtilitiesMeta(this.outcomes, this.objectives, this.weights);
  }

  getAttachedObjectiveData(objectiveIndex: number) {
    const data: AttachedObjectiveData[] = [];

    const objective = this.objectives[objectiveIndex];

    // Alternative Ideas
    const group = this.hintAlternatives.ideas ? this.hintAlternatives.ideas.noteGroups[objectiveIndex] : undefined;
    const hasIdeas = (group ? group.notes.length : 0) > 0;

    if (hasIdeas) {
      data.push('ideas');
    }

    // Impact Matrix
    const defaultObjective = new NumericalObjectiveData();
    const hasOutcomes = this.outcomes.some(row => row[objectiveIndex].processed);
    const hasCustomScale =
      !objective.isNumerical ||
      objective.numericalData.from !== defaultObjective.from ||
      objective.numericalData.to !== objective.numericalData.to;

    if (hasOutcomes) {
      data.push('outcomes');
    }

    if (hasCustomScale) {
      data.push('scale');
    }

    // Objective Weighting
    const defaultWeight = Weights.getDefault();
    const objectiveWeight = this.weights.getWeights()[objectiveIndex];
    const hasCustomWeight =
      objectiveWeight && (objectiveWeight.value !== defaultWeight.value || objectiveWeight.precision !== defaultWeight.precision);

    if (hasCustomWeight) {
      data.push('weights');
    }

    // Utility Function
    const defaultUtilityFunction = new UtilityFunction();

    let hasCustomUtilityFunction = false;
    if (objective.isVerbal) {
      hasCustomUtilityFunction =
        objective.verbalData.hasCustomUtilityValues ||
        objective.verbalData.c !== defaultUtilityFunction.c ||
        objective.verbalData.precision !== defaultUtilityFunction.precision;
    } else {
      const utilityFunction = objective.isNumerical
        ? objective.numericalData.utilityfunction
        : objective.isIndicator
        ? objective.indicatorData.utilityfunction
        : undefined;
      hasCustomUtilityFunction =
        utilityFunction &&
        (utilityFunction.c !== defaultUtilityFunction.c || utilityFunction.precision !== defaultUtilityFunction.precision);
    }

    if (hasCustomUtilityFunction) {
      data.push('utility');
    }

    // Comment
    if (!isRichTextEmpty(objective.comment)) {
      data.push('comment');
    }

    // No Data found
    return data;
  }

  getInaccuracies() {
    const inaccuracies = {
      utilityNumericalIndicator: false,
      utilityVerbal: false,
      weights: false,
      probabilities: false,
      userdefinedInfluenceFactors: false,
      predefinedInfluenceFactors: false,
    };

    for (let i = 0; i < this.objectives.length; i++) {
      const objective = this.objectives[i];

      // Utility Functions
      if (objective.isNumerical) {
        if (objective.numericalData.utilityfunction.precision > 0) {
          inaccuracies.utilityNumericalIndicator = true;
        }
      } else if (objective.isVerbal) {
        if (objective.verbalData.precision > 0) {
          inaccuracies.utilityVerbal = true;
        }
      } else if (objective.isIndicator) {
        if (objective.indicatorData.utilityfunction.precision > 0) {
          inaccuracies.utilityNumericalIndicator = true;
        }
      }

      // Weights
      const weight = this.weights.getWeight(i);

      if (weight.precision > 0) {
        inaccuracies.weights = true;
      }
    }

    for (const ocsForAlternative of this.outcomes) {
      for (const outcome of ocsForAlternative) {
        if (outcome.influenceFactor instanceof UserdefinedInfluenceFactor) {
          inaccuracies.userdefinedInfluenceFactors = true;
          inaccuracies.probabilities ||= outcome.influenceFactor.precision > 0;
        } else if (outcome.influenceFactor instanceof PredefinedInfluenceFactor) {
          inaccuracies.predefinedInfluenceFactors = true;
        }
      }
    }

    return inaccuracies;
  }

  /**
   * Find an Object identified by UUID.
   *
   * @param objectId - The UUID of the object
   * @returns the object
   */
  findObject(objectId: string) {
    const findInCollection = <T extends { uuid: string }>(collection: T[]) => {
      const index = collection.findIndex(object => object.uuid === objectId);

      if (index < 0) {
        return null;
      }

      return { object: collection[index], index };
    };

    return (
      findInCollection(this.objectives) ??
      findInCollection(this.alternatives) ??
      findInCollection(this.influenceFactors) ??
      findInCollection(this.outcomes.flat())
    );
  }

  /**
   * Returns a subset of properties of this DecisionData object that is relevant for detecting changes made by the user.   *
   * Among others, this excludes the Observables found in this object, and DecisionData references within children.
   */
  getPropertiesForChangeDetection() {
    const data = pick(this, [
      'projectMode',
      'decisionProblem',
      'decisionQuality',
      'decisionStatement',
      'objectiveHierarchyMainElement',
      'objectives',
      'objectiveAspects',
      'weights',
      'alternatives',
      'hintAlternatives',
      'influenceFactors',
      'projectNotes',
      'stepExplanations',
      'outcomes',
      'resultSubstepProgress',
      'savedValues',
    ]);
    for (const key of Object.keys(data)) {
      if (data[key] && typeof data[key] === 'object' && 'toJSON' in data[key] && typeof data[key]['toJSON'] === 'function') {
        data[key] = data[key].toJSON();
      }
    }
    return data;
  }
}
