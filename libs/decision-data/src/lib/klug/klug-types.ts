export type KlugCompareResult = 'more' | 'equal' | 'less' | 'missing';
export type KlugPairComparisons = KlugCompareResult[][];

export type KlugData = { pairComparisons: KlugPairComparisons };

export const KLUG_EXTRA_DATA_FIELD = 'klug';
