export type ScrewConfiguration = (number | null)[];

export interface ScrewConfigurationProvider {
  name: string;
  screwConfiguration: ScrewConfiguration;
}
