// the order of the array defines the import order. 'outcomes' has to be behind 'influenceFactors', 'alternatives' and 'objectives'
import {
  Alternative,
  Outcome,
  DecisionQuality,
  DecisionStatement,
  UserdefinedInfluenceFactor,
  Objective,
  Weights,
  ProjectMode,
  SavedValues,
  TimeRecording,
} from '../classes';
import { ObjectiveAspects, ObjectiveElement } from '../steps/hint-aspects';
import { HintAlternatives, NaviStep } from '../steps';

export const PROPERTY_NAMES = [
  'projectMode',
  'authorName',
  'decisionProblem',
  'decisionQuality',
  'decisionStatement',
  'objectiveHierarchyMainElement',
  'objectives',
  'objectiveAspects',
  'weights',
  'alternatives',
  'hintAlternatives',
  'influenceFactors',
  'projectNotes',
  'stepExplanations',
  'outcomes',
  'version',
  'lastUrl',
  'resultSubstepProgress',
  'savedValues',
  'timeRecording',
  'extraData',
];

// arrays for type checks

// properties of the type Array
export const ARRAY_PROPERTY_NAMES = ['objectives', 'alternatives', 'influenceFactors', 'outcomes'];
// properties of the type Object
export const OBJECT_PROPERTY_NAMES = [
  'decisionQuality',
  'decisionStatement',
  'hintAlternatives',
  'stepExplanations',
  'objectiveAspects',
  'weights',
  'objectiveHierarchyMainElement',
  'savedValues',
  'timeRecording',
  'extraData',
];
// optional properties
export const OPTIONAL_PROPERTY_NAMES = [
  'projectMode',
  'authorName',
  'decisionProblem',
  'decisionQuality',
  'decisionStatement',
  'objectiveHierarchyMainElement',
  'outcomes',
  'projectNotes',
  'stepExplanations',
  'objectiveAspects',
  'version',
  'lastUrl',
  'resultSubstepProgress',
  'savedValues',
  'timeRecording',
  'extraData',
];

export interface CommonData {
  projectMode: ProjectMode;
  authorName: string;
  decisionProblem: string;
  decisionQuality: DecisionQuality;
  decisionStatement: DecisionStatement;
  objectiveHierarchyMainElement: ObjectiveElement;
  objectives: Objective[];
  objectiveAspects: ObjectiveAspects;
  weights: Weights;
  alternatives: Alternative[];
  hintAlternatives: HintAlternatives;
  influenceFactors: UserdefinedInfluenceFactor[];
  projectNotes: string;
  stepExplanations: Record<NaviStep, string>;
  outcomes: Outcome[][];
  version: string;
  lastUrl: string;
  resultSubstepProgress: number;
  savedValues: SavedValues;
  timeRecording: TimeRecording;
  extraData: { [key: string]: unknown };
}

export interface ExportData extends CommonData {
  exportVersion: string;
}
