import { Tree, ensureIsRichText, isRichTextEmpty } from '@entscheidungsnavi/tools';
import { defaultTo } from 'lodash';
import Delta from 'quill-delta/dist/Delta';

import { DecisionData } from '../decision-data';
import {
  Alternative,
  DECISION_QUALITY_CRITERIA,
  DecisionQuality,
  DecisionStatement,
  Indicator,
  IndicatorObjectiveData,
  InfluenceFactor,
  Note,
  NoteGroup,
  NotePage,
  NumericalObjectiveData,
  Objective,
  ObjectiveType,
  ObjectiveWeight,
  Outcome,
  PREDEFINED_INFLUENCE_FACTORS,
  SavedValues,
  TimeRecording,
  UserdefinedInfluenceFactor,
  UserdefinedState,
  UtilityFunction,
  Value,
  VerbalObjectiveData,
  VerbalObjectiveDataInitParams,
} from '../classes';
import { Weights } from '../classes/weights';
import { ObjectiveAspects, ObjectiveElement } from '../steps/hint-aspects';
import { HintAlternatives, NAVI_STEP_ORDER, NaviStep, Screw } from '../steps';

export function loadStatement(ds: DecisionStatement, target: DecisionData) {
  if (ds != null) {
    if (ds.notes && ds.notes.length > 4) {
      ds.notes[3] = ds.notes
        .slice(3)
        .filter(v => v)
        .join('\n');

      ds.notes = ds.notes.slice(0, 4) as [string, string, string, string];
    }

    target.decisionStatement = new DecisionStatement(
      ds.statement,
      ds.statement_attempt,
      ds.statement_attempt_2,
      ds.questions,
      ds.values.map(value => new Value(value.name, value.val, value.valueId)),
      ds.preNotes,
      ds.afterNotes,
      ds.preNotes_2,
      ds.afterNotes_2,
      ds.preNotesFinal,
      ds.afterNotesFinal,
      ds.notes,
      ds.subStepProgression
    );
  }
}

export function loadObjectives(objectives: Objective[], target: DecisionData) {
  objectives.forEach((z: Objective) => {
    target.objectives.push(loadObjective(z));
  });
}

export function loadObjective(objectiveLike: Objective) {
  // Compatibility to older version of objectives which only contained
  // numerical and verbal objectives.
  let objectiveType: ObjectiveType;
  if (objectiveLike['isNumerisch'] != null) {
    objectiveType = objectiveLike['isNumerisch'] ? ObjectiveType.Numerical : ObjectiveType.Verbal;
  } else {
    objectiveType = objectiveLike.objectiveType;
  }

  // Transform string[] aspects into tree
  if (objectiveLike.aspects && Array.isArray(objectiveLike.aspects)) {
    const stringAspects = <string[]>objectiveLike.aspects;
    objectiveLike.aspects = new Tree<ObjectiveElement>(
      new ObjectiveElement(objectiveLike.name),
      stringAspects.map(aspect => {
        return new Tree<ObjectiveElement>(new ObjectiveElement(aspect));
      })
    );
  }

  // Convert old plain text comments into quill json
  if (objectiveLike.comment) {
    objectiveLike.comment = ensureIsRichText(objectiveLike.comment);
  }

  const numericalData = loadNumericalObjectiveData(objectiveLike.numericalData ?? objectiveLike['zielNumerisch']);
  const verbalData = loadVerbalObjectiveData(objectiveLike.verbalData ?? objectiveLike['zielVerbal']);
  const indicatorData = loadIndicatorObjectiveData(objectiveLike.indicatorData ?? objectiveLike['zielIndicator']);

  // Put together the objective object
  return new Objective(
    objectiveLike.name,
    numericalData,
    verbalData,
    indicatorData,
    objectiveType,
    objectiveLike.displayed,
    objectiveLike.comment,
    objectiveLike.aspects,
    objectiveLike.placeholder,
    objectiveLike.uuid
  );
}

function loadNumericalObjectiveData(nu: NumericalObjectiveData): NumericalObjectiveData {
  let nf: UtilityFunction;
  const sourceNfNum = nu.utilityfunction ?? nu['nutzenfunktion'];
  if (sourceNfNum) {
    nf = loadUtilityFunction(sourceNfNum);
  } else {
    // import old format without UtilityFunction class
    nf = new UtilityFunction(nu['nutzenFunktion']);
  }

  return new NumericalObjectiveData(
    nu.from ?? nu['von'],
    nu.to ?? nu['bis'],
    nu.unit ?? nu['einheit'],
    nf,
    nu.commentFrom ?? nu['comment_von'],
    nu.commentTo ?? nu['comment_bis'],
    nu.explanationFrom ?? nu['explanation_von'],
    nu.explanationTo ?? nu['explanation_bis']
  );
}

function loadVerbalObjectiveData(ve: VerbalObjectiveData): VerbalObjectiveData {
  const initParams: VerbalObjectiveDataInitParams = ve.initParams ?? ve['zielVerbalOptionInitParams'];
  const initParamsObj = initParams
    ? new VerbalObjectiveDataInitParams(
        initParams.stepNumber ?? initParams['stufen'],
        initParams.from ?? initParams['von'],
        initParams.to ?? initParams['bis']
      )
    : new VerbalObjectiveDataInitParams();

  const options: string[] = ve.options ?? ve['optionen'],
    comments = ve.comments ?? [],
    explanations = ve.explanations ?? [];
  let utilities = ve.utilities ?? ve['nutzen'] ?? [];

  // In previous versions, utilities, comments, and explanations arrays may have been empty.
  if (utilities.length === 0) utilities = options.map((_, index) => (index / (options.length - 1)) * 100);
  while (comments.length < options.length) comments.push('');
  while (explanations.length < options.length) explanations.push('');

  return new VerbalObjectiveData(
    options,
    utilities,
    ve.precision ?? ve['praezision'],
    comments,
    explanations,
    initParamsObj,
    ve.c,
    ve.hasCustomUtilityValues ?? true,
    ve.utilityFunctionExplanation
  );
}

function loadIndicatorObjectiveData(ind: IndicatorObjectiveData): IndicatorObjectiveData {
  if (ind != null) {
    let nfIndicator: UtilityFunction;
    if (ind.utilityfunction) {
      nfIndicator = loadUtilityFunction(ind.utilityfunction);
    } else {
      nfIndicator = new UtilityFunction();
    }
    return new IndicatorObjectiveData(
      loadIndicators(ind.indicators),
      nfIndicator,
      defaultTo(ind.useCustomAggregation, false),
      ind.customAggregationFormula,
      defaultTo(ind.defaultAggregationWorst, 0),
      defaultTo(ind.defaultAggregationBest, 100),
      defaultTo(ind.aggregatedUnit, '%'),
      ind.stages,
      ind.automaticCustomAggregationLimits
    );
  } else {
    return new IndicatorObjectiveData();
  }
}

function loadUtilityFunction(uf: UtilityFunction): UtilityFunction {
  return new UtilityFunction(
    uf.c,
    uf.precision ?? uf['praezisiongrad'],
    uf.width ?? uf['breite'],
    uf.level ?? uf['niveau'],
    uf.explanation
  );
}

// copy the indicators
function loadIndicators(indicators: Indicator[]): Indicator[] {
  return indicators.map(i => {
    let { min, max, coefficient } = i;

    // In newer versions, we enforce that coefficients are non-negative. If we import an indicator with a negative
    // coefficient, we flip the min/max bounds and take the absolute of the coefficient to achieve the same effect.
    if (coefficient < 0) {
      [min, max] = [max, min];
      coefficient *= -1;
    }

    // Make sure min !== max
    if (min === max) {
      max += 1;
    }

    return new Indicator(i.name, min, max, i.unit, coefficient, i.comment, i.verbalIndicatorCategories);
  });
}

export function loadAlternatives(alternatives: Alternative[], target: DecisionData) {
  alternatives.forEach((a: Alternative) => {
    // Convert old plain text comments into quill json
    if (a.comment) {
      a.comment = ensureIsRichText(a.comment);
    }

    target.alternatives.push(Alternative.clone(a));
  });
}

export function loadHintAlternatives(ha: HintAlternatives, target: DecisionData) {
  if (ha.subStepProgression) {
    target.hintAlternatives.subStepProgression = ha.subStepProgression;
  }

  if (ha.screws) {
    target.hintAlternatives.screws = ha.screws.map(screw => {
      if (typeof screw === 'string') {
        return new Screw(screw);
      }
      return new Screw(screw.name, screw.states, screw.comment);
    });
  }

  if (ha.ideas) {
    target.hintAlternatives.ideas = loadNotePage(ha.ideas);
  }
}

export function loadInfluenceFactors(ifs: UserdefinedInfluenceFactor[], target: DecisionData) {
  ifs.forEach((e: UserdefinedInfluenceFactor) => {
    // Convert old plain text comments into quill json
    if (e.comment) {
      e.comment = ensureIsRichText(e.comment);
    }

    target.influenceFactors.push(
      new UserdefinedInfluenceFactor(
        e.name,
        (e.states ?? e['zustaende']).map(
          (zustand: UserdefinedState) => new UserdefinedState(zustand.name, zustand.probability, zustand.comment)
        ),
        e.id,
        e.precision ?? e['praezision'],
        e.comment,
        e.uuid
      )
    );
  });
}

export function loadOutcomes(outcomesData: Outcome[][], target: DecisionData) {
  if (outcomesData) {
    const outcomes: Outcome[][] = outcomesData.map((e: any[]) =>
      e.map((a: any, objectiveIdx: number) => {
        let uf: InfluenceFactor;

        // (very) old export version (whole copy of the influence factor object)
        if (a.unsicherheitsfaktor != null) {
          const filteredUf: UserdefinedInfluenceFactor[] = target.influenceFactors.filter(
            (u: UserdefinedInfluenceFactor) => +u.id === +a.unsicherheitsfaktor.id
          );
          if (filteredUf.length === 1) {
            uf = filteredUf[0];
          } else {
            console.log(`Could not find UF ${a.unsicherheitsfaktor.id}`);
          }
        }

        // Current version saves the ID
        const ufId = a.influenceFactorId ?? a.unsicherheitsfaktor_id;
        if (ufId != null) {
          /* Get InfluenceFactor from either the hardcoded array or Decision Data. */
          // check if influenceFactorId is a string (predefined IF) or a number (user defined IF)
          if (typeof ufId === 'number') {
            /* Outcome uses a user defined IF. */
            const filteredUf: UserdefinedInfluenceFactor[] = target.influenceFactors.filter(
              (u: UserdefinedInfluenceFactor) => +u.id === +ufId
            );
            if (filteredUf.length === 1) {
              uf = filteredUf[0];
            } else {
              console.log(`Could not find UF ${ufId}`);
            }
          } else {
            /* Outcome uses a predefined IF. */
            uf = PREDEFINED_INFLUENCE_FACTORS[ufId];
          }
        }

        // Convert old plain text comments into quill json
        if (a.comment) {
          a.comment = ensureIsRichText(a.comment);
        }

        return new Outcome(a.values, target.objectives[objectiveIdx], uf, a.processed, a.comment, a.uuid);
      })
    );

    // insert missing fields
    target.outcomes = target.alternatives.map((_a, i) => {
      const row: Outcome[] = i < outcomes.length ? outcomes[i] : [];
      return target.objectives.map((objective, j) => (j < row.length ? row[j] : new Outcome(null, objective)));
    });
  }
}

export function loadObjectiveAspects(objectiveAspects: ObjectiveAspects, target: DecisionData) {
  if (objectiveAspects) {
    target.objectiveAspects = new ObjectiveAspects(
      target,
      objectiveAspects.subStepProgression,
      objectiveAspects.listOfAspects,
      objectiveAspects.listOfDeletedAspects
    );
  }
}

export function loadWeights(weights: Weights, target: DecisionData) {
  target.weights.tradeoffObjectiveIdx = weights.tradeoffObjectiveIdx;

  weights.preliminaryWeights.forEach(weight => {
    target.weights.preliminaryWeights.push(loadWeight(weight));
  });

  if (weights.tradeoffWeights) {
    weights.tradeoffWeights.forEach(weight => {
      if (weight === null) {
        target.weights.tradeoffWeights.push(null);
      } else {
        target.weights.tradeoffWeights.push(loadWeight(weight));
      }
    });
  } else {
    target.objectives.forEach(() => target.weights.tradeoffWeights.push(null));
  }

  if (weights.unverifiedWeights) {
    weights.unverifiedWeights.forEach(weight => target.weights.unverifiedWeights.push(weight));
  } else {
    target.weights.unverifiedWeights.length = target.weights.preliminaryWeights.length;
    target.weights.unverifiedWeights.fill(false);
  }

  if (weights.manualTradeoffs) {
    target.weights.manualTradeoffs = weights.manualTradeoffs;
  } else {
    target.weights.manualTradeoffs = target.objectives.map(() => null);
  }

  target.weights.explanations = weights.explanations ?? target.weights.preliminaryWeights.map(() => '');
}

function loadWeight(weight: number | ObjectiveWeight): ObjectiveWeight {
  if (typeof weight === 'number') {
    return new ObjectiveWeight(weight);
  } else {
    return new ObjectiveWeight(
      weight.value,
      weight.precision ?? weight['praezision'],
      weight.comparisonPointX,
      weight.activeReferencePointIndex
    );
  }
}

export function loadNotePage(input: NotePage): NotePage {
  const noteGroups: NoteGroup[] = input.noteGroups ?? input['notizGroups'];
  return new NotePage(
    noteGroups.map(group => {
      const notes: Note[] = group.notes ?? group['notizen'];
      return new NoteGroup(
        group.id,
        notes.map(entry => new Note(entry.id, entry.name)),
        group.name
      );
    })
  );
}

export function loadStepExplanations(explanations: Record<NaviStep, string>, target: DecisionData) {
  // Very old projects had no explanations associated
  if (explanations == null) {
    for (const step of NAVI_STEP_ORDER) {
      target.stepExplanations[step] = '';
    }
    return;
  }

  // Old explanations from steps objective weighting and utility function should be
  // moved to the results explanation
  if (explanations['utilityFunctions'] != null && explanations['weightsOfObjectives'] != null) {
    let delta = loadRichText(explanations.results);

    if (!isRichTextEmpty(explanations['utilityFunctions'])) {
      delta = loadRichText(explanations['utilityFunctions']).insert('\n').concat(delta);
    }
    if (!isRichTextEmpty(explanations['weightsOfObjectives'])) {
      delta = loadRichText(explanations['weightsOfObjectives']).insert('\n').concat(delta);
    }

    explanations.results = JSON.stringify(delta);
  }

  // Copy explanations over
  for (const step of NAVI_STEP_ORDER) {
    target.stepExplanations[step] = explanations[step];
  }
}

export function loadDecisionQuality(decisionQuality: DecisionQuality, target: DecisionData) {
  for (const criteria of DECISION_QUALITY_CRITERIA) {
    if (decisionQuality?.criteriaValues[criteria] != null) {
      target.decisionQuality.criteriaValues[criteria] = decisionQuality.criteriaValues[criteria];
    }
  }
}

export function loadSavedValues(savedValues: SavedValues, target: DecisionData) {
  if (savedValues == null || savedValues.projectDescriptionSelectedIndex < 0) {
    target.savedValues.projectDescriptionSelectedIndex = isRichTextEmpty(target.projectNotes) ? 3 : 0;
  } else {
    target.savedValues.projectDescriptionSelectedIndex = savedValues.projectDescriptionSelectedIndex;
  }
}

export function loadTimeRecording(timeRecording: TimeRecording, target: DecisionData) {
  if (timeRecording != null) {
    target.timeRecording.timers = timeRecording.timers;
  } else {
    target.timeRecording = new TimeRecording();
  }
}

/**
 * Tries to parse the input as Quill Delta. If that is not possible, falls back to plain text.
 */
function loadRichText(input: string): Delta {
  try {
    return new Delta(JSON.parse(input));
  } catch {
    // Could not parse JSON, assume it is plaintext
    const out = new Delta();
    if (input.length > 0) {
      out.insert(input);
    }
    return out;
  }
}
