import { clamp } from 'lodash';
import { Indicator, ObjectiveInput } from '../classes';
import { compileFormula } from '../tools';

/**
 * Returns the name of the variable in the aggregation function that corresponds to
 * the coefficient belonging to the given indicator.
 */
export function getIndicatorCoefficientName(indicatorIndex: number) {
  let name = '';
  let quotient = indicatorIndex + 1;

  // We basically convert the number to base 26 and display the digits as letters
  // from a to z.
  while (quotient > 0) {
    const remainder = (quotient - 1) % 26;
    name = String.fromCharCode(remainder + 97) + name;

    quotient = Math.floor((quotient - 1) / 26);
  }

  return name;
}

/**
 * Returns the name of the variable in the aggregation function that corresponds to
 * the value of the given indicator.
 */
export function getIndicatorValueName(indicatorIndex: number) {
  return `Ind${indicatorIndex + 1}`;
}

/**
 * Cache existing custom formulas so they don't have to be recompiled everytime
 */
const customFormulaCache = new Map<string, math.EvalFunction>();

/**
 * Either a custom formula or a worst/best combination for the default formula.
 */
export type AggregationSetting = string | { worst: number; best: number };

/**
 * Maps an indicator objective input to the aggregate.
 */
export type AggregationFunction = (values: ObjectiveInput) => number;

/**
 * Returns the aggregator function for the given indicator objective. The function calculates the aggregated
 * outcome value from the indicator input values. It uses the custom aggregator function in the objective
 * if available.
 *
 * @param indicators - The coefficients of the indicators
 * @param aggregationSetting - Either a custom aggregation formula as string to use or a worst/best value pair to use
 *                             with the default aggregation formula
 */
export function getIndicatorAggregationFunction(
  indicators: Pick<Indicator, 'min' | 'max' | 'coefficient' | 'verbalIndicatorCategories'>[],
  aggregationSetting: AggregationSetting
): AggregationFunction {
  // A function to verify that the input is valid
  const validateInput = (objectiveValues: ObjectiveInput) => {
    return objectiveValues.every(values => values != null && values.every(value => value != null));
  };

  if (typeof aggregationSetting !== 'string') {
    // Use the default aggregation function if no custom one is specified:
    // (coeff0 * (ind0 - min0) / (max0 - min0) + ...) / (coeff1 + coeff2 + ...)
    // Special consideration is given to when the coefficient is negative.
    const sumOfCoefficients = indicators.reduce((accumulator, currentValue) => accumulator + currentValue.coefficient, 0);

    // Avoid division by zero
    if (sumOfCoefficients === 0) {
      return (_values: ObjectiveInput) => 0;
    } else {
      return (values: ObjectiveInput) => {
        if (!validateInput(values)) {
          return NaN;
        }

        let summedValue = 0;
        for (let i = 0; i < indicators.length; i++) {
          const indicatorValue = calculateIndicatorValue(values[i], indicators[i]);
          summedValue += (indicators[i].coefficient * (indicatorValue - indicators[i].min)) / (indicators[i].max - indicators[i].min);
        }
        const normalizedValue = summedValue / sumOfCoefficients;

        // Stretch the normalized value across the bandwidth set by the aggregationSetting. Clamp to prevent numerical issues.
        if (aggregationSetting.best > aggregationSetting.worst) {
          return clamp(
            normalizedValue * (aggregationSetting.best - aggregationSetting.worst) + aggregationSetting.worst,
            aggregationSetting.worst,
            aggregationSetting.best
          );
        } else {
          return clamp(
            (1 - normalizedValue) * (aggregationSetting.worst - aggregationSetting.best) + aggregationSetting.best,
            aggregationSetting.best,
            aggregationSetting.worst
          );
        }
      };
    }
  } else {
    // Use the custom aggregation formula. This may throw an Error if the formula is invalid.
    let calc: math.EvalFunction;
    if (customFormulaCache.has(aggregationSetting)) {
      calc = customFormulaCache.get(aggregationSetting);
    } else {
      calc = compileFormula(aggregationSetting);
      customFormulaCache.set(aggregationSetting, calc);
    }

    const indicatorValueNames = indicators.map((_, i) => getIndicatorValueName(i));
    const scope = new Map<string, number>();

    for (let i = 0; i < indicators.length; i++) {
      scope.set(getIndicatorCoefficientName(i), indicators[i].coefficient);
    }

    return (values: ObjectiveInput) => {
      if (!validateInput(values)) {
        return NaN;
      }

      // Build the scope consisting of variables for coefficients and indicators and calculate the result
      for (let i = 0; i < indicators.length; i++) {
        const indicatorValue = calculateIndicatorValue(values[i], indicators[i]);
        scope.set(indicatorValueNames[i], indicatorValue);
      }
      const result = calc.evaluate(scope);

      if (isFinite(result)) {
        return result;
      } else {
        return NaN;
      }
    };
  }
}

// retrieves the indicator value for a single indicator vector outcome.values[stateIdx][indicatorIdx]
// returns the first entry for a STANDARD indicator
// calculates the indicator value based on the additive formula for a VERBAL indicator
export function calculateIndicatorValue(values: number[], indicator: Pick<Indicator, 'min' | 'max' | 'verbalIndicatorCategories'>) {
  if (indicator.verbalIndicatorCategories.length === 0) return values[0]; // no categories
  let sum = 0;
  let divisor = 0; // can't stay 0
  values.forEach((valuePerCategory, categoryIdx) => {
    // between 0 and 1
    const scalePortion = valuePerCategory / (indicator.verbalIndicatorCategories[categoryIdx].stages.length - 1);

    sum += indicator.verbalIndicatorCategories[categoryIdx].weight * scalePortion;
    divisor += indicator.verbalIndicatorCategories[categoryIdx].weight;
  });
  // do computation, based on value and weight (same as additive formula for indicators)
  return indicator.min + (indicator.max - indicator.min) * (sum / divisor);
}
