import {
  Outcome,
  UtilityFunction,
  UserdefinedState,
  UserdefinedInfluenceFactor,
  Objective,
  ObjectiveWeight,
  NumericalObjectiveData,
  VerbalObjectiveData,
  ObjectiveType,
  IndicatorObjectiveData,
  Indicator,
  Weights,
} from '../classes';
import { UtilityGeneratorResult, randomizedUtilityGenerator } from './uncertainty';
import { getAlternativeUtilitiesMeta } from './utility-meta';

/**
 * Helper function that turns an array of numerical weights into a weight object.
 *
 * @param numericalWeights - The weights to embed into the object
 * @returns The weights object
 */
function getWeights(numericalWeights: number[]) {
  const weights = new Weights();
  numericalWeights.forEach((weight, index) => {
    weights.addObjective(index);
    weights.preliminaryWeights[index].value = weight;
  });
  return weights;
}

describe('Uncertainty', () => {
  describe('randomizedUtilityGenerator()', () => {
    const objectives = [
      new Objective(
        '',
        new NumericalObjectiveData(0, 10, undefined, new UtilityFunction(5, 1)),
        undefined,
        undefined,
        ObjectiveType.Numerical
      ),
      new Objective('', undefined, new VerbalObjectiveData(['', '', ''], [0, 10, 100], 5), undefined, ObjectiveType.Verbal),
      new Objective(
        '',
        undefined,
        undefined,
        new IndicatorObjectiveData(
          [new Indicator('', 0, 10, undefined, 1), new Indicator('', 5, 1, undefined, 3)],
          new UtilityFunction(3, 2)
        ),
        ObjectiveType.Indicator
      ),
    ];
    const outcomes = [
      [
        new Outcome([[[2]]]),
        new Outcome(
          [[[1]], [[3]]],
          undefined,
          new UserdefinedInfluenceFactor('', [new UserdefinedState('', 10), new UserdefinedState('', 90)], 0)
        ),
        new Outcome([[[4], [2]]]),
      ],
    ];
    const weights = [new ObjectiveWeight(10, 5), new ObjectiveWeight(20, 0), new ObjectiveWeight(70, 1)];

    it('matches the default calculation if all uncertainties are disabled', () => {
      const generator = randomizedUtilityGenerator(objectives, outcomes, weights, {
        influenceFactorScenarios: { predefined: false, userdefinedIds: [] },
        objectiveWeights: false,
        probabilities: false,
        utilityFunctions: false,
      });
      const result = generator.next().value as UtilityGeneratorResult;

      expect(result.alternativeUtilities).toEqual(getAlternativeUtilitiesMeta(outcomes, objectives, getWeights([10, 20, 70])));
    });

    it('does not return the same result twice', () => {
      const generator = randomizedUtilityGenerator(objectives, outcomes, weights, {
        influenceFactorScenarios: { predefined: false, userdefinedIds: [] },
        objectiveWeights: true,
        probabilities: true,
        utilityFunctions: true,
      });
      const result1 = generator.next().value as UtilityGeneratorResult;
      const result2 = generator.next().value as UtilityGeneratorResult;

      expect(result1.alternativeUtilities).not.toEqual(result2.alternativeUtilities);
    });

    test.each([0, 0.5, 1])('returns values within bounds (Math.random() = %i)', fakeRandom => {
      jest.spyOn(global.Math, 'random').mockReturnValue(fakeRandom);

      const generator = randomizedUtilityGenerator(objectives, outcomes, weights, {
        influenceFactorScenarios: { predefined: false, userdefinedIds: [] },
        objectiveWeights: true,
        probabilities: true,
        utilityFunctions: true,
      });
      const result1 = generator.next().value as UtilityGeneratorResult;
      for (const utility of result1.alternativeUtilities) {
        expect(utility).toBeGreaterThanOrEqual(0);
        expect(utility).toBeLessThanOrEqual(1);
      }

      jest.spyOn(global.Math, 'random').mockRestore();
    });
  });
});
