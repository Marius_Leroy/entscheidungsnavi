import { flatten } from 'lodash';
import { pickRandom } from 'mathjs';
import {
  Objective,
  ObjectiveType,
  ObjectiveWeight,
  Outcome,
  PredefinedScenario,
  UserdefinedInfluenceFactor,
  UserdefinedScenario,
  UtilityFunction,
  VerbalObjectiveData,
  generatePredefinedScenarios,
  generateUserdefinedScenarios,
} from '../classes';
import { InfluenceFactorStateMap } from '../tools';
import { mapToRelativeProbsFunc, mapToWeightFunction, scaleToRangeFunction } from './probability';
import {
  getIndicatorUtilityFunction,
  getNumericalUtilityFunction,
  getVerbalUtilityFunction,
  getAlternativeUtilities,
  applyWeightsToUtilityMatrix,
  ObjectiveUtilityFunction,
} from './utility';

/**
 * Generates the parameter c for a utility function, including uncertainty.
 *
 * @param utilityFunction - The utility function object for which to generate the c
 * @param includeUncertainty - Whether to include the precision. Otherwise, a fixed value is returned.
 * @returns A function that generates a random c on every invocation
 */
function utilityFunctionCGenerator(utilityFunction: UtilityFunction, includeUncertainty: boolean) {
  if (includeUncertainty) {
    const cachedFunction = scaleToRangeFunction(utilityFunction.c, utilityFunction.precision);
    return () => cachedFunction(Math.random());
  } else {
    return () => utilityFunction.c;
  }
}

/**
 * Generates utilities (i.e., the verbal utility function) for a verbal objective, including uncertainty.
 *
 * @param verbalData - The verbal objective data for which to compute the utilities
 * @param includeUncertainty - Whether to include precision. Otherwise, a fixed value is returned
 * @returns A function that generates a new set of utilities on every invocation
 */
function verbalUtilitiesGenerator(verbalData: VerbalObjectiveData, includeUncertainty: boolean): () => readonly number[] {
  if (!includeUncertainty) {
    return () => verbalData.utilities;
  }

  const res = new Array<number>(verbalData.utilities.length);

  return () => {
    verbalData.utilities.forEach((utility: number, i: number) => {
      if (i === 0 || i === verbalData.utilities.length - 1) {
        // no deviation for the first and the last state
        res[i] = utility;
      } else {
        let e;

        // calculate the relative precision
        const precision = (verbalData.precision * Math.min(100 - utility, utility)) / 100;
        const random = Math.random();

        // get the allowed range [bot, top]
        // bot must not be lower than the previous value
        const bot = Math.max(res[i - 1], utility - precision),
          // top must not be higher than 100
          top = Math.min(100, utility + precision);

        if (bot < utility) {
          // 50% for [bot, val] and 50% for [val, top]
          e = random < 0.5 ? bot + 2 * random * (utility - bot) : utility + 2 * (random - 0.5) * (top - utility);
        } else {
          // distribute equally in [bot, top], because val is not in (bot, top]
          e = bot + random * (top - bot);
        }
        res[i] = e;
      }
    });

    return res;
  };
}

/**
 * Generates an {@link ObjectiveUtilityFunction} for the given objective, including uncertainty.
 *
 * @param objective - The objective for which to create the utility function
 * @param includeUncertainty - Whether or not to include precision/uncertainty
 * @returns A function that returns a new utility function on every invocation
 */
function objectiveUtilityFunctionGenerator(objective: Objective, includeUncertainty: boolean) {
  switch (objective.objectiveType) {
    case ObjectiveType.Numerical: {
      const generator = utilityFunctionCGenerator(objective.numericalData.utilityfunction, includeUncertainty);
      return () => getNumericalUtilityFunction(generator(), objective.numericalData.from, objective.numericalData.to);
    }
    case ObjectiveType.Verbal: {
      const generator = verbalUtilitiesGenerator(objective.verbalData, includeUncertainty);
      return () => getVerbalUtilityFunction(generator());
    }
    case ObjectiveType.Indicator: {
      const generator = utilityFunctionCGenerator(objective.indicatorData.utilityfunction, includeUncertainty);
      // Precompute aggregation function and values
      const aggregationFunction = objective.indicatorData.aggregationFunction,
        worstValue = objective.indicatorData.worstValue,
        bestValue = objective.indicatorData.bestValue;
      return () => getIndicatorUtilityFunction(generator(), aggregationFunction, worstValue, bestValue);
    }
  }
}

/**
 * Generates an objective weight, including uncertainty.
 *
 * @param weight - The weight
 * @param includeUncertainty - Wether to include precision/uncertainty.
 * @returns A function that returns a new weight on every invocation
 */
function objectiveWeightGenerator(weight: ObjectiveWeight, includeUncertainty: boolean) {
  if (includeUncertainty) {
    const cachedFunction = mapToWeightFunction(weight.value, weight.precision);
    return () => cachedFunction(Math.random());
  } else {
    return () => weight.value;
  }
}

/**
 * Generates state probabilities for a userdefined influence factor, including uncertainty.
 *
 * @param influenceFactor - The influence factor
 * @param includeUncertainty - Wether or not to include uncertainty/precision.
 * @returns A function that returne a new set of state probabilities on every invocation
 */
function influenceFactorProbabilityGenerator(influenceFactor: UserdefinedInfluenceFactor, includeUncertainty: boolean): () => number[] {
  if (!includeUncertainty) {
    // We need to scale the 0-100 probabilities to 0-1
    const stateProbabilities = influenceFactor.states.map(state => state.probability / 100);
    return () => stateProbabilities;
  } else {
    const randomNumbers = new Array(influenceFactor.states.length);

    const cachedFunction = mapToRelativeProbsFunc(
      influenceFactor.states.map(state => state.probability),
      influenceFactor.precision
    );

    return () => {
      for (let i = 0; i < randomNumbers.length; i++) {
        randomNumbers[i] = Math.random();
      }
      const probs = cachedFunction(randomNumbers);
      // We need to scale the 0-100 probabilities to 0-1
      for (let i = 0; i < probs.length; i++) {
        probs[i] /= 100;
      }
      return probs;
    };
  }
}

/**
 * Randomly select one out of N states, each with its own probability,
 *
 * @param probabilities - The probabilities of the states. Must sum up to 1.
 * @returns The index of the chosen state
 */
function drawIndex(probabilities: number[]): number {
  const pickIndices = new Array<number>(probabilities.length);

  for (let i = 0; i < probabilities.length; i++) {
    pickIndices[i] = i;
  }

  // The math.js typings seem to be broken here...
  return pickRandom(pickIndices, 1, probabilities) as unknown as number;
}

/**
 * Calculates the expected utility value for the given set of influence factor scenarios.
 *
 * This does not pick a scenario, but calculates the expected utility value.
 *
 * @param scenarios - The possible scenarios
 * @param objectiveUtilityFunction - The utility function for the given objective
 * @returns The expected utility
 */
function expectedUtilityValue(scenarios: UserdefinedScenario[] | PredefinedScenario[], objectiveUtilityFunction: ObjectiveUtilityFunction) {
  let accumulatedUtility = 0;
  for (const combination of scenarios) {
    accumulatedUtility += objectiveUtilityFunction(combination.value) * combination.probability;
  }
  return accumulatedUtility;
}

export interface RandomizationParameters {
  utilityFunctions: boolean;
  objectiveWeights: boolean;
  // userdefined influence factor state probabilities
  probabilities: boolean;
  influenceFactorScenarios: {
    // wether or not to compute scenarios for predefined IFs
    predefined: boolean;
    // the IDs of the IFs for which to calculate scenarios
    userdefinedIds: number[];
  };
}

export interface UtilityGeneratorResult {
  /**
   * A utility for each alternative.
   */
  alternativeUtilities: number[];
  /**
   * The state of each influence factor. Only included for influence factors for which we compute scenarios.
   */
  influenceFactorStates: InfluenceFactorStateMap<number>;
}

/**
 * This function performs a random draw for all possible uncertain values in the utility value calculation and
 * uses them to calculate a utility for every alternative.
 *
 * We currently support uncertainties in the following quantities:
 * - Utility functions (used in numerical and indicator objectives)
 * - Verbal utility values
 * - Objective weights
 * - Influence factor probabilities
 *
 * Furthermore, it can either use scenarios or expected utilities for influence factors.
 *
 * @param objectives - The objectives that we consider
 * @param outcomes - The outcomes for which we calculate utilities
 * @param weights - Objective weights to consider
 * @param includeUncertainties - Which uncertainties should be considered
 * @returns An array of utility values, one for every alternative, as well as corresponding influence factor states
 */
export function* randomizedUtilityGenerator(
  objectives: Objective[],
  outcomes: Outcome[][],
  weights: ObjectiveWeight[],
  includeUncertainties: RandomizationParameters
): Generator<UtilityGeneratorResult, void> {
  /**
   * Step 1: Get all generators for the utility calculation. Each generator draws a new set of randomized parameters.
   *
   * We use generators to cache as much of the computed parameters as possible and save on computation time.
   */
  const objectiveUtilityFunctionGenerators = objectives.map(objective =>
    objectiveUtilityFunctionGenerator(objective, includeUncertainties.utilityFunctions)
  );

  const objectiveWeightGenerators = weights.map(weight => objectiveWeightGenerator(weight, includeUncertainties.objectiveWeights));

  // Map each influence factor ID to the array of its state's probabilities
  const userdefinedIfProbabilityGenerators = new Map<number, () => number[]>();
  // For predefined IFs, the probabilities are certain (not randomized). Thus, we can precompute the scenarios
  // for every outcome that is using the IF.
  const predefinedIfScenarios = new Map<Outcome, PredefinedScenario[]>();

  // We iterate over each influence factor. We might iterate over some of them multiple times,
  // but that is okay.
  for (const outcome of flatten(outcomes)) {
    if (outcome.influenceFactor == null) continue;

    if (outcome.influenceFactor instanceof UserdefinedInfluenceFactor) {
      userdefinedIfProbabilityGenerators.set(
        outcome.influenceFactor.id,
        influenceFactorProbabilityGenerator(outcome.influenceFactor, includeUncertainties.probabilities)
      );
    } else {
      const probabilities = outcome.influenceFactor.states.map(state => state.probability / 100);
      const scenarios = generatePredefinedScenarios(outcome.values, probabilities);
      predefinedIfScenarios.set(outcome, scenarios);
    }
  }

  /**
   * Step 2: Use the generators to run repeated random experiments
   */
  // Preallocate everything we need
  const userdefinedIfProbabilities = new Map<number, number[]>();
  const userdefinedIfState = new Map<number, number>();
  const objectiveUtilityFunctions = new Array<ObjectiveUtilityFunction>(objectiveUtilityFunctionGenerators.length);
  const objectiveWeights = new Array<number>(objectiveWeightGenerators.length);
  const utilityMatrix = outcomes.map(inner => inner.map(() => 0));
  // Map every influence factor to its state index
  const influenceFactorStates = new InfluenceFactorStateMap<number>();

  while (true) {
    // We call all the generators here to get new randomized outcomes
    for (let i = 0; i < objectiveUtilityFunctionGenerators.length; i++) {
      objectiveUtilityFunctions[i] = objectiveUtilityFunctionGenerators[i]();
    }
    for (const [key, value] of userdefinedIfProbabilityGenerators.entries()) {
      const probabilities = value();
      userdefinedIfProbabilities.set(key, probabilities);

      if (includeUncertainties.influenceFactorScenarios.userdefinedIds.includes(key)) {
        const stateIndex = drawIndex(probabilities);
        userdefinedIfState.set(key, stateIndex);
        influenceFactorStates.set(key, stateIndex);
      }
    }
    for (let i = 0; i < objectiveWeightGenerators.length; i++) {
      objectiveWeights[i] = objectiveWeightGenerators[i]();
    }

    // Use the randomized parameters to compute the utility matrix
    for (let alternativeIdx = 0; alternativeIdx < outcomes.length; alternativeIdx++) {
      const outcomeRow = outcomes[alternativeIdx];

      for (let objectiveIdx = 0; objectiveIdx < outcomeRow.length; objectiveIdx++) {
        const outcome = outcomeRow[objectiveIdx];
        const influenceFactor = outcome.influenceFactor;
        const objectiveUf = objectiveUtilityFunctions[objectiveIdx];

        if (influenceFactor == null) {
          utilityMatrix[alternativeIdx][objectiveIdx] = objectiveUf(outcome.values[0]);
        } else if (influenceFactor instanceof UserdefinedInfluenceFactor) {
          const combinations = generateUserdefinedScenarios(outcome.values, userdefinedIfProbabilities.get(influenceFactor.id));
          utilityMatrix[alternativeIdx][objectiveIdx] = includeUncertainties.influenceFactorScenarios.userdefinedIds.includes(
            influenceFactor.id
          )
            ? objectiveUf(combinations[userdefinedIfState.get(influenceFactor.id)].value)
            : expectedUtilityValue(combinations, objectiveUf);
        } else {
          const combinations = predefinedIfScenarios.get(outcome);

          if (includeUncertainties.influenceFactorScenarios.predefined) {
            const scenarioIndex = drawIndex(combinations.map(combination => combination.probability));
            combinations[scenarioIndex].stateIndices.forEach((stateIndex, indicatorIndex) => {
              influenceFactorStates.set(
                {
                  id: influenceFactor.id,
                  alternativeIndex: alternativeIdx,
                  objectiveIndex: objectiveIdx,
                  indicatorIndex,
                },
                stateIndex
              );
            });
            utilityMatrix[alternativeIdx][objectiveIdx] = objectiveUf(combinations[scenarioIndex].value);
          } else {
            utilityMatrix[alternativeIdx][objectiveIdx] = expectedUtilityValue(combinations, objectiveUf);
          }
        }
      }
    }
    applyWeightsToUtilityMatrix(utilityMatrix, objectiveWeights);
    const alternativeUtilities = getAlternativeUtilities(utilityMatrix);

    yield { alternativeUtilities, influenceFactorStates };
  }
}
