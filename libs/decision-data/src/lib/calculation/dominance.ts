import { zip } from 'lodash';
import { Alternative, Outcome, Objective } from '../classes';

export interface DominanceRelation {
  dominating: Alternative;
  dominated: Alternative;
}

interface ValueWithProbability {
  value: number;
  probability: number;
}

/**
 * Calculates which alternatives dominate other alternatives. Considers only alternatives
 * whose outcomes are processed (no null-values).
 *
 * Domination is defined as follows:
 * The worst utility of the dominating alternative has to be at least as good as the best utility
 * of the dominated alternative in every objective. It also has to be better in at least one case.
 * If an influence factor is set, the utility of the worst for the dominating and the best for the
 * dominated alternative is used as value.
 *
 * @param objectives - An array of objectives
 * @param alternatives - An array of alternatives
 * @param outcomes - An array of outcomes, where the first index corresponds to an alternative and the second index to an objective
 */
export function calculateDominanceRelations(
  objectives: Objective[],
  alternatives: Alternative[],
  outcomes: Outcome[][]
): DominanceRelation[] {
  // One flag for each alternative to denote whether it is processed.
  const isProcessedFlag = outcomes.map(objOutcomes => objOutcomes.every(outcome => outcome.processed));
  // We filter out the alternatives and outcomes according to that flag array.
  outcomes = outcomes.filter((_objOutcome, index) => isProcessedFlag[index]);
  alternatives = alternatives.filter((_alternative, index) => isProcessedFlag[index]);

  // The utility functions for each objective. This includes the utility function curvature.
  // The curvature has no influence on the results of this calculation.
  const utilityFunctions = objectives.map(objective => objective.getUtilityFunction());

  const valuesWithProbabilitiesArray: ValueWithProbability[][][] = outcomes.map(ocsForAlternative => {
    return ocsForAlternative.map((outcome, objectiveIndex) => {
      if (outcome.influenceFactor) {
        const combinations = outcome.influenceFactor.generateScenarios(outcome.values);
        return combinations.map(cp => {
          return {
            value: utilityFunctions[objectiveIndex](cp.value),
            probability: cp.probability,
          };
        });
      } else {
        // No influence factor, meaning outcome.values.length === 1
        return [{ value: utilityFunctions[objectiveIndex](outcome.values[0]), probability: 1 }];
      }
    });
  });

  // We check for every alternative if it dominates any other alternative
  const found: DominanceRelation[] = [];
  alternatives.forEach((outerAlternative, outerIndex) => {
    // We check if outerAlternative dominates innerAlternative
    alternatives.forEach((innerAlternative, innerIndex) => {
      if (innerAlternative === outerAlternative) {
        return;
      }

      const combinations = zip(valuesWithProbabilitiesArray[outerIndex], valuesWithProbabilitiesArray[innerIndex]);

      // Check if outer dominates inner in EVERY objective (doesn't have to be strict)
      // Check if outer STRICTLY dominates inner in AT LEAST ONE objective
      if (
        combinations.every(([outer, inner]) => checkStochasticDominance(outer, inner)) &&
        combinations.some(([outer, inner]) => checkStochasticDominance(outer, inner, true))
      ) {
        found.push({
          dominating: outerAlternative,
          dominated: innerAlternative,
        });
      }
    });
  });

  return found;
}

function checkStochasticDominance(left: ValueWithProbability[], right: ValueWithProbability[], checkStrict = false) {
  left = sortAndMergeDuplicates(left);
  right = sortAndMergeDuplicates(right);

  // Simulate going down the Step function
  // Check if Left >= Right at every point

  let leftIndex = 0,
    rightIndex = 0;
  let leftRemainingProbability = 100,
    rightRemainingProbability = 100;
  let isStrict = false;
  while (leftIndex < left.length && rightIndex < right.length) {
    const leftIndexCopy = leftIndex;
    const rightIndexCopy = rightIndex;
    if (left[leftIndexCopy].value <= right[rightIndexCopy].value) {
      // Go down 1 step (left)
      leftRemainingProbability -= left[leftIndexCopy].probability;
      leftIndex++;
    }
    if (left[leftIndexCopy].value >= right[rightIndexCopy].value) {
      // Go down 1 step (right)
      rightRemainingProbability -= right[rightIndexCopy].probability;
      rightIndex++;
    }
    if (leftRemainingProbability < rightRemainingProbability) {
      // Right dominates Left in this region => no stochastic dominance
      return false;
    }
    if (leftRemainingProbability > rightRemainingProbability) {
      isStrict = true;
    }
  }
  return !checkStrict || isStrict;
}

function sortAndMergeDuplicates(valuesWithProbabilities: ValueWithProbability[]) {
  // Sort asc
  valuesWithProbabilities.sort((a, b) => a.value - b.value);
  // Merge probabilities of duplicated values
  for (let i = 0; i < valuesWithProbabilities.length - 1; i++) {
    while (i + 1 < valuesWithProbabilities.length && valuesWithProbabilities[i].value === valuesWithProbabilities[i + 1].value) {
      valuesWithProbabilities[i].probability += valuesWithProbabilities[i + 1].probability;
      valuesWithProbabilities.splice(i + 1, 1);
    }
  }
  return valuesWithProbabilities;
}
