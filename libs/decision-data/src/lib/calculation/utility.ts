import { sum } from 'lodash';
import { InfluenceFactorScenario, ObjectiveInput } from '../classes';
import { normalizeNumber, normalizeContinuous } from './normalize';

/**
 * A utility function transforms a single outcome value to a utility in [0, 1].
 */
export type ObjectiveUtilityFunction = (outcomeValue: ObjectiveInput) => number;

/**
 * A utility function that transforms an outcome value (with influence factor) to a utility in [0, 1].
 */
export type OutcomeUtilityFunction = (outcomeValues: ObjectiveInput[]) => number;

/**
 * Returns a function that maps an outcome value to a utility value.
 *
 * @param c - Curvature parameter of the utility function
 * @returns The utility function
 */
export function getUtilityFunction(c: number) {
  if (c === 0) {
    return (x: number) => x;
  }
  const divisor = 1 - Math.exp(-c); // Precompute for performance reasons
  return (x: number) => (1 - Math.exp(-c * x)) / divisor;
}

/**
 * Returns a function that maps a utility value back to an outcome value.
 * Inverse of {@link getUtilityFunction}.
 *
 * @param c - Curvature parameter
 * @returns The inverse utility function
 */
export function getInverseUtilityFunction(c: number) {
  if (c === 0) {
    return (utility: number) => utility;
  }
  const factor = 1 - Math.exp(-c); // Precompute
  return (utility: number) => Math.log(1 - utility * factor) / -c;
}

/**
 * Returns a utility function for a numerical objective.
 *
 * Note that `worst > best` is allowed here..
 *
 * @param c - Utility function curvature
 * @param worst - Worst outcome value
 * @param best - Best outcome value
 * @returns The utility function
 */
export function getNumericalUtilityFunction(c: number, worst: number, best: number): ObjectiveUtilityFunction {
  const utilityFunction = getUtilityFunction(c);

  return (objectiveValues: ObjectiveInput) => {
    if (objectiveValues[0][0] == null) return NaN;

    const normalizedOutcome = normalizeNumber(objectiveValues[0][0], worst, best);
    return utilityFunction(normalizedOutcome);
  };
}

/**
 * Returns a utility function for a verbal objective, specified by {@link utilities}.
 *
 * @param utilities - The utilities for the discrete outcomes
 * @returns The utility function
 */
export function getVerbalUtilityFunction(utilities: readonly number[]): ObjectiveUtilityFunction {
  // The outcome needs to be a number since we are a verbal objective
  return (objectiveValues: ObjectiveInput) => {
    if (objectiveValues[0][0] == null) return NaN;

    return utilities[objectiveValues[0][0] - 1] / 100;
  };
}

/**
 * Returns a function (number[] -\> number) that takes the values
 * of the indicators as input and returns the calculated utility
 * as a value between 0 and 1.
 *
 * @param c - curvature of the utility function
 * @param aggregationFunction - the indicator aggregation function
 * @param worst - the worst value on the aggregated scale
 * @param best - the best value on the aggregated scale
 */
export function getIndicatorUtilityFunction(
  c: number,
  aggregationFunction: (values: ObjectiveInput) => number,
  worst: number,
  best: number
): ObjectiveUtilityFunction {
  const utilityFunction = getUtilityFunction(c);

  return (objectiveValues: ObjectiveInput) => {
    for (const indicatorRow of objectiveValues) {
      for (const value of indicatorRow) {
        if (value == null) return NaN;
      }
    }
    const aggregate = aggregationFunction(objectiveValues);
    const normalizedAggregate = normalizeNumber(aggregate, worst, best);
    return utilityFunction(normalizedAggregate);
  };
}

/**
 * Returns a function that maps an outcome value to a utility. The outcome may include an influence
 * factor.
 *
 * @param objectiveUtilityFunction - The utility function of the corresponding objective
 * @param influenceFactorScenarioGenerator - The scenario generator function corresponding to the
 *    influence factor (or null, if no IF is used).
 * @returns The outcome utility function
 */
export function getOutcomeUtilityFunction(
  objectiveUtilityFunction: ObjectiveUtilityFunction,
  influenceFactorScenarioGenerator?: (outcomeValues: ObjectiveInput[]) => InfluenceFactorScenario[]
): OutcomeUtilityFunction {
  if (influenceFactorScenarioGenerator) {
    // We have an influence factor -> generate scenarios
    return outcomeValues => {
      const scenarios = influenceFactorScenarioGenerator(outcomeValues);

      let accumulatedUtility = 0;
      for (const combination of scenarios) {
        accumulatedUtility += objectiveUtilityFunction(combination.value) * combination.probability;
      }
      return accumulatedUtility;
    };
  } else {
    // We do not have an influence factor -> outcomeValues.length === 1 -> simply forward the utility
    return outcomeValues => objectiveUtilityFunction(outcomeValues[0]);
  }
}

/**
 * Computes utilities for every combination of alternative and objective individually.
 *
 * The utilities for each objective are treated separately and the objective weights are not taken
 * into account.
 *
 * @param outcomeValues - The outcome values for which to compute the utilities
 * @param outcomeUtilityFunctions - The utility functions for the outcomes
 * @returns A matrix of utility values of with indices [alternativeIndex][objectiveIndex]. Each value is the utility
 *    for that alternative in that objective.
 */
export function getUtilityMatrix(outcomeValues: ObjectiveInput[][][], outcomeUtilityFunctions: OutcomeUtilityFunction[][]) {
  return outcomeValues.map((ocsForAlternative, alternativeIndex) =>
    ocsForAlternative.map((outcome, objectiveIndex) => outcomeUtilityFunctions[alternativeIndex][objectiveIndex](outcome))
  );
}

/**
 * Applies weights to the utility matrix IN PLACE.
 * This means that all utility values are scaled by the corresponding objective weight.
 *
 * @param utilityMatrix - Utility matrix consisting of utilities for each combination of objective and alternative.
 *    See {@link getUtilityMatrix}.
 * @param weights - The objective weights to apply. THE WEIGHTS ARE MODIFIED DURING COMPUTATION!
 */
export function applyWeightsToUtilityMatrix(utilityMatrix: number[][], weights: number[]) {
  normalizeContinuous(weights);

  for (let alternativeIdx = 0; alternativeIdx < utilityMatrix.length; alternativeIdx++) {
    for (let objectiveIdx = 0; objectiveIdx < utilityMatrix[alternativeIdx].length; objectiveIdx++) {
      utilityMatrix[alternativeIdx][objectiveIdx] *= weights[objectiveIdx];
    }
  }
}

/**
 * Calculate weighted utilities for every alternative out of the weighted utility matrix. The objective utilities are accumulated
 * using their objective weights.
 *
 * @param weightedUtilityMatrix - The weighted utility matrix containing weighted utility values for every outcome,
 *    meaning every combination of alternative and objective. See {@link applyWeightsToUtilityMatrix} for details.
 * @returns An array of utilities for every alterantive
 */
export function getAlternativeUtilities(weightedUtilityMatrix: number[][]) {
  const alternativeUtilities = new Array<number>(weightedUtilityMatrix.length);
  for (let alternativeIdx = 0; alternativeIdx < weightedUtilityMatrix.length; alternativeIdx++) {
    alternativeUtilities[alternativeIdx] = sum(weightedUtilityMatrix[alternativeIdx]);
  }
  return alternativeUtilities;
}
