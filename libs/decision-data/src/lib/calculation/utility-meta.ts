import { Objective, Outcome, Weights } from '../classes';
import { applyWeightsToUtilityMatrix, getAlternativeUtilities } from './utility';

/**
 * Computes utilities for every combination of alternative and objective individually.
 *
 * The utilities for each objective are treated separately and the objective weights are not taken
 * into account.
 *
 * @param outcomes - The outcomes to compute the matrix for
 * @param objective - The objectives that correspond to this set of outcomes
 * @returns A matrix of utility values of with indices [alternativeIndex][objectiveIndex]. Each value is the utility
 *    for that alternative in that objective.
 */
export function getUtilityMatrixMeta(outcomes: Outcome[][], objectives: Objective[]) {
  return outcomes.map(ocsForAlternative =>
    ocsForAlternative.map((outcome, objectiveIndex) => outcome.getUtility(objectives[objectiveIndex]))
  );
}

/**
 * Computes the weighted utility matrix. Each row corresponds to an alternative, the columns correspond
 * to the objectives.
 *
 * @param outcomes - The outcomes
 * @param objectives - The objectives
 * @param weights - The objective weight object
 * @returns The utility matrix
 */
export function getWeightedUtilityMatrixMeta(outcomes: Outcome[][], objectives: Objective[], weights: Weights) {
  const matrix = getUtilityMatrixMeta(outcomes, objectives);
  applyWeightsToUtilityMatrix(matrix, weights.getWeightValues());
  return matrix;
}

/**
 * Shorthand function that calculated utilities for every alternative.
 *
 * @param outcomes - The outcomes
 * @param objectives - The objectives
 * @param weights - The objective weights
 * @returns An array of utilities, one for every alternative
 */
export function getAlternativeUtilitiesMeta(outcomes: Outcome[][], objectives: Objective[], weights: Weights) {
  const matrix = getWeightedUtilityMatrixMeta(outcomes, objectives, weights);
  return getAlternativeUtilities(matrix);
}
