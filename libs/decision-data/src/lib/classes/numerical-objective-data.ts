import { UtilityFunction } from './utility-function';

export class NumericalObjectiveData {
  constructor(
    public from = 1,
    public to = 7,
    public unit = '',
    public utilityfunction: UtilityFunction = new UtilityFunction(),
    public commentFrom?: string,
    public commentTo?: string,
    public explanationFrom?: string,
    public explanationTo?: string
  ) {}
}
