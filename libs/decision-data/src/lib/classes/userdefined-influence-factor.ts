import { sum } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { normalizeDiscrete } from '../calculation/normalize';
import { ObjectiveInput } from './objective';

export class UserdefinedState {
  /**
   * @param probability - Is in [0, 100]
   */
  constructor(public name = '', public probability = 0, public comment?: string) {}
}

export interface UserdefinedScenario {
  value: ObjectiveInput;
  stateIndex: number;
  probability: number;
}

export class UserdefinedInfluenceFactor {
  static readonly P_MAX = 50;

  constructor(
    public name = '',
    public states: UserdefinedState[] = [],
    public id = -1,
    public precision = 0,
    public comment?: string,
    public uuid = uuidv4()
  ) {
    // there have to be at least 2 states
    if (states.length <= 1) {
      for (let i = states.length; i <= 1; i++) {
        this.states.push(new UserdefinedState());
      }
    }
  }

  probabilitiesSum(): number {
    return sum(this.states.map(state => state.probability ?? 0));
  }

  checkProbabilities(): boolean {
    return checkProbabilities(this.states.map(state => state.probability ?? 0));
  }

  normalizeProbabilities(): void {
    if (!this.checkProbabilities()) {
      normalizeProbabilities(this.states);
    }
  }

  generateScenarios(outcomeValues: ObjectiveInput[]) {
    return generateUserdefinedScenarios(
      outcomeValues,
      this.states.map(state => state.probability / 100)
    );
  }
}

/**
 * Return true, if the sum is 100 and every probability is a whole number.
 *
 * @param probabilities - The array of probabilities
 */
export function checkProbabilities(probabilities: number[]) {
  // We allow the sum to deviate slightly from 100 to alleviate floating point inaccuracy
  return Math.abs(100 - sum(probabilities)) < 1e-10 && probabilities.every(probability => probability >= 0);
}

/**
 * Normalize the probabilities so that they sum up to 100.
 * Keep the round-off error as small as possible.
 *
 * @param states - The array of states. It is modified in place.
 */
export function normalizeProbabilities(states: UserdefinedState[]) {
  const probabilities = states.map(state => state.probability ?? 0);
  const newProbabilities = normalizeDiscrete(probabilities, 100, 1);
  states.forEach((state, idx) => (state.probability = newProbabilities[idx]));
}

/**
 * Generate an array of all possible scenarios for a userdefined influence factor.
 * This corresponds to one scenario per state.
 *
 * @param outcomeValues - Outcome values for every state
 * @param stateProbabilities - Probability for every state in [0, 1]
 */
export function generateUserdefinedScenarios(outcomeValues: ObjectiveInput[], stateProbabilities: number[]) {
  const combinations = new Array<UserdefinedScenario>(stateProbabilities.length);
  for (let stateIndex = 0; stateIndex < stateProbabilities.length; stateIndex++) {
    combinations[stateIndex] = {
      stateIndex,
      value: outcomeValues[stateIndex],
      probability: stateProbabilities[stateIndex],
    };
  }
  return combinations;
}
