import { clamp } from 'lodash';

/**
 * Represents an interval with a {@link start} and {@link end} point. Allows for `end < start`.
 */
export class Interval {
  private readonly _start: number;
  private readonly _end: number;

  get start() {
    return this._start;
  }

  get end() {
    return this._end;
  }

  get midpoint() {
    return (this.start + this.end) / 2;
  }

  /**
   * @returns the signed length of this interval
   */
  get length(): number {
    return this.end - this.start;
  }

  constructor(start: number, end: number) {
    this._start = start;
    this._end = end;
  }

  isPoint(): boolean {
    return this.start === this.end;
  }

  ordered(): Interval {
    return new Interval(Math.min(this.start, this.end), Math.max(this.start, this.end));
  }

  includes(value: number): boolean;
  includes(otherInterval: Interval, includeProperly: boolean): boolean;
  includes(valueOrOtherInterval: number | Interval, includeProperly = false) {
    if (typeof valueOrOtherInterval === 'number') valueOrOtherInterval = new Interval(valueOrOtherInterval, valueOrOtherInterval);

    const thisOrdered = this.ordered();
    const otherOrdered = valueOrOtherInterval.ordered();

    if (includeProperly) {
      return thisOrdered.start < otherOrdered.start && otherOrdered.end < thisOrdered.end;
    } else {
      return thisOrdered.start <= otherOrdered.start && otherOrdered.end <= thisOrdered.end;
    }
  }

  map(callbackFn: (endpoint: number) => number): Interval {
    return this.mapStart(callbackFn).mapEnd(callbackFn);
  }

  mapStart(callbackFn: (start: number) => number): Interval {
    return new Interval(callbackFn(this.start), this.end);
  }

  mapEnd(callbackFn: (end: number) => number): Interval {
    return new Interval(this.start, callbackFn(this.end));
  }

  /**
   * Map a point from this inverval to [0, 1].
   *
   * @param point - The value to normalize
   * @returns The normalized value
   */
  normalizePoint(point: number) {
    return Math.abs(this.start - point) / Math.abs(this.length);
  }

  /**
   * Clamps a value to within this interval.
   */
  clampTo(value: number) {
    const ordered = this.ordered();
    return clamp(value, ordered.start, ordered.end);
  }

  clone(): Interval {
    return new Interval(this.start, this.end);
  }
}
