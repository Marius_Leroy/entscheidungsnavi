export class ObjectiveWeight {
  constructor(public value?: number, public precision = 0, public comparisonPointX?: number, public activeReferencePointIndex?: number) {}
}
