import { clamp } from 'lodash';
import { ObjectiveWeight } from './objective-weight';
import { Objective } from './objective';

// adjusts the length of the array, fills new entries using
function fillArray<T>(array: T[], length: number, fillFunction: () => T) {
  if (array.length > length) {
    array.length = length;
  } else {
    while (array.length < length) {
      array.push(fillFunction());
    }
  }
}

/**
 * Consists of two points with two coordinates each.
 * The coordinates are objective values and normalized in [0,1].
 */
export type TradeoffPointPair = [[number, number], [number, number]];

export class Weights {
  /**
   * We normalize such that the highest weight is equal to this value.
   */
  static readonly NORMALIZATION_TARGET_WEIGHT = 250;
  /**
   * Weights are not allowed to be bigger than this value
   */
  static readonly MAX_WEIGHT = 1000;

  tradeoffObjectiveIdx: number; // index of the objective (in decisionData.objectives) that is used for the tradeoffs

  preliminaryWeights: ObjectiveWeight[] = [];
  tradeoffWeights: ObjectiveWeight[] = [];
  unverifiedWeights: boolean[] = []; // force to open the tradeoff for objectives with true; only set when the tradeoff objective changes
  explanations: string[] = []; // Explanations for every tradeoff

  /**
   * These are the manual tradeoffs for each objective with the tradeoff objective. They consists of two points with
   * the second coordinate being from the tradeoff objective. They are normalized in [0, 1].
   * They are input by the user in the objective weighting step.
   *
   * null, when no manual tradeoff has been done or when the index belongs to the tradeoff objective.
   */
  manualTradeoffs: TradeoffPointPair[] = [];

  static getDefault() {
    return new ObjectiveWeight();
  }

  /**
   * if `length` is not defined sets the length of tradeoffWeights and unverifiedWeights to the length of preliminaryWeights
   * otherwise sets the length of all arrays to `length` (should be the amount of objectives)
   */
  restoreArrayLengths(length?: number) {
    if (Number.isInteger(length)) {
      fillArray(this.preliminaryWeights, length, Weights.getDefault);
    } else {
      length = this.preliminaryWeights.length;
    }
    fillArray(this.tradeoffWeights, length, () => null);
    fillArray(this.unverifiedWeights, length, () => false);
    fillArray(this.explanations, length, () => '');
    fillArray(this.manualTradeoffs, length, () => null);
  }

  /**
   * normalize the preliminary weights to the value 'DEFAULT_MAX_WEIGHT_VALUE'
   */
  normalizePreliminaryWeights() {
    if (!this.tradeoffWeights.some(w => w != null)) {
      const highestValue = Math.max(...this.getWeightValues());

      if (highestValue > 0) {
        const part = Weights.NORMALIZATION_TARGET_WEIGHT / highestValue;

        this.preliminaryWeights.forEach((w, i) => {
          // with rounding to two decimal places
          const value = Math.round(w.value * part * 100) / 100;
          this.changePreliminaryWeightValue(i, value);
        });
      } else {
        // All weights are 0 or less. We normalize this such that every objective gets the same value.
        this.preliminaryWeights.forEach((w, i) => {
          this.changePreliminaryWeightValue(i, Weights.NORMALIZATION_TARGET_WEIGHT);
        });
      }
    }
  }

  /**
   * change the reference objective
   * @returns true is a manual tradeoff was deleted in the process
   */
  changeTradeoffObjective(newIndex: number, objectives: Objective[]) {
    if (newIndex < 0 || newIndex >= objectives.length) {
      throw Error('invalid reference index');
    } else {
      if (this.tradeoffWeights[newIndex]) {
        const oldIdx = this.tradeoffObjectiveIdx;
        // move precision from the new tradeoff objective to the old one
        this.tradeoffWeights[oldIdx] = new ObjectiveWeight(this.getWeights()[oldIdx].value, this.tradeoffWeights[newIndex].precision);
        // reset all other precisions
        this.tradeoffWeights.forEach((w, i) => {
          if (w && i !== oldIdx) {
            w.precision = 0;
          }
        });

        // remove tradeoff
        this.preliminaryWeights[newIndex] = new ObjectiveWeight(this.tradeoffWeights[newIndex].value);
        this.tradeoffWeights[newIndex] = null;
      }
      this.unverifiedWeights.fill(true); // Mark all tradeoffs as unverified
      this.explanations.fill('');
      const hasManualTradeoff = this.manualTradeoffs.some(tradeoff => tradeoff != null);
      this.manualTradeoffs.fill(null);
      this.tradeoffObjectiveIdx = newIndex;
      return hasManualTradeoff;
    }
  }

  /**
   * reset tradeoffs
   */
  private resetTradeOffObjective() {
    this.tradeoffObjectiveIdx = null;
    this.tradeoffWeights.fill(null);
    this.unverifiedWeights.fill(false);
    this.explanations.fill('');
    this.manualTradeoffs.fill(null);
  }

  /**
   * reset all weights
   */
  resetWeights() {
    this.resetPreliminaryWeights();
    this.resetTradeOffObjective();
  }

  /**
   * reset the preliminaryWeights to the default weight
   */
  private resetPreliminaryWeights() {
    this.preliminaryWeights = this.preliminaryWeights.map(() => {
      return Weights.getDefault();
    });
  }

  /**
   * change the value of one specific preliminary weight
   */
  changePreliminaryWeightValue(index: number, newValue: number, normalize = false): boolean {
    const precisionRemoved = this.tradeoffWeights[index] && this.tradeoffWeights[index].precision > 0;

    this.tradeoffWeights[index] = null;
    if (index === this.tradeoffObjectiveIdx) {
      this.unverifiedWeights.fill(true);
    }

    this.preliminaryWeights[index].value = clamp(newValue, 0, Weights.MAX_WEIGHT);
    this.preliminaryWeights[index].precision = 0;
    if (normalize) {
      this.normalizePreliminaryWeights();
    }
    return precisionRemoved;
  }

  /**
   * adjust a specific preliminary weight value and optional normalize it
   */
  adjustPreliminaryWeightValue(index: number, amount: number, normalize = false): boolean {
    const newValue = this.getWeight(index).value + amount;
    return this.changePreliminaryWeightValue(index, newValue, normalize);
  }

  // get the trade-off weight with fallback to the preliminary weight
  getWeight(idx: number) {
    if (this.tradeoffWeights.length > idx && this.tradeoffWeights[idx]) {
      return this.tradeoffWeights[idx];
    } else {
      // fallback to preliminary values
      return this.preliminaryWeights[idx];
    }
  }

  getWeights(): ObjectiveWeight[] {
    return this.preliminaryWeights.map((_, i) => this.getWeight(i));
  }

  getWeightValues(): number[] {
    return this.getWeights().map(weight => weight.value);
  }

  addObjective(position: number) {
    this.preliminaryWeights.splice(position, 0, Weights.getDefault());
    this.tradeoffWeights.splice(position, 0, null);
    this.unverifiedWeights.splice(position, 0, false);
    this.explanations.splice(position, 0, '');
    this.manualTradeoffs.splice(position, 0, null);
    if (position <= this.tradeoffObjectiveIdx) {
      this.tradeoffObjectiveIdx++;
    }
  }

  moveObjective(from: number, to: number) {
    this.preliminaryWeights.splice(to, 0, ...this.preliminaryWeights.splice(from, 1));
    this.tradeoffWeights.splice(to, 0, ...this.tradeoffWeights.splice(from, 1));
    this.unverifiedWeights.splice(to, 0, ...this.unverifiedWeights.splice(from, 1));
    this.explanations.splice(to, 0, ...this.explanations.splice(from, 1));
    this.manualTradeoffs.splice(to, 0, ...this.manualTradeoffs.splice(from, 1));
    if (this.tradeoffObjectiveIdx === from) {
      this.tradeoffObjectiveIdx = to;
    } else if (this.tradeoffObjectiveIdx < from && this.tradeoffObjectiveIdx >= to) {
      this.tradeoffObjectiveIdx++;
    } else if (this.tradeoffObjectiveIdx > from && this.tradeoffObjectiveIdx <= to) {
      this.tradeoffObjectiveIdx--;
    }
  }

  removeObjective(position: number) {
    this.preliminaryWeights.splice(position, 1);
    this.tradeoffWeights.splice(position, 1);
    this.unverifiedWeights.splice(position, 1);
    this.explanations.splice(position, 1);
    this.manualTradeoffs.splice(position, 1);
    if (position === this.tradeoffObjectiveIdx) {
      this.resetTradeOffObjective();
    } else if (position < this.tradeoffObjectiveIdx) {
      this.tradeoffObjectiveIdx--;
    }
  }

  /**
   * Returns true when the manual tradeoff of a certain objective is active.
   *
   * @param index - The objective index
   * @returns True iff the tradeoff is active
   */
  isManualTradeoffActive(index: number) {
    const rawTradeoffValues = [
      [0, 0],
      [0, 0],
    ];
    const existingTradeoff = this.manualTradeoffs[index];

    if (!existingTradeoff || !this.tradeoffWeights[index]) {
      return false;
    }

    [[rawTradeoffValues[1][0], rawTradeoffValues[0][0]], [rawTradeoffValues[1][1], rawTradeoffValues[0][1]]] = existingTradeoff;

    const [leftPoint, rightPoint] = [
      [rawTradeoffValues[1][0], rawTradeoffValues[0][0]],
      [rawTradeoffValues[1][1], rawTradeoffValues[0][1]],
    ].sort((a, b) => a[0] - b[0]);
    const newSlope = -(rightPoint[1] - leftPoint[1]) / (rightPoint[0] - leftPoint[0]);

    const tradeoffWeight = clamp(newSlope * this.getWeight(this.tradeoffObjectiveIdx).value, 1, 1000);

    return tradeoffWeight === this.getWeight(index).value;
  }
}
