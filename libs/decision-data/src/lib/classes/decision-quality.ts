export class DecisionQuality {
  public criteriaValues: CriteriaValues = Object.assign(
    {},
    ...DECISION_QUALITY_CRITERIA.map(criteria => {
      return { [criteria]: -1 };
    })
  );
}

export type CriteriaValues = Record<DecisionQualityCriterion, number>;

export type DecisionQualityCriterion = (typeof DECISION_QUALITY_CRITERIA)[number];

export const DECISION_QUALITY_CRITERIA = [
  'APPROPRIATE_FRAME',
  'MEANINGFUL_RELIABLE_INFORMATION',
  'CREATIVE_DOABLE_ALTERNATIVES',
  'CLEAR_VALUES',
  'LOGICALLY_CORRECT_REASONING',
  'COMMITMENT_TO_FOLLOW_THROUGH',
] as const;
