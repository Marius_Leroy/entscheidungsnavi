import { inRange, round } from 'lodash';
import Delta from 'quill-delta/dist/Delta';
import { AggregationSetting, getIndicatorAggregationFunction } from '../calculation/indicator-aggregation';
import { UtilityFunction } from './utility-function';
import { Indicator } from './indicator';

export class IndicatorObjectiveData {
  readonly minStages = 2;
  readonly maxStages = 7;
  constructor(
    public indicators: Indicator[] = [new Indicator(), new Indicator()],
    public utilityfunction = new UtilityFunction(),
    // Set to true to use the custom aggregation formula
    public useCustomAggregation = false,
    public customAggregationFormula = '',
    // worst > best and worst < best are allowed.
    // This is always used if the default aggregation formula is used and otherwise if automaticCustomAggregationLimits is set to false
    public defaultAggregationWorst = 1,
    public defaultAggregationBest = 7,
    public aggregatedUnit = '',
    public stages: { value: number; description: string }[] = [],
    public automaticCustomAggregationLimits = true
  ) {}

  addIndicator(position: number = this.indicators.length, indicator: Indicator) {
    if (position >= 0 && position <= this.indicators.length) {
      this.indicators.splice(position, 0, indicator);
    }
  }

  removeIndicator(position: number) {
    if (position >= 0 && position < this.indicators.length) {
      this.indicators.splice(position, 1);
    }
  }

  moveIndicator(fromPosition: number, toPosition: number) {
    if (inRange(fromPosition, this.indicators.length) && inRange(toPosition, this.indicators.length)) {
      this.indicators.splice(toPosition, 0, ...this.indicators.splice(fromPosition, 1));
    }
  }

  // calculates the most fitting number of verbal stages
  calculateBestNumberOfVerbalStages() {
    const matchesPerNumberOfStages = new Map<number, number>();
    for (let i = this.minStages; i <= this.maxStages; i++) {
      // calculate countMatchingStages for every indicator category and every i [2, 7]
      // take the minimum (bottleneck)
      matchesPerNumberOfStages[i] = Math.min(
        ...this.indicators
          .map(ind =>
            ind.verbalIndicatorCategories.map(category => {
              return this.countMatchingStages(i, category.stages.length);
            })
          )
          .flat()
      );
    }
    // select the minimum i with maximal matchesPerNumberOfStages[i]
    let finalNumberOfStages = 2;
    let currentMaxMatch = 0;
    for (let i = this.maxStages; i >= this.minStages; i--) {
      if (matchesPerNumberOfStages[i] >= currentMaxMatch) {
        finalNumberOfStages = i;
        currentMaxMatch = matchesPerNumberOfStages[i];
      }
    }
    return finalNumberOfStages;
  }

  generateSuggestedVerbalStages(decimalPositions: number, numberOfStages?: number) {
    return this.generateStagesFromNumber(
      numberOfStages == null ? this.calculateBestNumberOfVerbalStages() : numberOfStages,
      decimalPositions
    );
  }

  /**
   * Returns the number of equal stages for two scale with X and Y stages (stages are equidistant)
   *
   * @example
   * x = 5 and y = 7 results in [0, 0.5, 1]
   * x = 3 and y = 7 results in [0, 1]
   */
  private countMatchingStages(x: number, y: number) {
    if (x === y) return x; // obvious
    if ((x === 4 && y === 7) || (x === 7 && y === 4)) return 4; // [0, 0.33, 0.67, 1]
    if (x % 2 === 1 && y % 2 === 1) return 3; // [0, 0.5, 1]
    return 2; // [0, 1]
  }

  private generateStagesFromNumber(stepNumber: number, decimalPositions: number) {
    const stages = [];
    for (let i = 0; i < stepNumber; i++) {
      const percentage = i / (stepNumber - 1);
      let description = new Delta();
      this.indicators.forEach(indicator => {
        const numberOfVerbalCategories = indicator.verbalIndicatorCategories.length;
        const indicatorValue = indicator.min + (indicator.max - indicator.min) * percentage;
        const indicatorValueRounded = round(indicatorValue, decimalPositions);
        const unit = (indicator.unit !== '' ? ' ' : '') + indicator.unit;
        if (numberOfVerbalCategories > 0) {
          // verbal indicator
          const categoryDescription = new Delta();
          let invalidNumberOfStagesFound = false;
          for (const category of indicator.verbalIndicatorCategories) {
            categoryDescription.insert(`${indicator.name} (${indicatorValueRounded}${unit})`, { bold: true });
            if (numberOfVerbalCategories > 1) {
              categoryDescription.insert(' - ', { bold: true });
            } else {
              categoryDescription.insert(': ', { bold: true });
            }
            const stageIdx = percentage * (category.stages.length - 1);
            if (stageIdx % 1 < Number.EPSILON) {
              // match in number of stages per category, insert verbal characteristics
              const followingSymbolStageName = category.stages[stageIdx].description === '' ? '' : ' - ';
              if (numberOfVerbalCategories !== 1) {
                categoryDescription.insert(`${category.name}: `, { bold: true, italic: true });
              }
              categoryDescription.insert(`${category.stages[stageIdx].name}`);
              categoryDescription.insert(`${followingSymbolStageName}`);
              categoryDescription.insert(`${category.stages[stageIdx].description}\n`);
            } else {
              // mismatch in number of stages per category, don't insert any verbal characteristics
              description.insert(`${indicator.name} (${indicatorValueRounded}${unit})\n`, { bold: true });
              invalidNumberOfStagesFound = true;
              break;
            }
          }
          if (!invalidNumberOfStagesFound) {
            description = categoryDescription.compose(description);
          }
        } else {
          // numerical indicator
          description.insert(`${indicator.name} (${indicatorValueRounded}${unit})\n`, { bold: true });
        }
      });
      const value = this.defaultAggregationWorst + (i * (this.defaultAggregationBest - this.defaultAggregationWorst)) / (stepNumber - 1);
      const valueRounded = round(value, decimalPositions);
      stages.push({ value: valueRounded, description: JSON.stringify(description) });
    }
    return stages;
  }

  get aggregationSetting(): AggregationSetting {
    return this.useCustomAggregation
      ? this.customAggregationFormula
      : { worst: this.defaultAggregationWorst, best: this.defaultAggregationBest };
  }

  /**
   * Returns the aggregation function for this indicator objective. Might throw an error
   * if the aggregation function is invalid.
   */
  get aggregationFunction() {
    return getIndicatorAggregationFunction(this.indicators, this.aggregationSetting);
  }

  /**
   * Returns the "best" outcome value for this objective or NaN if an invalid custom aggregation is used.
   */
  get worstValue() {
    return this.calculateWorstBest('worst');
  }

  /**
   * Returns the "worst" outcome value for this objective or NaN if an invalid custom aggregation is used.
   */
  get bestValue() {
    return this.calculateWorstBest('best');
  }

  private calculateWorstBest(mode: 'worst' | 'best'): number {
    if (this.useCustomAggregation && this.automaticCustomAggregationLimits) {
      try {
        const af = this.aggregationFunction;
        return af(this.indicators.map(ind => (mode === 'worst' ? [ind.min] : [ind.max])));
      } catch {
        return NaN;
      }
    } else {
      return mode === 'worst' ? this.defaultAggregationWorst : this.defaultAggregationBest;
    }
  }
}
