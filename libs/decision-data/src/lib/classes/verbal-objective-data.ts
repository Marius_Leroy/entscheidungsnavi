import { getUtilityFunction } from '../calculation/utility';
import { VerbalObjectiveDataInitParams } from './verbal-objective-data-init-params';

/**
 * Hold data for a verbal objective.
 * options[] -> from bad to good
 */
export class VerbalObjectiveData {
  constructor(
    public options: string[] = [],
    public utilities: number[] = [],
    public precision = 0,
    public comments: string[] = [],
    public explanations: string[] = [],
    public initParams: VerbalObjectiveDataInitParams = new VerbalObjectiveDataInitParams(),
    public c = 0,
    public hasCustomUtilityValues: boolean = false,
    public utilityFunctionExplanation = ''
  ) {
    if (this.utilities && this.utilities.length > 0) {
      // reset the utility of the first and last state
      this.utilities[0] = 0;
      this.utilities[this.utilities.length - 1] = 100;
    }

    if (!this.hasCustomUtilityValues) {
      this.adjustUtilityToFunction();
    }
  }

  addOption(position?: number, name = '') {
    const optionLen = this.options.length;
    // add option at the end if an invalid position is given
    if (position == null || position < 0 || position > optionLen) {
      position = optionLen;
    }

    let utility = 0;

    if (position !== 0 && position !== this.options.length) {
      utility = Math.round((this.utilities[position - 1] + this.utilities[position]) / 2);
    }
    this.utilities.splice(position, 0, utility);

    this.options.splice(position, 0, name);
    this.comments.splice(position, 0, '');
    this.explanations.splice(position, 0, '');

    if (!this.hasCustomUtilityValues) {
      this.adjustUtilityToFunction();
    }

    this.setBoundaries();
  }

  removeOption(position: number) {
    this.utilities.splice(position, 1);

    this.setBoundaries();

    this.options.splice(position, 1);
    this.comments.splice(position, 1);
    this.explanations.splice(position, 1);

    if (!this.hasCustomUtilityValues) {
      this.adjustUtilityToFunction();
    }
  }

  setBoundaries() {
    // reset the utility of the first and last state
    if (this.utilities.length > 1) {
      this.utilities[0] = 0;
    }
    if (this.utilities.length > 0) {
      this.utilities[this.utilities.length - 1] = 100;
    }
  }

  optionCount() {
    return this.options.length;
  }

  getOption(position: number) {
    return this.options[position];
  }

  adjustUtilityToFunction() {
    const utilityFunction = getUtilityFunction(this.c);
    this.options.forEach((_option, index) => {
      const y = utilityFunction(index / (this.options.length - 1));

      this.utilities[index] = y * 100;
    });
  }
}
