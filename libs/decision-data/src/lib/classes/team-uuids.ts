import { invert } from 'lodash';
import { NaviStep } from '../steps';

export class TeamUUIDs {
  static readonly STEPS = {
    decisionStatement: 'step-decisionStatement',
    objectives: 'step-objectives',
    alternatives: 'step-alternatives',
    impactModel: 'step-impactModel',
    results: 'step-results',
    finishProject: 'step-finishProject',
  } as const satisfies Record<NaviStep, string>;

  static readonly STEPS_INVERTED = invert(TeamUUIDs.STEPS) as Record<(typeof TeamUUIDs.STEPS)[NaviStep], NaviStep>;
}
