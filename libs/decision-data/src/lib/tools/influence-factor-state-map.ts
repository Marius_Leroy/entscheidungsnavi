import { checkType } from '@entscheidungsnavi/tools';
import { PredefinedInfluenceFactorId } from '../classes';

export type PredefinedIfKey = {
  id: PredefinedInfluenceFactorId;
  alternativeIndex: number;
  objectiveIndex: number;
  // Is always 0 for non-indicator objectives
  indicatorIndex: number;
};

export type IfKey = number | PredefinedIfKey;

/**
 * A class that maps individual influence factors as keys to some value.
 *
 * - Userdefined Influencefactors can be keys.
 * - Predefined Influencefactors together with their outcome index can be keys.
 * This is because different appearances of the same predefined IF are independent, while they are connected for userdefined IFs.
 */
export class InfluenceFactorStateMap<T> {
  private map = new Map<string, T>();

  static fromInnerMap<T>(map: ReadonlyMap<string, T>) {
    const newObj = new InfluenceFactorStateMap<T>();
    newObj.map = new Map(map);
    return newObj;
  }

  private getKey(key: IfKey) {
    if (typeof key === 'number') {
      return `U_${key}`;
    } else {
      return `P_${key.id}_${key.alternativeIndex}_${key.objectiveIndex}_${key.indicatorIndex}`;
    }
  }

  get(key: IfKey) {
    return this.map.get(this.getKey(key));
  }

  set(key: IfKey, value: T) {
    this.map.set(this.getKey(key), value);
  }

  has(key: IfKey) {
    return this.map.has(this.getKey(key));
  }

  get size() {
    return this.map.size;
  }

  *entries(): Generator<readonly [IfKey, T], void> {
    for (const [key, value] of this.map.entries()) {
      if (key.startsWith('U')) {
        yield [parseInt(key.substring(2)), value] as const;
      } else {
        const split = key.split('_');
        yield [
          checkType<PredefinedIfKey>({
            id: split[1] as PredefinedInfluenceFactorId,
            alternativeIndex: parseInt(split[2]),
            objectiveIndex: parseInt(split[3]),
            indicatorIndex: parseInt(split[4]),
          }),
          value,
        ] as const;
      }
    }
  }

  innerMap(): ReadonlyMap<string, T> {
    return this.map;
  }
}
