import { AfterViewInit, Directive, EventEmitter, Output } from '@angular/core';

@Directive({
  selector: '[dtAfterViewInit]',
  standalone: true,
})
export class AfterViewInitDirective implements AfterViewInit {
  @Output('dtAfterViewInit') emitter = new EventEmitter<void>();

  ngAfterViewInit() {
    this.emitter.emit();
  }
}
