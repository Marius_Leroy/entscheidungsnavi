import { Directive, ElementRef, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output } from '@angular/core';

/*
  This directive registers and calls the provided handler for a specific event outside of the angular zone, preventing
  change detection from running.
*/
@Directive({
  selector: '[dtDirectEvent]',
})
export class DirectEventDirective implements OnInit, OnDestroy {
  @Input()
  event: string | string[];

  @Output() dtDirectEvent = new EventEmitter();

  @Output() dtDirectEvent2 = new EventEmitter();
  @Output() dtDirectEvent3 = new EventEmitter();

  private privateHandler: Array<(event: Event) => any>;

  private events: string[];

  constructor(private zone: NgZone, private elementRef: ElementRef<HTMLElement>) {}

  ngOnInit() {
    this.events = Array.isArray(this.event) ? this.event : [this.event];
    this.zone.runOutsideAngular(() => {
      const nativeElement = this.elementRef.nativeElement;

      let index = 0;

      this.privateHandler = [];

      for (const e of this.events) {
        const myIndex = index++;
        this.privateHandler.push($event => {
          if (myIndex === 0) {
            this.dtDirectEvent.emit($event);
          } else if (myIndex === 1) {
            this.dtDirectEvent2.emit($event);
          } else if (myIndex === 2) {
            this.dtDirectEvent3.emit($event);
          }
        });

        nativeElement.addEventListener(e, this.privateHandler[myIndex], false);
      }
    });
  }

  ngOnDestroy() {
    let index = 0;

    for (const e of this.events) {
      const myIndex = index++;
      this.elementRef.nativeElement.removeEventListener(e, this.privateHandler[myIndex]);
    }
  }
}
