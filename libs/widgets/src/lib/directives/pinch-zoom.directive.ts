import { AfterViewInit, Directive, ElementRef, EventEmitter, NgZone, OnDestroy, Output } from '@angular/core';

type ActiveTouch = { ident: number; x: number; y: number };

/**
 * Important: The Events coming out of this directive don't trigger change detection,
 * if necessary you have to do that yourself.
 */
@Directive({
  selector: '[dtPinchZoom]',
  standalone: true,
})
export class PinchZoomDirective implements AfterViewInit, OnDestroy {
  pinchTouches: [ActiveTouch, ActiveTouch] = [null, null];

  @Output()
  dtPinchZoom: EventEmitter<number> = new EventEmitter();

  constructor(private el: ElementRef<HTMLElement>, private zone: NgZone) {}

  private touchStart = function (this: PinchZoomDirective, event: TouchEvent) {
    for (const ct of Array.from(event.changedTouches)) {
      const emptyIndex = this.pinchTouches.findIndex(at => !at);

      if (emptyIndex === -1) {
        // We already have the two touches we use for zooming
        return;
      }

      this.pinchTouches[emptyIndex] = { ident: ct.identifier, x: ct.pageX, y: ct.pageY };
    }
  }.bind(this);

  private touchMove = function (this: PinchZoomDirective, event: TouchEvent) {
    const hasBothTouches = this.pinchTouches[0] && this.pinchTouches[1];
    let oldDistance = -1;
    if (hasBothTouches) {
      oldDistance = Math.sqrt(
        Math.pow(this.pinchTouches[0].x - this.pinchTouches[1].x, 2) + Math.pow(this.pinchTouches[0].y - this.pinchTouches[1].y, 2)
      );
    }
    let changed = false;

    for (const ct of Array.from(event.changedTouches)) {
      if (ct.identifier === this.pinchTouches[0]?.ident) {
        this.pinchTouches[0].x = ct.pageX;
        this.pinchTouches[0].y = ct.pageY;
        changed = true;
      } else if (ct.identifier === this.pinchTouches[1]?.ident) {
        this.pinchTouches[1].x = ct.pageX;
        this.pinchTouches[1].y = ct.pageY;
        changed = true;
      }
    }

    if (changed && hasBothTouches) {
      const newDistance = Math.sqrt(
        Math.pow(this.pinchTouches[0].x - this.pinchTouches[1].x, 2) + Math.pow(this.pinchTouches[0].y - this.pinchTouches[1].y, 2)
      );

      const difference = (newDistance - oldDistance) / oldDistance;

      this.dtPinchZoom.emit(difference);

      event.preventDefault();
    }
  }.bind(this);

  private touchEnd = function (this: PinchZoomDirective, event: TouchEvent) {
    for (const ct of Array.from(event.changedTouches)) {
      if (ct.identifier === this.pinchTouches[0]?.ident) {
        this.pinchTouches[0] = null;
      } else if (ct.identifier === this.pinchTouches[1]?.ident) {
        this.pinchTouches[1] = null;
      }
    }
  }.bind(this);

  ngAfterViewInit() {
    const element = this.el.nativeElement;

    this.zone.runOutsideAngular(() => {
      element.addEventListener('touchstart', this.touchStart);
      element.addEventListener('touchmove', this.touchMove);
      element.addEventListener('touchend', this.touchEnd);
      element.addEventListener('touchcancel', this.touchEnd);
    });
  }

  ngOnDestroy() {
    const element = this.el.nativeElement;

    this.zone.runOutsideAngular(() => {
      element.removeEventListener('touchstart', this.touchStart);
      element.removeEventListener('touchmove', this.touchMove);
      element.removeEventListener('touchend', this.touchEnd);
      element.removeEventListener('touchcancel', this.touchEnd);
    });
  }
}
