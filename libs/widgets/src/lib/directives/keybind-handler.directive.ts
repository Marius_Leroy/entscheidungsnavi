import { Directive, ElementRef, NgZone, OnDestroy, OnInit, Renderer2 } from '@angular/core';

type KeyPressDirection = null | 'down' | 'up';

class KeyBind {
  constructor(
    public key: string | null,
    public ctrlKey: boolean | null,
    public shiftKey: boolean | null,
    public callback: (event?: KeyboardEvent) => void,
    public callbackOnBlur: boolean,
    public direction: KeyPressDirection,
    public onRepeat: boolean,
    public conditions: Array<(event: KeyboardEvent) => boolean>
  ) {}
}

/**
 * Utility Class to simplify Key Bindings, example in ObjectiveAspectHierarchyComponent
 */
@Directive({ standalone: true })
export class KeyBindHandlerDirective implements OnInit, OnDestroy {
  private bindings: KeyBind[] = [];

  private keyDownHandler: (event: KeyboardEvent) => void;
  private keyUpHandler: (event: KeyboardEvent) => void;
  private blurHandler: () => void;

  private lastKeyboardEvent: KeyboardEvent | null = null;

  constructor(private zone: NgZone, private elementRef: ElementRef<HTMLElement>, private renderer2: Renderer2) {}

  ngOnInit() {
    if (this.elementRef.nativeElement.tabIndex === -1) {
      // The element needs to be selectable to capture key events
      this.elementRef.nativeElement.tabIndex = 0;
      this.renderer2.setStyle(this.elementRef.nativeElement, 'outline', 'none');
    }

    this.zone.runOutsideAngular(() => {
      this.elementRef.nativeElement.addEventListener('keydown', (this.keyDownHandler = event => this.handleKeyPress(event, 'down')));
      this.elementRef.nativeElement.addEventListener('keyup', (this.keyUpHandler = event => this.handleKeyPress(event, 'up')));
      this.elementRef.nativeElement.addEventListener('blur', (this.blurHandler = () => this.handleBlur()));
    });
  }

  ngOnDestroy() {
    this.zone.runOutsideAngular(() => {
      this.elementRef.nativeElement.removeEventListener('keydown', this.keyDownHandler);
      this.elementRef.nativeElement.removeEventListener('keyup', this.keyUpHandler);
      this.elementRef.nativeElement.removeEventListener('blur', this.blurHandler);
    });
  }

  register({
    callback,
    key = null,
    ctrlKey = null,
    shiftKey = null,
    direction = 'down',
    callbackOnBlur = false,
    onRepeat = true,
    conditions = [],
  }: {
    callback: (event?: KeyboardEvent) => void;
    key?: string;
    ctrlKey?: boolean;
    shiftKey?: boolean;
    direction?: KeyPressDirection;
    callbackOnBlur?: boolean;
    onRepeat?: boolean;
    conditions?: Array<(event: KeyboardEvent) => boolean>;
  }): void {
    this.bindings.push(new KeyBind(key, ctrlKey, shiftKey, callback, callbackOnBlur, direction, onRepeat, conditions));
  }

  // ignore direction
  doesKeyBindMatch(keyBind: KeyBind, event: KeyboardEvent) {
    return (
      (keyBind.key === null || keyBind.key === event.key) &&
      (keyBind.ctrlKey == null || keyBind.ctrlKey === event.ctrlKey) &&
      (keyBind.shiftKey == null || keyBind.shiftKey === event.shiftKey) &&
      keyBind.conditions.every(c => c(event))
    );
  }

  handleKeyPress(event: KeyboardEvent, direction: KeyPressDirection) {
    const matchingBindings = this.bindings.filter((keyBind: KeyBind) => {
      if (event.repeat && !keyBind.onRepeat) return false;

      const matchesLastEvent = () => (this.lastKeyboardEvent ? this.doesKeyBindMatch(keyBind, this.lastKeyboardEvent) : false);
      const matchesCurrentEvent = () => this.doesKeyBindMatch(keyBind, event);

      if (keyBind.direction === null) {
        return matchesLastEvent() || matchesCurrentEvent();
      } else if (direction === 'down' && keyBind.direction === 'down') {
        return matchesCurrentEvent();
      } else if (direction === 'up' && keyBind.direction === 'up') {
        return matchesLastEvent();
      } else {
        return false;
      }
    });

    if (matchingBindings.length > 0) {
      this.zone.run(() => {
        matchingBindings.forEach((triggeredKeyBind: KeyBind) => {
          triggeredKeyBind.callback(event);
        });
      });
    }

    this.lastKeyboardEvent = event;
  }

  handleBlur() {
    this.bindings.filter(binding => binding.callbackOnBlur).forEach(keyUpBinding => this.zone.run(() => keyUpBinding.callback()));
  }
}
