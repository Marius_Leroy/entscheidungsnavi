import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  DoCheck,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { Screw } from '@entscheidungsnavi/decision-data/steps';
import { intersection, some, sortBy } from 'lodash';
import { MatRipple, MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { filter, map, pairwise, startWith } from 'rxjs';
import { ScrewConfiguration, ScrewConfigurationProvider } from '@entscheidungsnavi/decision-data/interfaces';
import { NoteBtnComponent, NoteBtnPresetPipe } from '../note-btn';
import { ClickOutsideDirective, OverflowDirective } from '../directives';
import { KeyBindHandlerDirective } from '../directives/keybind-handler.directive';

type StatePosition = number;

type StateConnection = {
  // Why an array and not separately for each connection. It is for optimization purposes:
  // With many alternatives that are going through the not defined states between screws A and B,
  // we only want to render one connection between A and B.
  configurationIndices: number[];
  start: number;
  end: number;
};

@Component({
  selector: 'dt-screw-list',
  templateUrl: './screw-list.component.html',
  styleUrls: ['./screw-list.component.scss'],
  standalone: true,
  imports: [
    ClickOutsideDirective,
    CommonModule,
    DragDropModule,
    MatRippleModule,
    MatIconModule,
    NoteBtnComponent,
    OverflowDirective,
    NoteBtnPresetPipe,
    KeyBindHandlerDirective,
  ],
  hostDirectives: [KeyBindHandlerDirective],
})
export class ScrewListComponent implements OnInit, DoCheck, AfterViewInit {
  @HostBinding('class.is-dragging-screw')
  isDraggingScrew = false;

  @HostBinding('class.is-pressing-shift')
  isShiftPressed = false;

  @ViewChild('configurationLabelContainer') configurationLabelsContainer: ElementRef<HTMLDivElement>;

  @ViewChild('configurationLock', { read: MatRipple }) ripple: MatRipple;

  @ViewChildren('configurationLabel') configurationLabels: QueryList<ElementRef<HTMLDivElement>>;

  @ViewChildren('screw') screwElements: QueryList<ElementRef<HTMLDivElement>>;

  @Input()
  screws: Screw[];

  @Input()
  // Dimension: #Configurations x #Screws
  // null => not defined
  screwConfigurationProviders: readonly ScrewConfigurationProvider[];

  @Input()
  canBlur = false;

  // when not editable it enables the dragging of screws/stages, hides empty fields and disables any changes to the data
  // used in the definition modal for verbal indicators
  @Input()
  editable = true;

  @Output()
  screwConfigurationChangeAtProvider = new EventEmitter<[configurationIndex: number, changedScrewConfiguration: ScrewConfiguration]>();

  @Input()
  newScrewConfiguration: ScrewConfiguration;

  @Output()
  newScrewConfigurationChange = new EventEmitter<ScrewConfiguration>();

  selectedProvider = -1;

  @Output()
  appendScrew = new EventEmitter<void>();

  @Output()
  moveScrew = new EventEmitter<[number, number]>();

  @Output()
  editOrDeleteScrew = new EventEmitter<number>();

  @Output()
  dblClickedElement = new EventEmitter<[categoryName: string, htmlElement: HTMLElement]>();

  readonly connectorStripeWidth = 60;

  highlightedConfigurationIndices: number[] = [];

  ratioHiddenStateLabelsTop = 0;
  ratioHiddenStateLabelsBottom = 0;

  stateConnections: StateConnection[][];

  isSelectedConfigurationUnlocked = false;

  constructor(private cdr: ChangeDetectorRef, private keyBindHandler: KeyBindHandlerDirective) {}

  ngOnInit() {
    this.stateConnections = this.screws.map(() => []);
    this.setupKeyBindings();
  }

  ngAfterViewInit() {
    this.screwElements.changes
      .pipe(
        map(screws => screws.length),
        startWith(this.screwElements.length),
        pairwise(),
        filter(([oldScrewCount, newScrewCount]) => oldScrewCount < newScrewCount)
      )
      .subscribe(() => {
        this.screwElements.last.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
      });

    this.configurationLabels.changes
      .pipe(
        map(configurationLabels => configurationLabels.length),
        startWith(this.configurationLabels.length),
        pairwise(),
        filter(([oldConfigurationLabelCount, newConfigurationLabelCount]) => oldConfigurationLabelCount < newConfigurationLabelCount)
      )
      .subscribe(([_, newConfigurationLabelCount]) => {
        this.selectConfiguration(newConfigurationLabelCount - 1);
        this.scrollConfigurationLabelIntoView(newConfigurationLabelCount - 1);
        // To avoid an ExpressionChangedAfterItHasBeenCheckedError.
        this.cdr.detectChanges();
      });
  }

  setupKeyBindings() {
    this.keyBindHandler.register({
      key: 'Escape',
      callback: () => {
        if (this.isSelectedConfigurationUnlocked) {
          this.toggleLockOfSelectedConfiguration();
        } else {
          this.selectConfiguration(this.selectedProvider);
          if (this.selectedProvider === -1) this.newScrewConfigurationChange.emit(this.newScrewConfiguration.map(() => null));
        }
      },
    });

    this.keyBindHandler.register({
      callbackOnBlur: true,
      callback: () => {
        this.isShiftPressed = false;
      },
      conditions: [event => event.key !== 'Shift'],
    });

    this.keyBindHandler.register({
      key: 'Shift',
      direction: 'down',
      onRepeat: false,
      callback: () => {
        if (this.isSelectedConfigurationUnlocked) {
          this.ripple.launch({ centered: true });
        } else {
          this.isShiftPressed = true;
        }
      },
    });

    this.keyBindHandler.register({
      key: 'Shift',
      direction: 'up',
      onRepeat: false,
      callbackOnBlur: true,
      callback: () => {
        this.isShiftPressed = false;
      },
    });
  }

  selectConfiguration(configurationIndex: number) {
    if (this.selectedProvider === configurationIndex) {
      configurationIndex = -1;
    }

    this.isSelectedConfigurationUnlocked = false;

    if (configurationIndex === -1 && this.selectedProvider !== -1) {
      this.newScrewConfigurationChange.emit(this.newScrewConfiguration.map(() => null));
    }

    this.selectedProvider = configurationIndex;
  }

  toggleLockOfSelectedConfiguration() {
    if (this.isShiftPressed) return;
    this.isSelectedConfigurationUnlocked = !this.isSelectedConfigurationUnlocked;
  }

  ngDoCheck() {
    // Do not update while dragging because there is some DOM manipulation going on during the dnd process.
    if (this.isDraggingScrew) return;

    const statePositionCache = new Map<string, StatePosition>();
    this.stateConnections = this.screws
      .filter((_, screwIndex) => screwIndex < this.screws.length - 1)
      .map((_, screwIndex) => this.getForwardConnectionsOf(screwIndex, statePositionCache));
  }

  private getForwardConnectionsOf(screwIndex: number, statePositionCache: Map<string, StatePosition>): StateConnection[] | null {
    const getStatePositionOf = (screwIndex: number, stateIndex: number | null): StatePosition | null => {
      if (stateIndex == null) stateIndex = this.screws[screwIndex].states.length;

      const stateKey = [screwIndex, stateIndex].join(';');

      if (statePositionCache.has(stateKey)) {
        return statePositionCache.get(stateKey);
      }

      const stateElement = this.screwElements?.get(screwIndex)?.nativeElement.querySelectorAll<HTMLDivElement>('.state').item(stateIndex);
      if (!stateElement) return null;

      const statePosition = stateElement.offsetTop + stateElement.offsetHeight / 2;

      statePositionCache.set(stateKey, statePosition);

      return statePosition;
    };

    const forwardStateConnections: StateConnection[] = [];
    const statePairToConnection = new Map<string, number>();

    this.screwConfigurationProviders
      .map(screwConfigurationProvider => screwConfigurationProvider.screwConfiguration)
      .forEach((screwConfiguration, configurationIndex) => {
        const fromStateIndex = screwConfiguration[screwIndex];
        const toStateIndex = screwConfiguration[screwIndex + 1];

        const fromStatePosition = getStatePositionOf(screwIndex, fromStateIndex);
        const toStatePosition = getStatePositionOf(screwIndex + 1, toStateIndex);

        if (!(fromStatePosition && toStatePosition)) return;

        const stateConnectionKey = [fromStateIndex, toStateIndex].join('-');

        if (statePairToConnection.has(stateConnectionKey)) {
          forwardStateConnections[statePairToConnection.get(stateConnectionKey)].configurationIndices.push(configurationIndex);
        } else {
          forwardStateConnections.push({
            configurationIndices: [configurationIndex],
            start: fromStatePosition,
            end: toStatePosition,
          });

          statePairToConnection.set(stateConnectionKey, forwardStateConnections.length - 1);
        }
      });

    return forwardStateConnections;
  }

  clickState(screwIndex: number, stateIndex: number | null, event: MouseEvent) {
    if (!this.editable) {
      return;
    }
    let newScrewConfiguration: ScrewConfiguration;

    if (this.selectedProvider === -1) {
      newScrewConfiguration = this.newScrewConfiguration;
    } else {
      newScrewConfiguration = [...this.screwConfigurationProviders[this.selectedProvider].screwConfiguration];
    }

    newScrewConfiguration[screwIndex] = stateIndex;

    const handleConfigurationChange = (force: boolean) => {
      if (this.selectedProvider !== -1) {
        if (force || this.isSelectedConfigurationUnlocked) {
          this.screwConfigurationChangeAtProvider.emit([this.selectedProvider, newScrewConfiguration]);
        } else {
          this.ripple.launch({ centered: true });
        }
      } else {
        this.newScrewConfigurationChange.emit(newScrewConfiguration);
      }
    };

    if (event.detail === 1) {
      handleConfigurationChange(this.isShiftPressed);
    } else if (event.detail === 2) {
      handleConfigurationChange(true);
    }
  }

  toggleHighlightOfConfigurationsGoingThroughState(screwIndex: number, stateIndex: number) {
    const configurationsGoingThroughState = this.screwConfigurationProviders
      .map((configurationProvider, configurationIndex) => [configurationProvider.screwConfiguration, configurationIndex] as const)
      .filter(([configuration, _]) => configuration[screwIndex] === stateIndex)
      .map(([_, configurationIndex]) => configurationIndex);

    if (
      configurationsGoingThroughState.length === 0 ||
      intersection(configurationsGoingThroughState, this.highlightedConfigurationIndices).length > 0
    ) {
      this.disableHighlightOnConfigurations();
    } else {
      this.enableHighlightOnConfigurations(configurationsGoingThroughState);
    }
  }

  enableHighlightOnConfigurations(configurationIndices: number[]) {
    if (!this.editable) return;
    this.highlightedConfigurationIndices = configurationIndices;
    this.updateHiddenStateLabelRatios();
  }

  updateHiddenStateLabelRatios() {
    const configurationLabelsContainer = this.configurationLabelsContainer.nativeElement;
    const configurationLabelContainerTop = configurationLabelsContainer.scrollTop;
    const configurationLabelContainerBottom = configurationLabelsContainer.scrollTop + configurationLabelsContainer.offsetHeight;

    this.ratioHiddenStateLabelsTop = 0;
    this.ratioHiddenStateLabelsBottom = 0;

    this.configurationLabels.forEach((configurationLabel, configurationIndex) => {
      if (!this.highlightedConfigurationIndices.includes(configurationIndex)) return;
      const configurationLabelTop = configurationLabel.nativeElement.offsetTop;
      const configurationLabelBottom = configurationLabelTop + configurationLabel.nativeElement.offsetHeight;

      if (configurationLabelTop <= configurationLabelContainerTop) {
        this.ratioHiddenStateLabelsTop += Math.min(configurationLabelBottom, configurationLabelContainerTop) - configurationLabelTop;
      } else if (configurationLabelBottom >= configurationLabelContainerBottom) {
        this.ratioHiddenStateLabelsBottom += configurationLabelBottom - Math.max(configurationLabelTop, configurationLabelContainerBottom);
      }
    });

    const sumOfHiddenLengths = this.ratioHiddenStateLabelsTop + this.ratioHiddenStateLabelsBottom;

    if (sumOfHiddenLengths !== 0) {
      this.ratioHiddenStateLabelsTop = this.ratioHiddenStateLabelsTop / sumOfHiddenLengths;
      this.ratioHiddenStateLabelsBottom = this.ratioHiddenStateLabelsBottom / sumOfHiddenLengths;
    } else {
      // this.ratioHiddenStateLabelsTop = this.ratioHiddenStateLabelsBottom = 0 already holds.
    }
  }

  getHiddenStatesIndicatorColor(ratioHiddenStateLabels: number) {
    return `2px solid rgba(248, 147, 30, ${ratioHiddenStateLabels})`;
  }

  disableHighlightOnConfigurations() {
    this.highlightedConfigurationIndices = [];
    this.ratioHiddenStateLabelsTop = this.ratioHiddenStateLabelsBottom = 0;
  }

  scrollConfigurationLabelIntoView(configurationIndex: number) {
    if (configurationIndex === -1) return;

    this.configurationLabels.get(configurationIndex).nativeElement.scrollIntoView({
      behavior: 'smooth',
      block: 'nearest',
    });
  }

  onScrollScrewContainer(wheelEvent: WheelEvent) {
    if (this.highlightedConfigurationIndices.length === 0) return;

    this.configurationLabelsContainer.nativeElement.scrollBy(0, wheelEvent.deltaY);
    wheelEvent.stopPropagation();
    wheelEvent.preventDefault();
  }

  isStateHighlighted(screwIndex: number, stateIndex: number) {
    return some(
      this.highlightedConfigurationIndices.map(
        highlightedConfigurationIndex =>
          this.screwConfigurationProviders[highlightedConfigurationIndex].screwConfiguration[screwIndex] === stateIndex
      )
    );
  }

  isStateSelected(screwIndex: number, stateIndex: number) {
    return (
      (this.selectedProvider === -1
        ? this.newScrewConfiguration
        : this.screwConfigurationProviders[this.selectedProvider].screwConfiguration)[screwIndex] === stateIndex
    );
  }

  leftRightConnectorToPathDefinition(leftRightConnector: StateConnection) {
    const middleX = this.connectorStripeWidth / 2;
    return `M0 ${leftRightConnector.start} C${middleX} ${leftRightConnector.start},
     ${middleX} ${leftRightConnector.end},
     ${this.connectorStripeWidth} ${leftRightConnector.end}
    `;
  }

  sortStateConnectionsByLevelOfHighlight(stateConnections: StateConnection[]) {
    return sortBy(stateConnections, (stateConnection: StateConnection) => {
      if (this.isStateConnectionSelected(stateConnection)) return 2;
      if (this.isStateConnectionHighlighted(stateConnection)) return 1;
      return 0;
    });
  }

  trackStateConnectionsByEndpoints(_: number, screwStateConnection: StateConnection) {
    return `${screwStateConnection.start};${screwStateConnection.end}`;
  }

  isStateConnectionHighlighted(stateConnection: StateConnection) {
    return intersection(this.highlightedConfigurationIndices, stateConnection.configurationIndices).length > 0;
  }

  isStateConnectionSelected(stateConnection: StateConnection) {
    return stateConnection.configurationIndices.includes(this.selectedProvider);
  }

  // enables dragging of states as text (for non-editable screws)
  dragStart(event: DragEvent, text: string) {
    event.dataTransfer.dropEffect = 'move';
    event.dataTransfer.setData('text', text);
  }
}
