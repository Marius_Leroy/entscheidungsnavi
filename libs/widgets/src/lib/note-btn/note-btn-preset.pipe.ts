import { Pipe, PipeTransform } from '@angular/core';

type StringPresets =
  | 'alternative'
  | 'screw'
  | 'objective'
  | 'objectiveWeight'
  | 'utilityFunction'
  | 'influenceFactor'
  | 'verbalOption'
  | 'indicator'
  | 'step';

@Pipe({ name: 'noteBtnPreset', standalone: true })
export class NoteBtnPresetPipe implements PipeTransform {
  transform(value: string, type: StringPresets): string;
  transform(value: { alternative: string; objective: string }, type: 'outcome'): { title: string; subtitle: string };
  transform(value: any, type: StringPresets | 'outcome'): any {
    switch (type) {
      case 'alternative':
        return $localize`Erläuterung zur Alternative "${value}"`;
      case 'screw':
        return $localize`Erläuterung zum Stellhebel "${value}"`;
      case 'objective':
        return $localize`Erläuterung zum Ziel "${value}"`;
      case 'objectiveWeight':
        return $localize`Erläuterung zum Gewicht für das Ziel "${value}"`;
      case 'utilityFunction':
        return $localize`Erläuterung zur Nutzenfunktion für das Ziel "${value}"`;
      case 'influenceFactor':
        return $localize`Erläuterung zum Einflussfaktor "${value}"`;
      case 'verbalOption':
        return $localize`Erläuterung der Ausprägung "${value}"`;
      case 'indicator':
        return $localize`Erläuterung zum Indikator "${value}"`;
      case 'outcome':
        return {
          title: $localize`Erläuterung zu einer Wirkungsprognose`,
          subtitle: $localize`Ziel "${value.objective}", Alternative "${value.alternative}"`,
        };
      case 'step':
        return $localize`Notizen zum Schritt "${value}"`;
    }
  }
}
