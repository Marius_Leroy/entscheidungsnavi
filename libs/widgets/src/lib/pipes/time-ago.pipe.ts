import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'timeAgo', pure: true, standalone: true })
export class TimeAgoPipe implements PipeTransform {
  transform(timestamp: Date) {
    const seconds = Math.floor((new Date().getTime() - timestamp.getTime()) / 1000);
    if (seconds < 60) {
      return $localize`Gerade eben`;
    } else if (seconds < 3600) {
      const minutes = Math.floor(seconds / 60);
      return minutes > 1 ? $localize`${minutes} Minuten` : $localize`1 Minute`;
    } else if (seconds < 86400) {
      const hours = Math.floor(seconds / 3600);
      return hours > 1 ? $localize`${hours} Stunden` : $localize`1 Stunde`;
    } else {
      const days = Math.floor(seconds / 86400);
      return days > 1 ? $localize`${days} Tagen` : $localize`1 Tag`;
    }
  }
}
