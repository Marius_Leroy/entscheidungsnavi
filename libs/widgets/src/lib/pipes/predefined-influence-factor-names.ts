import { PredefinedInfluenceFactorId } from '@entscheidungsnavi/decision-data/classes';

export const PREDEFINED_INFLUENCE_FACTOR_NAMES: Record<PredefinedInfluenceFactorId, { name: string; stateNames: string[] }> = {
  '3-Scenarios': {
    name: $localize`Worst-Best-Verteilung`,
    stateNames: [$localize`Worst (p.10)`, $localize`Median (p.50)`, $localize`Best (p.90)`],
  },
};
