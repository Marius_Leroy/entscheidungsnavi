import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
import { LocalizedString } from '@entscheidungsnavi/api-types';

@Pipe({ name: 'localizedString' })
export class LocalizedStringPipe implements PipeTransform {
  constructor(@Inject(LOCALE_ID) private locale: string) {}

  transform(value: LocalizedString): string {
    return this.locale.includes('de') ? value.de : value.en;
  }
}
