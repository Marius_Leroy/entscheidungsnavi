import { Pipe, PipeTransform } from '@angular/core';
import { InfluenceFactor, PredefinedInfluenceFactor, UserdefinedInfluenceFactor } from '@entscheidungsnavi/decision-data/classes';
import { PREDEFINED_INFLUENCE_FACTOR_NAMES } from './predefined-influence-factor-names';

/**
 * Example Usage: {{ value | stateName: 2 }}
 *
 * Casts the given value to a number.
 */

@Pipe({ name: 'stateName', pure: false })
export class StateNamePipe implements PipeTransform {
  transform(influenceFactor: InfluenceFactor, stateIdx: number): string {
    if (influenceFactor == null || stateIdx == null) {
      return '';
    } else if (influenceFactor instanceof UserdefinedInfluenceFactor) {
      /* Userdefined influence factor */
      if (stateIdx < influenceFactor.states.length) {
        return influenceFactor.states[stateIdx].name;
      }
    } else if (influenceFactor instanceof PredefinedInfluenceFactor) {
      /* Predefined influence factor */
      return PREDEFINED_INFLUENCE_FACTOR_NAMES[influenceFactor.id].stateNames[stateIdx] || '';
    }
    return '';
  }
}
