import { Pipe, PipeTransform } from '@angular/core';
import '@entscheidungsnavi/tools/number_rounding';

@Pipe({
  name: 'round',
})
export class NumberRoundingPipe implements PipeTransform {
  transform(value: number, sigDigits = 0): number {
    return value.round(sigDigits);
  }
}
