import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'isInteger', standalone: true })
export class IsIntegerPipe implements PipeTransform {
  transform(value: number): boolean {
    return Number.isInteger(value);
  }
}
