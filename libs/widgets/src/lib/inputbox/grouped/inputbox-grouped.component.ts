import { animate, AnimationEvent, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, HostBinding, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { ModalComponent } from '../../modal';
import { NoteBtnCaption, NoteBtnComponent } from '../../note-btn';
import { InputboxComponent } from '../inputbox.component';

/*
 Usage:
 <dt-inputbox [deleteButton]="true" [collapsibleButton]="true">
 <div class="caption-wrapper">Headline</div>
 <div class="control-wrapper">
 Option to add certain controls e.g. a delete button
 </div>
 <div class="content-wrapper">
 Test Content
 </div>
 </dt-inputbox>
 */

interface Child {
  name: string;
  identity: any;
}

@Component({
  selector: 'dt-inputbox-grouped',
  templateUrl: 'inputbox-grouped.component.html',
  styleUrls: ['inputbox-grouped.component.scss'],
  animations: [
    trigger('deleteAnimation', [
      state('false', style({ opacity: 1 })),
      state('true', style({ opacity: 0 })),
      transition('false => true', animate('500ms ease')),
    ]),
  ],
})
export class InputboxGroupedComponent {
  @Input() notes: string;
  @Input() notesEditable = true;
  @Input() noteCaption?: NoteBtnCaption;

  @Input() children: Child[];
  @Input() isLocked: boolean;
  @Input() lockedChildren: boolean[];
  @Output() childrenChange = new EventEmitter<Child[]>();

  @Input() name: string;
  @Input() namePlaceholder: string;

  @Input() collapsedByDefault = false;
  @Input() readonly = false;

  @Input() customData: any;

  @Output() deleteEvent = new EventEmitter();

  @Output() nameChange = new EventEmitter();
  @Output() notesChange = new EventEmitter();

  @Output() noteModalClick = new EventEmitter();

  @ViewChild(InputboxComponent, { static: true })
  inputBoxBase: InputboxComponent;

  @Input() confirmDeleteTemplate: TemplateRef<any>;
  @Input() confirmChildDeleteTemplate: TemplateRef<{ $implicit: Child }>;

  editorModules = {};

  @HostBinding('attr.group')
  group = true;

  @ViewChild(NoteBtnComponent, { static: true })
  noteBtn: NoteBtnComponent;

  @ViewChild('modalConfirmDeleteChild', { static: true }) confirmDeleteChildModal: ModalComponent;
  deletingChildIndex: number;

  deletedChild = false;

  constructor() {
    this.editorModules = {
      toolbar: {
        container: [['bold', 'italic', 'underline', 'strike']],
      },
    };
  }

  public getBase() {
    return this.inputBoxBase;
  }

  openNotes() {
    if (this.isLocked) {
      return;
    }
    this.noteBtn.open();
  }

  deleteChild(i: number) {
    this.deletingChildIndex = i;
    this.confirmDeleteChildModal.open();
  }

  confirmDeleteChild() {
    this.confirmDeleteChildModal.close();
    this.deletedChild = true;
  }

  finishDeleteChild(event: AnimationEvent) {
    if ((event.toState as any) === true) {
      this.deletedChild = false;

      this.children.splice(this.deletingChildIndex, 1);
      this.childrenChange.emit(this.children);
    }
  }

  onDelete() {
    this.deleteEvent.emit();
  }

  onNameChange(event: any) {
    this.nameChange.emit(event);
  }

  onNotesChange(event: any) {
    this.notesChange.emit(event);
  }
}
