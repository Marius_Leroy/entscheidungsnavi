export function getPixelDeltaY(event: WheelEvent, lineHeight = 25, pageHeight = 150) {
  let pixelDeltaY = 120;
  switch (event.deltaMode) {
    case 0:
      pixelDeltaY = event.deltaY;
      break;
    case 1:
      pixelDeltaY = event.deltaY * lineHeight;
      break;
    case 2:
      pixelDeltaY = event.deltaY * pageHeight;
      break;
  }

  return pixelDeltaY;
}
