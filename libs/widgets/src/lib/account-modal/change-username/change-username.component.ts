import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '@entscheidungsnavi/api-client';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, filter, takeUntil } from 'rxjs';

@Component({
  selector: 'dt-change-username',
  templateUrl: 'change-username.component.html',
  styleUrls: ['./change-username.component.scss'],
})
export class ChangeUsernameComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  originalUsername: string;
  usernameForm = this.fb.group({
    username: this.fb.control<string>(null),
  });

  constructor(private authService: AuthService, private fb: FormBuilder) {
    this.authService.user$.pipe(filter(Boolean), takeUntil(this.onDestroy$)).subscribe(user => {
      this.originalUsername = user.name;
      if (this.usernameForm.value.username == null) {
        this.usernameForm.controls.username.setValue(this.originalUsername);
      }
    });
  }

  onSubmit() {
    this.usernameForm.updateValueAndValidity();

    if (this.usernameForm.valid) {
      // Replace empty username with null
      const username = this.usernameForm.controls.username.value || null;

      this.usernameForm.disable({ emitEvent: false });

      this.authService.changeUserName(username).subscribe({
        next: () => {
          this.usernameForm.enable();
          this.originalUsername = username;
        },
        error: () => {
          this.usernameForm.enable();

          this.usernameForm.setErrors({ 'server-error': true });
        },
      });
    }
  }
}
