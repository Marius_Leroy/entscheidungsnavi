import { Component, Input, OnChanges, SimpleChanges, ChangeDetectionStrategy, HostBinding } from '@angular/core';

@Component({
  selector: 'dt-triangle',
  templateUrl: './triangle.component.html',
  styleUrls: ['./triangle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TriangleComponent implements OnChanges {
  @Input()
  size: number; // In Pixel

  @Input()
  direction: 'left' | 'up' | 'right' | 'down' = 'left';

  svgString: string;
  h: number; // Triangle Height

  @HostBinding('style.width.px')
  width: number;

  @HostBinding('style.height.px')
  height: number;

  ngOnChanges(changes: SimpleChanges) {
    if ('size' in changes || 'direction' in changes) {
      this.calculate();
    }
  }

  private calculate() {
    // √3 / 2 * a
    this.h = 0.86602540378 * this.size;
    const halfSize = this.size / 2;

    let points: [number, number][] = [];

    switch (this.direction) {
      case 'left':
        points = [
          [0, halfSize],
          [this.h, 0],
          [this.h, this.size],
        ];
        break;
      case 'up':
        points = [
          [0, this.h],
          [halfSize, 0],
          [this.size, this.h],
        ];
        break;
      case 'right':
        points = [
          [0, 0],
          [this.h, halfSize],
          [0, this.size],
        ];
        break;
      case 'down':
        points = [
          [halfSize, this.h],
          [0, 0],
          [this.size, 0],
        ];
        break;
    }

    this.svgString =
      `M${points[0][0]} ${points[0][1]} L${points[1][0]} ${points[1][1]} ` +
      `L${points[2][0]} ${points[2][1]} L${points[0][0]} ${points[0][1]}`;

    if (this.direction === 'left' || this.direction === 'right') {
      this.width = this.h;
      this.height = this.size;
    } else {
      this.width = this.size;
      this.height = this.h;
    }
  }
}
