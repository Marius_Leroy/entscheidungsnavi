import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

/**
 * A version of dt-range-input with logarithmic scale.
 */
@Component({
  selector: 'dt-range-input-log',
  templateUrl: './range-input-log.component.html',
  styleUrls: ['./range-input-log.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RangeInputLogComponent {
  @Input() min: number;
  @Input() max: number;
  @Input() stepCount = 1000;

  @Input()
  set value(newValue: number) {
    if (newValue !== this.value) {
      this.sliderPos = this.inverseLogSlider(newValue);
    }
  }
  get value() {
    return this.sliderRoundFunction(this.logSlider(this.sliderPos));
  }

  @Output() valueChange = new EventEmitter<number>();

  // The internal linear slider position
  protected sliderPos = 450;

  protected get maxValueLength() {
    return Math.ceil(Math.log10(this.max));
  }

  private get scale() {
    return (Math.log(this.max) - Math.log(this.min)) / this.stepCount;
  }

  // map a value (position) from [minSteps, maxSteps] to [minVal, maxVal], exponentially distributed
  private logSlider(position: number) {
    return Math.exp(Math.log(this.min) + this.scale * position);
  }

  private inverseLogSlider(value: number) {
    return (Math.log(value) - Math.log(this.min)) / this.scale;
  }

  // round the value to one significant figure
  private sliderRoundFunction(value: number) {
    return Math.round(value / Math.pow(10, Math.floor(Math.log10(value)))) * Math.pow(10, Math.floor(Math.log10(value)));
  }

  protected onSliderValueChange() {
    this.valueChange.emit(this.value);
  }
}
