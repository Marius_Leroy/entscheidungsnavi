import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dt-aspect-box',
  templateUrl: './aspect-box.component.html',
  styleUrls: ['./aspect-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AspectBoxComponent {
  @Input() name: string;
  @Output() nameChange = new EventEmitter<string>();
  @Input() readonly = false;
  @Input() grayedOut = false;
  @Output() deleteClick = new EventEmitter<void>();

  isEditing = false;

  constructor(private cdRef: ChangeDetectorRef) {}

  edit() {
    this.isEditing = true;
    this.cdRef.markForCheck();
  }
}
