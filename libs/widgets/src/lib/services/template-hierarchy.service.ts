import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { Language, QuickstartHierarchyNode } from '@entscheidungsnavi/api-types';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data/steps';
import { ITree, Tree } from '@entscheidungsnavi/tools';

export class TemplateHierarchyElement extends ObjectiveElement {
  constructor(public id: string, name: string) {
    super(name);
  }
}

@Injectable({ providedIn: 'root' })
export class TemplateHierarchyService {
  private get language(): Language {
    return this.locale.includes('de') ? 'de' : 'en';
  }

  constructor(@Inject(LOCALE_ID) private locale: string) {}

  extractHierarchy(tree: ITree<QuickstartHierarchyNode>, language: Language = this.language): Tree<TemplateHierarchyElement> {
    return Tree.from({
      value: new TemplateHierarchyElement(tree.value.id, language === 'de' ? tree.value.name.de : tree.value.name.en),
      children: tree.children.map(child => this.extractHierarchy(child)),
    });
  }
}
