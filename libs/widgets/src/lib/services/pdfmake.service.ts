import { Injectable } from '@angular/core';
import { TCreatedPdf } from 'pdfmake/build/pdfmake';
import { TDocumentDefinitions } from 'pdfmake/interfaces';

/**
 * This service loads PDFMake on demand. This ensures it is not part of the main bundle.
 */
@Injectable({
  providedIn: 'root',
})
export class PdfmakeService {
  private _createPdf: (documentDefinitions: TDocumentDefinitions) => TCreatedPdf;

  private async loadPdfMake() {
    if (this._createPdf == null) {
      const pdfmake = await import('pdfmake/build/pdfmake');
      const vfsFonts = await import('pdfmake/build/vfs_fonts');
      this._createPdf = (documentDefinitions: TDocumentDefinitions) =>
        pdfmake.createPdf(documentDefinitions, undefined, undefined, vfsFonts.pdfMake.vfs);
    }
  }

  async createPdf(documentDefinitions: TDocumentDefinitions): Promise<TCreatedPdf> {
    await this.loadPdfMake();
    return this._createPdf(documentDefinitions);
  }
}
