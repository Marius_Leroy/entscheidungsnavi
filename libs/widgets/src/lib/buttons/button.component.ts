import { Component, HostBinding, Input, OnInit } from '@angular/core';

/*
 Usage:
 <dt-button (click)="foo()" [style]="'less'" [maxWidth]="true" [icon]="'clear'">Zustand hinzufügen</dt-button>
 */

export type ButtonColor = 'primary' | 'accent' | 'warn' | 'basic' | 'muted';

@Component({
  selector: 'dt-button',
  styleUrls: ['./button.component.scss'],
  templateUrl: './button.component.html',
})
export class ButtonComponent implements OnInit {
  @Input() type: 'basic' | 'raised' | 'raised-less' | 'stroked' | 'flat' = 'raised';
  @Input() noSubmit: boolean;
  @Input() disabled: boolean;
  @Input() icon: string;

  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('color')
  _color: ButtonColor = 'accent';
  get color() {
    return this._color === 'muted' ? 'accent' : this._color;
  }

  @Input() navigationIcon?: 'before' | 'next';
  @Input() iconPosition: 'left' | 'right' = 'right';
  @Input() @HostBinding('class.max-width') maxWidth = false;
  @Input() maxWidthWrapper = false; // Can be necessary to properly overflow the text inside the button
  @Input() minWidth = true; // default: 64px;
  @Input() defaultPadding = true; // default: 0 16px;
  @Input() @HostBinding('style.margin-left.px') @HostBinding('style.margin-right.px') marginLeftRight: number;
  @Input() @HostBinding('style.margin-top.px') @HostBinding('style.margin-bottom.px') marginTopBottom: number;
  @Input() margin: number;
  @Input() size: 'small' | 'normal' | 'large' = 'normal';
  @Input() noDisabledBg = false;
  @Input() @HostBinding('class.max-height') maxHeight = false;
  @Input() transparent = false;
  @Input() iconOnly = false;
  @Input() weightControlButton = false;
  @Input() transformUppercase = false;

  @HostBinding('class.small')
  get small() {
    return this.size === 'small';
  }

  @HostBinding('class.large')
  get large() {
    return this.size === 'large';
  }

  ngOnInit(): void {
    if (this.navigationIcon) {
      this.iconPosition = this.navigationIcon === 'before' ? 'left' : 'right';
    }

    if (this.margin > -1000) {
      this.marginTopBottom = this.margin;
      this.marginLeftRight = this.margin;
    }
  }
}
