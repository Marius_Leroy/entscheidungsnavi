import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { MatIconModule } from '@angular/material/icon';
import { ToggleButtonComponent } from './toggle-button/toggle-button.component';
import { ButtonComponent, ButtonTinyComponent } from '.';

@NgModule({
  imports: [CommonModule, MatButtonModule, MatIconModule, ButtonTinyComponent, MatTooltipModule],
  declarations: [ButtonComponent, ToggleButtonComponent],
  exports: [ButtonComponent, ButtonTinyComponent, ToggleButtonComponent],
})
export class ButtonModule {}
