import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'dt-passwordstrength',
  templateUrl: './password-strength.component.html',
  styleUrls: ['./password-strength.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PasswordStrengthComponent implements OnChanges, AfterViewInit {
  @Input()
  password: string;

  @Output()
  passwordValid = new EventEmitter<boolean>();

  passwordScore: number;

  zxcvbn: typeof import('zxcvbn');
  zxcvbnPromise: Promise<typeof import('zxcvbn')>;

  constructor(private cdRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.zxcvbnPromise = new Promise(resolve => {
      import('zxcvbn').then(imp => {
        resolve(imp.default);
      });
    });

    this.zxcvbnPromise.then(zxcvbn => {
      this.zxcvbn = zxcvbn;
    });
  }

  async ngOnChanges(changes: SimpleChanges) {
    if ('password' in changes) {
      await this.calculatePasswordScore();

      this.passwordValid.emit(this.passwordScore !== -1);
    }
  }

  private async calculatePasswordScore() {
    if (!this.password || this.password.length < 8) {
      this.passwordScore = -1;
      return;
    }

    const result = (await (this.zxcvbn ?? this.zxcvbnPromise))(this.password);

    this.passwordScore = result.score;
    this.cdRef.detectChanges();
  }
}
