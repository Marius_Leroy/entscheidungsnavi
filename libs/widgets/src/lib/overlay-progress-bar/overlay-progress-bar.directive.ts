import { ComponentRef, Directive, ElementRef, Input, OnInit, Renderer2, ViewContainerRef } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { MatProgressBar } from '@angular/material/progress-bar';
import { BehaviorSubject, delay, distinctUntilChanged, Observable, of, switchMap } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[dtOverlayProgress]',
  standalone: true,
})
export class OverlayProgressBarDirective implements OnInit {
  private visible$ = new BehaviorSubject<boolean>(false);

  @OnDestroyObservable()
  onDestroy$: Observable<void>;

  @Input('dtOverlayProgressColor')
  color: ThemePalette = 'primary';

  componentRef: ComponentRef<MatProgressBar>;

  constructor(private viewContainer: ViewContainerRef, private elementRef: ElementRef, private render2: Renderer2) {}

  @Input('dtOverlayProgress')
  set visible(newValue: boolean) {
    this.visible$.next(newValue);
  }

  ngOnInit() {
    this.render2.setStyle(this.elementRef.nativeElement, 'position', 'relative');

    // Instantiate Material Progressbar and add it to parent view
    this.componentRef = this.viewContainer.createComponent(MatProgressBar);
    this.componentRef.instance.mode = 'indeterminate';
    this.componentRef.instance.color = this.color;

    // Get HTML Element of the Progress Bar
    const htmlElement = this.componentRef.location.nativeElement as HTMLElement;

    // Apply Styles
    this.render2.setStyle(htmlElement, 'position', 'absolute');
    this.render2.setStyle(htmlElement, 'display', 'none');
    this.render2.setStyle(htmlElement, 'left', '0');
    this.render2.setStyle(htmlElement, 'right', '0');
    this.render2.setStyle(htmlElement, 'bottom', '0');

    // Move Progressbar INSIDE element the directive is attached to
    this.render2.appendChild(this.elementRef.nativeElement, htmlElement);

    this.visible$
      .pipe(
        distinctUntilChanged(),
        switchMap(visible => of(visible).pipe(delay(visible ? 500 : 0))),
        takeUntil(this.onDestroy$)
      )
      .subscribe(visible => {
        if (this.componentRef) {
          const htmlElement = this.componentRef.location.nativeElement as HTMLElement;
          this.render2.setStyle(htmlElement, 'display', visible ? 'block' : 'none');
        }
      });
  }
}
