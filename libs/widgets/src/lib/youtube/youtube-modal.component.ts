import { Component, Input, ViewChild } from '@angular/core';
import { ModalComponent } from '../modal';

@Component({
  selector: 'dt-youtube-modal',
  templateUrl: './youtube-modal.component.html',
  styleUrls: ['./youtube-modal.component.scss'],
})
export class YoutubeModalComponent {
  consent = false;
  @Input() videoId: string;
  @Input() modalTitle: string;
  @ViewChild(ModalComponent) modal: ModalComponent;

  open() {
    this.modal.open();
  }
}
