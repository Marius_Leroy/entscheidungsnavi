import { AfterContentInit, Component, ContentChildren, Input, QueryList } from '@angular/core';
import { findIndex } from 'lodash';
import { TabComponent } from './tab.component';

@Component({
  selector: 'dt-tabs',
  styleUrls: ['./tabs.component.scss'],
  template: `
    <div class="dt-tabs">
      <div
        *ngFor="let tab of tabs"
        (click)="!disabled && _selectTab(tab)"
        [class.underline]="underlineInactiveTabs"
        [class.active]="tab.active"
        class="dt-tab"
        [class.disabled]="disabled"
        [matTooltip]="tab.title"
        [matTooltipShowDelay]="500"
        >{{ tab.title }}
      </div>
    </div>
    <ng-content select="dt-tab"></ng-content>
    <ng-content select="div.tabs-footer"></ng-content>
  `,
})
export class TabsComponent implements AfterContentInit {
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  @Input() disabled = false;
  @Input() underlineInactiveTabs = false;
  @Input() autoSelect = true; // Whether clicking on a tab should select it automatically

  private get activeTabs() {
    return this.tabs.filter(tab => tab.active);
  }

  ngAfterContentInit() {
    if (this.activeTabs.length === 0 && this.tabs.length > 0) {
      setTimeout(() => this.selectTab(this.tabs.first));
    }
  }

  _selectTab(selectedTab: TabComponent) {
    selectedTab.tabClick.emit();
    if (this.autoSelect) {
      this.selectTab(selectedTab);
    }
  }

  selectTab(selectedTab: TabComponent): void {
    this.activeTabs.forEach(tab => tab.deactivate());
    selectedTab.activate();
  }

  selectNextTab() {
    this.selectTab(this.getNextTab());
  }

  selectPreviousTab() {
    this.selectTab(this.getPreviousTab());
  }

  private getNextTab(): TabComponent {
    const arr = this.tabs.toArray();
    const activeIndex = findIndex(arr, tab => tab.active);
    if (activeIndex >= 0 && activeIndex < this.tabs.length - 1) {
      return arr[activeIndex + 1];
    } else {
      return undefined;
    }
  }

  private getPreviousTab(): TabComponent {
    const arr = this.tabs.toArray();
    const activeIndex = findIndex(arr, tab => tab.active);
    if (activeIndex > 0) {
      return arr[activeIndex - 1];
    } else {
      return undefined;
    }
  }

  firstIsActive(): boolean {
    return this.tabs && this.tabs.first.active;
  }

  lastIsActive(): boolean {
    return (this.tabs && this.tabs.last.active) || false;
  }
}
