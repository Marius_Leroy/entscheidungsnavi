import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { Subject } from 'rxjs';

@Injectable()
export class PaginatorIntlService implements MatPaginatorIntl {
  changes = new Subject<void>();

  firstPageLabel = $localize`Erste Seite`;
  itemsPerPageLabel = $localize`Elemente pro Seite:`;
  lastPageLabel = $localize`Letzte Seite`;

  nextPageLabel = $localize`Nächste Seite`;
  previousPageLabel = $localize`Vorherige Seite`;

  getRangeLabel(page: number, pageSize: number, length: number): string {
    if (length == 0 || pageSize == 0) {
      return $localize`0 von ${length}`;
    }

    const pageStart = Math.min(length, page * pageSize + 1);
    const pageEnd = Math.min(length - page * pageSize, pageSize);
    return $localize`${pageStart} – ${pageEnd} von ${length}`;
  }
}
