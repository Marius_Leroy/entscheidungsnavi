import { ConnectedPosition } from '@angular/cdk/overlay';
import { Directive, ElementRef, HostListener, Input, OnDestroy, TemplateRef } from '@angular/core';
import { fromEvent, Subject, takeUntil, timer } from 'rxjs';
import { PopOverRef, PopOverService, ScrollStrategy } from './popover.service';

export type SimplePosition = 'bottom' | 'right' | 'left' | 'top';

@Directive({
  selector: '[dtHoverPopover]',
  standalone: true,
})
export class HoverPopOverDirective implements OnDestroy {
  @Input('dtHoverPopover')
  popoverTemplate: TemplateRef<any>;

  @Input() dtHoverPopoverPosition: SimplePosition = 'bottom';

  @Input() dtHoverPopoverScrollStrategy: ScrollStrategy = 'noop';

  @Input() dtHoverPopoverDelay = 0;

  @Input()
  dtHoverPopoverDisabled = false;

  @Input()
  dtHoverPopoverDisableAnimation = false;

  @Input()
  dtHoverPopoverDisableClick = false;

  @Input()
  dtHoverPopoverClickthrough = false;

  private popOverRef: PopOverRef = null;

  constructor(private popOverService: PopOverService, private hostRef: ElementRef<any>) {}

  @HostListener('pointerenter', ['$event'])
  private open(event: PointerEvent) {
    if (!event === null && event.pointerType === 'touch') {
      return;
    }

    if (!this.dtHoverPopoverDisabled) {
      if (this.popOverRef === null) {
        // Use bottom placement as fallback,
        // even when right position is selected. On smaller screens, right positioning may not be possible.
        const position: ConnectedPosition[] = [
          { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' },
          { originX: 'start', originY: 'top', overlayX: 'start', overlayY: 'bottom' },
        ];

        if (this.dtHoverPopoverPosition === 'right') {
          position.splice(
            0,
            0,
            { originX: 'end', originY: 'center', overlayX: 'start', overlayY: 'center' },
            { originX: 'start', originY: 'center', overlayX: 'end', overlayY: 'center' }
          );
        } else if (this.dtHoverPopoverPosition === 'left') {
          position.splice(
            0,
            0,
            { originX: 'start', originY: 'center', overlayX: 'end', overlayY: 'center' },
            { originX: 'end', originY: 'center', overlayX: 'start', overlayY: 'center' }
          );
        } else if (this.dtHoverPopoverPosition === 'top') {
          position.splice(
            0,
            0,
            { originX: 'start', originY: 'top', overlayX: 'start', overlayY: 'bottom' },
            { originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }
          );
        }

        const closeSubject = new Subject<void>();

        closeSubject.subscribe(() => {
          this.close();
        });

        timer(this.dtHoverPopoverDelay)
          .pipe(takeUntil(closeSubject))
          .subscribe(() => {
            if (this.popOverRef === null) {
              this.popOverRef = this.popOverService.open(this.popoverTemplate, this.hostRef, {
                hasBackdrop: false,
                panelClass: 'dt-hover-popover-panel',
                position,
                disableAnimation: this.dtHoverPopoverDisableAnimation,
                scrollStrategy: this.dtHoverPopoverScrollStrategy,
              });

              if (this.dtHoverPopoverClickthrough) {
                this.popOverRef.overlayRef.overlayElement.style.pointerEvents = 'none';
              }
            }
          });

        fromEvent(document, 'mouseover')
          .pipe(takeUntil(closeSubject))
          .subscribe(event => {
            if (
              !event
                .composedPath()
                .some(element => element === this.popOverRef?.overlayRef.overlayElement || element === this.hostRef.nativeElement)
            ) {
              closeSubject.next();
              closeSubject.complete();
            }
          });

        // Close popover on touch scroll anywhere on the screen
        fromEvent(document, 'touchmove')
          .pipe(takeUntil(closeSubject))
          .subscribe(event => {
            if (event) {
              closeSubject.next();
              closeSubject.complete();
            }
          });
      }
    }
  }

  private close() {
    this.popOverRef?.close();
    this.popOverRef = null;
  }

  @HostListener('click', ['$event'])
  private openClick(event: MouseEvent) {
    if (this.dtHoverPopoverDisableClick) {
      return;
    }

    if (this.popOverRef === null) {
      this.popOverRef = this.popOverService.open(this.popoverTemplate, this.hostRef, {
        closeCallback: () => (this.popOverRef = null),
      });
    }
    event.stopImmediatePropagation();
  }

  ngOnDestroy() {
    this.popOverRef?.close();
    this.popOverRef = null;
  }
}
