import { Directive, ElementRef, EventEmitter, HostListener, Input, Output, inject } from '@angular/core';
import { PopOverService } from '../popover.service';

@Directive({ selector: '[dtConfirmPopover]', standalone: true })
export class ConfirmPopoverDirective {
  popoverService = inject(PopOverService);
  elementRef: ElementRef<HTMLElement> = inject(ElementRef);

  @Input('dtConfirmPopover') prompt: string;
  @Output('dtConfirmPopoverConfirm') confirm = new EventEmitter<void>();
  @Input('dtConfirmPopoverConfirmText') confirmText: string;
  @Input() disabled = false;

  @HostListener('click')
  async onClick() {
    if (this.disabled) return;

    if (await this.popoverService.confirm(this.elementRef, this.prompt, { confirmText: this.confirmText })) {
      this.confirm.emit();
    }
  }
}
