import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatLegacyButtonModule } from '@angular/material/legacy-button';

@Component({
  template: `
    <div class="prompt">{{ prompt }}</div>
    <div>
      <button mat-button color="warn" data-cy="confirm-popover-confirm-button" (click)="confirmClick.emit()">{{ confirmText }}</button>
      <button mat-button (click)="discardClick.emit()">{{ discardText }}</button>
    </div>
  `,
  styles: [
    `
      @use '@angular/material' as mat;

      :host {
        @include mat.elevation(8);

        display: block;
        border-radius: 5px;
        padding: 8px 8px 0;
        margin: 8px;
        width: 275px;

        background-color: #404040;
        color: white;

        text-align: center;
      }

      .prompt {
        padding: 0 4px;
      }
    `,
  ],
  standalone: true,
  imports: [CommonModule, MatLegacyButtonModule],
})
export class ConfirmPopOverComponent {
  @Input() prompt: string;
  @Input() confirmText = $localize`Löschen`;
  @Input() discardText = $localize`Abbrechen`;

  @Output() confirmClick = new EventEmitter<void>();
  @Output() discardClick = new EventEmitter<void>();
}
