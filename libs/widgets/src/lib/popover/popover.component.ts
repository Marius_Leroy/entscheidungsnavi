import { Input, TemplateRef, Component, HostBinding, Output, HostListener, EventEmitter, ViewChild } from '@angular/core';
import { trigger, transition, style, animate, animateChild, AnimationEvent } from '@angular/animations';
import { CdkPortalOutlet } from '@angular/cdk/portal';

@Component({
  selector: 'dt-popover',
  template: `<ng-container *ngTemplateOutlet="templateRef; context: templateContext"></ng-container>
    <ng-template [cdkPortalOutlet]></ng-template>`,
  styles: [``],
  animations: [
    trigger('appear', [
      transition(':enter', [
        style({ opacity: 0, transform: 'scale(0.7)' }),
        animate('150ms cubic-bezier(0, 0, 0.2, 1)', style({ transform: 'none', opacity: 1 })),
        animateChild(),
      ]),
      transition(':leave', [
        style({ transform: 'none' }),
        animate('150ms cubic-bezier(0.4, 0.0, 0.2, 1)', style({ opacity: 0 })),
        animateChild(),
      ]),
    ]),
  ],
})
export class PopOverComponent {
  @Input()
  templateRef: TemplateRef<any>;

  @Input()
  @HostBinding('@.disabled')
  disableAnimation = false;

  @HostBinding('@appear') public appear = true;

  @Output()
  finishedLeaveAnimation = new EventEmitter();

  @Output()
  popoverClose = new EventEmitter();

  @ViewChild(CdkPortalOutlet, { static: true })
  portalOutlet: CdkPortalOutlet;

  get templateContext() {
    return {
      close: () => this.popoverClose.emit(),
    };
  }

  @HostListener('@appear.done', ['$event'])
  animationDone(event: AnimationEvent) {
    if (event.toState === 'void') {
      this.finishedLeaveAnimation.emit();
    }
  }
}
