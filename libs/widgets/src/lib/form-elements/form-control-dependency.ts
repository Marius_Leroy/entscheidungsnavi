import { AbstractControl, FormControl } from '@angular/forms';
import { identity } from 'lodash';
import { Observable, merge, startWith, takeUntil } from 'rxjs';

/**
 * A special version of {@link createControlDependencyWith} that works with boolean FormControls.
 *
 * Ensures that {@link dependentControl} is enable if and only if {@link requiredControl} is enabled and its value is true. *
 *
 * @param requiredControl - The control whose value should control whether {@link dependentControl} is enabled.
 * @param dependentControl - The control whose enabled state should be controlled
 * @param controlDestroyed$ - An observable that emits when any of the two controls is destroyed
 */
export function createControlDependency(
  requiredControl: FormControl<boolean>,
  dependentControl: AbstractControl<unknown>,
  controlDestroyed$: Observable<void>
) {
  return createControlDependencyWith(requiredControl, dependentControl, identity, controlDestroyed$);
}

/**
 * Ensures that {@link dependentControl} is enabled if and only if {@link requiredControl} is enabled and its value satisfies
 * {@link predicate}.
 *
 * @param requiredControl - The control whose value should control whether {@link dependentControl} is enabled.
 * @param dependentControl - The control whose enabled state should be controlled
 * @param predicate - A function that maps the value of {@link requiredControl} to a boolean indicating enabled/disabled.
 * @param controlDestroyed$ - An observable that emits when any of the two controls is destroyed
 */
export function createControlDependencyWith<T>(
  requiredControl: FormControl<T>,
  dependentControl: AbstractControl<unknown>,
  predicate: (value: T) => boolean,
  controlDestroyed$: Observable<void>
) {
  merge(requiredControl.valueChanges, requiredControl.statusChanges, dependentControl.statusChanges)
    .pipe(startWith(null), takeUntil(controlDestroyed$))
    .subscribe(() => {
      const predicateValue = predicate(requiredControl.value);
      const shouldBeEnabled = predicateValue && requiredControl.enabled;

      if (shouldBeEnabled !== dependentControl.enabled) {
        if (shouldBeEnabled) dependentControl.enable();
        else dependentControl.disable();
      }
    });
}
