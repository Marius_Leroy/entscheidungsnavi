import { AbstractControl, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import { pick } from 'lodash';
import { Observable, distinctUntilChanged, map, takeUntil } from 'rxjs';

/**
 * Validates that the value of FormControl matches that of another FormControl
 *
 * @param otherControl - The control whose value this control should match
 * @param controlDestroyed$ - An observable that emits whenever one of the two controls is destroyed
 * @returns A validator
 */
export function matchOtherValidator<T>(otherControl: FormControl<T>, controlDestroyed$: Observable<void>): ValidatorFn {
  let isSubscribed = false;

  return (control: FormControl<T>) => {
    if (!isSubscribed) {
      otherControl.valueChanges.pipe(takeUntil(controlDestroyed$)).subscribe(() => {
        control.updateValueAndValidity();
      });
      isSubscribed = true;
    }

    if (otherControl.value !== control.value) {
      return {
        matchOther: true,
      };
    }

    return null;
  };
}

/**
 * An Angular Form ValidatorFn that ensures that the string is a valid RegExp.
 */
export const regexValidator: ValidatorFn = (control: FormControl<string>) => {
  try {
    new RegExp(control.value);
    return null;
  } catch {
    return { regex: true };
  }
};

/**
 * A validator that duplicates a certain error from the parent to the child.
 *
 * Using this validator, cross form field errors can be shown directly on one or more of the fields.
 *
 * @param errorName - The name of the error that should be mirrored onto this observable
 * @param controlDestroyed$ - An observable that emits whenever the observable or its parent are destroyed
 * @returns A form validator
 *
 * @example
 * ```
 * this.fb.group(
 *   {
 *     min: [entry.min],
 *     max: [entry.max, matchParentError('minMaxCollapsed', this.onDestroy$)]
 *   },
 *   {
 *     validators: (group: FormGroup<{ min: FormControl<number>; max: FormControl<number> }>) =>
 *      group.value.min != null && group.value.max != null && group.value.min > group.value.max ? { minMaxCollapsed: true } : null,
 *   }
 * );
 * ```
 */
export function matchParentError(errorName: string, controlDestroyed$: Observable<void>) {
  let subscribedToParent = false;

  return (control: AbstractControl<unknown>) => {
    if (!control.parent) return null;

    if (!subscribedToParent) {
      control.parent.valueChanges
        .pipe(
          map(() => control.parent.hasError(errorName)),
          distinctUntilChanged(),
          takeUntil(controlDestroyed$)
        )
        .subscribe(hasError => {
          if (hasError && control.untouched) control.markAsTouched();
          control.updateValueAndValidity();
        });
      subscribedToParent = true;
    }

    if (control.parent.hasError(errorName)) {
      return pick(control.parent.errors, errorName);
    }

    return null;
  };
}

/**
 * A validator function for a form array that requires at least one child to have a truthy value.
 */
export const atLeastOneValidator: ValidatorFn = (control: FormArray<FormControl<any>>) => {
  const atLeastOne = control?.controls.some(control => !!control.value);
  return atLeastOne ? null : { atLeastOne: true };
};

/**
 * A validator function that returns an error when the value matches a value in the given array.
 */
export function notOneOfValidator(array: string[], caseSensitive: boolean): ValidatorFn {
  if (!caseSensitive) {
    array = array.map(entry => entry.toLowerCase());
  }

  return (control: FormControl<string>) => {
    const contained = array.includes(caseSensitive ? control.value : control.value.toLowerCase());

    if (contained) {
      return { notOneOf: true };
    } else {
      return null;
    }
  };
}
