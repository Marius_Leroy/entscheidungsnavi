import { Directive, EventEmitter, OnInit, Output, inject } from '@angular/core';
import { isEqual } from 'lodash';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert_unreachable';
import { LocationSequence, LocationSequenceMap, OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { PlatformDetectService } from '../services/platform-detect.service';
import { HierarchyAction, HierarchyInterfaceDirective } from './interface.directive';

/**
 * This directive handles the focused and collapsed state of hierarchy nodes.
 */
@Directive({ standalone: true })
export class HierarchyNodeStateDirective<T> implements OnInit {
  @Output() focusChange = new EventEmitter<LocationSequence[]>();

  // All nodes that are currently focused
  focused: LocationSequence[] = [];
  // All nodes that are currently collapsed
  collapsed: LocationSequence[] = [];

  // Encodes a bias towards which child we navigate using the arrow keys
  private navigationBias = new LocationSequenceMap<number>();

  private platformDetectService = inject(PlatformDetectService);
  private interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  ngOnInit() {
    // Set focus to the newly inserted node on insert
    this.interface.action.pipe(takeUntil(this.onDestroy$)).subscribe(action => this.processAction(action));
  }

  private processAction(action: HierarchyAction) {
    if (action.type === 'insert') {
      this.setFocus(action.location);
    } else if (action.type === 'compound') {
      action.actions.forEach(action => this.processAction(action));
    }
  }

  clearFocus() {
    this.focused = [];
    this.focusChange.emit([]);
  }

  isFocused(node: LocationSequence) {
    return !!this.focused.find(element => isEqual(element, node));
  }

  nodeClick(event: MouseEvent, node: LocationSequence) {
    const commandKeyPressed = this.platformDetectService.macOS && event.metaKey;

    if (!this.isFocused(node)) {
      if (!event.ctrlKey && !event.shiftKey && !commandKeyPressed) {
        this.focused = [];
      }

      const lastSelectedElement = this.focused.at(-1);
      const isRangeSelection = event.shiftKey && this.focused.length > 0 && lastSelectedElement.isSiblingOf(node);

      if (isRangeSelection) {
        // Handle range selection:
        // Make sure all elements between the last selected element and the current selected element are selected

        const parent = node.getParent();
        const fromSiblingIndex = lastSelectedElement.value.at(-1);
        const toSiblingIndex = node.value.at(-1);

        for (let sibling = Math.min(fromSiblingIndex, toSiblingIndex); sibling <= Math.max(fromSiblingIndex, toSiblingIndex); sibling++) {
          const seq = new LocationSequence([...parent.value, sibling]);
          // Avoid double-pushing.
          if (!this.isFocused(seq)) {
            this.focused.push(seq);
          }
        }
      } else {
        // Handle single selection.
        this.focused.push(node);
      }
      this.focusChange.emit(this.focused);
    } else if (commandKeyPressed || event.ctrlKey) {
      // Handle deselection:
      // On macOS we can deselect with the command key.
      // On Windows (and other OSes) we can deselect with the control key.
      this.focused = this.focused.filter(element => !isEqual(element, node));
      this.focusChange.emit(this.focused);
    }
  }

  unfocusAllChildren(node: LocationSequence) {
    this.focused = this.focused.filter(s => !s.isDescendantOf(node));
    this.focusChange.emit(this.focused);
  }

  navigate(direction: 'up' | 'down' | 'left' | 'right') {
    if (this.focused.length !== 1) {
      return;
    }

    const selectedElement = this.focused[0];
    const selectedTree = this.interface.tree.getNode(selectedElement);

    let newSelectedElement: LocationSequence;

    switch (direction) {
      case 'left':
        if (!selectedElement.isRoot()) {
          newSelectedElement = selectedElement.getParent();
          this.navigationBias.set(newSelectedElement, selectedElement.value.at(-1));
        }
        break;
      case 'right':
        if (selectedTree.children && selectedTree.children.length > 0 && !this.isCollapsed(selectedElement)) {
          const bias = this.navigationBias.get(selectedElement);
          let childIndex = 0;
          if (bias != null) {
            if (bias < selectedTree.children.length) {
              childIndex = bias;
            }
            this.navigationBias.clear(selectedElement);
          }

          newSelectedElement = new LocationSequence([...selectedElement.value, childIndex]);
        }
        break;
      case 'up':
        newSelectedElement = this.interface.tree.findPreviousNode(selectedElement);
        break;
      case 'down':
        newSelectedElement = this.interface.tree.findNextNode(selectedElement);
        break;
      default:
        assertUnreachable(direction);
    }

    if (newSelectedElement) {
      this.setFocus(newSelectedElement);
    }
  }

  setFocus(node: LocationSequence) {
    this.focused = [node];
    this.focusChange.emit(this.focused);
  }

  collapseNode(node: LocationSequence) {
    this.collapsed.push(node);
    this.unfocusAllChildren(node);
  }

  expandNode(node: LocationSequence) {
    this.collapsed = this.collapsed.filter(element => !isEqual(element, node));
  }

  isCollapsed(node: LocationSequence) {
    return !!this.collapsed.find(element => isEqual(element, node));
  }
}
