import {
  AfterViewInit,
  ApplicationRef,
  ComponentRef,
  Directive,
  ElementRef,
  EnvironmentInjector,
  HostListener,
  Injector,
  NgZone,
  OnDestroy,
  Renderer2,
  createComponent,
  inject,
} from '@angular/core';
import { LocationSequence } from '@entscheidungsnavi/tools';
import { HierarchyInterfaceDirective } from '../interface.directive';
import { HierarchyNodeStateDirective } from '../node-state.directive';
import { HierarchyDragImageComponent } from './drag-image.component';
import { HierarchyNodeComponent } from './hierarchy-node.component';

/**
 * This directive handles dragging (NOT dropping) of a hierarchy node.
 */
@Directive({ selector: '[dtHierarchyNodeDrag]', standalone: true })
export class HierarchyNodeDragDirective<T> implements AfterViewInit, OnDestroy {
  private nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);
  private interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);
  private node = inject<HierarchyNodeComponent<T>>(HierarchyNodeComponent);
  private elementRef = inject<ElementRef<HTMLElement>>(ElementRef);

  private environmentInjector = inject(EnvironmentInjector);
  private elementInjector = inject(Injector);
  private appRef = inject(ApplicationRef);
  private renderer = inject(Renderer2);
  private zone = inject(NgZone);

  private dragElementComponent: ComponentRef<HierarchyDragImageComponent<T>>;

  ngAfterViewInit() {
    this.setDraggable(true);

    this.zone.runOutsideAngular(() => {
      // Bind to all inputs elements within us and disable dragging when they are focused.
      // Otherwise, the cursor position cannot be changed using the mouse.
      for (const input of this.elementRef.nativeElement.getElementsByTagName('input')) {
        input.addEventListener('mousedown', () => this.setDraggable(false));
        input.addEventListener('mouseup', () => this.setDraggable(true));
      }
    });
  }

  private setDraggable(state: boolean) {
    if (state) {
      this.renderer.setAttribute(this.elementRef.nativeElement, 'draggable', 'true');
    } else {
      this.renderer.removeAttribute(this.elementRef.nativeElement, 'draggable');
    }
  }

  ngOnDestroy() {
    this.destroyDragImage();
  }

  @HostListener('dragstart', ['$event'])
  nodeDragStart(event: DragEvent) {
    if (this.node.locationSequence.isRoot() || this.interface.readonly) {
      event.preventDefault();
      return;
    }

    if (!this.nodeState.isFocused(this.node.locationSequence)) {
      this.nodeState.setFocus(this.node.locationSequence);
    }

    this.createDragImage(this.nodeState.focused);
    event.dataTransfer.setDragImage(this.dragElementComponent.location.nativeElement, event.offsetX, event.offsetY);
  }

  private createDragImage(nodes: LocationSequence[]) {
    this.destroyDragImage();

    this.dragElementComponent = createComponent(HierarchyDragImageComponent<T>, {
      environmentInjector: this.environmentInjector,
      elementInjector: this.elementInjector,
    });
    this.dragElementComponent.setInput('nodes', nodes);

    this.renderer.setStyle(this.dragElementComponent.location.nativeElement, 'position', 'absolute');
    this.renderer.setStyle(this.dragElementComponent.location.nativeElement, 'top', '-500px');
    document.body.appendChild(this.dragElementComponent.location.nativeElement);
    this.appRef.attachView(this.dragElementComponent.hostView);
  }

  private destroyDragImage() {
    if (!this.dragElementComponent) return;

    this.appRef.detachView(this.dragElementComponent.hostView);
    document.body.removeChild(this.dragElementComponent.location.nativeElement);
    this.dragElementComponent.destroy();
    this.dragElementComponent = null;
  }
}
