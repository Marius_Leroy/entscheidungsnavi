import { Component, ElementRef, Input, OnInit, ViewChild, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { Observable, takeUntil } from 'rxjs';
import { LocationSequence, OnDestroyObservable } from '@entscheidungsnavi/tools';
import { isEqual } from 'lodash';
import { HierarchyNodeStateDirective } from '../node-state.directive';
import { HierarchyInterfaceDirective } from '../interface.directive';
import { HierarchyDragImageComponent } from './drag-image.component';
import { HierarchyNodeDragDirective } from './drag.directive';
import { HierarchyNodeDropDirective } from './drop.directive';

@Component({
  selector: 'dt-hierarchy-node',
  standalone: true,
  imports: [CommonModule, MatIconModule, HierarchyDragImageComponent, HierarchyNodeDragDirective, HierarchyNodeDropDirective],
  templateUrl: './hierarchy-node.component.html',
  styleUrls: ['./hierarchy-node.component.scss'],
})
export class HierarchyNodeComponent<T> implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() locationSequence: LocationSequence;

  addButtonsOpen = false;

  nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);
  interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);

  @ViewChild('nodeWrapper') nodeWrapper: ElementRef<HTMLDivElement>;

  ngOnInit() {
    this.nodeState.focusChange.pipe(takeUntil(this.onDestroy$)).subscribe(newFocus => {
      if (newFocus.length !== 1 || !isEqual(newFocus[0], this.locationSequence)) {
        this.addButtonsOpen = false;
      }
    });
  }

  add(event: MouseEvent, where: 'above' | 'below' | 'right' | 'left') {
    event.stopImmediatePropagation();
    this.interface.insert(where, this.locationSequence);
    this.addButtonsOpen = false;
  }

  delete() {
    this.interface.delete(this.locationSequence);
  }
}
