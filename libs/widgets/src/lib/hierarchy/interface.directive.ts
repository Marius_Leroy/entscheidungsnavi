import { ContentChild, Directive, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { LocationSequence, Tree } from '@entscheidungsnavi/tools';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert_unreachable';

interface Insert {
  type: 'insert';
  location: LocationSequence;
}
interface Move {
  type: 'move';
  from: LocationSequence;
  to: LocationSequence;
}
interface Delete {
  type: 'delete';
  location: LocationSequence;
}
interface CompoundAction {
  type: 'compound';
  actions: HierarchyAction[];
}

export type HierarchyAction = Insert | Move | Delete | CompoundAction;

/**
 * This directive constitutes the outside interface of the hierarchy.
 */
@Directive({ standalone: true })
export class HierarchyInterfaceDirective<T> {
  @Input() tree: Tree<T>;

  @Input() readonly = false;
  @Output() action = new EventEmitter<HierarchyAction>();

  @ContentChild('node')
  nodeTemplate: TemplateRef<{ $implicit: T; locationSequence: LocationSequence; focused: boolean }>;

  insert(where: 'above' | 'below' | 'right' | 'left', loc: LocationSequence) {
    switch (where) {
      case 'above':
        this.action.emit({ type: 'insert', location: loc });
        break;
      case 'below':
        this.action.emit({ type: 'insert', location: new LocationSequence([...loc.value.slice(0, -1), loc.value.at(-1) + 1]) });
        break;
      case 'right':
        this.action.emit({ type: 'insert', location: new LocationSequence([...loc.value, 0]) });
        break;
      case 'left':
        this.action.emit({
          type: 'compound',
          actions: [
            { type: 'insert', location: loc },
            {
              type: 'move',
              from: new LocationSequence([...loc.value.slice(0, -1), loc.value.at(-1) + 1]),
              to: new LocationSequence([...loc.value, 0]),
            },
          ],
        });
        break;
      default:
        assertUnreachable(where);
    }
  }

  delete(loc: LocationSequence) {
    this.action.emit({ type: 'delete', location: loc });
  }

  group(locs: LocationSequence[]) {
    // There must be at least two nodes and they may not be the root
    if (locs.length < 2 || !locs[0].isRoot()) return false;

    // All the nodes must be siblings
    for (let i = 1; i < locs.length; i++) {
      if (!locs[0].isSiblingOf(locs[i])) return false;
    }

    const parentLoc = locs[0].getParent();
    // Child indices sorted descending
    const childIndices = locs.map(loc => loc.value.at(-1)).sort((a, b) => b - a);

    // The new node will be inserted before all other existing nodes
    const newGroupedNodeLoc = new LocationSequence([...parentLoc.value, childIndices.at(-1)]);

    this.action.emit({
      type: 'compound',
      actions: [
        // Create a new node before all nodes to be grouped
        { type: 'insert', location: newGroupedNodeLoc },
        // Move all the children behind that node
        ...childIndices.map(
          childIndex =>
            ({
              type: 'move',
              from: new LocationSequence([...parentLoc.value, childIndex + 1]),
              to: new LocationSequence([...newGroupedNodeLoc.value, 0]),
            } satisfies HierarchyAction)
        ),
      ],
    });

    return true;
  }

  move(from: LocationSequence[], to: LocationSequence) {
    const moveActions: Move[] = [];

    // Sort by depth in the tree descending
    from.sort((a, b) => b.value.length - a.value.length);

    from.forEach((fromLoc, fromIndex) => {
      to = to.adjustForRemovalOf(fromLoc);

      moveActions.push({
        type: 'move',
        from: fromLoc,
        to,
      });

      // Adjust all remaining Locs for this action
      for (let a = fromIndex + 1; a < from.length; a++) {
        from[a] = from[a].adjustForRemovalOf(fromLoc).adjustForInsertionOf(to);
      }

      to = to.adjustForInsertionOf(to);
    });

    this.action.emit({
      type: 'compound',
      actions: moveActions,
    });
  }
}
