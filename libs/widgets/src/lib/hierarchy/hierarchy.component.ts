import { CommonModule } from '@angular/common';
import { Component, ElementRef, QueryList, ViewChild, ViewChildren, inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { LocationSequence } from '@entscheidungsnavi/tools';
import { isEqual } from 'lodash';
import { PinchZoomDirective } from '../directives';
import { KeyBindHandlerDirective } from '../directives/keybind-handler.directive';
import { HierarchyNodeStateDirective } from './node-state.directive';
import { HierarchyInterfaceDirective } from './interface.directive';
import { HierarchyKeybindsDirective } from './keybinds.directive';
import { HierarchyMovementDirective } from './movement.directive';
import { HierarchyNodeComponent } from './node/hierarchy-node.component';

@Component({
  selector: 'dt-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss'],
  imports: [
    CommonModule,
    PinchZoomDirective,
    MatIconModule,
    HierarchyNodeStateDirective,
    HierarchyInterfaceDirective,
    HierarchyKeybindsDirective,
    KeyBindHandlerDirective,
    HierarchyNodeComponent,
    HierarchyMovementDirective,
  ],
  hostDirectives: [
    {
      directive: HierarchyInterfaceDirective,
      inputs: ['tree', 'readonly'],
      // eslint-disable-next-line @angular-eslint/no-outputs-metadata-property
      outputs: ['action'],
    },
    {
      directive: HierarchyNodeStateDirective,
      // eslint-disable-next-line @angular-eslint/no-outputs-metadata-property
      outputs: ['focusChange'],
    },
    KeyBindHandlerDirective,
    HierarchyKeybindsDirective,
    HierarchyMovementDirective,
  ],
  standalone: true,
})
export class HierarchyComponent<T> {
  @ViewChild('mindMap', { static: true })
  mindMap: ElementRef<HTMLDivElement>;

  @ViewChild('moveContainer', { static: true })
  moveContainer: ElementRef<HTMLDivElement>;

  @ViewChildren(HierarchyNodeComponent)
  nodes: QueryList<HierarchyNodeComponent<T>>;

  nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);
  protected interface = inject<HierarchyInterfaceDirective<T>>(HierarchyInterfaceDirective);

  protected readonly rootLocationSequence = new LocationSequence([]);

  getNodeElement(node: LocationSequence) {
    return this.nodes.find(item => isEqual(item.locationSequence, node));
  }
}
