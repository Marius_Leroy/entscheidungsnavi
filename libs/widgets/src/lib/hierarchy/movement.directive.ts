import { AfterViewInit, ChangeDetectorRef, Directive, HostListener, NgZone, OnInit, inject } from '@angular/core';
import { LocationSequence, OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { clamp } from 'lodash';
import { getPixelDeltaY } from '../tools';
import { PinchZoomDirective } from '../directives';
import { HierarchyComponent } from './hierarchy.component';
import { HierarchyNodeStateDirective } from './node-state.directive';

@Directive({ standalone: true, hostDirectives: [PinchZoomDirective] })
export class HierarchyMovementDirective<T> implements OnInit, AfterViewInit {
  // Every pixel in scroll delta we zoom in/out 1 percent.
  static readonly SCROLL_TO_ZOOM_RATIO = 0.01;
  static readonly MINIMAL_SCALE = 0.40187757201;

  private hierarchy = inject<HierarchyComponent<T>>(HierarchyComponent);
  private pinchZoomDirective = inject(PinchZoomDirective);
  private zone = inject(NgZone);
  private cdRef = inject(ChangeDetectorRef);
  private nodeState = inject<HierarchyNodeStateDirective<T>>(HierarchyNodeStateDirective);

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  private modX = 0;
  private modY = 0;
  private scale = 1.0;

  // Whether or not we are currently moving/panning the mindmap
  private panning = false;

  ngOnInit() {
    this.nodeState.focusChange.pipe(takeUntil(this.onDestroy$)).subscribe(newFocus => {
      if (newFocus.length === 1) this.moveIntoView(newFocus[0]);
    });
    this.pinchZoomDirective.dtPinchZoom.pipe(takeUntil(this.onDestroy$)).subscribe(event => this.pinchZoom(event));

    this.updateTransform();
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this.hierarchy.mindMap.nativeElement.addEventListener('wheel', event => {
        event.preventDefault();
        event.stopImmediatePropagation();
        const pixelDeltaY = getPixelDeltaY(event, 25, 500);

        if (event.ctrlKey) {
          this.zoom(-pixelDeltaY);
        } else {
          if (event.shiftKey) {
            this.modX += clamp(pixelDeltaY, -50, 50) * -1;
          } else {
            this.modY += clamp(pixelDeltaY, -50, 50) * -1;
          }

          this.updateTransform();
        }
      });

      this.hierarchy.mindMap.nativeElement.addEventListener('mousedown', (event: MouseEvent) => {
        const target = event.target as HTMLElement;

        if (this.hierarchy.mindMap.nativeElement.contains(target)) {
          const isInNode = target.closest('dt-hierarchy-node') !== null;
          const isMiddleClick = event.button === 1;

          if (!isInNode || isMiddleClick) {
            this.panning = true;
            this.cdRef.detectChanges();
            event.preventDefault();
          }

          if (!isInNode) {
            // Purpose: If the user clicks in the white space of the hierarchy we want to clear the selection.
            // If the click happened in another node however we are not responsible for handling the selection clearing.
            // If the click happens outside the hierarchy we are also not responsible for handling that.
            this.nodeState.clearFocus();
          }
        }
      });

      this.hierarchy.mindMap.nativeElement.addEventListener('mouseup', () => (this.panning = false));
      this.hierarchy.mindMap.nativeElement.addEventListener('mouseleave', () => (this.panning = false));

      {
        // Mouse tracking for event.movement[X/Y] polyfill.
        let mx: number = null;
        let my: number = null;
        this.hierarchy.mindMap.nativeElement.addEventListener('mousemove', (event: MouseEvent) => {
          const difX = event.screenX - (mx ?? event.screenX);
          const difY = event.screenY - (my ?? event.screenY);

          mx = event.screenX;
          my = event.screenY;

          if (this.panning) {
            event.preventDefault();
            event.stopImmediatePropagation();

            this.modX += difX;
            this.modY += difY;

            this.updateTransform();
          }
        });
      }

      // Touch Moving
      let movingTouch: { ident: number; x: number; y: number } = null;
      this.hierarchy.mindMap.nativeElement.addEventListener('touchstart', (event: TouchEvent) => {
        this.panning = true;

        movingTouch = {
          ident: event.changedTouches[0].identifier,
          x: event.changedTouches[0].pageX,
          y: event.changedTouches[0].pageY,
        };

        this.cdRef.detectChanges();
      });

      this.hierarchy.mindMap.nativeElement.addEventListener('touchmove', (event: TouchEvent) => {
        for (const changedTouch of Array.from(event.changedTouches)) {
          if (changedTouch.identifier === movingTouch?.ident) {
            const difX = changedTouch.pageX - movingTouch.x;
            const difY = changedTouch.pageY - movingTouch.y;

            movingTouch.x = changedTouch.pageX;
            movingTouch.y = changedTouch.pageY;

            this.modX += difX;
            this.modY += difY;

            this.updateTransform();
            event.preventDefault();
          }
        }
      });

      const cancelTouch = (event: TouchEvent) => {
        for (const changedTouch of Array.from(event.changedTouches)) {
          if (changedTouch.identifier === movingTouch?.ident) {
            movingTouch = null;
            this.panning = false;
            break;
          }
        }
      };

      this.hierarchy.mindMap.nativeElement.addEventListener('touchend', cancelTouch);
      this.hierarchy.mindMap.nativeElement.addEventListener('touchcancel', cancelTouch);
    });
  }

  @HostListener('window:mousedown', ['$event'])
  mouseDown(event: MouseEvent) {
    // Unselect everything if we click outside the hierarchy
    if (!this.hierarchy.mindMap.nativeElement.contains(event.target as HTMLElement)) {
      this.nodeState.clearFocus();
    }
  }

  pinchZoom(change: number) {
    this.scale = Math.max(this.scale + change, HierarchyMovementDirective.MINIMAL_SCALE);
    this.updateTransform();
  }

  zoom(delta: number) {
    const scaleDelta = clamp(delta * HierarchyMovementDirective.SCROLL_TO_ZOOM_RATIO, -0.25, 0.25);
    const oldScale = this.scale;
    this.scale += scaleDelta;
    this.scale = Math.max(this.scale, HierarchyMovementDirective.MINIMAL_SCALE);
    // center on current box center (required because of the transformXCorrection in setupTransform())
    if (delta > 0) {
      this.modX -= (this.hierarchy.moveContainer.nativeElement.offsetWidth / 2) * Math.abs(this.scale - oldScale);
    } else {
      this.modX += (this.hierarchy.moveContainer.nativeElement.offsetWidth / 2) * Math.abs(this.scale - oldScale);
    }
    this.updateTransform();
  }

  private updateTransform() {
    window.requestAnimationFrame(() => {
      const moveContainerOriginalWidth = this.hierarchy.moveContainer.nativeElement.offsetWidth; // width not affected by scale
      /* Fixes moving tree, when expanding/collapsing elements with a scale level different from 1. */
      const transformXCorrection = (moveContainerOriginalWidth * this.scale - moveContainerOriginalWidth) / 2;
      this.hierarchy.moveContainer.nativeElement.style.transform = `translate(${this.modX + transformXCorrection}px, ${
        this.modY
      }px) scale(${this.scale})`;
    });
  }

  private moveIntoView(node: LocationSequence) {
    // In some instances, e.g. when the focus is changed to a new element before change detection has instantiated
    // the corresponding node, the node container cannot be found.
    const nodeContainer = this.hierarchy.getNodeElement(node)?.nodeWrapper.nativeElement;
    if (!nodeContainer) return;

    const limitContainer = this.hierarchy.mindMap.nativeElement;

    const nodeRect = nodeContainer.getBoundingClientRect();
    const limitRect = limitContainer.getBoundingClientRect();

    // Move Vertical
    if (nodeRect.top < limitRect.top) {
      this.modY += limitRect.top - nodeRect.top + 5;
    } else if (nodeRect.bottom > limitRect.bottom) {
      this.modY -= nodeRect.bottom - limitRect.bottom + 5;
    }

    // Move Horizontal
    if (nodeRect.left < limitRect.left) {
      this.modX += limitRect.left - nodeRect.left + 5;
    } else if (nodeRect.right > limitRect.right) {
      this.modX -= nodeRect.right - limitRect.right + 5;
    }

    this.updateTransform();
  }
}
