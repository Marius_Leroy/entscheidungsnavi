import { AfterViewInit, ChangeDetectionStrategy, Component, Input, OnDestroy, TemplateRef } from '@angular/core';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { BaseChartComponent, Orientation } from '@swimlane/ngx-charts';
import { id } from '@swimlane/ngx-charts';
import { scaleLinear, scalePoint, scaleTime } from 'd3-scale';
import { CurveFactory, curveLinear } from 'd3-shape';
import { Observable, Subject } from 'rxjs';
import { debounceTime, takeUntil, tap } from 'rxjs/operators';
import {
  LineSeries,
  XScale,
  ScaleType,
  YScale,
  AreaSeries,
  TextCircleSeries,
  Axis,
  ExtendedViewDimensions,
  LineChartColorHelper,
  calculateViewDimensions,
} from '../common';

class XAxisProperties {
  height = 0;
  scale: XScale;
  scaleType: ScaleType;
}
class YAxisProperties {
  width = 0;
  scale: YScale;
}

@Component({
  selector: 'dt-line-area-chart',
  templateUrl: './line-area-chart.component.html',
  styleUrls: ['./line-area-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LineAreaChartComponent extends BaseChartComponent implements AfterViewInit, OnDestroy {
  @Input() lineChartSeries: LineSeries[];
  // An array of x-values whose points should be shown permanently from the line graph.
  // All other points are just shown on hover.
  @Input() permanentXValues: any[] = [];
  @Input() areaChartSeries: AreaSeries[];
  @Input() textCircleSeries: TextCircleSeries[];

  @Input() xAxis: Axis;
  @Input() yAxis: Axis;
  xAxisProperties = new XAxisProperties();
  yAxisProperties = new YAxisProperties();

  @Input() curveType: CurveFactory = curveLinear;

  @Input() tooltipDisabled = false;
  @Input() textCircleTooltipTemplate: TemplateRef<any>;
  @Input() lineCircleTooltipTemplate: TemplateRef<any>;

  override animations = false;

  dimensions: ExtendedViewDimensions;
  margins: [number, number, number, number] = [10, 20, 10, 20];
  readonly yAxisOrientation = Orientation.Left;
  transform: string;
  lineChartColors: LineChartColorHelper;

  // The complete sorted set of distinct x-values (sorted depending on the axis direction)
  lineXSet: any[];
  // The x-value which is currently being hovered over (or null if no hover)
  hoveredXValue: any = null;
  // The x-value of the points currently being shown
  visiblePoints: any[] = [];

  clipPathId: string;
  clipPath: string;
  relaxedClipPathId: string;
  relaxedClipPath: string;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  private resizeObserver: ResizeObserver;

  isResizing = false;

  override ngAfterViewInit() {
    super.ngAfterViewInit();

    // We do not use the window:resize hook from the parent class
    this.resizeSubscription?.unsubscribe();
    this.resizeSubscription = null;

    // Instead, we use a ResizeObserver
    const resizeObservable = new Subject<void>();
    this.resizeObserver = new ResizeObserver(() => resizeObservable.next());
    this.resizeObserver.observe(this.chartElement.nativeElement);
    resizeObservable
      .pipe(
        takeUntil(this.onDestroy$),
        tap(() => {
          if (!this.isResizing) {
            this.zone.run(() => {
              this.isResizing = true;
              this.cd.markForCheck();
            });
          }
        }),
        debounceTime(100)
      )
      .subscribe(() => {
        this.zone.run(() => {
          this.isResizing = false;
          this.update();
          this.cd.markForCheck();
        });
      });
  }

  override ngOnDestroy() {
    super.ngOnDestroy();
    this.resizeObserver?.disconnect();
  }

  override update(): void {
    super.update();

    this.dimensions = calculateViewDimensions({
      width: this.width,
      height: this.height,
      margins: this.margins,
      showXAxis: this.xAxis.displayAxis,
      showYAxis: this.yAxis.displayAxis,
      xAxisHeight: this.xAxisProperties.height,
      yAxisWidth: this.yAxisProperties.width,
      showXLabel: this.xAxis.showLabel,
      showYLabel: this.yAxis.showLabel,
    });

    const xDomain = this.getXDomain([...this.lineChartSeries, ...this.areaChartSeries]);
    this.xAxisProperties.scale = this.getXScale(xDomain, this.dimensions.width);
    const yDomain = this.getYDomain([...this.lineChartSeries, ...this.areaChartSeries]);
    this.yAxisProperties.scale = this.getYScale(yDomain, this.dimensions.height);
    this.lineChartColors = new LineChartColorHelper(this.lineChartSeries);
    this.calculateLineXSet();
    this.transform = `translate(${this.dimensions.xOffset}, ${this.margins[0]})`;
    this.updateHighlightedXValues();

    this.clipPathId = 'clip' + id().toString();
    this.clipPath = `url(#${this.clipPathId})`;
    this.relaxedClipPathId = 'clip' + id().toString();
    this.relaxedClipPath = `url(#${this.relaxedClipPathId})`;
  }

  private getScaleType(values: any[]): ScaleType {
    let isDate = true;
    let isNum = true;

    for (const value of values) {
      if (!(value instanceof Date)) {
        isDate = false;
      }
      if (typeof value !== 'number') {
        isNum = false;
      }
    }

    return isDate ? 'time' : isNum ? 'linear' : 'ordinal';
  }

  private getXDomain(series: Array<LineSeries | AreaSeries>): any[] {
    // Get all unique x-values from all given series
    const uniqueValues: any[] = [];
    for (const oneSeries of series) {
      for (const point of oneSeries.series) {
        if (!uniqueValues.includes(point.name)) {
          uniqueValues.push(point.name);
        }
      }
    }
    this.xAxisProperties.scaleType = this.getScaleType(uniqueValues);

    if (this.xAxisProperties.scaleType !== 'ordinal') {
      let min = Math.min(...uniqueValues);
      let max = Math.max(...uniqueValues);

      if (this.xAxis.direction === 'dsc') {
        [min, max] = [max, min];
      }
      if (this.xAxis.min != null) {
        min = this.xAxis.min;
      }
      if (this.xAxis.max != null) {
        max = this.xAxis.max;
      }

      if (this.xAxisProperties.scaleType === 'time') {
        return [new Date(min), new Date(max)];
      } else {
        // linear
        return [min, max];
      }
    } else {
      // ordinal values are assumed to be in order (we have no way of sorting them anyway).
      return uniqueValues;
    }
  }

  private getXScale(domain: any, width: any): XScale {
    let scale;
    if (this.xAxisProperties.scaleType === 'time') {
      scale = scaleTime().range([0, width]).domain(domain);
    } else if (this.xAxisProperties.scaleType === 'linear') {
      scale = scaleLinear().range([0, width]).domain(domain);
    } else {
      scale = scalePoint().range([0, width]).padding(0.1).domain(domain);
    }
    return scale;
  }

  /**
   * Calculate the set of distinct X values to be used by the tooltip.
   * Needs to be called after xAxisProperties.scaleType is calculated.
   */
  private calculateLineXSet() {
    const uniqueValues: any[] = [];
    for (const series of this.lineChartSeries) {
      for (const point of series.series) {
        if (!uniqueValues.includes(point.name)) {
          uniqueValues.push(point.name);
        }
      }
    }

    if (this.xAxisProperties.scaleType !== 'ordinal') {
      let lineXSet: any[];

      if (this.xAxisProperties.scaleType === 'time') {
        lineXSet = (uniqueValues as Date[]).sort((a, b) => {
          if (a > b) {
            return 1;
          }
          if (a < b) {
            return -1;
          }
          return 0;
        });
      } else {
        // linear
        lineXSet = (uniqueValues as number[]).sort((a, b) => a - b);
      }

      // Depending on the direction of the axis, we need to reverse
      // this set. It is currently sorted ascending.
      if (this.xAxis.min != null && this.xAxis.max != null) {
        if (this.xAxis.min > this.xAxis.max) {
          lineXSet = lineXSet.reverse();
        }
      } else if (this.xAxis.direction === 'dsc') {
        lineXSet = lineXSet.reverse();
      }

      this.lineXSet = lineXSet;
    } else {
      // ordinal values are assumed to be in order
      this.lineXSet = uniqueValues;
    }
  }

  private getYDomain(series: Array<LineSeries | AreaSeries>): [number, number] {
    if (this.yAxis.min != null && this.yAxis.max != null) {
      return [this.yAxis.min, this.yAxis.max];
    } else {
      // Get all unique y-values from all given series
      const uniqueValues: number[] = [];
      for (const oneSeries of series) {
        for (const point of oneSeries.series) {
          const value = point.value;
          // The point can be from a LineSeries or an AreaSeries
          if (typeof value === 'number') {
            if (!uniqueValues.includes(value)) {
              uniqueValues.push(value);
            }
          } else {
            if (!uniqueValues.includes(value[0])) {
              uniqueValues.push(value[0]);
            }
            if (!uniqueValues.includes(value[1])) {
              uniqueValues.push(value[1]);
            }
          }
        }
      }

      let min = Math.min(...uniqueValues);
      let max = Math.max(...uniqueValues);

      if (this.yAxis.direction === 'dsc') {
        [min, max] = [max, min];
      }
      if (this.yAxis.min != null) {
        min = this.yAxis.min;
      }
      if (this.yAxis.max != null) {
        max = this.yAxis.max;
      }

      return [min, max];
    }
  }

  private getYScale(domain: any, height: any): YScale {
    const scale = scaleLinear().range([height, 0]).domain(domain);

    return scale;
  }

  /**
   * Wird von der X-Axis als Event aufgerufen.
   */
  updateXAxisHeight({ height }: { height: number }) {
    this.xAxisProperties.height = height;
    this.update();
  }
  /**
   * Wird von der Y-Axis als Event aufgerufen.
   */
  updateYAxisWidth({ width }: { width: number }) {
    this.yAxisProperties.width = width;
    this.update();
  }

  /**
   * Called by the tooltip-area.
   * @param value is the x-value that is being hovered over.
   */
  updateHoveredValue({ value }: { value: any }) {
    this.hoveredXValue = value;
    this.updateHighlightedXValues();
  }
  /**
   * Called when the mouse leaves the tooltip-area to hide the circles.
   */
  hideCircles() {
    this.hoveredXValue = null;
    this.updateHighlightedXValues();
  }

  /**
   * Called to update the highlightedXValues array.
   */
  private updateHighlightedXValues() {
    if (this.hoveredXValue != null) {
      this.visiblePoints = [...this.permanentXValues, this.hoveredXValue];
    } else {
      this.visiblePoints = this.permanentXValues;
    }
  }
}
