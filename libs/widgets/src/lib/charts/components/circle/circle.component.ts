import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, ElementRef, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { select } from 'd3-selection';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'g[dt-charts-circle]',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('showHide', [
      transition('false => true', animate('250ms ease-in')),
      transition('true => false', animate('250ms ease-out')),
      state('true', style({ opacity: 1 })),
      state('*', style({ opacity: 0 })),
    ]),
  ],
})
export class CircleComponent implements OnChanges {
  @Input() cx: number;
  @Input() cy: number;
  @Input() r: number;
  @Input() fill: string;
  @Input() visible = true;
  @Input() animations = true;

  @ViewChild('circle', { static: true }) element: ElementRef;

  ngOnChanges(changes: SimpleChanges) {
    if ('cx' in changes || 'cy' in changes || 'r' in changes) {
      const node = select(this.element.nativeElement);

      if (this.animations) {
        node.transition().duration(250);
      }

      node.attr('cx', this.cx).attr('cy', this.cy).attr('r', this.r);
    }
  }
}
