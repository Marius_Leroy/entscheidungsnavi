import { ChangeDetectionStrategy, Component, ElementRef, Input, OnChanges, ViewChild } from '@angular/core';
import { sortByDomain, sortByTime, sortLinear } from '@swimlane/ngx-charts';
import { interpolatePath } from 'd3-interpolate-path';
import { ScaleLinear, ScalePoint, ScaleTime } from 'd3-scale';
import { select } from 'd3-selection';
import { CurveFactory, line, Line } from 'd3-shape';
import { LineSeries, XScale, YScale, ScaleType, Point } from '../../common/chart-data-types';

/**
 * A simple line.
 * See https://github.com/swimlane/ngx-charts/blob/master/src/line-chart/line-series.component.ts
 */

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'g[dt-charts-line]',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LineComponent implements OnChanges {
  @Input() data: LineSeries;
  @Input() xScale: XScale;
  @Input() yScale: YScale;
  @Input() scaleType: ScaleType;
  @Input() curve: CurveFactory;
  @Input() animations = true;

  @ViewChild('line', { static: true }) element: ElementRef;

  ngOnChanges() {
    this.update();
  }

  private update(): void {
    const data = this.sortData(this.data.series);

    const lineGen = this.getLineGenerator();
    const path = lineGen(data) || '';

    const node = select(this.element.nativeElement);

    if (this.animations) {
      node
        .transition()
        .duration(250)
        .attrTween('d', () => {
          const previous = node.attr('d');
          return interpolatePath(previous, path);
        });
    } else {
      node.attr('d', path);
    }
  }

  private getLineGenerator(): Line<Point<number>> {
    return line<Point<number>>()
      .x(d => {
        const label = d.name;
        let value;
        if (this.scaleType === 'time') {
          value = (this.xScale as ScaleTime<number, number>)(label);
        } else if (this.scaleType === 'linear') {
          value = (this.xScale as ScaleLinear<number, number>)(Number(label));
        } else {
          value = (this.xScale as ScalePoint<string>)(label);
        }
        return value;
      })
      .y(d => this.yScale(d.value))
      .curve(this.curve);
  }

  private sortData(data: Point<number>[]) {
    if (this.scaleType === 'linear') {
      data = sortLinear(data, 'name');
    } else if (this.scaleType === 'time') {
      data = sortByTime(data, 'name');
    } else {
      data = sortByDomain(data, 'name', 'asc', this.xScale.domain());
    }

    return data;
  }
}
