import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  ViewChild,
} from '@angular/core';
import { ScalePoint } from 'd3-scale';
import { truncate } from 'lodash';
import { Axis, ExtendedViewDimensions, ScaleType, XScale } from '../../common';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'g[dt-charts-x-axis-ticks]',
  templateUrl: './x-axis-ticks.component.html',
  styleUrls: ['./x-axis-ticks.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class XAxisTicksComponent implements OnChanges, AfterViewInit {
  @Input() xScale: XScale;
  @Input() xScaleType: ScaleType;
  @Input() xAxis: Axis;
  @Input() dims: ExtendedViewDimensions;

  @Output() dimensionsChanged = new EventEmitter<{ height: number }>();
  @ViewChild('ticksel') ticksElement: ElementRef;

  height = 0;
  angle = 0;

  get textAnchor(): 'middle' | 'end' {
    return this.angle !== 0 ? 'end' : 'middle';
  }
  get verticalSpacing() {
    return this.angle !== 0 ? 10 : 20;
  }
  private adjustedScale: (input: any) => number;

  // The width of one character which we calculate with as an approximation
  private charWidth = 8;
  private maxTextLength = 40;

  ngOnChanges() {
    this.update();
  }

  ngAfterViewInit() {
    setTimeout(() => this.updateDims(), 0);
  }

  private updateDims() {
    const height = parseInt(this.ticksElement.nativeElement.getBoundingClientRect().height, 10);
    if (height !== this.height) {
      this.height = height;
      this.dimensionsChanged.emit({ height });
      setTimeout(() => this.updateDims(), 0);
    }
  }

  private update() {
    this.angle = this.getRotationAngle();

    if (this.xScaleType === 'ordinal') {
      this.adjustedScale = (input: any) => this.xScale(input) + (this.xScale as ScalePoint<string>).bandwidth() * 0.5;
    } else {
      this.adjustedScale = this.xScale;
    }

    setTimeout(() => this.updateDims(), 0);
  }

  /**
   * The rotation angle is calculated by an approximation. We calculate how many characters are displayed per pixel
   * at maximum for the individual ticks. This is then compared to the possible maximum (1 / characterWidth).
   * If we are above the maximum, we increase the angle to reduce the number of characters per pixel in the x-direction.
   */
  getRotationAngle(): number {
    // Handle the edge case of 0 or 1 ticks
    if (this.xAxis.ticks.length < 2) {
      return 0;
    }

    let angle = 0;
    // How many characters we have per pixel of width at maximum
    let maxCharactersPerPixel = 0;

    if (this.xScaleType === 'ordinal') {
      // This data is spaced evenly, so we just need the max tick length because the width is shared equally
      const maxTickLength = Math.max(...this.xAxis.ticks.map(tick => this.tickTrim(this.tickFormat(tick)).length));
      maxCharactersPerPixel = maxTickLength / (this.dims.width / this.xAxis.ticks.length);
    } else {
      // linear and time data is not spaced evenly along the axis
      let ticks = this.xScaleType === 'time' ? (this.xAxis.ticks as Date[]).map(v => v.valueOf()) : (this.xAxis.ticks as number[]);

      // Shift the input scale from [ticks[0], ticks[ticks.length - 1]] to [0, dims.width]
      // (the data is pre-sorted)
      const scale = (input: number) => ((input - ticks[0]) / (ticks[ticks.length - 1] - ticks[0])) * this.dims.width;
      ticks = ticks.map(v => scale(v));

      // For every tick we calculate its text length and its available width on the scale.
      // By dividing, we get the characters per pixel.
      ticks.forEach((tick, index) => {
        const length = this.tickTrim(this.tickFormat(tick)).length;
        let tickWidth: number;

        // The maximum width this tick is allowed to have.
        // For the outer-most ticks we do not assume that the texts have to be centered. For all other ticks we
        // do assume that.
        if (index === 0) {
          tickWidth = this.dims.xOffset + Math.abs(tick - ticks[index + 1]) / 2;
        } else if (index === ticks.length - 1) {
          tickWidth = this.dims.margins[1] + Math.abs(tick - ticks[index - 1]) / 2;
        } else {
          tickWidth = Math.min(Math.abs(tick - ticks[index - 1]), Math.abs(tick - ticks[index + 1])) / 2;
        }

        const charPerPixel = length / tickWidth;
        maxCharactersPerPixel = Math.max(charPerPixel, maxCharactersPerPixel);
      });
    }

    // calculate angle under which all characters fit in 15 degree steps.
    let baseCharactersPerPixel = maxCharactersPerPixel;

    while (baseCharactersPerPixel > 1 / this.charWidth && angle > -90) {
      angle -= 15;
      baseCharactersPerPixel = Math.cos(angle * (Math.PI / 180)) * maxCharactersPerPixel;
    }

    return angle;
  }

  tickTrim(tickText: string): string {
    return truncate(tickText, { length: this.maxTextLength });
  }

  tickFormat(tickValue: any): string {
    if (this.xAxis.format != null) {
      return this.xAxis.format(tickValue);
    } else {
      return tickValue.toLocaleString();
    }
  }

  tickTransform(tick: any) {
    return `translate(${this.adjustedScale(tick)}, ${this.verticalSpacing})`;
  }

  /**
   * We first apply the rotation to the text.
   * Then, we force the outer-most texts to stay inside the boundaries of the chart-box by applying an x-translation.
   */
  textTransform(tick: any, textElement: SVGTextElement) {
    let translation = '';
    const index = this.xAxis.ticks.indexOf(tick);
    // We take the width of the BoundingClientRect and not the BBox because it takes the rotation into account.
    const width = textElement.getBoundingClientRect().width;

    // In the first iteration width may be zero when the element is not drawn yet.
    if (width > 0) {
      let xTranslation = 0;

      // We only care for the left-most and right-most element.
      if (index === 0) {
        // For the left-most element, we know dims.xOffset pixels are free to the left.
        if (this.textAnchor === 'middle') {
          xTranslation = Math.max(0, -this.dims.xOffset + width / 2);
        } else {
          xTranslation = Math.max(0, -this.dims.xOffset + width);
        }
      } else if (index === this.xAxis.ticks.length - 1 && this.textAnchor === 'middle') {
        // For the right-most element, we know that only the right margin is free to the right.
        xTranslation = -(width / 2 - this.dims.margins[1]);
      }
      // If the anchor is 'end', then every element spreads left of its anchor, so there can never be any overhang to the right side.
      // Therefore, we do not need to consider this case.

      translation = `translate(${xTranslation}, 0) `;
    }
    return `${translation}rotate(${this.angle})`;
  }

  gridLineTransform() {
    return `translate(0, ${-this.verticalSpacing - 5})`;
  }
}
