import { ChangeDetectionStrategy, Component, ElementRef, Input, OnChanges, ViewChild } from '@angular/core';
import { sortByDomain, sortByTime, sortLinear } from '@swimlane/ngx-charts';
import { ScaleLinear, ScalePoint, ScaleTime } from 'd3-scale';
import { select, Selection } from 'd3-selection';
import { area, Area, CurveFactory } from 'd3-shape';
import { Transition } from 'd3-transition';
import { AreaSeries, Point, ScaleType, XScale, YScale } from '../../common';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'g[dt-charts-area]',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AreaComponent implements OnChanges {
  @Input() data: AreaSeries;
  @Input() curveType: CurveFactory;
  @Input() scaleType: ScaleType;
  @Input() xScale: XScale;
  @Input() yScale: YScale;
  @Input() animations = true;

  @ViewChild('area', { static: true }) element: ElementRef;

  ngOnChanges() {
    this.update();
  }

  private update() {
    const data = this.sortData(this.data.series);

    const areaGen = this.getAreaGenerator();
    const path = areaGen(data) || '';

    let node: Selection<any, unknown, null, undefined> | Transition<any, unknown, null, undefined> = select(this.element.nativeElement);

    if (this.animations) {
      node = node.transition().duration(250);
    }

    node.attr('d', path);
  }

  private getAreaGenerator(): Area<Point<[number, number]>> {
    return area<Point<[number, number]>>()
      .x(d => {
        const label = d.name;
        let value;
        if (this.scaleType === 'time') {
          value = (this.xScale as ScaleTime<number, number>)(label);
        } else if (this.scaleType === 'linear') {
          value = (this.xScale as ScaleLinear<number, number>)(Number(label));
        } else {
          value = (this.xScale as ScalePoint<string>)(label);
        }
        return value;
      })
      .y0(d => this.yScale(d.value[0]))
      .y1(d => this.yScale(d.value[1]))
      .curve(this.curveType);
  }

  private sortData(data: Array<Point<[number, number]>>): Array<Point<[number, number]>> {
    if (this.scaleType === 'linear') {
      data = sortLinear(data, 'name');
    } else if (this.scaleType === 'time') {
      data = sortByTime(data, 'name');
    } else {
      data = sortByDomain(data, 'name', 'asc', this.xScale.domain());
    }

    return data;
  }
}
