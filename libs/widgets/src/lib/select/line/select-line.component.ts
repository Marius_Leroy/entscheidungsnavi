import { Component, Input } from '@angular/core';

@Component({
  selector: 'dt-select-line',
  templateUrl: './select-line.component.html',
  styleUrls: ['./select-line.component.scss'],
})
export class SelectLineComponent {
  @Input()
  align: 'start' | 'end' = 'end';
}
