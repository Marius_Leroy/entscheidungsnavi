import { Directive, ElementRef } from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'dt-select-label',
})
export class SelectLabelDirective {
  get text() {
    return this.elementRef.nativeElement.textContent.trim();
  }

  constructor(private elementRef: ElementRef) {}
}
