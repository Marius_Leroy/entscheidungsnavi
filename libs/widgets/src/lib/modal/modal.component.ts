import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AbstractModalConfigurationDirective } from './modal-configuration.directive';

@Component({
  selector: 'dt-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent extends AbstractModalConfigurationDirective implements OnDestroy, AfterViewInit, OnChanges {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  @Input() opened = false;
  @Input() disableClose = false;

  @Output() modalClose = new EventEmitter();
  @Output() modalOpen = new EventEmitter();

  dialogRef: MatDialogRef<any>;

  constructor(private dialog: MatDialog, private viewContainerRef: ViewContainerRef) {
    super();
  }

  ngOnDestroy() {
    this.finishClose();
  }

  ngOnChanges(changes: SimpleChanges) {
    if ('opened' in changes && !changes['opened'].isFirstChange()) {
      const before: boolean = changes['opened'].previousValue;
      const now: boolean = changes['opened'].currentValue;

      if (!before && now) {
        this.open();
      } else if (before && !now) {
        this.forceClose();
      }
    }
  }

  ngAfterViewInit() {
    if (this.opened) {
      this.open();
    }
  }

  open() {
    if (this.dialogRef != null) {
      return;
    }

    this.dialogRef = this.dialog.open(this.modalContent, { viewContainerRef: this.viewContainerRef });
    this.modalOpen.emit();
    this.opened = true;
  }

  close() {
    this.modalClose.emit();
    if (!this.disableClose) {
      this.finishClose();
    }
  }

  forceClose() {
    this.modalClose.emit();
    this.finishClose();
  }

  private finishClose() {
    this.opened = false;
    this.dialogRef?.close();
    this.dialogRef = null;
  }
}
