import { ContentChild, Directive, Input, OnInit, TemplateRef } from '@angular/core';

@Directive()
export abstract class AbstractModalConfigurationDirective implements OnInit {
  @ContentChild('content', { static: true })
  contentTemplate: TemplateRef<any>;

  @ContentChild('title', { static: true })
  titleTemplate: TemplateRef<any>;

  @ContentChild('actions', { static: true })
  actionsTemplate: TemplateRef<any>;

  @ContentChild('subtitle', { static: true })
  subTitleTemplate: TemplateRef<any>;

  @ContentChild('subheader', { static: true })
  subHeaderTemplate: TemplateRef<any>;

  @ContentChild('footer', { static: true })
  footerTemplate: TemplateRef<any>;

  @Input() showX = true;
  @Input() showFooter: boolean; // If this is not set, a footer will be shown if a template is provided
  @Input() size: 'small' | 'normal' | 'big' | 'giant' | 'flex' = 'normal'; // giant also increases modal height to up to 90vh
  @Input() contentPadding = true; // Whether to apply a default 12px/24px Padding for the Modal Content

  /**
   * If this is set to true, the content-container will be a flex-container and will not have
   * scroll bars. The given template has to handle scrolling itself.
   */
  @Input() flexContent = false;

  ngOnInit() {
    this.showFooter = this.showFooter ?? this.footerTemplate != null;
  }
}
