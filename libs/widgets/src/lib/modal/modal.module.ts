import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatLegacyButtonModule as MatButtonModule } from '@angular/material/legacy-button';
import { ButtonModule } from '../buttons/button.module';
import { ConfirmModalComponent, ModalContentComponent, ModalComponent } from '.';

@NgModule({
  imports: [CommonModule, ButtonModule, MatIconModule, MatDividerModule, MatButtonModule],
  declarations: [ModalComponent, ModalContentComponent, ConfirmModalComponent],
  exports: [ModalComponent, ModalContentComponent, ConfirmModalComponent],
})
export class ModalModule {}
