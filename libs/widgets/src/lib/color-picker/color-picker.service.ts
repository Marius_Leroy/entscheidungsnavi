import { ElementRef, EventEmitter, Injectable } from '@angular/core';
import { ColorEvent } from 'ngx-color';
import { SketchComponent } from 'ngx-color/sketch';
import { PopOverService } from '../popover';

export const DEFAULT_COLOR_PRESET = [
  '#D0021B',
  '#F8931E',
  '#F8E71C',
  '#8B572A',
  '#7ED321',
  '#417505',
  '#BD10E0',
  '#9013FE',
  '#4A90E2',
  '#50E3C2',
  '#B8E986',
  '#000000',
  '#3B4C5B',
  '#D3D3D3',
  '#FFFFFF',
];

@Injectable({ providedIn: 'root' })
export class ColorPickerService {
  constructor(private popOver: PopOverService) {}

  openColorPicker(element: ElementRef<HTMLElement> | HTMLElement, initialColor: string, presetColors?: string[]) {
    const colorEmitter = new EventEmitter<string>();

    const sketchComponent = this.popOver.open(SketchComponent, element, {
      closeCallback: () => colorEmitter.complete(),
      dimensions: { width: 220, height: 310 },
    }).componentInstance;

    sketchComponent.color = initialColor;

    if (presetColors) {
      sketchComponent.presetColors = presetColors;
    } else {
      sketchComponent.presetColors = DEFAULT_COLOR_PRESET;
    }

    sketchComponent.onChangeComplete.subscribe((event: ColorEvent) => {
      colorEmitter.emit(event.color.hex);
    });

    return colorEmitter;
  }
}
