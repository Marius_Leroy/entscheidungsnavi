import { SortDirection } from './sort';

export interface KlugProject {
  id: string;
  data: string;
  token: string;
  isOfficial: boolean;
  finished: boolean;
  readonly: boolean;
  expiresAt?: Date;
  createdAt: Date;
  updatedAt: Date;
}

export interface KlugProjectFilter {
  isOfficial?: boolean;
  finished?: boolean;
  startDate?: Date;
  endDate?: Date;
}

export interface KlugProjectList {
  items: KlugProject[];
  count: number;
}

export interface KlugProjectSort {
  sortBy?: KlugProjectSortBy;
  sortDirection?: SortDirection;
}

export interface KlugFinishProject {
  email?: string;
  pdfExportImages?: KlugPdfExportImages;
}

export interface KlugPdfExportImages {
  decisionStatementImage: string;
  objectiveListImage: string;
  alternativeListImage: string;
  assessmentGraphImage: string;
  comparisonGraphImage: string;
  evaluationGraphImage: string;
}

export const KLUG_PROJECT_SORT_BY = ['createdAt'] as const;
export type KlugProjectSortBy = (typeof KLUG_PROJECT_SORT_BY)[number];
