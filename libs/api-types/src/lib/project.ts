import { Team } from './team';

// 10MB limit
export const PROJECT_SIZE_LIMIT = 10e6;

export interface Project {
  id: string;
  name: string;
  userId: string;
  shareToken?: string;
  team?: ProjectTeamInfo;
  createdAt: Date;
  updatedAt: Date;
}

export interface ProjectWithData extends Project {
  data: string;
}

// Either send data or a patch
type ProjectDataUpdate = { data?: string } | { dataPatch: string; oldDataHash: number };
export type ProjectUpdate = Partial<Pick<Project, 'name'>> & ProjectDataUpdate & { resolvesConflict?: boolean };

export type ProjectTeamInfo = Pick<Team, 'id' | 'name'>;

export interface ProjectHistoryEntry {
  id: string;
  resolvesConflict: boolean;
  versionTimestamp: Date;
}
