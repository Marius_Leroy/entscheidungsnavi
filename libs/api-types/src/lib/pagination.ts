export interface PaginationParams {
  offset?: number;
  limit?: number;
}

export interface PaginatedList<T> {
  items: T[];
  count: number;
}
