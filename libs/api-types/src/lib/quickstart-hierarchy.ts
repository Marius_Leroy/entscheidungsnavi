import { ITree } from '@entscheidungsnavi/tools';
import { LocalizedString } from './localized-string';
import { PaginatedList } from './pagination';
import { SortDirection } from './sort';

export interface QuickstartHierarchyNode {
  id: string;
  name: LocalizedString;
  comment: LocalizedString;
}

export interface QuickstartHierarchy {
  id: string;
  name: LocalizedString;
  tags: string[];
  tree: ITree<QuickstartHierarchyNode>;
  createdAt: Date;
  updatedAt: Date;
}

interface PaginatedListWithTags<T> extends PaginatedList<T> {
  activeTags: string[];
}

export type QuickstartHierarchyList = PaginatedListWithTags<QuickstartHierarchy>;
export interface QuickstartHierarchyNodeWithStats extends QuickstartHierarchyNode {
  accumulatedScore: number;
}

export interface QuickstartHierarchyWithStats extends QuickstartHierarchy {
  tree: ITree<QuickstartHierarchyNodeWithStats>;
  totalAccumulatedScore: number;
}

export type QuickstartHierarchyListWithStats = PaginatedListWithTags<QuickstartHierarchy>;

export const QUICKSTART_HIERARCHY_SORT_BY = ['nameDe', 'nameEn', 'totalAccumulatedScore', 'createdAt'] as const;
export type QuickstartHierarchySortBy = (typeof QUICKSTART_HIERARCHY_SORT_BY)[number];

export interface QuickstartHierarchySort {
  sortBy: QuickstartHierarchySortBy;
  sortDirection: SortDirection;
}

export type QuickstartHierarchyFilter = Partial<{
  nameQuery: string;
  tags: string[];
}>;

export interface QuickstartObjective {
  id: string;
  hierarchyId: string;
  hierarchyName: LocalizedString;
  name: LocalizedString;
  comment: LocalizedString;
  level: number;
  tags: string[];
}

export type QuickstartObjectiveList = PaginatedListWithTags<QuickstartObjective>;

export interface QuickstartObjectiveWithStats extends QuickstartObjective {
  accumulatedScore: number;
}

export type QuickstartObjectiveListWithStats = PaginatedListWithTags<QuickstartObjectiveWithStats>;

export const QUICKSTART_OBJECTIVE_LEVEL_FILTERS = ['only-fundamental', 'only-aspects'] as const;
export type QuickstartObjectiveLevelFilter = (typeof QUICKSTART_OBJECTIVE_LEVEL_FILTERS)[number];

export type QuickstartObjectiveFilter = Partial<{
  query: string;
  tags: string[];
  level: QuickstartObjectiveLevelFilter;
}>;

export const QUICKSTART_OBJECTIVE_SORT_BY = ['nameDe', 'nameEn', 'level', 'accumulatedScore'] as const;
export type QuickstartObjectiveSortBy = (typeof QUICKSTART_OBJECTIVE_SORT_BY)[number];

export interface QuickstartObjectiveSort {
  sortBy: QuickstartObjectiveSortBy;
  sortDirection: SortDirection;
}

export const QUICKSTART_OBJECTIVE_GROUP_BY = ['nameDe', 'nameEn'] as const;
export type QuickstartObjectiveGroupBy = (typeof QUICKSTART_OBJECTIVE_GROUP_BY)[number];

export interface GroupedQuickstartObjective {
  name: string;
  items: QuickstartObjective[];
}

export type GroupedQuickstartObjectiveList = PaginatedListWithTags<GroupedQuickstartObjective>;

export interface GroupedQuickstartObjectiveWithStats extends GroupedQuickstartObjective {
  items: QuickstartObjectiveWithStats[];
  accumulatedScore: number;
}

export type GroupedQuickstartObjectiveListWithStats = PaginatedListWithTags<GroupedQuickstartObjectiveWithStats>;

export const GROUPED_QUICKSTART_OBJECTIVE_SORT_BY = ['name', 'itemCount', 'accumulatedScore'] as const;
export type GroupedQuickstartObjectiveSortBy = (typeof GROUPED_QUICKSTART_OBJECTIVE_SORT_BY)[number];

export interface GroupedQuickstartObjectiveSort {
  sortBy: GroupedQuickstartObjectiveSortBy;
  sortDirection: SortDirection;
}
