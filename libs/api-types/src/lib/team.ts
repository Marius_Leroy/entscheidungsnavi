import { User } from './user';

export interface Team {
  id: string;
  name: string;
  members: readonly TeamMember[];
  createdAt: Date;
  pendingInvites: TeamInvite[];
  editor: string;
  owner: string;
  whiteboard: string;
}

export type TeamPartialUserInfo = Pick<User, 'id' | 'email' | 'name'>;

export interface TeamMember {
  id: string;
  user: TeamPartialUserInfo;
  project: string;
  comments: TeamComment[];
  unreadComments: string[];
}

export interface TeamComment {
  id: string;
  objectId: string;
  author: string;
  content: string;
  createdAt: Date;
}

export interface TeamInvite {
  email: string;
  token: string;
  createdAt: Date;
}

export interface TeamInviteInfo {
  teamName: string;
  inviterName: string;
}
