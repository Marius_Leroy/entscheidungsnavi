import { LocationSequence } from './location-sequence';

describe('LocationSequence', () => {
  describe('adjustForRemovalOf()', () => {
    it('works for first children', () => {
      const loc = new LocationSequence([1]).adjustForRemovalOf(new LocationSequence([0]));
      expect(loc.value).toEqual([0]);
    });

    it('works for common ancestors', () => {
      const loc = new LocationSequence([0, 1, 1]).adjustForRemovalOf(new LocationSequence([0, 0]));
      expect(loc.value).toEqual([0, 0, 1]);
    });
  });
});
