import { LocationSequence } from './location-sequence';

/**
 * A simple tree interface.
 */
export interface ITree<T> {
  value: T;
  children: Array<ITree<T>>;
}

/**
 * An implementation of {@link ITree}.
 */
export class Tree<T> implements ITree<T> {
  /**
   * Creates a new tree object from another tree. Beware that this does not clone tree values.
   *
   * @param tree - The tree to clone
   * @returns An instance of tree
   */
  static from<T>(tree: ITree<T>): Tree<T> {
    return new Tree(
      tree.value,
      tree.children.map(child => Tree.from(child))
    );
  }

  constructor(public value: T, public children: Array<Tree<T>> = []) {}

  /**
   * Get a child tree indentified by a location sequence.
   *
   * @param location - The location of the element
   * @returns The child tree
   */
  getNode(location: LocationSequence): Tree<T> {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    let current: Tree<T> = this;

    for (const childIndex of location.value) {
      current = current.children[childIndex];
    }

    return current;
  }

  removeNode(location: LocationSequence) {
    return this.getNode(location.getParent()).children.splice(location.value.at(-1), 1)[0];
  }

  insertNode(location: LocationSequence, node: Tree<T>) {
    this.getNode(location.getParent()).children.splice(location.value.at(-1), 0, node);
  }

  /**
   * Finds the next node of the node identified by {@link element}.
   *
   * @param element - The location sequence identifiying the node
   * @returns The location sequence of the next node or null, if there is none
   */
  findNextNode(element: LocationSequence): LocationSequence {
    for (let searchDepth = 1; searchDepth <= element.value.length; searchDepth++) {
      const ancestor = element.getAncestor(searchDepth);
      const childIndex = element.value.at(-searchDepth);

      const ancestorNode = this.getNode(ancestor);

      if (childIndex < ancestorNode.children.length - 1) {
        const sequence = [childIndex + 1];
        let nextNode = ancestorNode.children[childIndex + 1];

        while (nextNode.children.length > 0 && sequence.length < searchDepth) {
          sequence.push(0);
          nextNode = nextNode.children[0];
        }

        return new LocationSequence([...ancestor.value, ...sequence]);
      }
    }

    return null;
  }

  /**
   * Finds the previous node of the node identified by {@link element}.
   *
   * @param element - The location sequence identifiying the node
   * @returns The location sequence of the previous node or null, if there is none
   */
  findPreviousNode(element: LocationSequence): LocationSequence {
    for (let searchDepth = 1; searchDepth <= element.value.length; searchDepth++) {
      const ancestor = element.getAncestor(searchDepth);
      const childIndex = element.value.at(-searchDepth);

      const ancestorNode = this.getNode(ancestor);

      if (childIndex > 0) {
        const sequence = [childIndex - 1];
        let previousNode = ancestorNode.children[childIndex - 1];

        while (previousNode.children.length > 0 && sequence.length < searchDepth) {
          sequence.push(previousNode.children.length - 1);
          previousNode = previousNode.children[previousNode.children.length - 1];
        }

        return new LocationSequence([...ancestor.value, ...sequence]);
      }
    }

    return null;
  }
}
