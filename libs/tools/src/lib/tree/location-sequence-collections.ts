import { LocationSequence } from './location-sequence';

/**
 * A map from location sequences to arbitrary values.
 */
export class LocationSequenceMap<T> {
  private mirrorValues: Map<string, T>;

  constructor() {
    this.mirrorValues = new Map<string, T>();
  }

  get(locationSequence: LocationSequence) {
    return this.mirrorValues.get(locationSequence.value.join());
  }

  set(locationSequence: LocationSequence, value: T) {
    this.mirrorValues.set(locationSequence.value.join(), value);
  }

  clear(locationSequence: LocationSequence) {
    this.mirrorValues.delete(locationSequence.value.join());
  }
}

/**
 * A set of location sequences
 */
export class LocationSequenceSet {
  private innerSet = new Set<string>();

  has(locationSequence: LocationSequence) {
    return this.innerSet.has(locationSequence.value.join());
  }

  add(locationSequence: LocationSequence) {
    this.innerSet.add(locationSequence.value.join());
  }

  delete(locationSequence: LocationSequence) {
    this.innerSet.delete(locationSequence.value.join());
  }

  clear() {
    this.innerSet.clear();
  }

  get size() {
    return this.innerSet.size;
  }
}
