export type SelectiveReadonly<T, TK extends keyof T> = Readonly<Pick<T, TK>> & Omit<T, TK>;
