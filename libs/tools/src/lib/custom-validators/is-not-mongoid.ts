import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments, isMongoId } from 'class-validator';

// Also see https://github.com/typestack/class-validator/blob/develop/src/decorator/string/IsMongoId.ts.

@ValidatorConstraint({ name: 'isNotMongoId', async: false })
export class IsNotMongoId implements ValidatorConstraintInterface {
  validate(value: unknown) {
    return !isMongoId(value);
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must not be a mongodb id.`;
  }
}
