const nxPreset = require('@nrwl/jest/preset').default;
const esModules = ['mathjs'].join('|');

module.exports = {
  ...nxPreset,
  testMatch: ['**/+(*.)?(e2e-)+(spec|test).+(ts|js)?(x)'],
  // This means we ignore everything in node_modules except files from one of the esModules or .mjs files.
  transformIgnorePatterns: [`/node_modules/(?!${esModules}|.+\.mjs)`],
  transform: {
    '^.+\\.(ts|js|html)$': 'ts-jest',
  },
  resolver: '@nrwl/jest/plugins/resolver',
  moduleFileExtensions: ['ts', 'js', 'html'],
  coverageReporters: ['json', 'text', 'html'],
};
