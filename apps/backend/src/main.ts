import helmet from 'helmet';
import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { json, text } from 'body-parser';
import { AppModule } from './app/app.module';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app
    // Set base path of the api
    .setGlobalPrefix('/api')
    // IPs are extracted from the X-Forwarded-For header. Needed for rate limiting.
    .enable('trust proxy')
    // Increase max payload limit
    .use(json({ limit: '10mb' }), text({ limit: '10mb' }))
    // Add security related HTTP Headers
    .use(helmet());

  const port = app.get(ConfigService).get<number>('PORT');
  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/api');
  });
}

bootstrap();
