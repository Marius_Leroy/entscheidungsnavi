import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DECISION_QUALITY_CRITERIA, TIMER_COUNT } from '@entscheidungsnavi/decision-data/classes';
import { NAVI_STEP_ORDER, NaviStep, STEP_NAMES } from '@entscheidungsnavi/decision-data/steps';
import { richToPlainText } from '@entscheidungsnavi/tools';
import { flatten, range, round, sum } from 'lodash';
import { Column, TableBuilder } from './table-builder';

export interface ProjectColumnData {
  projectData: DecisionData;
}

export function addProjectInformationColumns(builder: TableBuilder<ProjectColumnData>) {
  builder.addGroup('Grundlegende Projektinformationen', [
    { name: 'Version bei Start', data: s => s.projectData.version },
    { name: 'Projektname', data: s => s.projectData.decisionProblem },
    { name: 'Projektnotiz', data: s => richToPlainText(s.projectData.projectNotes) },
    { name: 'Entscheidungsfrage', data: s => s.projectData.decisionStatement.statement },
  ]);
}

export function addModelParameterCountsColumns(builder: TableBuilder<ProjectColumnData>) {
  builder.addGroup('Anzahl Parameter im Modell', [
    { name: '#Ziele', data: s => s.projectData.objectives.length },
    { name: '#Alternativen', data: s => s.projectData.alternatives.length },
    { name: '#Einflussfaktoren', data: s => s.projectData.influenceFactors.length },
    {
      name: '#Indikatoren',
      data: s => sum(s.projectData.objectives.filter(o => o.isIndicator).map(o => o.indicatorData.indicators.length)),
    },
  ]);
}

export function addProcessStatusColumns(builder: TableBuilder<ProjectColumnData>) {
  builder.addGroup('Fortschritt im Entscheidungsprozess', [
    { name: 'Alle Ziele kommentiert?', data: s => s.projectData.objectives.every(o => o.comment) },
    { name: 'Alle Alternativen kommentiert?', data: s => s.projectData.alternatives.every(o => o.comment) },
    { name: 'Alle Einflussfaktoren kommentiert?', data: s => s.projectData.influenceFactors.every(o => o.comment) },
    { name: 'Alle Trade-Offs durchgeführt?', data: s => s.projectData.weights.tradeoffWeights.every(w => w != null) },
  ]);
}

export function addModelNames(
  builder: TableBuilder<ProjectColumnData>,
  parameters: { objectiveCount: number; alternativeCount: number; influenceFactorCount: number }
) {
  if (parameters.objectiveCount > 0) {
    builder.addGroup(
      'Ziele',
      range(parameters.objectiveCount).map(index => ({
        name: `Ziel ${index + 1}`,
        data: s => (s.projectData.objectives.length > index ? s.projectData.objectives[index].name : ''),
      }))
    );
  }

  if (parameters.alternativeCount > 0) {
    builder.addGroup(
      'Alternativen',
      range(parameters.alternativeCount).map(index => ({
        name: `Alternative ${index + 1}`,
        data: s => (s.projectData.alternatives.length > index ? s.projectData.alternatives[index].name : ''),
      }))
    );
  }

  if (parameters.influenceFactorCount > 0) {
    builder.addGroup(
      'Einflussfaktoren',
      range(parameters.influenceFactorCount).map(index => ({
        name: `Einflussfaktor ${index + 1}`,
        data: s => (s.projectData.influenceFactors.length > index ? s.projectData.influenceFactors[index].name : ''),
      }))
    );
  }
}

export function addStepExplanationColumns(builder: TableBuilder<ProjectColumnData>) {
  builder.addGroup(
    'Schritterläuterungen',
    range(NAVI_STEP_ORDER.length - 1).map(index => ({
      name: `Erläuterung Schritt ${index + 1}`,
      data: s => richToPlainText(s.projectData.stepExplanations[NAVI_STEP_ORDER[index]]),
    }))
  );
}

export function addObjectiveExplanationColumns(builder: TableBuilder<ProjectColumnData>, objectiveCount: number) {
  builder.addGroup(
    'Zielerläuterungen',
    range(objectiveCount).map(index => ({
      name: `Erläuterung Ziel ${index + 1}`,
      data: s => (s.projectData.objectives.length > index ? richToPlainText(s.projectData.objectives[index].comment) : ''),
    }))
  );
}

export function addAlternativeExplanationColumns(builder: TableBuilder<ProjectColumnData>, alternativeCount: number) {
  builder.addGroup(
    'Alternativenerläuterungen',
    range(alternativeCount).map(index => ({
      name: `Erläuterung Alternative ${index + 1}`,
      data: s => (s.projectData.alternatives.length > index ? richToPlainText(s.projectData.alternatives[index].comment) : ''),
    }))
  );
}

export function addInfluenceFactorExplanationColumns(builder: TableBuilder<ProjectColumnData>, influenceFactorCount: number) {
  builder.addGroup(
    'Einflussfaktorerläuterungen',
    range(influenceFactorCount).map(index => ({
      name: `Erläuterung Einflussfaktor ${index + 1}`,
      data: s => (s.projectData.influenceFactors.length > index ? richToPlainText(s.projectData.influenceFactors[index].comment) : ''),
    }))
  );
}

export function addDecisionQualityColumns(builder: TableBuilder<ProjectColumnData>) {
  builder.addGroup(
    'Decision Quality Scores',
    range(DECISION_QUALITY_CRITERIA.length).map(index => ({
      name: `Erläuterung DQ ${index + 1}`,
      data: s => s.projectData.decisionQuality.criteriaValues[DECISION_QUALITY_CRITERIA[index]],
    }))
  );
}

export function addTimeRecordingColumns(builder: TableBuilder<ProjectColumnData>) {
  // Active and Total for each:
  //   - individual timer
  //   - aggregate by step
  //   - total
  const groups: Array<[string, Column<ProjectColumnData>[]]> = [];

  const subStepNames = NAVI_STEP_ORDER.map(step => STEP_NAMES.de[step].subSteps).flat(1);
  NAVI_STEP_ORDER.forEach(step => {
    // One for each individual timer
    const timers: Column<ProjectColumnData>[] = flatten(
      range(TIMER_COUNT[step]).map(timerIndex => {
        const subStepName = stepAndIndexToName(step, timerIndex, subStepNames);
        return [
          {
            name: `${subStepName} Aktiv`,
            data: s => s.projectData.timeRecording.timers[step][timerIndex].activeTime,
          },
          {
            name: `${subStepName} Total`,
            data: s => s.projectData.timeRecording.timers[step][timerIndex].totalTime,
          },
        ];
      })
    );

    // Aggregate per step (ignore steps with no sub-steps)
    if (step !== 'finishProject') {
      timers.push(
        {
          name: `Aggregiert ${STEP_NAMES.de[step].name} Aktiv`,
          data: s => sum(s.projectData.timeRecording.timers[step].map(timer => timer.activeTime)),
        },
        {
          name: `Aggregiert ${STEP_NAMES.de[step].name} Total`,
          data: s => sum(s.projectData.timeRecording.timers[step].map(timer => timer.totalTime)),
        }
      );
    }

    groups.push([STEP_NAMES.de[step].name, timers]);
  });

  groups.push([
    'Aggregiert',
    [
      {
        name: 'Gesamt Aktiv',
        data: s => sum(flatten(Object.values(s.projectData.timeRecording.timers)).map(timer => timer.activeTime)),
      },
      {
        name: 'Gesamt Total',
        data: s => sum(flatten(Object.values(s.projectData.timeRecording.timers)).map(timer => timer.totalTime)),
      },
    ],
  ]);

  builder.addGroupSequence('Zeiterfassung', groups);
}

function stepAndIndexToName(step: NaviStep, timerIndex: number, subStepNames: string[]) {
  // Impact Model is a special case
  if (step === 'impactModel') {
    if (timerIndex === 0) {
      return 'Einflussfaktoren';
    } else {
      return `Wirkungsmodell`;
    }
  }

  if (timerIndex === STEP_NAMES.de[step].subSteps.length) {
    return (
      (['decisionStatement', 'objectives', 'alternatives', 'results'].includes(step) ? 'Ergebnis ' : 'Hauptschritt ') +
      STEP_NAMES.de[step].name
    );
  } // Result must be unique
  const subStepName = STEP_NAMES.de[step].subSteps[timerIndex];
  const numberOfMatches = subStepNames.filter(s => s === subStepName).length;
  return subStepName + (numberOfMatches === 1 ? '' : ` (${STEP_NAMES.de[step].name})`);
}

export function addObjectiveWeightColumns(builder: TableBuilder<ProjectColumnData>, objectiveCount: number) {
  builder.addGroup(
    'Zielgewichte',
    range(objectiveCount).map(index => ({
      name: `Zielgewicht Ziel ${index + 1}`,
      data: s => {
        if (index >= s.projectData.objectives.length || !s.projectData.validateWeights()[0]) {
          return '';
        } else {
          const objectiveWeight = s.projectData.weights.getWeight(index).value;

          if (objectiveWeight == null) {
            return '';
          }

          const weightValues = s.projectData.weights.getWeightValues();

          const weightSum = weightValues.reduce((acc, weightValue) => acc + weightValue, 0);

          return Math.round((s.projectData.weights.getWeight(index).value / weightSum) * 100);
        }
      },
    }))
  );
}

export function addAlternativeUtilityValues(builder: TableBuilder<ProjectColumnData>, alternativeCount: number) {
  builder.addGroup(
    'Nutzenwerte',
    range(alternativeCount).map(index => ({
      name: `Nutzen Alternative ${index + 1}`,
      data: s => {
        if (s.projectData.alternatives.length <= index || !s.projectData.validateWeights()[0]) {
          return '';
        } else {
          const utilityValues = s.projectData.getAlternativeUtilities();

          return round(utilityValues[index] * 100, 1);
        }
      },
    }))
  );
}
