import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { plainToClass } from 'class-transformer';
import { Model, Types } from 'mongoose';
import { QuickstartProject, QuickstartProjectDocument } from '../schemas/quickstart-project.schema';
import { CreateQuickstartProjectDto } from './dto/create-quickstart-project.dto';
import { QuickstartProjectDto } from './dto/quickstart-project.dto';
import { UpdateQuickstartProjectDto } from './dto/update-quickstart-project.dto';

@Injectable()
export class QuickstartProjectsService {
  private baseUrl: string;

  constructor(
    @InjectModel(QuickstartProject.name) private quickstartProjectModel: Model<QuickstartProjectDocument>,
    configService: ConfigService
  ) {
    this.baseUrl = configService.get<string>('BASE_URL');
  }

  async create(project: CreateQuickstartProjectDto) {
    const doc = await this.quickstartProjectModel.create(project);
    return this.toDto(doc);
  }

  async listAll() {
    const docs = await this.quickstartProjectModel.find({}, '-data').sort({ _id: 1 }).exec();
    return docs.map(doc => this.toDto(doc));
  }

  async listVisible() {
    const docs = await this.quickstartProjectModel
      // Although we set a default value for `visible`, default values are set not until the documents have been returned from the database.
      // Since they may be documents that do not have the `visible` property, we have to consider that in the query.
      .find({ visible: { $ne: false } }, '-data')
      .sort({ _id: 1 })
      .exec();
    return docs.map(doc => this.toDto(doc));
  }

  async findById(id: Types.ObjectId) {
    return this.toDto(await this.quickstartProjectModel.findById(id).exec());
  }

  async findByShareToken(shareToken: string) {
    return this.toDto(await this.quickstartProjectModel.findOne({ shareToken }).exec());
  }

  async update(id: Types.ObjectId, project: UpdateQuickstartProjectDto) {
    const result = await this.quickstartProjectModel
      .findByIdAndUpdate(id, project, { new: true, ...(!project.data && { projection: '-data' }) })
      .exec();
    return this.toDto(result);
  }

  async remove(id: Types.ObjectId) {
    const result = await this.quickstartProjectModel.deleteOne({ _id: id }).exec();
    if (result.deletedCount === 0) {
      throw new NotFoundException();
    }
  }

  async removeTagReferences(tagId: Types.ObjectId) {
    await this.quickstartProjectModel.updateMany({}, { $pull: { tags: tagId } }).exec();
  }

  private toDto(doc: QuickstartProjectDocument) {
    if (doc == null) return null;

    return plainToClass(QuickstartProjectDto, {
      ...doc.toObject({ getters: true }),
      shareUrl: `${this.baseUrl}/start?quickstart=${doc.shareToken ?? doc._id}`,
    });
  }
}
