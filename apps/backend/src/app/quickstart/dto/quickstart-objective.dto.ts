import {
  LocalizedString,
  QUICKSTART_OBJECTIVE_GROUP_BY,
  QUICKSTART_OBJECTIVE_SORT_BY,
  QuickstartObjective,
  QuickstartObjectiveGroupBy,
  QuickstartObjectiveList,
  QuickstartObjectiveSort,
  QuickstartObjectiveSortBy,
  SORT_DIRECTIONS,
  SortDirection,
} from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsOptional, IsEnum } from 'class-validator';
import { LocalizedStringDto } from '../../common/localized-string.dto';

@Exclude()
export class QuickstartObjectiveDto implements QuickstartObjective {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @Type(() => LocalizedStringDto)
  name: LocalizedStringDto;

  @Expose()
  @Type(() => LocalizedStringDto)
  comment: LocalizedString;

  @Expose()
  @Type(() => String)
  hierarchyId: string;

  @Expose()
  @Type(() => LocalizedStringDto)
  hierarchyName: LocalizedStringDto;

  @Expose()
  @Type(() => String)
  tags: string[];

  @Expose()
  level: number;
}

@Exclude()
export class QuickstartObjectiveListDto implements QuickstartObjectiveList {
  @Expose()
  @Type(() => QuickstartObjectiveDto)
  items: QuickstartObjectiveDto[];

  @Expose()
  @Type(() => String)
  activeTags: string[];

  @Expose()
  count: number;
}

export class QuickstartObjectiveGroupByDto {
  @IsOptional()
  @IsEnum(QUICKSTART_OBJECTIVE_GROUP_BY)
  groupBy?: QuickstartObjectiveGroupBy;
}

export class QuickstartObjectiveSortDto implements Partial<QuickstartObjectiveSort> {
  @IsOptional()
  @IsEnum(QUICKSTART_OBJECTIVE_SORT_BY)
  sortBy?: QuickstartObjectiveSortBy;

  @IsOptional()
  @IsEnum(SORT_DIRECTIONS)
  sortDirection?: SortDirection;
}
