import {
  QUICKSTART_OBJECTIVE_LEVEL_FILTERS,
  QuickstartObjectiveFilter,
  QuickstartObjectiveLevelFilter,
} from '@entscheidungsnavi/api-types';
import { Transform } from 'class-transformer';
import { IsOptional, IsString, IsArray, IsMongoId, IsEnum } from 'class-validator';
import { escapeRegExp } from 'lodash';

export class QuickstartObjectiveFilterDto implements QuickstartObjectiveFilter {
  @IsOptional()
  @IsString()
  @Transform(({ value }) => escapeRegExp(value))
  query?: string;

  @IsOptional()
  @IsArray()
  @IsMongoId({ each: true })
  @Transform(({ value }) => (Array.isArray(value) ? value : [value]))
  tags?: string[];

  @IsOptional()
  @IsEnum(QUICKSTART_OBJECTIVE_LEVEL_FILTERS)
  level?: QuickstartObjectiveLevelFilter;
}
