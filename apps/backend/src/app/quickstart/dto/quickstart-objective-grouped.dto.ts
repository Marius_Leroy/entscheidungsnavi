import {
  GroupedQuickstartObjective,
  GroupedQuickstartObjectiveList,
  GroupedQuickstartObjectiveListWithStats,
  GroupedQuickstartObjectiveWithStats,
} from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { QuickstartObjectiveDto } from './quickstart-objective.dto';
import { QuickstartObjectiveWithStatsDto } from './quickstart-objective-stats.dto';

@Exclude()
export class GroupedQuickstartObjectiveDto implements GroupedQuickstartObjective {
  @Expose({ name: '_id' })
  name: string;

  @Expose()
  @Type(() => QuickstartObjectiveDto)
  items: QuickstartObjectiveDto[];
}

@Exclude()
export class GroupedQuickstartObjectiveListDto implements GroupedQuickstartObjectiveList {
  @Expose()
  @Type(() => GroupedQuickstartObjectiveDto)
  items: GroupedQuickstartObjectiveDto[];

  @Expose()
  @Type(() => String)
  activeTags: string[];

  @Expose()
  count: number;
}

@Exclude()
export class GroupedQuickstartObjectiveWithStatsDto extends GroupedQuickstartObjectiveDto implements GroupedQuickstartObjectiveWithStats {
  @Expose()
  @Type(() => QuickstartObjectiveWithStatsDto)
  override items: QuickstartObjectiveWithStatsDto[];

  @Expose()
  accumulatedScore: number;
}

@Exclude()
export class GroupedQuickstartObjectiveListWithStatsDto implements GroupedQuickstartObjectiveListWithStats {
  @Expose()
  @Type(() => GroupedQuickstartObjectiveWithStatsDto)
  items: GroupedQuickstartObjectiveWithStatsDto[];

  @Expose()
  @Type(() => String)
  activeTags: string[];

  @Expose()
  count: number;
}
