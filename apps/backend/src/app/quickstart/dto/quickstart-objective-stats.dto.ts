import { QuickstartObjectiveWithStats, QuickstartObjectiveListWithStats } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { QuickstartObjectiveDto, QuickstartObjectiveListDto } from './quickstart-objective.dto';

@Exclude()
export class QuickstartObjectiveWithStatsDto extends QuickstartObjectiveDto implements QuickstartObjectiveWithStats {
  @Expose()
  accumulatedScore: number;
}

@Exclude()
export class QuickstartObjectiveListWithStatsDto extends QuickstartObjectiveListDto implements QuickstartObjectiveListWithStats {
  @Expose()
  @Type(() => QuickstartObjectiveWithStatsDto)
  override items: QuickstartObjectiveWithStatsDto[];
}
