import { Controller, Get, HttpCode, HttpStatus, InternalServerErrorException, Post } from '@nestjs/common';

@Controller()
export class AppController {
  @HttpCode(HttpStatus.NO_CONTENT)
  @Get('health-check')
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  healthCheck() {}

  @Post('create-error')
  errorCheck() {
    throw new InternalServerErrorException('This is a meaningless test error. No actual problem occurred.');
  }
}
