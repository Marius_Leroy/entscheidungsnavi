import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Worksheet } from 'exceljs';
import { range } from 'lodash';
import { KlugPairComparisons } from '@entscheidungsnavi/decision-data/klug';
import {
  addAlternativeExplanationColumns,
  addAlternativeUtilityValues,
  addModelNames,
  addObjectiveExplanationColumns,
  addObjectiveWeightColumns,
} from '../../common/excel/project-columns';
import { TableBuilder } from '../../common/excel/table-builder';
import { KlugProjectDocument } from '../../schemas/klug-project.schema';

export type KlugLoadedProject = Omit<KlugProjectDocument, 'data'> & {
  projectData: DecisionData;
  pairComparisons?: KlugPairComparisons;
};

export function createKlugExportBuilder(
  ws: Worksheet,
  parameters: {
    alternativeCount: number;
    objectiveCount: number;
  }
) {
  const builder = new TableBuilder<KlugLoadedProject>(ws);

  builder.addColumns(
    { name: 'Token', data: s => s.token },
    { name: 'Erstellzeitpunkt', data: s => s.updatedAt },
    { name: 'Entscheidungsfrage', data: s => s.projectData.decisionStatement.statement }
  );

  addModelNames(builder, {
    alternativeCount: parameters.alternativeCount,
    objectiveCount: parameters.objectiveCount,
    influenceFactorCount: 0,
  });

  addObjectiveExplanationColumns(builder, parameters.objectiveCount);
  addAlternativeExplanationColumns(builder, parameters.alternativeCount);

  addObjectiveWeightColumns(builder, parameters.objectiveCount);
  addAlternativeUtilityValues(builder, parameters.alternativeCount);

  builder.addGroup(
    'Paarvergleiche',
    range(parameters.objectiveCount - 1, 0, -1).flatMap(columnIndex =>
      range(parameters.objectiveCount - columnIndex).map(rowIndex => {
        const adjustedColumnIndex = columnIndex + rowIndex;
        return { name: `Paar A${rowIndex + 1}/A${adjustedColumnIndex + 1}`, data: s => s.pairComparisons[rowIndex][adjustedColumnIndex] };
      })
    )
  );

  return builder;
}
