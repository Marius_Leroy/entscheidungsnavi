import { KlugProject } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

@Exclude()
export class KlugProjectDto implements KlugProject {
  @Expose()
  @Type(() => String)
  id: string;

  @IsString()
  @Expose()
  data: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  token: string;

  @IsBoolean()
  @Expose()
  isOfficial: boolean;

  @IsBoolean()
  @Expose()
  finished: boolean;

  @IsBoolean()
  @Expose()
  readonly: boolean;

  @Expose()
  expiresAt?: Date;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}
