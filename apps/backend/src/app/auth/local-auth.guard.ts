import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

@Injectable()
export class LocalAuthGuard extends AuthGuard('local') {
  override async canActivate(context: ExecutionContext): Promise<boolean> {
    // check email and password
    if (!(await super.canActivate(context))) return false;

    // initialize the session
    const request = context.switchToHttp().getRequest<Request>();
    await super.logIn(request);

    // if no exceptions were thrown, allow the access to the route
    return true;
  }
}
