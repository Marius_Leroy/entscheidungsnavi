import { PassportSerializer } from '@nestjs/passport';
import { Types } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { UserDocument } from '../schemas/user.schema';

@Injectable()
export class LocalSerializer extends PassportSerializer {
  constructor(private usersService: UsersService) {
    super();
  }

  override serializeUser(user: UserDocument, done: CallableFunction) {
    // Save the user ID in the session
    done(null, user.id);
  }

  override async deserializeUser(userId: string, done: CallableFunction) {
    // Get the user data from the user ID that is saved in the session
    const user: UserDocument = await this.usersService.findOne(new Types.ObjectId(userId));
    // Save the user in the request object. Note that it might be null if the user could not be found.
    done(null, user);
  }
}
