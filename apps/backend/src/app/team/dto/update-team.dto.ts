import { PartialType, PickType } from '@nestjs/mapped-types';
import { TeamDto } from './team.dto';

export class UpdateTeamDto extends PartialType(PickType(TeamDto, ['name', 'editor', 'whiteboard'] as const)) {}
