import { PickType } from '@nestjs/mapped-types';
import { TeamInviteDto } from './team-invite.dto';

export class CreateInviteDto extends PickType(TeamInviteDto, ['email']) {}
