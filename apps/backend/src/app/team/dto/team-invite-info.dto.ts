import { TeamInviteInfo } from '@entscheidungsnavi/api-types';
import { Exclude, Expose } from 'class-transformer';
import { IsString } from 'class-validator';

@Exclude()
export class TeamInviteInfoDto implements TeamInviteInfo {
  @Expose()
  @IsString()
  teamName: string;

  @Expose()
  @IsString()
  inviterName: string;
}
