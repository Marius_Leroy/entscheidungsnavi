import { Exclude, Expose, Type } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import { TeamComment as ApiTeamComment } from '@entscheidungsnavi/api-types';

@Exclude()
export class TeamCommentDto implements ApiTeamComment {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @IsNotEmpty()
  objectId: string;

  @IsNotEmpty()
  @Expose()
  @Type(() => String)
  author: string;

  @IsNotEmpty()
  @Expose()
  content: string;

  @Expose()
  createdAt: Date;
}
