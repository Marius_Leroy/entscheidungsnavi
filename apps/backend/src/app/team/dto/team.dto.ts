import { Exclude, Expose, Type } from 'class-transformer';
import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Team as ApiTeam } from '@entscheidungsnavi/api-types';
import { TeamMemberDto } from './team-member.dto';
import { TeamInviteDto } from './team-invite.dto';

@Exclude()
export class TeamDto implements ApiTeam {
  @Expose()
  @Type(() => String)
  id: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @Expose()
  @Type(() => TeamMemberDto)
  @ValidateNested({ each: true })
  members: TeamMemberDto[];

  @Expose()
  @Type(() => TeamInviteDto)
  @ValidateNested({ each: true })
  pendingInvites: TeamInviteDto[];

  @Expose()
  @IsString()
  @Type(() => String)
  owner: string;

  @Expose()
  @IsString()
  @Type(() => String)
  editor: string;

  @IsString()
  @Expose()
  whiteboard: string;

  @Expose()
  createdAt: Date;
}
