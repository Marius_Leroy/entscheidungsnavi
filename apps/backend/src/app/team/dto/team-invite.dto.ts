import { Exclude, Expose } from 'class-transformer';
import { IsString } from 'class-validator';
import { TeamInvite as ApiTeamInvite } from '@entscheidungsnavi/api-types';

@Exclude()
export class TeamInviteDto implements ApiTeamInvite {
  @Expose()
  @IsString()
  email: string;

  @Expose()
  @IsString()
  token: string;

  @Expose()
  createdAt: Date;
}
