import { PickType } from '@nestjs/mapped-types';
import { TeamCommentDto } from './team-comment.dto';

export class AddTeamCommentDto extends PickType(TeamCommentDto, ['objectId', 'content']) {}
