import { TeamPartialUserInfo as APITeamPartialUserInfo } from '@entscheidungsnavi/api-types';
import { PickType } from '@nestjs/mapped-types';
import { Exclude } from 'class-transformer';
import { UserDto } from '../../users-api/dto/user.dto';

@Exclude()
export class PartialUserInfoDto extends PickType(UserDto, ['id', 'email', 'name']) implements APITeamPartialUserInfo {}
