import { Language } from '@entscheidungsnavi/api-types';
import { Body, Controller, Delete, Get, Param, Patch, Post, Req, UseGuards, UseInterceptors } from '@nestjs/common';
import { Types } from 'mongoose';
import { Lang } from '../common/language.decorator';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { UpdateProjectDto } from '../projects/dto/update-project.dto';
import { AuthenticatedRequest, LoginGuard } from '../auth/login.guard';
import { AddTeamCommentDto } from './dto/add-team-comment.dto';
import { CreateInviteDto as CreateTeamInviteDto } from './dto/create-invite.dto';
import { CreateTeamDto } from './dto/create-team.dto';
import { TeamTransferDto } from './dto/transfer.dto';
import { TeamsService } from './teams.service';
import { UpdateTeamDto } from './dto/update-team.dto';

@UseGuards(LoginGuard)
@Controller('teams')
export class TeamsController {
  constructor(private teamService: TeamsService) {}

  @Post()
  async create(@Req() request: AuthenticatedRequest, @Body() team: CreateTeamDto) {
    return await this.teamService.create(team.teamName, request.user._id, new Types.ObjectId(team.projectId));
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get(':id')
  async getOne(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) teamId: Types.ObjectId) {
    return await this.teamService.findById(teamId, request.user._id);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get('invites/:token')
  async getInviteInfo(@Param('token') token: string) {
    return await this.teamService.getInviteInfoFromToken(token);
  }

  @Post(':id/invite')
  async invite(
    @Req() request: AuthenticatedRequest,
    @Param('id', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Body() invite: CreateTeamInviteDto,
    @Lang() lang: Language
  ) {
    return await this.teamService.invite(teamId, request.user._id, invite.email, lang);
  }

  @Post('invites/:token')
  async join(@Req() request: AuthenticatedRequest, @Param('token') token: string) {
    return await this.teamService.joinUsingToken(request.user._id, token);
  }

  @Get(':teamId/:memberId/project')
  async getProject(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId
  ) {
    return await this.teamService.getTeamProject(teamId, memberId, request.user._id);
  }

  @Get(':teamId/:memberId/comments')
  async getComments(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId
  ) {
    return await this.teamService.getComments(teamId, memberId, request.user._id);
  }

  @Get(':teamId/:memberId/comments/object/:objectId')
  async getCommentsForObject(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Param('objectId') objectId: string
  ) {
    return await this.teamService.getCommentsForObject(teamId, memberId, objectId, request.user._id);
  }

  @Post(':teamId/:memberId/comments')
  async comment(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Body() comment: AddTeamCommentDto
  ) {
    return await this.teamService.comment(teamId, memberId, request.user._id, comment.objectId, comment.content);
  }

  @Delete(':teamId/:memberId/comments/:commentId')
  async deleteComment(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Param('commentId', ParseMongoIdPipe) commentId: Types.ObjectId
  ) {
    return await this.teamService.deleteComment(teamId, memberId, request.user._id, commentId);
  }

  @Post(':teamId/:memberId/transfer')
  async transferObject(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Param('memberId', ParseMongoIdPipe) memberId: Types.ObjectId,
    @Body() transfer: TeamTransferDto
  ) {
    return await this.teamService.transferObject(teamId, memberId, request.user._id, transfer.objectId);
  }

  @Patch(':teamId')
  async updateTeam(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Body() update: UpdateTeamDto
  ) {
    return await this.teamService.updateTeam(teamId, update, request.user._id);
  }

  @Patch(':teamId/mainproject')
  async updateMainProject(
    @Req() request: AuthenticatedRequest,
    @Param('teamId', ParseMongoIdPipe) teamId: Types.ObjectId,
    @Body() update: UpdateProjectDto
  ) {
    return await this.teamService.updateMainProject(teamId, update, request.user._id);
  }
}
