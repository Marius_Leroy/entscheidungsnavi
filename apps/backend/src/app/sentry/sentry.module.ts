import { Module } from '@nestjs/common';
import * as Sentry from '@sentry/node';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { EnvironmentType } from '../app.module';
import { SentryService } from './sentry.service';
import { SentryInterceptor } from './sentry.interceptor';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { version } = require('../../../../../package.json');

@Module({
  providers: [
    SentryService,
    {
      provide: APP_INTERCEPTOR,
      useClass: SentryInterceptor,
    },
  ],
  exports: [SentryService],
})
export class SentryModule {
  constructor(configService: ConfigService) {
    Sentry.init({
      dsn: configService.get<string>('SENTRY_DSN'),
      release: version,
      environment: configService.get<EnvironmentType>('ENVIRONMENT_TYPE'),
      integrations: [
        new Sentry.Integrations.Mongo({
          useMongoose: true,
        }),
      ],
      tracesSampleRate: 1.0,
      beforeSend: event => {
        // Make sure there are no passwords in the transmitted data. To this end, we look for the key password
        // in the request body and delete the whole body if it exists.
        const data = event.request?.data;

        if (typeof data !== 'string' || data.toLowerCase().includes('password')) {
          delete event.request.data;
        }

        return event;
      },
    });
  }
}
