import { CallHandler, ExecutionContext, HttpException, Injectable, NestInterceptor, Scope } from '@nestjs/common';
import { finalize, Observable, tap } from 'rxjs';
import * as Sentry from '@sentry/node';
import { SentryService } from './sentry.service';

/**
 * We must be in Request scope as we inject SentryService
 */
@Injectable({ scope: Scope.REQUEST })
export class SentryInterceptor implements NestInterceptor {
  constructor(private sentryService: SentryService) {}

  intercept(_context: ExecutionContext, next: CallHandler): Observable<any> {
    // start a child span for performance tracing
    const span = this.sentryService.startChild({ op: `route handler` });

    return next.handle().pipe(
      tap({
        error: error => {
          // Filter out Http Request errors
          if (error instanceof HttpException && error.getStatus() < 500) return;

          Sentry.captureException(error, this.sentryService.span.getTraceContext());
        },
      }),
      finalize(() => {
        span.finish();
        this.sentryService.span.finish();
      })
    );
  }
}
