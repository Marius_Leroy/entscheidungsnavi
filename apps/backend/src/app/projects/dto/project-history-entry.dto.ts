import { ProjectHistoryEntry } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';

@Exclude()
export class ProjectHistoryEntryDto implements ProjectHistoryEntry {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  resolvesConflict: boolean;

  @Expose()
  versionTimestamp: Date;
}
