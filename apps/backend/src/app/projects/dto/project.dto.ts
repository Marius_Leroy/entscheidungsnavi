import { Exclude, Expose, Type } from 'class-transformer';
import { IsNotEmpty, IsString } from 'class-validator';
import { Project as ApiProject, ProjectWithData } from '@entscheidungsnavi/api-types';
import { ProjectTeamInfoDto } from './project-team-info.dto';

@Exclude()
export class ProjectDto implements ApiProject {
  @Expose()
  @Type(() => String)
  id: string;

  @IsNotEmpty()
  @IsString()
  @Expose()
  name: string;

  @Expose()
  @Type(() => String)
  userId: string;

  @Expose()
  shareToken?: string;

  @Expose()
  @Type(() => ProjectTeamInfoDto)
  team?: ProjectTeamInfoDto;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}

@Exclude()
export class ProjectWithDataDto extends ProjectDto implements ProjectWithData {
  @Expose()
  @IsString()
  data: string;
}
