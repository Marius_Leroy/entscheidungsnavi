import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ProjectsService } from './projects.service';

@Controller('shared')
export class SharedController {
  constructor(private projectsService: ProjectsService) {}

  @UseInterceptors(NotFoundInterceptor)
  @Get(':token')
  async getFromToken(@Param('token') token: string) {
    return await this.projectsService.findByShareToken(token);
  }
}
