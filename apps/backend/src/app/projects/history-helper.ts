import * as zlib from 'zlib';

export function compressPatch(patchString: string) {
  return zlib.brotliCompressSync(patchString, {
    params: {
      [zlib.constants.BROTLI_PARAM_QUALITY]: 11,
      [zlib.constants.BROTLI_PARAM_MODE]: zlib.constants.BROTLI_MODE_TEXT,
    },
  });
}

export function decompressPatch(patch: Buffer) {
  return zlib.brotliDecompressSync(patch).toString('utf-8');
}
