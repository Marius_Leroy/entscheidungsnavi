import { Body, Controller, Delete, Get, Param, Patch, Post, Req, UseGuards, UseInterceptors } from '@nestjs/common';
import { Types } from 'mongoose';
import { NotFoundInterceptor } from '../common/not-found.interceptor';
import { ParseMongoIdPipe } from '../common/parse-mogo-id.pipe';
import { AuthenticatedRequest, LoginGuard } from '../auth/login.guard';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { ProjectsService } from './projects.service';

@UseGuards(LoginGuard)
@Controller('projects')
export class ProjectsController {
  constructor(private projectsService: ProjectsService) {}

  @Post()
  async create(@Body() createProjectDto: CreateProjectDto, @Req() req: AuthenticatedRequest) {
    return await this.projectsService.create(req.user._id, createProjectDto);
  }

  @Get()
  async list(@Req() request: AuthenticatedRequest) {
    return await this.projectsService.list(request.user._id);
  }

  @UseInterceptors(NotFoundInterceptor)
  @Get(':id')
  async getOne(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    if (request.user.roles.includes('admin')) {
      return await this.projectsService.findByIdAdmin(id);
    } else {
      return await this.projectsService.findById(request.user._id, id);
    }
  }

  @Get(':id/history')
  async getHistory(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.projectsService.getHistory(request.user._id, id);
  }

  @Get(':id/history/:entryId/data')
  async getDataFromHistory(
    @Req() request: AuthenticatedRequest,
    @Param('id', ParseMongoIdPipe) id: Types.ObjectId,
    @Param('entryId', ParseMongoIdPipe) entryId: Types.ObjectId
  ) {
    return await this.projectsService.getDataFromHistory(request.user._id, id, entryId);
  }

  @Patch(':id')
  update(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId, @Body() updateDto: UpdateProjectDto) {
    return this.projectsService.update(request.user._id, id, updateDto);
  }

  @Delete(':id')
  async del(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.projectsService.deleteOne(request.user._id, id);
  }

  @Post(':id/share')
  async enableShare(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    return await this.projectsService.enableShare(request.user._id, id);
  }

  @Delete(':id/share')
  async disableShare(@Req() request: AuthenticatedRequest, @Param('id', ParseMongoIdPipe) id: Types.ObjectId) {
    await this.projectsService.disableShare(request.user._id, id);
  }
}
