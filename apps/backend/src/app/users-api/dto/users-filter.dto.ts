import { Role, ROLES, UserFilter } from '@entscheidungsnavi/api-types';
import { Transform } from 'class-transformer';
import { IsBoolean, IsEnum, IsOptional, IsString } from 'class-validator';
import { escapeRegExp } from 'lodash';

export class UsersFilterDto implements UserFilter {
  @IsOptional()
  @IsString()
  @Transform(({ value }) => escapeRegExp(value))
  query?: string;

  @IsOptional()
  @IsBoolean()
  @Transform(({ value }) => value === 'true')
  emailConfirmed?: boolean;

  @IsOptional()
  @IsEnum(ROLES, { each: true })
  roles?: Role[];
}
