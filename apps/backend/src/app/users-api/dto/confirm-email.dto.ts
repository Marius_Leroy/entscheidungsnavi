import { Length } from 'class-validator';

export class ConfirmEmailDto {
  @Length(6, 6)
  token: string;
}
