import { PickType } from '@nestjs/mapped-types';
import { MinLength } from 'class-validator';
import { UserDto } from './user.dto';

export class CreateUserDto extends PickType(UserDto, ['name', 'email']) {
  @MinLength(8)
  password: string;
}
