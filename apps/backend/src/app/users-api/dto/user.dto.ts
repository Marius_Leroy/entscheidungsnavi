import { Exclude, Expose, Type } from 'class-transformer';
import { IsEmail, IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Role, ROLES, User as ApiUser } from '@entscheidungsnavi/api-types';

@Exclude()
export class UserDto implements ApiUser {
  @Expose()
  @Type(() => String)
  id: string;

  @IsEmail()
  @Expose()
  email: string;

  @Expose()
  emailConfirmed: boolean;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @Expose()
  name?: string;

  @Expose()
  @IsEnum(ROLES, { each: true })
  roles: Role[];

  @Expose()
  createdAt: Date;
}
