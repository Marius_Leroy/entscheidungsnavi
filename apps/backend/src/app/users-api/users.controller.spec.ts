import { Role } from '@entscheidungsnavi/api-types';
import {
  CanActivate,
  ClassSerializerInterceptor,
  ExecutionContext,
  INestApplication,
  Injectable,
  mixin,
  ValidationPipe,
} from '@nestjs/common';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { Test } from '@nestjs/testing';
import { Types } from 'mongoose';
import request from 'supertest';
import { ProjectsService } from '../projects/projects.service';
import { UsersController } from './users.controller';
import { UsersApiService } from './users-api.service';

const userId = new Types.ObjectId();

jest.mock('../auth/roles.guard.ts', () => ({
  RolesGuard: jest.fn().mockImplementation((_roles: Role[]) => {
    @Injectable()
    class RolesGuardMock implements CanActivate {
      canActivate(context: ExecutionContext) {
        const req = context.switchToHttp().getRequest();
        req.user = { _id: userId, roles: ['admin'] };
        return true;
      }
    }

    return mixin(RolesGuardMock);
  }),
}));

describe('UsersController', () => {
  let app: INestApplication;
  let createUser: jest.Mock;
  let getUser: jest.Mock;
  let updateRoles: jest.Mock;
  let findUsers: jest.Mock;

  beforeEach(async () => {
    createUser = jest.fn();
    getUser = jest.fn();
    updateRoles = jest.fn();
    findUsers = jest.fn();

    const module = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        { provide: UsersApiService, useValue: { create: createUser, updateRoles, getOne: getUser, findUsers } },
        { provide: ProjectsService, useValue: {} },
        {
          provide: APP_PIPE,
          useValue: new ValidationPipe({ whitelist: true, transform: true, transformOptions: { exposeUnsetFields: false } }),
        },
        { provide: APP_INTERCEPTOR, useValue: ClassSerializerInterceptor },
      ],
    }).compile();

    app = module.createNestApplication();
    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  describe('GET /users', () => {
    beforeEach(() => {
      findUsers.mockResolvedValue([]);
    });

    it('escapes the query string correctly', async () => {
      const filter = { query: 'abcd [ ] * + ? { } . ( ) ^ $ | - \\' };

      await request(app.getHttpServer()).get('/users').query(filter).expect(200);

      expect(findUsers.mock.calls[0][0]).toMatchObject({ query: 'abcd \\[ \\] \\* \\+ \\? \\{ \\} \\. \\( \\) \\^ \\$ \\| - \\\\' });
    });
  });

  describe('POST /users', () => {
    const createdUserId = new Types.ObjectId();
    const createdUser = { id: createdUserId, email: 'test@entscheidungsnavi.de', emailConfirmed: false };
    const createdUserDto = { ...createdUser, id: createdUserId.toString() };

    beforeEach(() => {
      createUser.mockResolvedValue(createdUser);
    });

    it('creates a user', async () => {
      const requestUser = { email: 'test@entscheidungsnavi.de', password: '12345678' };

      await request(app.getHttpServer()).post('/users').send(requestUser).expect(201).expect(createdUserDto);

      expect(createUser.mock.calls[0][0]).toMatchObject(requestUser);
    });

    it('fails on invalid email', () => {
      return request(app.getHttpServer()).post('/users').send({ email: 'thisisnotanemail', password: '12345678' }).expect(400);
    });

    it('fails on short password', () => {
      return request(app.getHttpServer()).post('/users').send({ email: 'test@entscheidungsnavi.de', password: '123' }).expect(400);
    });

    it('fails on empty name', () => {
      return request(app.getHttpServer())
        .post('/users')
        .send({ email: 'test@entscheidungsnavi.de', name: '', password: '123' })
        .expect(400);
    });
  });

  describe('GET /users/:id', () => {
    it('returns user', () => {
      const user = { id: userId, email: 'test@entscheidungsnavi.de', emailConfirmed: false };
      getUser.mockResolvedValueOnce(user);

      return request(app.getHttpServer())
        .get(`/users/${user.id.toString()}`)
        .expect({ ...user, id: userId.toString() });
    });
  });

  describe('PATCH /users/:id', () => {
    let targetUserId: Types.ObjectId;

    beforeEach(() => {
      updateRoles.mockResolvedValue(null);
      targetUserId = new Types.ObjectId();
    });

    it('updates roles correctly', async () => {
      await request(app.getHttpServer())
        .patch(`/users/${targetUserId.toString()}`)
        .send({ roles: ['admin'] })
        .expect(200);

      expect(updateRoles.mock.calls.length).toBe(1);
      expect(updateRoles.mock.calls[0][0]).toEqual(targetUserId);
      expect(updateRoles.mock.calls[0][1]).toEqual(['admin']);
    });

    it('allows clearing roles', async () => {
      await request(app.getHttpServer()).patch(`/users/${targetUserId.toString()}`).send({ roles: [] }).expect(200);

      expect(updateRoles.mock.calls.length).toBe(1);
      expect(updateRoles.mock.calls[0][0]).toEqual(targetUserId);
      expect(updateRoles.mock.calls[0][1]).toEqual([]);
    });

    it('throws if an admin wants to remove his own admin rights', async () => {
      await request(app.getHttpServer()).patch(`/users/${userId.toString()}`).send({ roles: [] }).expect(403);
      expect(updateRoles.mock.calls.length).toBe(0);
    });

    it('fails on missing roles', async () => {
      await request(app.getHttpServer()).patch(`/users/${userId.toString()}`).send({}).expect(400);
    });
  });
});
