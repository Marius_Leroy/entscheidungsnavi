import { getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { Types } from 'mongoose';
import { ProjectsService } from '../projects/projects.service';
import { EventRegistration } from '../schemas/events/event-registration.schema';
import { EventsService } from './events.service';
import { EventRegistrationsService } from './event-registrations.service';

describe('EventRegistrationsService', () => {
  let eventRegistrationsService: EventRegistrationsService;
  let modelExec: jest.Mock;
  let findEvent: jest.Mock;
  let findProject: jest.Mock;

  beforeEach(async () => {
    modelExec = jest.fn();
    findEvent = jest.fn();
    findProject = jest.fn();

    const module = await Test.createTestingModule({
      providers: [
        {
          provide: getModelToken(EventRegistration.name),
          useValue: {
            find: jest.fn().mockReturnThis(),
            findOne: jest.fn().mockReturnThis(),
            findOneAndUpdate: jest.fn().mockReturnThis(),
            updateOne: jest.fn().mockReturnThis(),
            populate: jest.fn().mockReturnThis(),
            exec: modelExec,
          },
        },
        {
          provide: EventsService,
          useValue: {
            findById: findEvent,
          },
        },
        { provide: ProjectsService, useValue: { findById: findProject } },
        EventRegistrationsService,
      ],
    }).compile();

    eventRegistrationsService = module.get(EventRegistrationsService);
  });

  describe('listSubmissions()', () => {
    it('removes unreleased questionnaires', async () => {
      modelExec.mockResolvedValue([
        {
          id: new Types.ObjectId(),
          event: {
            questionnaire: [
              {
                entries: [{ entryType: 'heading' }],
              },
            ],
            questionnaireReleased: false,
            freeTextConfig: { released: false },
          },
        },
      ]);

      const result = await eventRegistrationsService.listSubmissions(new Types.ObjectId());
      expect(result[0].event.questionnaire).toBeNull();
    });
  });

  describe('updateSubmission()', () => {
    beforeEach(() => {
      findEvent.mockResolvedValue({
        questionnaireReleased: true,
        questionnaire: [],
        freeTextConfig: { enabled: true },
      });
    });

    it('runs without errors', async () => {
      modelExec.mockResolvedValue({ questionnaireResponses: null, freeText: null, submitted: false });

      const result = await eventRegistrationsService.updateSubmission(new Types.ObjectId(), new Types.ObjectId(), {
        freeText: 'text',
        questionnaireResponses: [],
      });
      expect(result).toBeTruthy();
    });

    it('throws on finalized submission', async () => {
      modelExec.mockResolvedValue({ submitted: true });
      await expect(eventRegistrationsService.updateSubmission(new Types.ObjectId(), new Types.ObjectId(), {})).rejects.toThrow();
    });
  });

  describe('submit()', () => {
    beforeEach(() => {
      modelExec.mockResolvedValue({
        user: new Types.ObjectId(),
        event: new Types.ObjectId(),
        submitted: false,
        projectData: 'fake project data',
        freeText: 'some text',
        questionnaireResponses: [
          [1, 'response'],
          [null, 'okay'],
        ],
      });
    });

    it('accepts a valid submission', async () => {
      findEvent.mockResolvedValue({
        freeTextConfig: {
          enabled: true,
          released: true,
          minLength: 4,
        },
        questionnaireReleased: true,
        questionnaire: [
          {
            entries: [
              { entryType: 'numberQuestion', caption: '' },
              { entryType: 'textQuestion', caption: '' },
            ],
          },
          {
            entries: [
              { entryType: 'textBlock', text: '', type: 'body' },
              { entryType: 'textQuestion', caption: '' },
            ],
          },
        ],
        startDate: new Date(new Date().getTime() - 1e6),
        endDate: new Date(new Date().getTime() + 1e6),
      });
      findProject.mockResolvedValue({ date: '' });

      await eventRegistrationsService.submit(new Types.ObjectId(), new Types.ObjectId());
    });

    it('checks the start date', async () => {
      // Return a start date in the future
      findEvent.mockResolvedValue({ startDate: new Date(new Date().getTime() + 1e6) });
      await expect(eventRegistrationsService.submit(new Types.ObjectId(), new Types.ObjectId())).rejects.toThrow();
    });

    it('checks the end date', async () => {
      // Return a start date in the future
      findEvent.mockResolvedValue({ endDate: new Date(new Date().getTime() - 1e6) });
      await expect(eventRegistrationsService.submit(new Types.ObjectId(), new Types.ObjectId())).rejects.toThrow();
    });
  });
});
