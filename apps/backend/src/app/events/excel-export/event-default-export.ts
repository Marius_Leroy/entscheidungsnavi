import { EventDefaultExportAttribute } from '@entscheidungsnavi/api-types';
import { Workbook } from 'exceljs';
import { EventDocument } from '../../schemas/events/event.schema';
import { createEventExportBuilder, EventSubmission } from './event-columns';
import { createErrorsWorksheet, createMetadataWorksheet, loadProjectData, PopulatedEventRegistration } from './event-export-helper';

export interface EventExcelExportParameters {
  alternativeLimit?: number;
  objectiveLimit?: number;
  influenceFactorLimit?: number;
  attributes: EventDefaultExportAttribute[];
  naviBaseUrl: string;
}

/**
 * Generate an Excel Export of the corresponding event submissions.
 *
 * @param events - The events that are part of this export.
 * @param eventSubmissions - All submissions that are part of this export.
 *                           Only submitted registrations must be included. `user` must be populated.
 * @param parameters - Settings for the export.
 * @returns The Excel workbook
 */
export async function createDefaultExport(
  events: EventDocument[],
  eventSubmissions: PopulatedEventRegistration[],
  parameters: EventExcelExportParameters
) {
  // Import all decision data objects
  const [loadedProjectSubmissions, importErrors] = loadProjectData(eventSubmissions);

  // Calculate how many of each to display
  const maxAlternativeCount = Math.max(...loadedProjectSubmissions.map(s => s.projectData.alternatives.length), 0);
  const maxObjectiveCount = Math.max(...loadedProjectSubmissions.map(s => s.projectData.objectives.length), 0);
  const maxInfluenceFactorCount = Math.max(...loadedProjectSubmissions.map(s => s.projectData.influenceFactors.length), 0);
  const displayedAlternativeCount = parameters.alternativeLimit ?? maxAlternativeCount;
  const displayedObjectiveCount = parameters.objectiveLimit ?? maxObjectiveCount;
  const displayedInfluenceFactorCount = parameters.influenceFactorLimit ?? maxInfluenceFactorCount;

  // Create the workbook
  const wb = new Workbook();
  wb.created = new Date();

  createMetadataWorksheet(wb, events, eventSubmissions, parameters, null, importErrors.length);

  // Create a worksheet with the errors, if applicable
  if (importErrors.length > 0) {
    createErrorsWorksheet(wb, importErrors);
  }

  // Create the actual export worksheet, if applicable
  if (eventSubmissions.length > 0) {
    createExportWorksheet(wb, events, loadedProjectSubmissions, {
      alternativeCount: displayedAlternativeCount,
      objectiveCount: displayedObjectiveCount,
      influenceFactorCount: displayedInfluenceFactorCount,
      attributes: parameters.attributes,
      naviBaseUrl: parameters.naviBaseUrl,
    });
  }

  return wb;
}

function createExportWorksheet(
  wb: Workbook,
  events: EventDocument[],
  eventSubmissions: EventSubmission[],
  parameters: {
    alternativeCount: number;
    objectiveCount: number;
    influenceFactorCount: number;
    attributes: EventDefaultExportAttribute[];
    naviBaseUrl: string;
  }
) {
  const exportWs = wb.addWorksheet('Export', { properties: { defaultColWidth: 20 } });
  const table = createEventExportBuilder(exportWs, events, parameters).buildTable('Export', eventSubmissions);
  table.commit(); // Save the table
}
