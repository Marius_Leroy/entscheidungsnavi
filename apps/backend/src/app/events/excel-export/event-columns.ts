import { EventDefaultExportAttribute } from '@entscheidungsnavi/api-types';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { richToPlainText, richTextLength } from '@entscheidungsnavi/tools';
import { Worksheet } from 'exceljs';
import { Types } from 'mongoose';
import { EventRegistrationDocument } from '../../schemas/events/event-registration.schema';
import { EventDocument } from '../../schemas/events/event.schema';
import { User } from '../../schemas/user.schema';
import {
  addAlternativeExplanationColumns,
  addDecisionQualityColumns,
  addInfluenceFactorExplanationColumns,
  addModelNames,
  addModelParameterCountsColumns,
  addObjectiveExplanationColumns,
  addProcessStatusColumns,
  addProjectInformationColumns,
  addStepExplanationColumns,
  addTimeRecordingColumns,
} from '../../common/excel/project-columns';
import { TableBuilder, Column } from '../../common/excel/table-builder';

export type EventSubmission = Omit<EventRegistrationDocument, 'projectData' | 'event' | 'user'> & {
  projectData: DecisionData;
  event: Types.ObjectId;
  user?: User;
};

export function createEventExportBuilder(
  ws: Worksheet,
  events: EventDocument[],
  parameters: {
    alternativeCount: number;
    objectiveCount: number;
    influenceFactorCount: number;
    attributes: EventDefaultExportAttribute[];
    naviBaseUrl: string;
  }
) {
  const builder = new TableBuilder<EventSubmission>(ws);

  builder.addColumns(
    { name: 'Abgabe ID', data: s => s.id },
    { name: 'Benutzer ID', data: s => s.user?.id ?? '—' },
    { name: 'Benutzer Email', data: s => s.user?.email ?? '—' },
    { name: 'Projektlink', data: s => ({ text: 'Link', hyperlink: `${parameters.naviBaseUrl}/start?eventSubmission=${s.id}` }) },
    { name: 'Abgabezeitpunkt', data: s => s.updatedAt }
  );

  if (parameters.attributes.includes('projectInformation')) {
    addProjectInformationColumns(builder);
  }
  if (parameters.attributes.includes('modelParameterCounts')) {
    addModelParameterCountsColumns(builder);
  }
  if (parameters.attributes.includes('processStatus')) {
    addProcessStatusColumns(builder);
  }
  addModelNames(builder, parameters);
  if (parameters.attributes.includes('stepExplanations')) {
    addStepExplanationColumns(builder);
  }
  if (parameters.attributes.includes('objectiveExplanations')) {
    addObjectiveExplanationColumns(builder, parameters.objectiveCount);
  }
  if (parameters.attributes.includes('alternativeExplanations')) {
    addAlternativeExplanationColumns(builder, parameters.alternativeCount);
  }
  if (parameters.attributes.includes('influenceFactorExplanations')) {
    addInfluenceFactorExplanationColumns(builder, parameters.influenceFactorCount);
  }
  if (parameters.attributes.includes('decisionQuality')) {
    addDecisionQualityColumns(builder);
  }
  if (parameters.attributes.includes('timeRecording')) {
    addTimeRecordingColumns(builder);
  }

  if (parameters.attributes.includes('submissionQuestionnaire')) {
    if (events.length !== 1) {
      throw new Error('Can only export questionnaire for single event at a time');
    }
    addSubmissionQuestionnaireColumns(builder, events[0]);
  }
  if (parameters.attributes.includes('submissionFreeText')) {
    addSubmissionFreeTextColumns(builder);
  }

  return builder;
}

function addSubmissionQuestionnaireColumns(builder: TableBuilder<EventSubmission>, event: EventDocument) {
  const pages = event.questionnaire.map(page => page.entries);

  const groups: Array<[string, Column<EventSubmission>[]]> = [];

  pages.forEach((page, pageIndex) => {
    page.forEach((question, questionIndex) => {
      switch (question.entryType) {
        case 'numberQuestion':
        case 'textQuestion':
          groups.push([
            question.question,
            [
              {
                name: `Frage ${pageIndex}.${questionIndex}: ${question.label}`,
                data: s => s.questionnaireResponses[pageIndex][questionIndex],
              },
            ],
          ]);
          break;
        case 'optionsQuestion':
          groups.push([
            question.question +
              ` (1 = ${question.options[0]}, ${question.options.length} = ${question.options[question.options.length - 1]})`,
            [
              {
                name: `Frage ${pageIndex}.${questionIndex}: ${question.label}`,
                data: s => (s.questionnaireResponses[pageIndex][questionIndex] as number) + 1,
              },
            ],
          ]);
          break;
        case 'tableQuestion':
          groups.push([
            question.baseQuestion +
              ` (1 = ${question.options[0]}, ${question.options.length} = ${question.options[question.options.length - 1]})`,
            question.subQuestions.map((sq, sqIndex) => ({
              name: `Frage ${pageIndex}.${questionIndex}.${sqIndex}: ${sq}`,
              data: s => s.questionnaireResponses[pageIndex][questionIndex][sqIndex] + 1,
            })),
          ]);
          break;
      }
    });
  });

  builder.addGroupSequence('Abgabe Fragebogen', groups);
}

function addSubmissionFreeTextColumns(builder: TableBuilder<EventSubmission>) {
  builder.addGroup('Abgabe Freitext', [
    { name: 'Freitext', data: s => richToPlainText(s.freeText) },
    { name: '#Zeichen Freitext', data: s => richTextLength(s.freeText) },
  ]);
}
