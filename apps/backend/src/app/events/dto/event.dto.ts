import { Event, EventWithStats } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsBoolean, IsDate, IsNotEmpty, IsOptional, IsString, MinLength, ValidateNested } from 'class-validator';
import { PickType } from '@nestjs/mapped-types';
import { UserDto } from '../../users-api/dto/user.dto';
import { EventFreeTextConfigDto } from './event-free-text-config.dto';
import { EventProjectRequirementsDto } from './event-project-requirements.dto';
import { QuestionnairePageDto } from './event-questionnaire.dto';

@Exclude()
export class EventPermissionDto extends PickType(UserDto, ['email']) {}

@Exclude()
export class EventDto implements Event {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @IsNotEmpty()
  @IsString()
  name: string;

  @Expose()
  @Type(() => EventPermissionDto)
  @ValidateNested()
  owner: EventPermissionDto;

  @Expose()
  @Type(() => EventPermissionDto)
  @ValidateNested({ each: true })
  editors: EventPermissionDto[];

  @Expose()
  @IsOptional()
  @MinLength(3)
  code?: string;

  @Expose()
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  startDate?: Date;

  @Expose()
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  endDate?: Date;

  @Expose()
  @Type(() => EventProjectRequirementsDto)
  @ValidateNested()
  projectRequirements: EventProjectRequirementsDto;

  @Expose()
  @Type(() => QuestionnairePageDto)
  @ValidateNested({ each: true })
  questionnaire: QuestionnairePageDto[];

  @Expose()
  @IsBoolean()
  questionnaireReleased: boolean;

  @Expose()
  @Type(() => EventFreeTextConfigDto)
  @ValidateNested()
  freeTextConfig: EventFreeTextConfigDto;

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}

@Exclude()
export class EventWithStatsDto extends EventDto implements EventWithStats {
  @Expose()
  registrationCount: number;

  @Expose()
  submissionCount: number;
}
