import { EventRegistration, QuestionnaireEntryResponse } from '@entscheidungsnavi/api-types';
import { Exclude, Expose, Type } from 'class-transformer';
import { IsArray, IsBoolean, IsOptional, IsString } from 'class-validator';
import { UserEventDto } from './user-event.dto';

@Exclude()
export class EventRegistrationDto implements EventRegistration {
  @Expose()
  @Type(() => String)
  id: string;

  @Expose()
  @Type(() => UserEventDto)
  event?: UserEventDto;

  @Expose()
  @IsBoolean()
  submitted: boolean;

  @IsOptional()
  @IsString()
  projectData?: string;

  @Expose()
  @IsOptional()
  @IsString()
  freeText?: string;

  @Expose()
  @IsOptional()
  @IsArray()
  questionnaireResponses?: QuestionnaireEntryResponse[][];

  @Expose()
  createdAt: Date;

  @Expose()
  updatedAt: Date;
}
