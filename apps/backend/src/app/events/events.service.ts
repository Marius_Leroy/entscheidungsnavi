import { EventAltAndObjExportRequest, EventDefaultExportRequest, EventProjectsExportRequest } from '@entscheidungsnavi/api-types';
import { BadRequestException, ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { ClassConstructor, plainToClass } from 'class-transformer';
import { uniq } from 'lodash';
import { Model, Types } from 'mongoose';
import { EventRegistration, EventRegistrationDocument } from '../schemas/events/event-registration.schema';
import { Event, EventDocument } from '../schemas/events/event.schema';
import { User } from '../schemas/user.schema';
import { UsersService } from '../users/users.service';
import { CreateEventDto } from './dto/create-event.dto';
import { EventDto, EventPermissionDto, EventWithStatsDto } from './dto/event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { createDefaultExport } from './excel-export/event-default-export';
import { createProjectsExport } from './projects-export/projects-export';
import { createEventAltAndObjExport } from './excel-export/event-alt-and-obj-export';

function toDto(
  doc: Omit<EventDocument, 'owner' | 'editors'> & { owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[] },
  target: ClassConstructor<EventDto> | ClassConstructor<EventWithStatsDto> = EventDto
) {
  return plainToClass(target, doc);
}

@Injectable()
export class EventsService {
  private baseUrl: string;

  constructor(
    configService: ConfigService,
    private usersService: UsersService,
    @InjectModel(Event.name) private eventModel: Model<EventDocument>,
    @InjectModel(EventRegistration.name) private eventRegistrationModel: Model<EventRegistrationDocument>
  ) {
    this.baseUrl = configService.get<string>('BASE_URL');
  }

  async create(userId: Types.ObjectId, event: CreateEventDto) {
    const { owner, editors, ...remainingEvent } = event;

    const doc = await this.eventModel.create({
      ...remainingEvent,
      owner: owner != null ? (await this.loadUserIdsFromPermissions([owner]))[0] : userId,
      editors: editors != null ? await this.loadUserIdsFromPermissions(editors) : undefined,
    });

    return toDto(
      await doc.populate<{ owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[] }>([
        { path: 'owner', select: 'email' },
        { path: 'editors', select: 'email' },
      ])
    );
  }

  async update(userId: Types.ObjectId, eventId: Types.ObjectId, update: UpdateEventDto) {
    // Get the current event
    const event = await this.findById(eventId);
    if (event == null) {
      throw new NotFoundException();
    }

    // Ensure the user has sufficient permissions
    if (update.owner != null || update.editors != null) {
      if (!userId.equals(event.owner as Types.ObjectId)) {
        throw new ForbiddenException('only the owner can change permissions');
      }
    } else {
      if (!event.editors.some(editor => userId.equals(editor as Types.ObjectId)) && !userId.equals(event.owner as Types.ObjectId)) {
        throw new ForbiddenException('only the owner or editors may modify an event');
      }
    }

    // The user is trying to unrelease the questionnaire
    if (event.questionnaireReleased && update.questionnaireReleased === false) {
      throw new BadRequestException('The questionnaire cannot be unreleased');
    }

    // The user is trying to modify the questionnaire after it was released
    if (event.questionnaireReleased && update.questionnaire) {
      throw new BadRequestException('The questionnaire cannot be changed after it has been released');
    }

    // Perform the update
    const { owner, editors, ...remainingUpdate } = update;
    Object.assign(event, remainingUpdate);

    // Handle changes in permissions
    if (update.owner != null) {
      event.owner = (await this.loadUserIdsFromPermissions([owner]))[0];
    }
    if (update.editors != null) {
      event.editors = await this.loadUserIdsFromPermissions(editors);
    }

    await event.save();

    return toDto(
      await event.populate<{ owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[] }>([
        { path: 'owner', select: 'email' },
        { path: 'editors', select: 'email' },
      ])
    );
  }

  /**
   * Loads exactly one user ID for every entry in {@link permissions}.
   */
  private async loadUserIdsFromPermissions(permissions: EventPermissionDto[]): Promise<Types.ObjectId[]> {
    // Make sure there are no duplicates in the editors
    const emails = uniq(permissions.map(permission => permission.email));
    const userIds = (await this.usersService.findManyByEmail(emails)).map(user => user._id);

    if (userIds.length !== emails.length) throw new NotFoundException('some emails do not belong to a user');

    return userIds;
  }

  async listEvents(userId: Types.ObjectId) {
    const docs = await this.eventModel
      .aggregate([
        {
          $match: { $or: [{ owner: userId }, { editors: userId }] },
        },
        {
          $lookup: {
            from: 'eventregistrations',
            localField: '_id',
            foreignField: 'event',
            as: 'registrationCount',
          },
        },
        {
          $lookup: {
            from: 'eventregistrations',
            localField: '_id',
            foreignField: 'event',
            pipeline: [{ $match: { submitted: true } }],
            as: 'submissionCount',
          },
        },
        {
          $set: {
            registrationCount: { $size: '$registrationCount' },
            submissionCount: { $size: '$submissionCount' },
            id: {
              $toString: '$_id',
            },
          },
        },
        {
          $sort: { createdAt: -1 },
        },
      ])
      .exec();
    return (
      await this.eventModel.populate<{ owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[] }>(docs, [
        {
          path: 'owner',
          select: 'email',
        },
        { path: 'editors', select: 'email' },
      ])
    ).map(doc => toDto(doc, EventWithStatsDto));
  }

  async getOne(userId: Types.ObjectId, eventId: Types.ObjectId) {
    const doc = await this.eventModel
      .findOne({ _id: eventId, $or: [{ owner: userId }, { editors: userId }] })
      .populate<{ owner: Pick<User, 'email'>; editors: Pick<User, 'email'>[] }>([
        { path: 'owner', select: 'email' },
        { path: 'editors', select: 'email' },
      ])
      .exec();
    return toDto(doc);
  }

  async findById(eventId: Types.ObjectId) {
    return await this.eventModel.findOne({ _id: eventId }).exec();
  }

  async findByCode(code: string) {
    return await this.eventModel.findOne({ code: code.toLowerCase() }).exec();
  }

  async deleteOne(userId: Types.ObjectId, eventId: Types.ObjectId) {
    const res = await this.eventModel.deleteOne({ _id: eventId, $or: [{ owner: userId }, { editors: userId }] }).exec();

    if (res.deletedCount === 0) {
      throw new NotFoundException();
    }

    // Also delete all submissions
    await this.eventRegistrationModel.deleteMany({ event: eventId }).exec();
  }

  async getSubmittedProject(userId: Types.ObjectId, registrationId: Types.ObjectId) {
    const submission = await this.eventRegistrationModel
      .findOne({ _id: registrationId, submitted: true })
      .populate<{ event: Event }>('event');
    if (submission == null) {
      throw new NotFoundException();
    }

    if (
      !userId.equals(submission.event.owner as Types.ObjectId) &&
      (submission.event.editors as Types.ObjectId[]).find(id => userId.equals(id)) == null
    ) {
      throw new ForbiddenException();
    }

    return submission.projectData;
  }

  async generateDefaultExport(userId: Types.ObjectId, request: EventDefaultExportRequest) {
    if (request.eventIds.length > 1 && request.attributes.includes('submissionQuestionnaire')) {
      throw new BadRequestException('Can only export questionnaire when exporting a single event');
    }

    const events = await this.eventModel
      .find({
        _id: { $in: request.eventIds.map(id => new Types.ObjectId(id)) },
        $or: [{ owner: userId }, { editors: userId }],
      })
      .exec();

    if (events.length !== request.eventIds.length) {
      throw new NotFoundException('Some events could not be found. This might be due to lacking permissions.');
    }

    const submissions = await this.eventRegistrationModel
      .find({ event: { $in: events.map(event => event._id) }, submitted: true })
      .populate<{ user: User; event: Types.ObjectId }>('user')
      .sort({ _id: 1 })
      .exec();

    return await createDefaultExport(events, submissions, { ...request, naviBaseUrl: this.baseUrl });
  }

  async generateAltAndObjExport(userId: Types.ObjectId, request: EventAltAndObjExportRequest) {
    if (request.eventIds.length === 0) {
      throw new BadRequestException('Can only export template when exporting at least one event');
    }

    const events = await this.eventModel
      .find({
        _id: { $in: request.eventIds.map(id => new Types.ObjectId(id)) },
        $or: [{ owner: userId }, { editors: userId }],
      })
      .exec();

    if (events.length !== request.eventIds.length) {
      throw new NotFoundException('Some events could not be found. This might be due to lacking permissions.');
    }

    const submissions = await this.eventRegistrationModel
      .find({ event: { $in: events.map(event => event._id) }, submitted: true })
      .populate<{ user: User; event: Types.ObjectId }>('user')
      .sort({ _id: 1 })
      .exec();

    return await createEventAltAndObjExport(events, submissions, request.alternativeAttributes, request.objectiveAttributes);
  }

  async generateProjectsExport(userId: Types.ObjectId, request: EventProjectsExportRequest) {
    const eventIds = request.eventIds.map(id => new Types.ObjectId(id));

    // We need to load the events to check user permissions
    const eventCount = await this.eventModel
      .find({
        _id: { $in: eventIds },
        $or: [{ owner: userId }, { editors: userId }],
      })
      .countDocuments();

    if (eventCount !== request.eventIds.length) {
      throw new NotFoundException('Some events could not be found. This might be due to lacking permissions.');
    }

    const submissions = await this.eventRegistrationModel.find({ event: { $in: eventIds }, submitted: true }).exec();
    return await createProjectsExport(submissions);
  }
}
