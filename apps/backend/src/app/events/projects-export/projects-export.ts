import JSZip from 'jszip';
import { Types } from 'mongoose';
import { EventRegistrationDocument } from '../../schemas/events/event-registration.schema';

export async function createProjectsExport(eventSubmissions: EventRegistrationDocument[]) {
  const zip = new JSZip();

  eventSubmissions.forEach(submission => {
    zip.folder((submission.event as Types.ObjectId).toString()).file(`${submission.id}.json`, submission.projectData);
  });

  return await zip.generateAsync({ type: 'nodebuffer' });
}
