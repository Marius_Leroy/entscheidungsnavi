import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { LocalizedString } from '@entscheidungsnavi/api-types';
import { LocalizedStringSchema } from './common/localized-string.schema';

export type QuickstartTagDocument = QuickstartTag & Document;

@Schema({ timestamps: true })
export class QuickstartTag {
  readonly id: string;

  @Prop({ required: true, type: LocalizedStringSchema })
  name: LocalizedString;

  @Prop()
  weight?: number;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const QuickstartTagSchema = SchemaFactory.createForClass(QuickstartTag);
