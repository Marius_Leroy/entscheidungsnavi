import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { LocalizedString } from '@entscheidungsnavi/api-types';
import { LocalizedStringSchema } from './common/localized-string.schema';

@Schema({ timestamps: false })
export class QuickstartHierarchyNode {
  readonly id: string;

  @Prop({ required: true, type: LocalizedStringSchema })
  name: LocalizedString;

  @Prop({ required: true, type: LocalizedStringSchema })
  comment: LocalizedString;

  // What level of the tree the node is in (level 0 = root)
  @Prop({ required: true, min: 0 })
  level: number;

  // A (partial) count of how many times this node has been selected
  @Prop({ default: 0 })
  accumulatedScore: number;
}

export const QuickstartHierarchyNodeSchema = SchemaFactory.createForClass(QuickstartHierarchyNode);

/**
 * The hierarchy tree is flattened and saved as a list. See {@link @entscheidungsnavi/tools#FlattenedTree}
 * for details. This removes the deep nesting and enables us to directly execute queries on individual nodes using Mongo.
 *
 * @example
 * The tracking counter for one specific tree node can directly be increased with a single query.
 * ```
 * this.hierarchyModel.updateOne(
 *   {
 *     _id: new Types.ObjectId('649c2e2933c90ad7125df893'),
 *     'tree._id': new Types.ObjectId('649c2e2933c90ad7125df894'),
 *   },
 *   { $inc: { 'tree.$.accumulatedScore': 1 } }
 * );
 * ```
 */
@Schema({ timestamps: true })
export class QuickstartHierarchy {
  @Prop({ required: true, type: LocalizedStringSchema })
  name: LocalizedString;

  @Prop({ required: true, type: [{ type: Types.ObjectId, ref: 'QuickstartTag' }], default: (): Types.ObjectId[] => [] })
  tags: Types.Array<Types.ObjectId>;

  // A flattened version of the tree
  @Prop({ required: true, type: [{ type: QuickstartHierarchyNodeSchema }] })
  tree: Types.DocumentArray<QuickstartHierarchyNode>;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export type QuickstartHierarchyDocument = QuickstartHierarchy & Document<Types.ObjectId>;
export const QuickstartHierarchySchema = SchemaFactory.createForClass(QuickstartHierarchy)
  .index({ 'tree._id': 1 })
  .index({ tags: 1 })
  .index({ tags: 1, 'name.de': 1, 'name.en': 1 }, { collation: { locale: 'en' } })
  .index({ 'tree.name.de': 1, 'tree.name.en': 1 }, { collation: { locale: 'en' } });
