import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { LocalizedString } from '@entscheidungsnavi/api-types';
import { LocalizedStringSchema } from './common/localized-string.schema';

export type QuickstartValueDocument = QuickstartValue & Document;

@Schema({ timestamps: true })
export class QuickstartValue {
  readonly id: string;

  @Prop({ required: true, type: LocalizedStringSchema })
  name: LocalizedString;

  // How many times the value was in the top 5
  @Prop({ default: 0 })
  countInTop5: number;

  // How many times the value was assigned a weight at all
  @Prop({ default: 0 })
  countAssigned: number;

  // How many times the value was tracked, i.e., shown to a user and then tracked.
  // This includes countAssigned as well as all instances where the values was not assigned a weight (but others were).
  @Prop({ default: 0 })
  countTracked: number;

  // The sum of all scores ever assigned to that value
  @Prop({ default: 0 })
  accumulatedScore: number;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const QuickstartValueSchema = SchemaFactory.createForClass(QuickstartValue);
