import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type EventRegistrationDocument = EventRegistration & Document;

@Schema({ timestamps: true })
export class EventRegistration {
  readonly id: string;

  @Prop({ required: true, type: Types.ObjectId, ref: 'User' })
  user: Types.ObjectId;

  @Prop({ required: true, type: Types.ObjectId, ref: 'Event' })
  event: Types.ObjectId;

  @Prop({ default: false })
  submitted: boolean;

  @Prop()
  projectData?: string; // The actually submitted project data

  @Prop()
  freeText?: string;

  @Prop({ type: [[]] })
  questionnaireResponses?: (number | number[] | string | null)[][];

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export const EventRegistrationSchema = SchemaFactory.createForClass(EventRegistration);
// User/Event combination must be unique
EventRegistrationSchema.index({ user: 1, event: 1 }, { unique: true });
