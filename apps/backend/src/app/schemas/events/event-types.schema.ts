import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema({ _id: false })
export class EventProjectRequirements {
  @Prop({ default: false })
  requireDecisionStatement: boolean;

  @Prop({ default: false })
  requireObjectives: boolean;

  @Prop({ default: false })
  requireAlternatives: boolean;

  @Prop({ default: false })
  requireImpactModel: boolean;

  @Prop({ default: false })
  requireObjectiveWeights: boolean;

  @Prop({ default: false })
  requireDecisionQuality: boolean;
}

export const EventProjectRequirementsSchema = SchemaFactory.createForClass(EventProjectRequirements);

@Schema({ _id: false })
export class EventFreeTextConfig {
  @Prop({ default: false })
  enabled: boolean;

  @Prop({ default: '' })
  name: string;

  @Prop({ default: '' })
  placeholder: string;

  @Prop({ default: 0, min: 0 })
  minLength: number;
}

export const EventFreeTextConfigSchema = SchemaFactory.createForClass(EventFreeTextConfig);
