import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import {
  Alternative,
  DecisionStatement,
  Objective,
  ObjectiveInput,
  ObjectiveType,
  Weights,
} from '@entscheidungsnavi/decision-data/classes';
import { getDefaultValueFor, validateValue } from '@entscheidungsnavi/decision-data/validations';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class InterpolationService {
  private static readonly decisionStatementDefault = $localize`Wie soll ich mich entscheiden?`;

  private static interpolateDecisionStatement(decisionData: DecisionData): boolean {
    if (!decisionData.validateDecisionStatement()[0]) {
      decisionData.decisionStatement = new DecisionStatement(InterpolationService.decisionStatementDefault);

      return true;
    }

    return false;
  }

  private static interpolateObjectives(decisionData: DecisionData): boolean {
    let interpolated = false;

    if (decisionData.objectives.length === 0) {
      decisionData.addObjective(new Objective());
      interpolated = true;
    }

    decisionData.objectives.forEach((objective, objectiveIdx) => {
      if (!objective.name) {
        objective.name = $localize`Ziel` + ' ' + (objectiveIdx + 1);
        interpolated = true;
      }
    });

    return interpolated;
  }

  private static interpolateAlternatives(decisionData: DecisionData): boolean {
    let interpolated = false;

    while (decisionData.alternatives.length < 2) {
      decisionData.addAlternative({
        alternative: new Alternative(0, $localize`Alternative` + ' ' + (decisionData.alternatives.length + 1)),
      });

      interpolated = true;
    }

    decisionData.alternatives.forEach((alternative, i) => {
      if (!alternative.name) {
        alternative.name = $localize`Alternative` + ' ' + (i + 1);
        interpolated = true;
      }
    });

    return interpolated;
  }

  private static interpolateImpactModel(decisionData: DecisionData): boolean {
    let interpolated = false;

    for (let i = 0; i < decisionData.alternatives.length; i++) {
      for (let j = 0; j < decisionData.objectives.length; j++) {
        if (decisionData.outcomes[i][j].influenceFactor) {
          for (let k = 0; k < decisionData.outcomes[i][j].influenceFactor.states.length; k++) {
            const [maybeInterpolatedValue, valueInterpolated] = InterpolationService.interpolateValue(
              decisionData.outcomes[i][j].values[k],
              decisionData.objectives[j]
            );

            if (valueInterpolated) {
              decisionData.outcomes[i][j].values[k] = maybeInterpolatedValue;
              interpolated = true;
            }
          }
        } else {
          const [maybeInterpolatedValue, valueInterpolated] = InterpolationService.interpolateValue(
            decisionData.outcomes[i][j].values[0],
            decisionData.objectives[j]
          );

          if (valueInterpolated) {
            decisionData.outcomes[i][j].values[0] = maybeInterpolatedValue;
            interpolated = true;
          }
        }
      }
    }

    return interpolated;
  }

  private static interpolateValue(value: ObjectiveInput, objective: Objective): [ObjectiveInput | null, boolean] {
    const needsInterpolation = !validateValue(value, objective)[0];
    if (!needsInterpolation) return [value, false];

    const defaultValueForObjective = getDefaultValueFor(objective);

    if (objective.objectiveType === ObjectiveType.Indicator) {
      // Keep valid indicator values.
      return [
        value.map((indicatorValue, indicatorIdx) => {
          const indicatorInterval = objective.indicatorData.indicators[indicatorIdx].getRangeInterval();
          return indicatorValue.map((v, categoryIdx) =>
            indicatorInterval.clampTo(v ?? defaultValueForObjective[indicatorIdx][categoryIdx])
          );
        }),
        true,
      ];
    } else {
      return [defaultValueForObjective, true];
    }
  }

  private static interpolateResults(decisionData: DecisionData): boolean {
    let interpolated = false;

    for (let i = 0; i < decisionData.objectives.length; i++) {
      const objectiveWeight = decisionData.weights.preliminaryWeights[i];

      if (objectiveWeight.value == null || objectiveWeight.value < 0) {
        objectiveWeight.value = Weights.NORMALIZATION_TARGET_WEIGHT;
        interpolated = true;
      } else if (objectiveWeight.value > Weights.MAX_WEIGHT) {
        objectiveWeight.value = Weights.MAX_WEIGHT;
        interpolated = true;
      }
    }

    if (decisionData.weights.tradeoffObjectiveIdx == null || decisionData.objectives[decisionData.weights.tradeoffObjectiveIdx] == null) {
      decisionData.weights.tradeoffObjectiveIdx = 0;
      interpolated = true;
    }

    return interpolated;
  }

  constructor(private decisionData: DecisionData, private snackBar: MatSnackBar) {}

  /**
   * Interpolates decision statement, objectives and alternatives.
   */
  interpolateForFirstProfessionalTab() {
    InterpolationService.interpolateDecisionStatement(this.decisionData);
    InterpolationService.interpolateObjectives(this.decisionData);
    InterpolationService.interpolateAlternatives(this.decisionData);
  }

  /**
   * Interpolates all steps.
   */
  interpolateForSecondProfessionalTab(options: { suppressObjectiveWeightsSnackbar?: boolean } = {}) {
    this.interpolateForFirstProfessionalTab();

    if (InterpolationService.interpolateImpactModel(this.decisionData)) {
      this.snackBar.open(
        $localize`Das Wirkungsmodell wurde nicht vollständig von Dir ausgefüllt.
    Dieses wurde deshalb mit Minimumwerten vervollständigt. Kehre zum ersten Tab zurück, wenn Du es noch einmal anpassen willst.`,
        $localize`Schließen`
      );
    }

    if (InterpolationService.interpolateResults(this.decisionData) && !options.suppressObjectiveWeightsSnackbar) {
      this.snackBar.open(
        $localize`Die Zielgewichte waren von Dir nicht gesetzt, sind aber im weiteren Verlauf erforderlich.
         Sie wurden daher auf Standardwerte gesetzt. Kehre zur Zielgewichtung zurück, wenn Du sie anpassen möchtest.`,
        $localize`Schließen`
      );
    }
  }
}
