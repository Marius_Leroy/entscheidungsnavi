import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { calculateCurrentProgress } from '@entscheidungsnavi/decision-data/steps';
import { sum } from 'lodash';
import { ProjectViewData } from '../project-view';
import { ExcelExportService } from '../excel-export';
import { DecisionDataExportService } from '../data/decision-data-export.service';
import { OnlineProject, ProjectService } from '../data/project';

@Component({
  selector: 'dt-project-information',
  templateUrl: './project-information.component.html',
  styleUrls: ['./project-information.component.scss'],
})
export class ProjectInformationComponent implements OnInit {
  data: ProjectViewData;
  projectLocation: 'online' | 'local';
  selectedTabIndex: number;
  projectStatusText: string[] = ['', '', '', '', ''];
  dates: Date[];

  projectSize: number;

  constructor(
    protected decisionData: DecisionData,
    private excelExportService: ExcelExportService,
    public dialogRef: MatDialogRef<ProjectInformationComponent>,
    private exportService: DecisionDataExportService,
    private projectService: ProjectService
  ) {}

  async ngOnInit() {
    this.data = new ProjectViewData(this.decisionData);
    const project = this.projectService.getProject();

    this.projectSize = this.exportService.getProjectSize();

    // online/linked/quickstart projects belong to Entscheidungsnavi Cloud / online
    // whereas projects loaded from own device and newly created projects belong to local storage
    if (project instanceof OnlineProject) {
      this.dates = [project.info.createdAt, project.info.updatedAt];
      this.projectLocation = 'online';
    } else {
      this.projectLocation = 'local';
    }

    this.calculateProjectStatusValues();
  }

  // determines the text displaying how far a user has worked in the single steps
  calculateProjectStatusValues(): void {
    const currentProgress = calculateCurrentProgress(this.decisionData);
    let numberOfObjectivesAspects = 0;
    let numberOfAlternatives = 0;
    let numberOfProcessedOutcomes = 0;
    let numberOfTradeoffs = 0;

    this.projectStatusText[0] =
      this.decisionData.decisionStatement.statement === null || this.decisionData.decisionStatement.statement === ''
        ? $localize`Entscheidungsfrage fehlt`
        : $localize`Entscheidungsfrage formuliert`;

    // if there are any objectives get the number of all aspects of the objectives
    if (this.decisionData.objectives.length > 0) {
      numberOfObjectivesAspects = this.decisionData.objectives
        .map(obj => obj.getAspectsAsArray())
        .map(asp => asp.length)
        .reduce((acc, curr) => acc + curr);
    }

    if (this.decisionData.objectives.length === 0) {
      this.projectStatusText[1] += $localize`Noch nicht bearbeitet`;
    } else if (this.decisionData.objectives.length === 1) {
      this.projectStatusText[1] += $localize`1 Fundamentalziel, `;
    } else {
      this.projectStatusText[1] += $localize`${this.decisionData.objectives.length} Fundamentalziele, `;
    }

    if (this.decisionData.objectives.length > 0) {
      this.projectStatusText[1] +=
        numberOfObjectivesAspects === 1 ? $localize`1 Teilziel` : $localize`${numberOfObjectivesAspects} Teilziele`;
    }

    // count alternatives which do have an entry
    this.decisionData.alternatives.forEach(alt => {
      if (alt.name !== '') {
        numberOfAlternatives += 1;
      }
    });

    if (numberOfAlternatives === 0) {
      this.projectStatusText[2] = $localize`Noch nicht bearbeitet`;
    } else if (numberOfAlternatives === 1) {
      this.projectStatusText[2] = $localize`${numberOfAlternatives} Alternative`;
    } else {
      this.projectStatusText[2] = $localize`${numberOfAlternatives} Alternativen`;
    }

    // get number of influence factors and forecasts of outcomes
    if (currentProgress.step === 'impactModel' || currentProgress.step === 'results') {
      // go over all impact matrix entries and count all which are processed
      for (let altInd = 0; altInd < this.decisionData.alternatives.length; altInd++) {
        for (let objInd = 0; objInd < this.decisionData.objectives.length; objInd++) {
          if (this.decisionData.outcomes[altInd][objInd].processed) {
            numberOfProcessedOutcomes += 1;

            // consider all impact matrix entries where an influence factor was used
            if (this.decisionData.outcomes[altInd][objInd].influenceFactor !== undefined) {
              numberOfProcessedOutcomes += this.decisionData.outcomes[altInd][objInd].influenceFactor.states.length - 1;
            }
          }
        }
      }

      this.projectStatusText[3] =
        numberOfProcessedOutcomes === 1 ? $localize`1 Wirkungsprognose` : $localize`${numberOfProcessedOutcomes} Wirkungsprognosen`;

      if (this.decisionData.projectMode !== 'starter') {
        this.projectStatusText[3] += ', ';

        this.projectStatusText[3] +=
          this.decisionData.influenceFactors.length === 1
            ? $localize`1 Einflussfaktor`
            : $localize`${this.decisionData.influenceFactors.length} Einflussfaktoren`;

        this.projectStatusText[3] += ', ';

        const usedIndicators = sum(
          this.decisionData.objectives.map(objective => (objective.isIndicator ? objective.indicatorData.indicators.length : 0))
        );
        this.projectStatusText[3] += usedIndicators === 1 ? $localize`1 Indikator` : $localize`${usedIndicators} Indikatoren`;
      }
    } else {
      this.projectStatusText[3] = $localize`Noch nicht bearbeitet`;
    }

    // make sure that the user is already at the last step in order to process the last step
    if (currentProgress.step === 'results' && (currentProgress.subStepIndex === undefined || currentProgress.subStepIndex === 1)) {
      this.projectStatusText[4] +=
        this.decisionData.objectives.length === 1 ? $localize`Nutzenfunktion definiert und ` : $localize`Nutzenfunktionen definiert und `;

      if (this.decisionData.projectMode === 'starter') {
        this.projectStatusText[4] = '';
      } else if (this.decisionData.projectMode === 'educational') {
        numberOfTradeoffs = this.countTradeoffs();

        this.projectStatusText[4] += numberOfTradeoffs === 1 ? $localize`1 Trade-Off` : $localize`${numberOfTradeoffs} Trade-Offs`;
      }
    } else if (currentProgress.step === 'results' && currentProgress.subStepIndex === 0) {
      this.projectStatusText[4] = $localize`Nutzenfunktionen definiert und 0 Trade-Offs`;
    } else {
      this.projectStatusText[4] = $localize`Noch nicht bearbeitet`;
    }
  }

  exportAsExcel(onlyExpanded: boolean) {
    this.excelExportService.performExcelExport(onlyExpanded, this.data);
  }

  // only for educational, since we do not have any tradeoffs in starter
  countTradeoffs(): number {
    let tradeoffs = 0;

    if (this.decisionData.weights.tradeoffObjectiveIdx === null) {
      // if there is no reference objective set we do not have any tradeoffs
      return tradeoffs;
    } else {
      // in educational mode we count every verified tradeoff if reference objective is set
      this.decisionData.weights.tradeoffWeights.forEach((tradeoffWeight, index) => {
        if (tradeoffWeight !== null && !this.decisionData.weights.unverifiedWeights[index]) {
          tradeoffs += 1;
        }
      });

      // subtract one if all weights and therefore the objective reference are set
      if (tradeoffs === this.decisionData.objectives.length) {
        tradeoffs -= 1;
      }

      return tradeoffs;
    }
  }
}
