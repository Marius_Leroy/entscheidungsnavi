import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthService } from '@entscheidungsnavi/api-client';
import { map } from 'rxjs';
import { ENVIRONMENT } from '../../../environments/environment';
import { AppSettingsService } from '../../data/app-settings.service';

@Component({
  templateUrl: './app-settings-modal.component.html',
  styleUrls: ['./app-settings-modal.component.scss'],
})
export class AppSettingsModalComponent {
  get nightly() {
    return ENVIRONMENT.nightly;
  }

  readonly isPrivilegedUser$ = this.authService.user$.pipe(map(user => user?.roles?.includes('privileged-user')));

  constructor(
    protected dialogRef: MatDialogRef<AppSettingsModalComponent>,
    protected appSettings: AppSettingsService,
    private authService: AuthService
  ) {}
}
