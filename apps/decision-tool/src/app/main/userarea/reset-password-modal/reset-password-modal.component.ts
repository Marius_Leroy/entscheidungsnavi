import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Inject, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from '@entscheidungsnavi/api-client';
import { DebugService } from '../../../debug/debug.service';
import { LoginModalComponent } from '../login-modal/login-modal.component';

const tokenRegexp = /^[\da-fA-F]{64}$/; // 64 hexadecimal digits

@Component({
  templateUrl: 'reset-password-modal.component.html',
  styles: [''],
})
export class ResetPasswordModalComponent implements OnInit, OnDestroy {
  tokenStatus: 'checking' | 'valid' | 'invalid-simple' | 'invalid-server' | 'rate-limit' = 'checking';

  passwordForm: UntypedFormGroup;

  done = false;

  @ViewChild('debug', { static: true })
  debugTemplate: TemplateRef<any>;

  constructor(
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) private token: string,
    private dialogRef: MatDialogRef<ResetPasswordModalComponent>,
    private debugService: DebugService,
    private dialog: MatDialog
  ) {}

  close() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.debugService.registerFleetingTemplate(this, this.debugTemplate);

    this.passwordForm = new UntypedFormGroup({});

    if (tokenRegexp.test(this.token)) {
      this.authService.checkResetToken(this.token).subscribe(
        (_res: HttpResponse<any>) => {
          this.tokenStatus = 'valid';
        },
        (err: HttpErrorResponse) => {
          switch (err.status) {
            case 429:
              this.tokenStatus = 'rate-limit';
              break;
            default:
              this.tokenStatus = 'invalid-server';
              break;
          }
        }
      );
    } else {
      this.tokenStatus = 'invalid-simple';
    }
  }

  ngOnDestroy() {
    this.debugService.removeFleetingTemplate(this);
  }

  onSubmit() {
    this.passwordForm.updateValueAndValidity();

    if (this.passwordForm.valid) {
      this.passwordForm.disable({ emitEvent: false });

      const password = this.passwordForm.controls.password.value;

      this.authService.resetPassword(this.token, password).subscribe(
        (_res: HttpResponse<any>) => {
          this.done = true;
        },
        (err: HttpErrorResponse) => {
          this.passwordForm.enable();
          let error = 'something';
          switch (err.status) {
            case 400:
              error = 'invalid-password';
              break;
            case 404:
              error = 'invalid-token';
              break;
            case 429:
              error = 'rate-limit';
              break;
            default:
              error = 'something';
              break;
          }

          this.passwordForm.setErrors({ 'server-error': error });
        }
      );
    }
  }

  openLoginModal() {
    this.close();
    this.dialog.open(LoginModalComponent);
  }
}
