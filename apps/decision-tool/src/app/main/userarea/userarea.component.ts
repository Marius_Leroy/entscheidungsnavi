import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { AccountModalComponent, EmailConfirmationModalComponent, PopOverRef, PopOverService } from '@entscheidungsnavi/widgets';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { Observable, firstValueFrom } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';
import { AuthService } from '@entscheidungsnavi/api-client';
import { OnlineProject, ProjectService } from '../../data/project';
import { ProjectsModalComponent } from '../../navigation';
import { AppSettingsModalComponent } from '../app-settings/app-settings-modal.component';
import { ServiceWorkerService } from '../../../services/service-worker.service';
import { TrackingService } from '../../data/tracking.service';
import { NaviHelperService } from '../../../services/navi-helper.service';
import { LanguageService } from '../../data/language.service';
import { SideBySideService } from '../../navigation-layout/side-by-side.service';
import { ChangeLanguageService } from '../../data/change-language.service';
import { EventsOverviewModalComponent } from './events';
import { LoginModalComponent } from './login-modal/login-modal.component';

@Component({
  selector: 'dt-userarea',
  templateUrl: 'userarea.component.html',
  styleUrls: ['./userarea.component.scss'],
})
export class UserareaComponent implements OnInit, AfterViewInit {
  userPopoverRef: PopOverRef;
  updatePopoverRef: PopOverRef;

  @Input() isStartPage = false;
  isUpdateAvailable = false;

  @Output()
  hamburgerButtonClick = new EventEmitter<void>();

  @ViewChild('updatePopover')
  updatePopoverTemplate: TemplateRef<any>;

  @ViewChild('userButton', { read: ElementRef })
  userButtonRef: ElementRef<HTMLButtonElement>;

  @ViewChild('updateButton', { read: ElementRef })
  updateButtonRef: ElementRef<HTMLButtonElement>;

  @OnDestroyObservable()
  private onDestroy$: Observable<any>;

  isMobileLayout = false;

  constructor(
    protected authService: AuthService,
    protected decisionData: DecisionData,
    private dialog: MatDialog,
    protected projectService: ProjectService,
    protected sideBySideService: SideBySideService,
    private popOverService: PopOverService,
    private serviceWorkerService: ServiceWorkerService,
    private trackingService: TrackingService,
    private naviHelperService: NaviHelperService,
    private languageService: LanguageService,
    private changeLanguageService: ChangeLanguageService
  ) {}

  ngOnInit() {
    this.authService.onLogin$.pipe(takeUntil(this.onDestroy$)).subscribe(user => {
      if (user.emailConfirmed === false) {
        this.dialog.open(EmailConfirmationModalComponent);
      }
    });

    this.authService.onLogout$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.userPopoverRef?.close();
    });
  }

  ngAfterViewInit() {
    this.serviceWorkerService.updateState$
      .pipe(
        filter(state => state === 'update-ready'),
        take(1),
        takeUntil(this.onDestroy$)
      )
      .subscribe(() => {
        this.isUpdateAvailable = true;
        this.toggleUpdatePopover();
      });
  }

  /**
   * Logout, close project and navigate to /start if an online project is loaded.
   */
  async logout() {
    const project = this.projectService.getProject();

    const needsToCloseProject = project && project instanceof OnlineProject;
    if (needsToCloseProject && !(await firstValueFrom(this.projectService.closeProject()))) {
      // We have an online project and the user does not want to close it
      return;
    }

    this.naviHelperService.logout();

    this.trackingService.trackEvent('logout', { category: 'user' });
  }

  activateNaviUpdate() {
    this.projectService
      .confirmProjectUnload({
        title: $localize`Ungespeicherte Änderungen`,
        prompt: $localize`Durch das Aktivieren des Updates wird Dein Projekt geschlossen und ungespeicherte Änderungen gehen verloren.
        Wir empfehlen Dein Projekt vorher online zu speichern. Möchtest Du Dein Projekt schließen und das Update aktivieren?`,
        buttonConfirm: $localize`Ja, Projekt schließen`,
      })
      .pipe(filter(Boolean))
      .subscribe(() => this.serviceWorkerService.activateUpdate());
  }

  openUserPopover(template: TemplateRef<any>) {
    this.userPopoverRef = this.popOverService.open(template, this.userButtonRef.nativeElement, {
      position: [{ originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }],
      closeCallback: () => (this.userPopoverRef = null),
    });
  }

  openEventModal() {
    this.dialog.open(EventsOverviewModalComponent);
  }

  openAccountModal() {
    this.dialog.open(AccountModalComponent);
  }

  toggleUpdatePopover() {
    if (this.updatePopoverRef) {
      this.updatePopoverRef.close();
      this.updatePopoverRef = null;
      return;
    }
    this.updatePopoverRef = this.popOverService.open(this.updatePopoverTemplate, this.updateButtonRef.nativeElement, {
      position: [{ originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }],
      hasBackdrop: false,
      closeCallback: () => (this.updatePopoverRef = null),
    });
  }

  openProjectModal() {
    this.dialog.open(ProjectsModalComponent);
  }

  openLoginModal() {
    this.dialog.open(LoginModalComponent);
  }

  openAppSettings() {
    this.dialog.open(AppSettingsModalComponent);
  }

  get language() {
    return this.languageService.isEnglish ? 'en' : 'de';
  }

  changeLanguage(newLanguage: 'de' | 'en') {
    if (this.language !== newLanguage) {
      this.changeLanguageService.changeLanguage();
    }
  }

  toggleHelpAssistant() {
    if (this.sideBySideService.isOpen) {
      this.sideBySideService.close();
    } else {
      this.sideBySideService.openHelp();
    }
  }
}
