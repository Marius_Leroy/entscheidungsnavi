import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Sort } from '@angular/material/sort';
import { throttle, without } from 'lodash';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';
import { ConfirmModalComponent, ConfirmModalData, SnackbarComponent, SnackbarData, Tag } from '@entscheidungsnavi/widgets';
import { DatePipe } from '@angular/common';
import { checkType, OnDestroyObservable } from '@entscheidungsnavi/tools';
import {
  AuthService,
  ProjectDto,
  ProjectListDto,
  QuickstartProjectDto,
  QuickstartProjectListDto,
  QuickstartService,
  QuickstartTagDto,
} from '@entscheidungsnavi/api-client';
import { Team } from '@entscheidungsnavi/api-types';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ProjectLinkModalComponent } from '../project-link-modal/project-link-modal.component';
import { OnlineProject, ProjectService } from '../../../data/project';
import { RenameProjectModalComponent } from '../rename-project-modal/rename-project-modal.component';
import { CreateTeamModalComponent } from '../create-team-modal/create-team-modal.component';
import { OnlineProjectManagementService } from '../project-management.service';

@Component({
  templateUrl: './project-list-modal.component.html',
  styleUrls: ['./project-list-modal.component.scss'],
})
export class ProjectListModalComponent implements OnInit {
  projects: ProjectListDto | QuickstartProjectListDto;
  quickstart: boolean;

  filterText = '';
  filterTagIds: string[] = [];

  filteredProjects: (ProjectDto | QuickstartProjectDto)[] = [];
  updateProjectsAndTags: () => void;

  activeSorting: Sort;

  tags: Tag[] = [];

  tagsById: { [id: string]: QuickstartTagDto };

  visibleTagCount = 3; // This is updated based on the table's width

  openOnlineProjectId: string;

  @OnDestroyObservable()
  private onDestroy$: Observable<any>;

  constructor(
    private authService: AuthService,
    private snackbar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) data: { quickstart: boolean; projectList: ProjectListDto | QuickstartProjectListDto },
    private dialogRef: MatDialogRef<ProjectListModalComponent>,
    private projectService: ProjectService,
    private projectManagementService: OnlineProjectManagementService,
    private dialog: MatDialog,
    private date: DatePipe,
    private cdRef: ChangeDetectorRef,
    quickstartService: QuickstartService,
    private decisionData: DecisionData
  ) {
    this.quickstart = data.quickstart;
    this.projects = data.projectList;

    this.projectService.project$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(project => (this.openOnlineProjectId = project instanceof OnlineProject ? project.info.id : null));

    this.updateProjectsAndTags = throttle(() => {
      let list: (ProjectDto | QuickstartProjectDto)[] = this.filterText ? this.projects.search(this.filterText) : this.projects.list;
      if (this.filterTagIds) {
        list = list.filter(project =>
          this.filterTagIds.every(tag => project instanceof QuickstartProjectDto && project.tags.includes(tag))
        );

        this.filterTagIds = [...this.filterTagIds];
      }

      this.filteredProjects = this.sort(list);

      this.tags.forEach(
        tag => (tag.isProductive = this.filteredProjects.some(p => p instanceof QuickstartProjectDto && p.tags.includes(tag.id)))
      );
    }, 50);

    if (data.quickstart) {
      quickstartService.getTags().subscribe({
        next: tags => {
          this.tags = tags;
          this.tagsById = tags.reduce((prev, curr) => {
            prev[curr.id] = curr;
            return prev;
          }, {});

          this.updateProjectsAndTags();
        },
        error: error => console.error(error),
      });
    }
  }

  ngOnInit() {
    this.activeSorting = this.quickstart ? { active: 'name', direction: 'asc' } : { active: 'date', direction: 'desc' };

    this.updateProjectsAndTags();

    if (!this.quickstart) {
      this.authService.onLogout$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
        this.dialogRef.close();
      });
    }
  }

  closeModal(loadedProject: boolean) {
    this.dialogRef.close(loadedProject);
  }

  sort(projectList: (ProjectDto | QuickstartProjectDto)[]) {
    return projectList.sort((a, b) => {
      let result = 0;
      if (this.activeSorting.active === 'name') {
        result = a.name.localeCompare(b.name);
      } else if (this.activeSorting.active === 'date') {
        result = a.updatedAt.getTime() - b.updatedAt.getTime();
      }

      return this.activeSorting.direction === 'asc' ? result : -result;
    });
  }

  changeSorting(sort: Sort) {
    this.activeSorting = sort;
    this.updateProjectsAndTags();
  }

  submitSearch(event: Event) {
    event.preventDefault();
    if (this.filteredProjects.length === 1) {
      this.loadProject(this.filteredProjects[0].id);
    }
  }

  loadProject(id: string, createCopy = false) {
    const loadObservable = this.quickstart
      ? this.projectService.loadQuickstartProject(id)
      : this.projectService.loadOnlineProject(id, { loadAsLocal: createCopy });

    loadObservable.subscribe({
      next: success => {
        if (!success) return;

        this.closeModal(true);

        if (createCopy) this.decisionData.decisionProblem += ' ' + $localize`(Kopie)`;
      },
      error: () => {
        this.snackbar.openFromComponent(SnackbarComponent, {
          data: checkType<SnackbarData>({
            message: $localize`Projekt konnte nicht geladen werden. Möglicherweise ist Deine Internetverbindung instabil.`,
            icon: 'error',
          }),
          duration: 8000,
        });
      },
    });
  }

  renameProject(project: ProjectDto) {
    this.dialog.open(RenameProjectModalComponent, { data: { project, projectList: this.projects } });
  }

  deleteProject(project: ProjectDto) {
    if (this.quickstart || project.team) {
      return;
    }

    this.dialog
      .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
        data: {
          title: $localize`Projekt Löschen`,
          prompt: $localize`Bist Du sicher, dass Du das Projekt "${project.name}" vom ${this.date.transform(
            project.updatedAt
          )} löschen möchten?`,
          template: 'delete',
        },
      })
      .afterClosed()
      .pipe(
        filter(Boolean),
        switchMap(() => this.projectManagementService.delete(project.id))
      )
      .subscribe({
        next: () => {
          // Remove the project from our list
          this.projects.list.splice(
            this.projects.list.findIndex(pr => pr.id === project.id),
            1
          );
          this.updateProjectsAndTags();
        },
        error: () => {
          this.snackbar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
            data: {
              message: $localize`Projekt konnte nicht gelöscht werden. Möglicherweise ist Deine Internetverbindung instabil.`,
              icon: 'error',
            },
            duration: 8000,
          });
        },
      });
  }

  createTeam(project: ProjectDto) {
    this.dialog
      .open<CreateTeamModalComponent, { projectId: string }, Team>(CreateTeamModalComponent, { data: { projectId: project.id } })
      .afterClosed()
      .subscribe(createdTeam => {
        if (createdTeam) {
          project.team = { ...createdTeam };
        }
      });
  }

  canCreateTeamProjectFrom(project: ProjectDto) {
    return project.team == null;
  }

  getQuickstartProjectLink(project: QuickstartProjectDto): string {
    return project.shareUrl;
  }

  createProjectLink(project: ProjectDto) {
    this.dialog.open(ProjectLinkModalComponent, { data: { project } });
  }

  onTagClick(tag: QuickstartTagDto) {
    if (!this.filterTagIds.includes(tag.id)) {
      this.filterTagIds.push(tag.id);
    } else {
      this.filterTagIds = without(this.filterTagIds, tag.id);
    }

    this.updateProjectsAndTags();
  }

  onTableWidthChange(newWidth: number) {
    const visibleTagCount = Math.floor((newWidth - 300) / 150);

    if (visibleTagCount !== this.visibleTagCount) {
      this.visibleTagCount = visibleTagCount;
      this.cdRef.detectChanges();
    }
  }
}
