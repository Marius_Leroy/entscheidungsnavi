import { Component, Inject } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProjectDto, OnlineProjectsService } from '@entscheidungsnavi/api-client';

@Component({
  templateUrl: './project-link-modal.component.html',
  styleUrls: ['./project-link-modal.component.scss'],
})
export class ProjectLinkModalComponent {
  project: ProjectDto;

  loading = false;

  constructor(
    private onlineProjectService: OnlineProjectsService,
    private location: Location,
    public dialogRef: MatDialogRef<ProjectLinkModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { project: ProjectDto }
  ) {
    this.project = data.project;
  }

  toggleLink() {
    this.loading = true;
    if (this.project.shareToken) {
      this.onlineProjectService.setShare(this.project.id, false).subscribe({
        complete: () => {
          this.project.shareToken = null;
          this.loading = false;
        },
      });
    } else {
      this.onlineProjectService
        .setShare(this.project.id, true)
        .subscribe({ next: token => (this.project.shareToken = token), complete: () => (this.loading = false) });
    }
  }

  get link() {
    return document.location.origin + this.location.prepareExternalUrl(`/start?linkedproject=${this.project.shareToken}`);
  }
}
