import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { QuickstartProjectDto, QuickstartTagDto } from '@entscheidungsnavi/api-client';

@Component({
  selector: 'dt-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagListComponent {
  @Input() project: QuickstartProjectDto;
  @Input() tagsById: { [id: string]: QuickstartTagDto };
  @Input() selectedTagIds: string[] = [];
  @Input() visibleTagCount: number;

  @Output() tagClick = new EventEmitter<QuickstartTagDto>();

  private get allTags() {
    return this.project.tags.map(tagId => this.tagsById[tagId]).sort((a, b) => (b.weight ?? -Infinity) - (a.weight ?? -Infinity));
  }

  get visibleTags() {
    return this.allTags.slice(0, this.visibleTagCount);
  }

  get overflowingTags() {
    return this.allTags.slice(this.visibleTagCount);
  }

  get hasOverflow() {
    return this.visibleTagCount < this.project.tags.length;
  }
}
