import { Pipe, PipeTransform } from '@angular/core';
import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { NaviStep, NaviStepNames } from '@entscheidungsnavi/decision-data/steps';
import { LanguageService } from '../data/language.service';
import { NavigationStepMetaData } from '../../modules/shared/navigation/navigation-step';

@Pipe({ name: 'filteredSubSteps' })
export class FilteredSubStepsPipe implements PipeTransform {
  constructor(private languageService: LanguageService) {}
  transform(step: NavigationStepMetaData & NaviStepNames & { id: NaviStep }, projectMode: ProjectMode): { name: string; index: number }[] {
    if (projectMode === 'educational') {
      return step.subSteps.map((name, index) => ({ name, index }));
    } else if (step.id === 'results') {
      return step.subSteps.map((name, index) => ({ name, index })).slice(1);
    } else {
      return [];
    }
  }
}
