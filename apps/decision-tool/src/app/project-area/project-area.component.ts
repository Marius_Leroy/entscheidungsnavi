import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { AuthService } from '@entscheidungsnavi/api-client';
import { auditTime, distinctUntilChanged } from 'rxjs';
import { ProjectInformationComponent } from '../project-information/project-information.component';
import { SaveAsModalComponent } from '../navigation';
import { OnlineProject, ProjectService } from '../data/project';
import { TeamModalComponent } from '../../modules/shared/team/team-modal';
import { AutoSaveService } from '../data/project/auto-save.service';
import { saveErrorToMessage } from '../data/project/save-notification';
import { SideBySideService } from '../navigation-layout/side-by-side.service';
import { LoginModalComponent } from '../main/userarea';

@Component({
  selector: 'dt-project-area',
  templateUrl: './project-area.component.html',
  styleUrls: ['./project-area.component.scss'],
})
export class ProjectAreaComponent {
  constructor(
    protected decisionData: DecisionData,
    private dialog: MatDialog,
    protected projectService: ProjectService,
    protected authService: AuthService,
    private autoSaveService: AutoSaveService,
    protected sideBySideService: SideBySideService
  ) {}

  get teamTrait() {
    return this.projectService.getTeamTrait();
  }

  get isTeamProject() {
    return this.teamTrait != null;
  }

  get onlineProject() {
    return this.projectService.getProject(OnlineProject);
  }

  get teamMemberEmail() {
    return this.teamTrait.activeMember.user.email;
  }

  get teamProjectName() {
    return this.teamTrait.activeMember.id === this.teamTrait.info.owner
      ? $localize`Hauptprojekt`
      : this.teamTrait.activeMember.user.name ?? this.teamTrait.activeMember.user.email;
  }

  get saveErrorMessage() {
    return saveErrorToMessage(this.autoSaveService.saveError);
  }

  // Delay the save state emission to make sure we do not change saving -> idle -> saving instantly
  saveState$ = this.autoSaveService.saveState$.pipe(auditTime(100), distinctUntilChanged());

  isProjectLoaded(): boolean {
    return this.projectService.isProjectLoaded();
  }

  openProjectNotes() {
    this.dialog.open(ProjectInformationComponent);
  }

  openTeamModal() {
    this.dialog.open(TeamModalComponent);
  }

  saveNonOnlineProject() {
    if (this.authService.loggedIn) {
      this.dialog.open(SaveAsModalComponent);
    } else {
      this.dialog
        .open(LoginModalComponent, { data: { showSaveHint: true } })
        .afterClosed()
        .subscribe(loggedIn => {
          if (loggedIn) this.dialog.open(SaveAsModalComponent);
        });
    }
  }
}
