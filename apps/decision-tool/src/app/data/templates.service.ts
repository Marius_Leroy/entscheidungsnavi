import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { map, Observable, shareReplay } from 'rxjs';
import { LanguageService } from './language.service';

export const TEMPLATES = [
  'after-the-bachelor',
  'internship-selection',
  'besides-studies',
  'doctorate',
  'master-thesis-topic',
  'focus-of-study',
  'bachelor-program-selection',
  'master-program-selection',
  'housing-situation',
  'semester-abroad',
  'job-selection',
  'my-life-plan',
  'school-apprenticeship',
  'school-internship',
  'school-bridge-wait-time',
  'school-straight-to-job',
  'school-live-abroad',
  'school-social-involvement',
  'school-general',
  'school-straight-to-university',
] as const;
export type TemplateType = (typeof TEMPLATES)[number];
export type TemplateCollectionType = Record<TemplateType, Record<'de' | 'en', unknown>>;

@Injectable({
  providedIn: 'root',
})
export class TemplatesService {
  private get languageCode() {
    return this.languageService.isEnglish ? 'en' : 'de';
  }

  // Only contains templates available in the correct language
  readonly availableTemplates: Observable<TemplateType[]>;

  private allTemplates: Observable<TemplateCollectionType>;

  constructor(private languageService: LanguageService, private http: HttpClient) {
    this.allTemplates = http.get<TemplateCollectionType>('assets/templates.service.json').pipe(shareReplay());
    this.availableTemplates = this.allTemplates.pipe(
      map(templates => {
        return TEMPLATES.filter(name => name in templates && this.languageCode in templates[name]);
      }),
      shareReplay()
    );
  }

  getTemplate(type: TemplateType) {
    return this.getTemplateObject(type).pipe(map(templateObject => readText(JSON.stringify(templateObject))));
  }

  getTemplateObject(type: TemplateType) {
    return this.allTemplates.pipe(map(allTemplates => allTemplates[type][this.languageCode]));
  }
}
