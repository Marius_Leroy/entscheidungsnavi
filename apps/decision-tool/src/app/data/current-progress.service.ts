import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { calculateCurrentProgress, NaviSubStep, numberToSubStep, subStepToNumber } from '@entscheidungsnavi/decision-data/steps';
import { Observable, Subject } from 'rxjs';
import { filter, map, shareReplay } from 'rxjs/operators';
import { urlToNaviSubStep } from '../../modules/shared/navigation/navigation-step';
import { ProjectService } from './project';

@Injectable({
  providedIn: 'root',
})
export class CurrentProgressService {
  // The currently opened step in the Navi
  activeStep: Observable<NaviSubStep | null>;

  // The current progress in the project
  currentProgress: NaviSubStep;

  currentProgressUpdate$ = new Subject<void>();

  constructor(private decisionData: DecisionData, projectService: ProjectService, router: Router) {
    projectService.project$.subscribe(() => {
      this.currentProgress = undefined;
      this.update();
    });

    this.activeStep = router.events.pipe(
      filter((event): event is NavigationEnd => event instanceof NavigationEnd),
      map(event => urlToNaviSubStep(event.url)),
      shareReplay(1)
    );

    this.decisionData.objectiveAdded$.subscribe(() => {
      const currentProgress = this.currentProgress;
      if (currentProgress.step === 'results' && currentProgress.subStepIndex == null) {
        this.decisionData.resultSubstepProgress = 1;
        this.unlock();
      }
    });
  }

  update() {
    const calculatedProgress = calculateCurrentProgress(this.decisionData);
    if (this.currentProgress == null) {
      this.currentProgress = calculatedProgress;
    } else {
      // Progress can never go backward
      this.currentProgress = numberToSubStep(Math.max(subStepToNumber(this.currentProgress), subStepToNumber(calculatedProgress)));
    }

    this.currentProgressUpdate$.next();
  }

  unlock() {
    this.currentProgress = undefined;
    this.update();
  }

  /**
   * Manually override the current progress. Used in the enforce-gradual-progression guard when steps are skipped.
   */
  confirmSkip(toStep: NaviSubStep) {
    this.currentProgress = toStep;
    this.update();
  }
}
