import { Injectable } from '@angular/core';
import { AuthService } from '@entscheidungsnavi/api-client';
import { Angulartics2Matomo } from 'angulartics2';

interface EventProperties {
  category: string;
  name?: string;
  label?: string;
  value?: number;
}

@Injectable()
export class TrackingService {
  constructor(private angulartics: Angulartics2Matomo, authService: AuthService) {
    authService.userHasRole('privileged-user').subscribe(privileged => angulartics.setUsername(privileged ? 'privileged-user' : false));

    angulartics.startTracking();
  }

  public trackEvent(action: string, properties: EventProperties) {
    this.angulartics.eventTrack(action, { value: undefined, ...properties });
  }
}
