import { HttpErrorResponse } from '@angular/common/http';
import { TeamMember, Team, TeamComment } from '@entscheidungsnavi/api-types';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { from, concatMap, map, toArray, delayWhen, tap, catchError, of, BehaviorSubject, Observable } from 'rxjs';
import { inject } from '@angular/core';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { Router } from '@angular/router';
import { SelectiveReadonly } from '@entscheidungsnavi/tools';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DecisionDataExportService } from '../decision-data-export.service';

export type LoadedMember = SelectiveReadonly<TeamMember, 'id' | 'user' | 'project'>;
export type LoadedTeam = SelectiveReadonly<
  Omit<Team, 'members' | 'editor'> & {
    members: readonly LoadedMember[];
    editor$: BehaviorSubject<string>;
  },
  'id' | 'owner' | 'createdAt'
>;

/**
 * This class handles the team functions of a project.
 */
export class TeamTrait {
  private teamsService = inject(TeamsService);
  private router = inject(Router);
  private exportService = inject(DecisionDataExportService);
  private decisionData = inject(DecisionData);

  readonly info: LoadedTeam;

  private readonly userIdToMember: Map<string, LoadedMember>;
  private readonly memberIdToMember: Map<string, LoadedMember>;

  readonly allProjects$: Observable<DecisionData[]>;

  constructor(team: Team, public readonly activeMember: LoadedMember, public readonly userMember: LoadedMember) {
    this.info = {
      ...team,
      editor$: new BehaviorSubject(team.editor),
    };

    this.allProjects$ = from(this.info.members).pipe(
      concatMap(member => this.teamsService.getTeamProject(this.info.id, member.id)),
      map(project => readText(project.data)),
      toArray()
    );

    this.userIdToMember = new Map(this.info.members.map(member => [member.user.id, member]));
    this.memberIdToMember = new Map(this.info.members.map(member => [member.id, member]));
  }

  get isOwnProject() {
    return this.activeMember.id === this.userMember.id;
  }

  get isOtherUsersProject() {
    return !this.isOwnProject;
  }

  /**
   * Copy the main project into our current project
   */
  takeMainProject() {
    return this.teamsService.getTeamProject(this.info.id, this.info.owner).pipe(
      delayWhen(() => from(this.router.navigateByUrl('/loading'))),
      tap(project => this.exportService.importText(project.data)),
      delayWhen(() =>
        from(this.router.navigateByUrl(this.decisionData.lastUrl || '/start', { onSameUrlNavigation: 'reload', replaceUrl: true }))
      )
    );
  }

  getTeamMemberFromUserId(userId: string) {
    return this.userIdToMember.get(userId);
  }

  getTeamMemberFromMemberId(memberId: string) {
    return this.memberIdToMember.get(memberId);
  }

  updateTeamData() {
    return this.teamsService.getTeam(this.info.id).pipe(
      map(team => {
        this.info.editor$.next(team.editor);
        this.info.pendingInvites = team.pendingInvites;
        this.info.whiteboard = team.whiteboard;
      })
    );
  }

  changeEditorTo(memberId: string) {
    return this.teamsService.updateTeam(this.info.id, { editor: memberId }).pipe(map(team => this.info.editor$.next(team.editor)));
  }

  getCommentsFor(objectId: string) {
    return this.teamsService.getTeamCommentsForObject(this.info.id, this.activeMember.id, objectId).pipe(
      tap(comments => {
        this.activeMember.comments[objectId] = comments;
      })
    );
  }

  addComment(objectId: string, commentText: string, memberId = this.activeMember.id) {
    return this.teamsService.addComment(this.info.id, memberId, objectId, commentText).pipe(
      tap(comments => {
        const member = this.memberIdToMember.get(memberId);
        let allComments = member.comments;
        allComments = allComments.filter(c => c.objectId !== objectId);
        allComments.push(...comments);
        member.comments = allComments;
      })
    );
  }

  deleteComment(commentId: string, memberId = this.activeMember.id) {
    return this.teamsService.deleteComment(this.info.id, memberId, commentId).pipe(
      catchError(thrownError => {
        if (thrownError instanceof HttpErrorResponse && thrownError.status === 404 && thrownError.error === 'comment') {
          return of();
        }

        throw thrownError;
      }),
      tap({
        complete: () => {
          const member = this.getTeamMemberFromMemberId(memberId);
          const allComments = member.comments;
          const index = allComments.findIndex(comment => comment.id === commentId);

          if (index >= 0) {
            allComments.splice(index, 1);
          }
        },
      })
    );
  }

  updateCommentsFor(objectId: string, memberId = this.activeMember.id) {
    return this.teamsService.getTeamCommentsForObject(this.info.id, memberId, objectId).pipe(
      tap(comments => {
        const member = this.memberIdToMember.get(memberId);

        let allComments = member.comments;
        allComments = allComments.filter(c => c.objectId !== objectId);
        allComments.push(...comments);
        member.comments = allComments;
      })
    );
  }

  getUsersUnreadComments() {
    const projectMap = new Map<TeamMember, TeamComment[]>();

    const unreadCommentIds = new Set(this.userMember.unreadComments);

    for (const member of this.info.members) {
      for (const comment of member.comments) {
        if (unreadCommentIds.has(comment.id)) {
          if (!projectMap.has(member)) {
            projectMap.set(member, []);
          }

          projectMap.get(member).push(comment);
        }
      }
    }

    return projectMap;
  }

  invite(email: string) {
    return this.teamsService.invite(this.info.id, email).pipe(tap(newInvites => (this.info.pendingInvites = newInvites)));
  }

  /**
   * Transfer an object from the currently loaded project to the current users project.
   * Requires that the loaded project does not belong to the user.
   *
   * @param objectId - The id of the object to transfer
   */
  transferObject(objectId: string) {
    return this.teamsService.transferObject(this.info.id, this.activeMember.id, objectId);
  }
}
