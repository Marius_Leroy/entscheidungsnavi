import { catchError, EMPTY, finalize, map, mergeMap, Observable, of, Subject, switchMap, throwError } from 'rxjs';
import { ProjectUpdate, ProjectWithData, Team } from '@entscheidungsnavi/api-types';
import { createPatch, hashCode } from '@entscheidungsnavi/tools';
import { inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HttpErrorResponse } from '@angular/common/http';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  ProjectConflictData,
  ProjectConflictModalComponent,
  ProjectConflictResult,
} from '../../project-conflict-modal/project-conflict-modal.component';
import { TeamTrait } from './team-trait';
import { AbstractProject } from './abstract-project';
import { checkOnlineProjectSize, saveTimeout } from './save-notification';

export interface OnlineProjectInfo extends Pick<ProjectWithData, 'id' | 'data' | 'userId' | 'createdAt' | 'updatedAt'> {
  // Is non null if we are currently in a loaded history entry of this project
  loadedHistoryEntry?: string;
}

/**
 * A personal project in an online account. It may or may not be a team project (of which we are the owner).
 */
export class OnlineProject extends AbstractProject {
  private dialog = inject(MatDialog);
  private teamsService = inject(TeamsService);
  private snackBar = inject(MatSnackBar);

  get info(): Readonly<OnlineProjectInfo> {
    return this._info;
  }

  private _team: TeamTrait | null;
  get team() {
    return this._team;
  }

  get isOwnProject() {
    return this.team == null || this.team.isOwnProject;
  }

  readonly canSave$: Observable<boolean>;
  readonly onSaved$ = new Subject<void>();

  constructor(private readonly _info: OnlineProjectInfo, team?: Team) {
    super();

    if (team) {
      const activeMember = team.members.find(member => member.user.id === this._info.userId);
      const userMember = team.members.find(member => member.user.id === this.authService.user?.id);
      this._team = new TeamTrait(team, activeMember, userMember);
    }

    this.canSave$ =
      // If this is our own personal project, we can always save (whether team or nor)
      this.isOwnProject
        ? of(true)
        : // If this is another user's team project that is NOT the owner, we can never save
        this.team.activeMember.id !== this.team.info.owner
        ? of(false)
        : // Otherwise (this is the team main project), we can save whenever we are the editor
          this.team.info.editor$.pipe(map(editor => editor === this.team.userMember.id));

    this.updateLastExportHash();
  }

  save(): Observable<void> {
    // Whether we save a personal project in our own account or a team main project
    const savingPersonalProject = this.team == null || this.team.isOwnProject;

    this.trackingService.trackEvent(savingPersonalProject ? 'save project' : 'save team main project', { category: 'project' });
    const name = this.decisionData.decisionProblem;

    const data = this.exportService.dataToText();
    const dataWithoutEphemeral = this.exportService.changeDetectionRelevantDataToText();

    const updateFunction = savingPersonalProject
      ? (update: ProjectUpdate) => this.onlineProjectsService.updateProject(this.info.id, update)
      : (update: ProjectUpdate) => this.teamsService.updateMainProject(this.team.info.id, update);

    return of(data).pipe(
      checkOnlineProjectSize(),
      mergeMap(data =>
        updateFunction({
          name,
          dataPatch: createPatch(this._info.data, data),
          oldDataHash: hashCode(this._info.data),
        }).pipe(saveTimeout())
      ),
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.status === 409) {
          // The project was modified since we last loaded it
          const dialog = this.dialog.open<ProjectConflictModalComponent, ProjectConflictData, ProjectConflictResult>(
            ProjectConflictModalComponent,
            {
              data: { projectId: this.info.id, localLoadTime: this._info.updatedAt, onlineUpdateTime: new Date(error.error.message) },
            }
          );

          return dialog.afterClosed().pipe(
            // Make sure the dialog is closed when the observable is cancelled
            finalize(() => dialog.close()),
            switchMap(result => {
              // If we overwrite the online version, we continue here. Otherwise, the modal handles the rest.
              if (result === 'take-local') {
                return updateFunction({ name, data, resolvesConflict: true }).pipe(saveTimeout());
              }
              return EMPTY;
            })
          );
        }

        return throwError(() => error);
      }),
      catchError(error => {
        if (!savingPersonalProject && error instanceof HttpErrorResponse && error.status === 403) {
          this.team.info.editor$.next(error.error.message);

          this.snackBar.open($localize`Du hast nicht länger das Recht das Hauptprojekt zu bearbeiten.`, 'Ok');

          return of();
        }

        return throwError(() => error);
      }),
      map(newProject => {
        this.updateLastExportHash(dataWithoutEphemeral);
        this._info.data = data;
        this._info.updatedAt = newProject.updatedAt;
        this._info.loadedHistoryEntry = null;
        this.onSaved$.next();

        return null;
      })
    );
  }
}
