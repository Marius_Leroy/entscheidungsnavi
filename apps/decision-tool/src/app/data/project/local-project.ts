import { AbstractProject } from './abstract-project';

export class LocalProject extends AbstractProject {
  constructor(public readonly type: 'local' | 'quickstart' | 'linked' | 'embedded', isInitiallySaved = true) {
    super();

    if (isInitiallySaved) {
      this.updateLastExportHash();
    }
  }

  override exportFile() {
    super.exportFile();
    // Exporting to JSON counts as saving for local projects
    this.updateLastExportHash();
  }
}
