import { EnvironmentInjector, Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, combineLatest, from, Observable, of, OperatorFunction } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ProjectMode } from '@entscheidungsnavi/decision-data/classes';
import { MatDialog } from '@angular/material/dialog';
import {
  AuthService,
  EventManagementService,
  KlugService,
  OnlineProjectsService,
  QuickstartService,
  TeamsService,
} from '@entscheidungsnavi/api-client';
import { ConfirmModalComponent, ConfirmModalData, FileExport } from '@entscheidungsnavi/widgets';
import { EmbeddableProject, EmbeddedDecisionToolPortal } from '@entscheidungsnavi/embedded-decision-tool';
import { ClassConstructor } from 'class-transformer';
import { ProjectWithData, Team } from '@entscheidungsnavi/api-types';
import { ProjectInformationComponent } from '../../project-information/project-information.component';
import { TemplatesService, TemplateType } from '../templates.service';
import { TrackingService } from '../tracking.service';
import { DecisionDataExportService } from '../decision-data-export.service';
import { TimeTrackingService } from '../time-tracking.service';
import { COCKPIT_ORIGIN } from '../../decision-tool.module';
import { UnloadOnlineProjectModalComponent } from '../../unload-online-project-modal/unload-online-project-modal.component';
import { projectLoadingModal } from '../../project-loading/project-loading-modal.component';
import { AbstractProject } from './abstract-project';
import { LocalProject } from './local-project';
import { OnlineProject } from './online-project';
import { AutoSaveService } from './auto-save.service';

// Manages the status of the actual project
@Injectable({ providedIn: 'root' })
export class ProjectService {
  readonly project$ = new BehaviorSubject<LocalProject | OnlineProject | null>(null);

  readonly teamTrait$ = this.project$.pipe(map(project => (project instanceof OnlineProject ? project.team : null)));

  constructor(
    private decisionData: DecisionData,
    private onlineProjectsService: OnlineProjectsService,
    private quickstartService: QuickstartService,
    private exportService: DecisionDataExportService,
    private templatesService: TemplatesService,
    private eventManagementService: EventManagementService,
    private router: Router,
    private trackingService: TrackingService,
    private dialog: MatDialog,
    private klugService: KlugService,
    private teamAPIService: TeamsService,
    private injector: EnvironmentInjector,
    private timeTrackingService: TimeTrackingService,
    private embeddedDecisionToolPortal: EmbeddedDecisionToolPortal,
    @Inject(COCKPIT_ORIGIN) private cockpitOrigin: string,
    private authService: AuthService,
    private autoSaveService: AutoSaveService
  ) {
    this.project$.subscribe(project => {
      this.timeTrackingService.reset();
      this.autoSaveService.updateProject(project instanceof OnlineProject ? project : null);
    });

    this.authService.onLogout$.subscribe(() => {
      const project = this.getProject();
      if (project && !(project instanceof LocalProject)) {
        // We are logged out and have a non local project loaded -> switch to a local project
        this.project$.next(this.injector.runInContext(() => new LocalProject('local', project.isProjectSaved())));
      }
    });
  }

  /**
   * Returns the currently loaded project of the specified type, if one is loaded, or null.
   * If no type is specified, it returns any type of project.
   *
   * @param type - The type of project to be returned
   * @returns The project
   */
  getProject(): AbstractProject;
  getProject<T extends AbstractProject>(type: ClassConstructor<T>): T;
  getProject<T extends AbstractProject>(type?: ClassConstructor<AbstractProject>): T | AbstractProject {
    const project = this.project$.value;

    if (type && !(project instanceof type)) {
      return null;
    }

    return project;
  }

  getTeamTrait() {
    return this.getProject(OnlineProject)?.team;
  }

  isProjectLoaded() {
    return !!this.getProject();
  }

  getProjectType(): typeof LocalProject.prototype.type | 'online' {
    const project = this.getProject();
    if (project instanceof LocalProject) {
      return project.type;
    } else if (project instanceof OnlineProject) {
      return 'online';
    } else {
      return null;
    }
  }

  // create a new project
  newProject(name: string, mode: ProjectMode) {
    return of(null).pipe(
      switchMap(() => {
        this.trackingService.trackEvent('new project', { category: 'project' });

        this.exportService.resetDecisionData();
        this.decisionData.decisionProblem = name;
        this.decisionData.projectMode = mode;
        this.project$.next(this.injector.runInContext(() => new LocalProject('local')));

        return from(this.router.navigateByUrl(mode === 'professional' ? '/professional' : '/decisionstatement/steps/1'));
      }),
      this.loadProject()
    );
  }

  loadEventSubmission(registrationId: string) {
    return this.eventManagementService.getSubmittedProject(registrationId).pipe(
      tap(content => {
        this.trackingService.trackEvent('load event submission', { category: 'project' });

        this.exportService.importText(content);
        this.project$.next(this.injector.runInContext(() => new LocalProject('local')));
      }),
      this.loadProject()
    );
  }

  loadQuickstartProject(handle: string) {
    return this.quickstartService.getProject(handle).pipe(
      tap(project => {
        this.trackingService.trackEvent('load quickstart project', { name: handle, category: 'project' });

        this.exportService.importText(project.data);
        this.project$.next(this.injector.runInContext(() => new LocalProject('quickstart')));

        this.dialog.open(ProjectInformationComponent);
      }),
      this.loadProject()
    );
  }

  loadOnlineProject(
    id: string,
    options: Partial<{ historyEntryId: string; loadAsLocal: boolean; overrideLastUrl: string; forceUnload: boolean }> = {}
  ) {
    return combineLatest([
      this.onlineProjectsService.getProject(id).pipe(this.loadTeamInfo()),
      options.historyEntryId ? this.onlineProjectsService.getDataFromHistory(id, options.historyEntryId) : of(null),
    ]).pipe(
      tap(([[project, team], historyData]) => {
        const event = ['load online project'];
        if (options.historyEntryId) event.push('history entry');
        if (options.loadAsLocal) event.push('as local');
        this.trackingService.trackEvent(event.join(' '), { category: 'project' });

        this.exportService.importText(historyData ?? project.data);
        this.decisionData.decisionProblem = project.name;

        const newProject = this.injector.runInContext(() =>
          options.loadAsLocal
            ? new LocalProject('local')
            : new OnlineProject({ ...project, loadedHistoryEntry: options.historyEntryId }, team)
        );
        this.project$.next(newProject);
      }),
      this.loadProject(options)
    );
  }

  private loadTeamInfo(): OperatorFunction<ProjectWithData, readonly [ProjectWithData, Team]> {
    return switchMap(project => {
      if (project.team) {
        return this.teamAPIService.getTeam(project.team.id).pipe(map(team => [project, team] as const));
      } else {
        return of([project, null] as const);
      }
    });
  }

  loadLinkedProject(token: string) {
    return this.onlineProjectsService.getProjectFromShareToken(token).pipe(
      tap(project => {
        this.trackingService.trackEvent('load linked project', { category: 'project' });

        this.exportService.importText(project.data);
        this.project$.next(this.injector.runInContext(() => new LocalProject('linked')));
      }),
      this.loadProject()
    );
  }

  loadTemplate(template: TemplateType, name?: string): Observable<boolean> {
    return this.templatesService.getTemplateObject(template).pipe(
      tap(templateObject => {
        this.trackingService.trackEvent('load template', { category: 'project' });

        this.exportService.importText(JSON.stringify(templateObject));
        this.decisionData.projectMode = 'starter';
        if (name) {
          this.decisionData.decisionProblem = name;
        }

        this.project$.next(this.injector.runInContext(() => new LocalProject('local')));
      }),
      this.loadProject()
    );
  }

  loadKlugProject(klugToken: string) {
    return this.klugService.getKlugProject(klugToken).pipe(
      tap(project => {
        this.trackingService.trackEvent('load klug project', { category: 'project' });

        this.exportService.importText(project.data);
        this.project$.next(this.injector.runInContext(() => new LocalProject('local')));
      }),
      this.loadProject()
    );
  }

  loadTeamProject(teamId: string, memberId: string) {
    const teamTrait = this.getTeamTrait();

    if (teamTrait?.userMember?.id === memberId) {
      return this.loadOnlineProject(teamTrait.userMember.project);
    } else {
      return combineLatest([this.teamAPIService.getTeamProject(teamId, memberId), this.teamAPIService.getTeam(teamId)]).pipe(
        tap(([project, team]) => {
          this.trackingService.trackEvent('load team project', { category: 'project' });

          this.exportService.importText(project.data);
          this.project$.next(this.injector.runInContext(() => new OnlineProject(project, team)));
        }),
        this.loadProject()
      );
    }
  }

  loadProjectFromCockpit() {
    this.embeddedDecisionToolPortal.open(
      this.cockpitOrigin,
      (project: EmbeddableProject) => {
        this.exportService.importText(project.data);
        this.project$.next(this.injector.runInContext(() => new LocalProject('embedded')));
        this.router.navigateByUrl(this.decisionData.lastUrl || '/start', { onSameUrlNavigation: 'reload' });
      },
      () => ({
        name: this.decisionData.decisionProblem,
        data: this.exportService.dataToText(),
      }),
      this.timeTrackingService.decisionDataChanged()
    );

    return this.embeddedDecisionToolPortal.isProjectLoaded();
  }

  importFile(file: File): Observable<boolean> {
    return FileExport.readFile(file).pipe(
      tap(fileContent => {
        this.trackingService.trackEvent('import project', { category: 'project' });

        this.exportService.importText(fileContent);
        this.project$.next(this.injector.runInContext(() => new LocalProject('local')));
      }),
      this.loadProject()
    );
  }

  closeProject() {
    return of(null).pipe(
      tap(() => {
        this.exportService.resetDecisionData();
        this.project$.next(null);
      }),
      this.loadProject()
    );
  }

  /**
   * Handles unsaved changes in the currently opened project, if there are any.
   * Returns true if unloading was successful and false if the user aborted.
   */
  confirmProjectUnload(closeConfirmModalData: ConfirmModalData): Observable<boolean> {
    const project = this.getProject();

    if (project == null || project.isProjectSaved()) {
      // The project is already saved, nothing to do
      return of(true);
    }

    if (project instanceof OnlineProject) {
      return this.dialog.open<UnloadOnlineProjectModalComponent, void, boolean>(UnloadOnlineProjectModalComponent).beforeClosed();
    } else {
      return this.dialog
        .open<ConfirmModalComponent, ConfirmModalData, boolean>(ConfirmModalComponent, {
          data: closeConfirmModalData,
        })
        .beforeClosed();
    }
  }

  /**
   * An RxJS operator that confirms with the user to close the project on unsaved changes, and then
   * executes the given observable. It
   *
   * Returns true if closing succeeded and false if not.
   */
  private loadProject<T>(options: Partial<{ overrideLastUrl: string; forceUnload: boolean }> = {}): OperatorFunction<T, boolean> {
    return observable =>
      (options.forceUnload
        ? of(true)
        : this.confirmProjectUnload({
            title: $localize`Projekt schließen`,
            prompt: $localize`Dein Projekt enthält ungespeicherte Änderungen.
            Wenn Du das Projekt schließt, gehen diese Änderungen verloren.
            Bist Du sicher, dass Du das aktuelle Projekt schließen möchtest?`,
            buttonConfirm: $localize`Ja, Projekt schließen`,
          })
      ).pipe(
        switchMap(confirmed => {
          if (!confirmed) {
            return of(false);
          }

          const previousUrl = this.router.url;

          return from(this.router.navigateByUrl('/loading')).pipe(
            switchMap(() =>
              observable.pipe(
                tap({
                  // If loading succeeded, we open the last url. This is done in an async function but DOES NOT BLOCK the observable.
                  next: async () => {
                    const result = await this.router.navigateByUrl(options.overrideLastUrl || this.decisionData.lastUrl || '/start', {
                      onSameUrlNavigation: 'reload',
                      replaceUrl: true,
                    });
                    if (!result) {
                      await this.router.navigateByUrl('/start', { onSameUrlNavigation: 'reload', replaceUrl: true });
                    }
                  },
                  // If loading has failed, we go back to the previous URL (asynchronously)
                  error: () => this.router.navigateByUrl(previousUrl, { replaceUrl: true }),
                }),
                map(() => true)
              )
            ),
            projectLoadingModal(this.dialog)
          );
        })
      );
  }
}
