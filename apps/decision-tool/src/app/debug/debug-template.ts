import { TemplateRef } from '@angular/core';

export interface Debug {
  hasDebugTemplate: boolean;
  debugTemplate: TemplateRef<any>;
}

export function implementsDebug(element: any): element is Debug {
  return element.hasDebugTemplate !== undefined;
}
