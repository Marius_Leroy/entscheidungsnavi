import { DecisionData } from '@entscheidungsnavi/decision-data';
import { PREDEFINED_INFLUENCE_FACTORS, UserdefinedInfluenceFactor } from '@entscheidungsnavi/decision-data/classes';
import { InfluenceFactorNamePipe, StateNamePipe } from '@entscheidungsnavi/widgets';
import { richToPlainText } from '@entscheidungsnavi/tools';
import { Cell, DataValidation, Worksheet } from 'exceljs';
import { addBorder, background, centerAndWrapText } from '../../excel-export/excel-helpers';

/**
 * Generates the required data and adds it to the provided worksheet {ws}.
 */
export function performInfluenceFactorExport(decisionData: DecisionData, ws: Worksheet, protectSheet: boolean) {
  let currentRowExport = 1;
  addHeader(
    ws,
    [$localize`Benutzerdefinierter Einflussfaktor`, $localize`Zustände`, $localize`Wahrscheinlichkeiten`, $localize`Präzisionsintervall`],
    currentRowExport++
  );
  currentRowExport = addUserdefinedInfluenceFactors(ws, decisionData.influenceFactors, currentRowExport);
  addHeader(ws, [$localize`Systemseitiger Einflussfaktor`, $localize`Szenarien`, $localize`Wahrscheinlichkeiten`], currentRowExport++);
  addPredefinedInfluenceFactors(ws, currentRowExport);
  if (protectSheet) {
    ws.protect('', { deleteRows: true, insertRows: true, formatRows: true, formatColumns: true, formatCells: true });
  }
}

function addHeader(ws: Worksheet, headers: string[], currentRowExport: number) {
  const row = ws.getRow(currentRowExport);
  row.height = 60;
  headers.forEach((text, i) => {
    const cell: Cell = row.getCell(i + 1);
    cell.value = {
      richText: [{ font: { bold: true, color: { argb: '00FFFFFF' } }, text: text }],
    };
    background(cell, '3B4C5B');
    addBorder(cell, '00ffffff');
    cell.border.left.style = 'thick';
    centerAndWrapText(cell);
  });
  ws.getColumn(1).width = 50;
  ws.getColumn(2).width = 30;
  ws.getColumn(3).width = 25;
  ws.getColumn(4).width = 15;
}

function addPredefinedInfluenceFactors(ws: Worksheet, currentRowExport: number) {
  const predefinedInfluenceFactors = Object.entries(PREDEFINED_INFLUENCE_FACTORS).map(entry => entry[1]);
  predefinedInfluenceFactors.forEach(influenceFactor => {
    ws.getCell(currentRowExport, 1).value = InfluenceFactorNamePipe.prototype.transform(influenceFactor);
    currentRowExport++;
    influenceFactor.states.forEach((state, stateIdx) => {
      ws.getCell(currentRowExport, 2).value = StateNamePipe.prototype.transform(influenceFactor, stateIdx);
      ws.getCell(currentRowExport, 3).value = state.probability;
      currentRowExport++;
    });
  });
}

function addUserdefinedInfluenceFactors(ws: Worksheet, influenceFactors: UserdefinedInfluenceFactor[], currentRowExport: number) {
  influenceFactors.forEach((influenceFactor, ufIdx) => {
    let row = ws.getRow(currentRowExport);
    const namePrefix = `i${ufIdx}`; // defined names for formulas, but not supported as ref for addConditionalFormatting()
    // name
    const nameCell = row.getCell(1);
    nameCell.value = influenceFactor.name;
    nameCell.font = { bold: true };
    nameCell.name = namePrefix + 'name';
    try {
      nameCell.note = richToPlainText(influenceFactor.comment);
    } catch (e) {}
    addNameFormatting(nameCell, namePrefix + 'probabilities');
    // precision
    const precisionCell = row.getCell(4);
    precisionCell.value = influenceFactor.precision;
    precisionCell.name = namePrefix + 'precision';
    precisionCell.protection = { locked: false };
    addNumberValidation(precisionCell, 0, 50, $localize`Präzisionsintervall (in Prozent)`);
    addWholeNumberFormatting(precisionCell, 0, 50);
    // states
    influenceFactor.states.forEach((state, stateIdx) => {
      row = ws.getRow(currentRowExport + stateIdx + 1);
      row.getCell(2).value = state.name;
      const probCell = row.getCell(3);
      probCell.value = state.probability;
      probCell.name = 'probability';
      probCell.addName(namePrefix + 'probabilities');
      probCell.protection = { locked: false };
      addNumberValidation(probCell, 0, 100, $localize`Wahrscheinlichkeit (in Prozent)`);
      addWholeNumberFormatting(probCell, 0, 100);
    });
    currentRowExport += influenceFactor.states.length + 1;
  });
  currentRowExport += 2; // leave empty rows between tables
  return currentRowExport;
}

function addNumberValidation(
  cell: Cell,
  min: number,
  max: number,
  title?: string,
  showError = false,
  type: 'whole' | 'decimal' = 'decimal'
) {
  let message: string, errorMessage: string;
  if (type === 'whole') {
    message = $localize`Eine ganze Zahl zwischen ${min} und ${max}`;
    errorMessage = $localize`Der Wert muss eine ganze Zahl zwischen ${min} und ${max} sein`;
  } else {
    message = $localize`Eine Zahl zwischen ${min} und ${max}`;
    errorMessage = $localize`Der Wert muss eine Zahl zwischen ${min} und ${max} sein`;
  }

  const validation: DataValidation = {
    type: type,
    operator: 'between',
    formulae: [min, max],
    showInputMessage: true,
    prompt: message,
  };
  if (title) {
    validation.promptTitle = title;
  }
  if (showError) {
    validation.showErrorMessage = true;
    validation.errorStyle = 'error';
    validation.error = errorMessage;
    if (title) {
      validation.errorTitle = title;
    }
  }

  cell.dataValidation = validation;
}

function addWholeNumberFormatting(cell: Cell, min: number, max: number) {
  cell.worksheet.addConditionalFormatting({
    ref: cell.fullAddress.address,
    rules: [
      {
        // empty cell
        type: 'containsText',
        priority: undefined,
        operator: 'containsBlanks',
        style: { fill: { type: 'pattern', pattern: 'solid', bgColor: { argb: 'FFFFFF00' } } },
      },
      {
        // valid range
        type: 'cellIs',
        priority: undefined,
        formulae: [min, max],
        operator: 'between',
        style: { fill: { type: 'pattern', pattern: 'solid', bgColor: { argb: 'FF00FF00' } } },
      },
      {
        // default
        type: 'expression',
        priority: undefined,
        formulae: ['True'],
        style: { fill: { type: 'pattern', pattern: 'solid', bgColor: { argb: 'FFFF0000' } } },
      },
    ],
  });
}

function addNameFormatting(cell: Cell, definedName: string) {
  cell.worksheet.addConditionalFormatting({
    ref: cell.fullAddress.address,
    rules: [
      {
        type: 'expression',
        priority: undefined,
        formulae: [`SUM(${definedName})=100`],
        style: { fill: { type: 'pattern', pattern: 'solid', bgColor: { argb: 'FFBBFFBB' } } },
      },
    ],
  });
}
