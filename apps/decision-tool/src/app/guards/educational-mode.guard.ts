import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';

@Injectable({
  providedIn: 'root',
})
export class EducationalModeGuard implements CanActivate {
  constructor(private decisionData: DecisionData, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, _state: RouterStateSnapshot) {
    if (this.decisionData.isEducational()) {
      return true;
    } else {
      let redirect: string;
      do {
        redirect = route.data.educationalModeRedirect;
      } while ((route = route.parent) && redirect == null);

      return this.router.parseUrl(redirect ?? '/start');
    }
  }
}
