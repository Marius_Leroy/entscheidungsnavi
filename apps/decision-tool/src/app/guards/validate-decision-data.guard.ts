import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { ErrorMsg } from '@entscheidungsnavi/decision-data/classes';
import { NAVI_STEP_ORDER, NaviStep } from '@entscheidungsnavi/decision-data/steps';
import { flattenErrors } from '@entscheidungsnavi/decision-data/validations';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ErrorModalComponent } from '../../modules/shared/error';
import { urlToNaviSubStep } from '../../modules/shared/navigation/navigation-step';
import { LanguageService } from '../data/language.service';

/**
 * This guard ensures that all previous steps are completed and valid.
 *
 * e.g. if we enter the alternatives, it makes sure that decision-statement and objectives are completed and valid.
 */
@Injectable({ providedIn: 'root' })
export class ValidateDecisionDataGuard implements CanActivateChild {
  // What checks need to succeed for a step to be valid
  static getChecksForSteps(dd: DecisionData): { [key in NaviStep]: (() => [boolean, ErrorMsg[]] | [boolean, ErrorMsg[], number])[] } {
    return {
      decisionStatement: [() => dd.validateDecisionStatement()],
      objectives: [() => dd.validateObjectives()],
      alternatives: [() => dd.validateAlternatives()],
      impactModel: [() => dd.validateObjectiveScales(), () => dd.validateOutcomes(), () => dd.validateInfluenceFactors()],
      results: [() => dd.validateWeights(), () => dd.validateUtilityFunctions()],
      finishProject: [],
    };
  }

  static getFirstErrorStepWithErrors(dd: DecisionData, untilStep: NaviStep = 'results'): [NaviStep | null, [boolean, ErrorMsg[]]] {
    let errorResult: [boolean, ErrorMsg[]] = [true, null];
    let firstErrorStep: NaviStep = null;

    const checkFunctions = ValidateDecisionDataGuard.getChecksForSteps(dd);
    // The steps are checked in order to ensure that we navigate to the _first_ step with an error
    for (const step of NAVI_STEP_ORDER.slice(0, NAVI_STEP_ORDER.indexOf(untilStep) + 1)) {
      const errorResultForThisStep: [boolean, ErrorMsg[]][] = checkFunctions[step].map(fct => {
        const [valid, messages] = fct();
        return [valid, messages];
      });

      errorResult = flattenErrors([errorResult, ...errorResultForThisStep]);

      // For the first error we find, we set firstError
      if (!errorResult[0] && firstErrorStep == null) {
        firstErrorStep = step;
      }
    }

    return [firstErrorStep, errorResult];
  }

  constructor(
    private decisionData: DecisionData,
    private dialog: MatDialog,
    private router: Router,
    private languageService: LanguageService
  ) {}

  canActivateChild(_childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const targetStep = urlToNaviSubStep(state.url);

    if (targetStep == null) {
      return true;
    }

    // eslint-disable-next-line prefer-const
    let [firstErrorStep, errorResult] = ValidateDecisionDataGuard.getFirstErrorStepWithErrors(this.decisionData, targetStep.step);

    // Navigation to decision statement should be safe.
    if (targetStep.step === 'decisionStatement') {
      errorResult = [true, []];
    }

    // Navigation to some prior (or the same) step should also be safe.
    if (['objectives', 'alternatives', 'impactModel'].includes(targetStep.step)) {
      if (NAVI_STEP_ORDER.indexOf(firstErrorStep) >= NAVI_STEP_ORDER.indexOf(targetStep.step)) {
        errorResult = [true, []];
      }
    }

    if (targetStep.step === 'results' && targetStep.subStepIndex <= 1 && firstErrorStep === 'results') {
      errorResult = [true, []];
    }

    if (!errorResult[0]) {
      ErrorModalComponent.open(
        this.dialog,
        $localize`Du musst zunächst den Schritt ${this.languageService.steps[firstErrorStep].name}
        abschließen, um hierhin navigieren zu können.`,
        errorResult[1]
      );

      const currentStep = urlToNaviSubStep(this.router.url);
      if (firstErrorStep !== currentStep?.step) {
        return this.router.parseUrl(this.languageService.steps[firstErrorStep].routerLink);
      } else {
        return false;
      }
    }

    return true;
  }
}
