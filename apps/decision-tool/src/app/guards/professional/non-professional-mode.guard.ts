import { Injectable } from '@angular/core';
import { UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { NaviStep } from '@entscheidungsnavi/decision-data/steps';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ModeTransitionService } from '../../../modules/shared/mode-transition/mode-transition.service';
import { SwitchProfessionalEducationalModalComponent } from '../../../modules/shared/mode-transition';

@Injectable({
  providedIn: 'root',
})
export class NonProfessionalModeGuard {
  constructor(private decisionData: DecisionData, private dialog: MatDialog, private modeTransitionService: ModeTransitionService) {}

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.decisionData.isProfessional()) {
      return this.handleInProfessional();
    } else {
      return true;
    }
  }

  handleInProfessional() {
    // beforeClosed to make sure the modal does not call history.pop()
    this.dialog
      .open(SwitchProfessionalEducationalModalComponent)
      .beforeClosed()
      .subscribe((stepToNavigateTo: NaviStep | null) => {
        if (stepToNavigateTo !== null) {
          this.modeTransitionService.transitionIntoEducational(stepToNavigateTo);
        }
      });
    // Block the current navigation. The transition service will handle it.
    return false;
  }
}
