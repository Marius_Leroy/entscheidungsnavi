import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert_unreachable';
import { ProjectService } from '../data/project';

export const projectLoadedGuard: CanActivateFn = () => {
  if (inject(ProjectService).project$.value != null) {
    return true;
  } else {
    return inject(Router).parseUrl('/start');
  }
};

export const startPageGuard: CanActivateFn = () => {
  if (inject(ProjectService).project$.value == null) return true;

  const router = inject(Router);
  const projectMode = inject(DecisionData).projectMode;

  switch (projectMode) {
    case 'starter':
      return router.parseUrl('/decisionstatement');
    case 'educational':
      return router.parseUrl('/decisionstatement/steps/1');
    case 'professional':
      return router.parseUrl('/unguided');
    default:
      assertUnreachable(projectMode);
  }
};
