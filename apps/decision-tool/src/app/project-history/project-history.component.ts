import { Component } from '@angular/core';
import { OnlineProjectsService } from '@entscheidungsnavi/api-client';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import { catchError, EMPTY, filter, map, startWith, switchMap } from 'rxjs';
import { Router } from '@angular/router';
import { OnlineProject, ProjectService } from '../data/project';

@Component({
  selector: 'dt-project-history',
  templateUrl: './project-history.component.html',
  styleUrls: ['./project-history.component.scss'],
})
export class ProjectHistoryComponent {
  hasError = false;

  projectData$ = this.projectService.project$.pipe(
    filter((project): project is OnlineProject => project instanceof OnlineProject),
    switchMap(project =>
      project.onSaved$.pipe(
        startWith(null),
        map(() => project)
      )
    ),
    switchMap(project =>
      this.onlineProjectsService.getHistory(project.info.id).pipe(map(history => ({ project, history: history.list.reverse() })))
    ),
    catchError(() => {
      this.hasError = true;
      return EMPTY;
    })
  );

  constructor(
    private projectService: ProjectService,
    private onlineProjectsService: OnlineProjectsService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  onClick(projectId: string, historyEntryId?: string) {
    this.projectService.loadOnlineProject(projectId, { historyEntryId, overrideLastUrl: this.router.url }).subscribe({
      error: () =>
        this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
          data: { icon: 'error', message: $localize`Beim Laden der Version ist ein Fehler aufgetreten` },
          duration: 5000,
        }),
    });
  }
}
