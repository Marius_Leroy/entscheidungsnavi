import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarComponent, SnackbarData } from '@entscheidungsnavi/widgets';
import { ProjectService } from '../data/project';

export interface ProjectConflictData {
  projectId: string;
  localLoadTime: Date;
  onlineUpdateTime: Date;
}

export type ProjectConflictResult = 'take-online' | 'take-local' | 'cancel';

@Component({
  templateUrl: './project-conflict-modal.component.html',
  styleUrls: ['./project-conflict-modal.component.scss'],
})
export class ProjectConflictModalComponent {
  isLoading = false;

  constructor(
    private dialogRef: MatDialogRef<ProjectConflictModalComponent>,
    @Inject(MAT_DIALOG_DATA) protected data: ProjectConflictData,
    private snackBar: MatSnackBar,
    private projectService: ProjectService
  ) {}

  close(result: ProjectConflictResult = 'cancel') {
    if (this.isLoading) return;

    if (result === 'take-online') {
      this.isLoading = true;

      this.projectService.loadOnlineProject(this.data.projectId, { forceUnload: true }).subscribe({
        next: () => this.dialogRef.close(result),
        error: () =>
          this.snackBar.openFromComponent<SnackbarComponent, SnackbarData>(SnackbarComponent, {
            data: {
              message: $localize`Fehler beim Laden des Projekts aus dem Account. Bitte versuche es noch einmal.`,
              dismissButton: true,
              icon: 'error',
            },
          }),
      });
    } else {
      this.dialogRef.close(result);
    }
  }
}
