import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UserdefinedInfluenceFactor } from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { checkType } from '@entscheidungsnavi/tools';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { NavLine, navLineElement, Navigation } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';
import { InfluenceFactorModalComponent, InfluenceFactorModalData } from './influence-factor-modal/influence-factor-modal.component';
import { InfluenceFactorHelperService } from './influence-factors-helper.service';

@Component({
  selector: 'dt-influence-factors',
  templateUrl: './influence-factors.component.html',
  styleUrls: ['./influence-factors.component.scss'],
})
@DisplayAtMaxWidth
export class InfluenceFactorsComponent implements OnInit, Navigation, HelpMenuProvider, OnDestroy {
  @Input()
  showTitle = true;

  navLine = new NavLine({
    left: [navLineElement().back('/impactmodel').build()],
  });

  helpMenu = getHelpMenu(this.languageService.steps, 'uncertaintyfactors');

  influenceFactors: Map<UserdefinedInfluenceFactor, { usage: number; isNormalized: boolean }>;

  // The influence factors split into the displayed columns
  influenceFactorColumnCount = 3;
  influenceFactorColumns: UserdefinedInfluenceFactor[][];

  constructor(
    protected decisionData: DecisionData,
    private languageService: LanguageService,
    private dialog: MatDialog,
    private influenceFactorHelper: InfluenceFactorHelperService
  ) {}

  ngOnInit() {
    this.influenceFactors = new Map();
    this.decisionData.influenceFactors.forEach(influenceFactor => {
      this.influenceFactors.set(influenceFactor, { usage: 0, isNormalized: true });
    });

    this.decisionData.outcomes.forEach(ocForAlt =>
      ocForAlt
        .map(outcome => outcome.influenceFactor)
        .filter(influenceFactor => influenceFactor && influenceFactor instanceof UserdefinedInfluenceFactor)
        .forEach(influenceFactor => this.influenceFactors.get(influenceFactor as UserdefinedInfluenceFactor).usage++)
    );

    this.recomputeColumns();
  }

  ngOnDestroy() {
    const normalizedInfluenceFactors = Array.from(this.influenceFactors.entries())
      .filter(([_, info]) => !info.isNormalized)
      .map(([influenceFactor]) => influenceFactor);

    if (normalizedInfluenceFactors.length > 0) {
      this.influenceFactorHelper.showInfluenceFactorsNormalizedNotification(
        normalizedInfluenceFactors.map(influenceFactor => influenceFactor.name)
      );
    }
  }

  addInfluenceFactor() {
    this.dialog.open(InfluenceFactorModalComponent, {
      data: checkType<InfluenceFactorModalData>({
        onSaveCallback: influenceFactorId => {
          this.influenceFactors.set(this.decisionData.influenceFactors[influenceFactorId], { usage: 0, isNormalized: true });
          this.recomputeColumns();
        },
      }),
    });
  }

  deleteInfluenceFactor(influenceFactor: UserdefinedInfluenceFactor) {
    this.influenceFactors.delete(influenceFactor);
    this.decisionData.removeInfluenceFactor(influenceFactor.id);
    this.recomputeColumns();
  }

  onWidthTriggerChange(state: { [key: string]: boolean }) {
    const columnCount = state['one-column'] ? 1 : state['two-column'] ? 2 : 3;
    if (columnCount !== this.influenceFactorColumnCount) {
      this.influenceFactorColumnCount = columnCount;
      this.recomputeColumns();
    }
  }

  private recomputeColumns() {
    // Heuristically split the influence factors into columns
    const columnStateCounts = new Array(this.influenceFactorColumnCount).fill(0);
    this.influenceFactorColumns = columnStateCounts.map(_ => []);

    for (const influenceFactor of this.decisionData.influenceFactors) {
      const smallestColumnHeight = Math.min(...columnStateCounts);
      const smallestColumnIndex = columnStateCounts.indexOf(smallestColumnHeight);

      this.influenceFactorColumns[smallestColumnIndex].push(influenceFactor);
      columnStateCounts[smallestColumnIndex] += influenceFactor.states.length;
    }
  }
}
