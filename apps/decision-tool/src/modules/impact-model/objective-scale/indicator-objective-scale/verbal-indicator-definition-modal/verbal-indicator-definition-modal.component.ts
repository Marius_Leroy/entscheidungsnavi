import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { cloneDeep, isEqual } from 'lodash';
import { Indicator, Objective, ObjectiveInput } from '@entscheidungsnavi/decision-data/classes';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data/steps';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { Tree } from '@entscheidungsnavi/tools';
import { firstValueFrom } from 'rxjs';

export type VerbalIndicatorDefinitionModalData = {
  indicatorIdx: number;
  indicatorCount: number;
  indicator: Indicator;
  objectiveIdx: number;
  objective: Objective;
  objectiveTree: Tree<ObjectiveElement>;
  outcomeValues: ObjectiveInput[][]; // [alternativeIdx][stageIdx][indicatorIdx][categoryIdx]
};

@Component({
  selector: 'dt-verbal-indicator-definition-modal',
  templateUrl: './verbal-indicator-definition-modal.component.html',
})
export class VerbalIndicatorDefinitionModalComponent {
  verbalIndicatorDefinitionModalData: VerbalIndicatorDefinitionModalData & { originalIndicator: Indicator };
  showEmptyField = false;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: VerbalIndicatorDefinitionModalData,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<VerbalIndicatorDefinitionModalComponent>
  ) {
    this.verbalIndicatorDefinitionModalData = {
      ...data,
      indicator: cloneDeep(data.indicator),
      originalIndicator: cloneDeep(data.indicator),
      outcomeValues: cloneDeep(data.outcomeValues),
    };
  }

  get isValid() {
    return (
      (this.verbalIndicatorDefinitionModalData.indicator.verbalIndicatorCategories.every(category => category.name !== '') ||
        this.verbalIndicatorDefinitionModalData.indicator.verbalIndicatorCategories.length === 1) &&
      this.verbalIndicatorDefinitionModalData.indicator.verbalIndicatorCategories.every(category =>
        category.stages.every(stage => stage.name !== '')
      )
    );
  }

  get hasChanges() {
    return !isEqual(this.verbalIndicatorDefinitionModalData.indicator, this.verbalIndicatorDefinitionModalData.originalIndicator);
  }

  apply() {
    if (!this.isValid) {
      return;
    }

    this.dialogRef.close({
      outcomeValues: this.verbalIndicatorDefinitionModalData.outcomeValues,
      verbalIndicatorCategories: this.verbalIndicatorDefinitionModalData.indicator.verbalIndicatorCategories,
    });
  }

  async deleteAndApply() {
    if (
      await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              title: $localize`Qualitative Skala löschen`,
              prompt: $localize`Bist Du sicher,
            dass Du den verbalen Indikator zurüclsetzen willst? Damit werden alle aktuelle Kategorien verloren.`,
              template: 'discard',
            },
          })
          .afterClosed()
      )
    ) {
      this.dialogRef.close({
        outcomeValues: null,
        verbalIndicatorCategories: [],
      });
    }
  }

  async tryClose() {
    if (
      !this.hasChanges ||
      (await firstValueFrom(
        this.dialog
          .open<ConfirmModalComponent, ConfirmModalData>(ConfirmModalComponent, {
            data: {
              title: $localize`Änderungen verwerfen`,
              prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen an der quantitativen Skala verwerfen willst?`,
              template: 'discard',
            },
          })
          .afterClosed()
      ))
    ) {
      this.dialogRef.close();
    }
  }
}
