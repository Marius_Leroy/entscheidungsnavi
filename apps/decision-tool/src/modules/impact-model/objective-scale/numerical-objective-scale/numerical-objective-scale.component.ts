import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import { NumericalObjectiveData } from '@entscheidungsnavi/decision-data/classes';
import { first } from 'rxjs/operators';

@Component({
  selector: 'dt-numerical-objective-scale',
  templateUrl: './numerical-objective-scale.component.html',
  styleUrls: ['./numerical-objective-scale.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumericalObjectiveScaleComponent implements OnInit {
  @Input() numericalData: NumericalObjectiveData;
  @Output() isValid = new EventEmitter<boolean>();
  @Output() isDirty = new EventEmitter<boolean>();

  form: UntypedFormGroup;

  constructor(private fb: UntypedFormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group(
      {
        from: this.fb.group({
          value: [this.numericalData.from, Validators.required],
          explanation: [this.numericalData.explanationFrom],
          comment: [this.numericalData.commentFrom],
        }),
        to: this.fb.group({
          value: [this.numericalData.to, Validators.required],
          explanation: [this.numericalData.explanationTo],
          comment: [this.numericalData.commentTo],
        }),
        unit: [this.numericalData.unit, Validators.maxLength(10)],
      },
      { validators: this.validateInterval as ValidatorFn }
    );

    this.form.statusChanges.subscribe(status => this.isValid.emit(status === 'VALID'));
    this.isValid.emit(this.form.valid);

    this.isDirty.emit(false);
    this.form.valueChanges.pipe(first()).subscribe(() => this.isDirty.emit(true));
  }

  save() {
    this.numericalData.from = this.form.get('from.value').value;
    this.numericalData.explanationFrom = this.form.get('from.explanation').value;
    this.numericalData.commentFrom = this.form.get('from.comment').value;
    this.numericalData.to = this.form.get('to.value').value;
    this.numericalData.explanationTo = this.form.get('to.explanation').value;
    this.numericalData.commentTo = this.form.get('to.comment').value;
    this.numericalData.unit = this.form.get('unit').value;
  }

  private validateInterval(group: UntypedFormGroup) {
    const from = group.get('from.value').value,
      to = group.get('to.value').value;
    if (from === to) {
      return { collapsedInterval: true };
    }
    return null;
  }
}
