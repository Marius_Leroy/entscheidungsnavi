import { Component } from '@angular/core';

@Component({
  template: `<p *dtProjectMode="'educational'" i18n
      >Gib für alle Alternativen nun deren Auswirkungen in den Zielen an. Wähle hierfür am besten individuelle Messskalen für die Ziele.
      Falls die Ergebnisse unsicher sind, kannst Du über das Kachelfeld in jedem Matrixfeld einen zusätzlichen Einflussfaktor definieren und
      die Unsicherheit explizit modellieren.</p
    >
    <div class="dt-help-padding">
      <p *dtProjectMode="'starter'" i18n
        >Gib für alle Alternativen an, wie gut Du diese in den Zielen einschätzt. Standardmäßig ist eine numerische Skala von 1 bis 7
        vorgegeben. 1 steht hierbei für ein sehr schlechtes Ergebnis, 7 für ein sehr gutes Ergebnis. In unseren Starter-Vorlagen, die Du zu
        Beginn wählen konntest, sind in den meisten Fällen numerische Skalen von 1 bis 10 eingestellt. Du kannst auch eine beliebige andere
        Skala selbst definieren und verwenden.</p
      >
    </div>`,
  styleUrls: ['../../hints.scss'],
})
export class ImpactModelHelpMainComponent {}
