import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ExplanationService } from '../../shared/decision-quality';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { HelpMenuProvider } from '../../../app/help/help';
import { getHelpMenu } from '../help';
import { NavLine, navLineElement, Navigation } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';

@Component({
  selector: 'dt-impactmodel',
  templateUrl: './impact-model.component.html',
  styleUrls: ['./impact-model.component.scss'],
})
@DisplayAtMaxWidth
export class ImpactModelComponent implements Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [navLineElement().back('/alternatives').build()],
    middle: [
      navLineElement()
        .label($localize`Einflussfaktoren`)
        .condition(() => this.decisionData.projectMode === 'educational')
        .link('/impactmodel/uncertaintyfactors')
        .build(),
      this.explanationService.generateAssessmentButton('CLEAR_VALUES'),
    ],
    right: [
      ...navLineElement()
        .continue('/results/steps/1')
        .condition(() => this.decisionData.projectMode === 'educational')
        .orElse(builder => {
          return builder.continue('/results/steps/2');
        }),
    ],
  });

  helpMenu = getHelpMenu(this.languageService.steps);

  constructor(
    private decisionData: DecisionData,
    private explanationService: ExplanationService,
    private languageService: LanguageService
  ) {}
}
