import { ChangeDetectorRef, Component, ElementRef, Input, OnInit } from '@angular/core';
import {
  generatePredefinedScenarios,
  Objective,
  ObjectiveInput,
  Outcome,
  PredefinedInfluenceFactor,
  PredefinedScenario,
} from '@entscheidungsnavi/decision-data/classes';
import { calculateIndicatorValue, getIndicatorAggregationFunction } from '@entscheidungsnavi/decision-data/calculation';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'dt-tornado-diagram',
  templateUrl: './tornado-diagram.component.html',
  styleUrls: ['./tornado-diagram.component.scss'],
})
export class TornadoDiagramComponent implements OnInit {
  @Input()
  objective: Objective;

  @Input()
  outcome: Outcome;

  /* Tornado diagram data. */
  tornado: { indicatorIndex: number; tornadoValues: { indicatorValue: number; outcomeValue: number }[] }[] = [];

  /* Current scale min/max. */
  scaleMin: number;
  scaleMax: number;

  /* Scale min/max when "Worst-Best-Szenarien" is selected. */
  worstBestScaleMin: number;
  worstBestScaleMax: number;

  /* Scale min/max when "fixed" is selected. */
  fixedScaleMin: number;
  fixedScaleMax: number;

  /* Total scale (indicator min and max). */
  indicatorScaleMin: number;
  indicatorScaleMax: number;

  /* Expected value in the probabilistic variant (center dashed line). */
  totalExpectedValue: number;

  indicatorFunction: (values: ObjectiveInput) => number;

  /* Select box values. */
  isProbabilisticVariant = false;
  scaleVariant: 'total' | 'worst-best' | 'fixed' = 'worst-best';

  /* Equal to the longest indicator name + unit. 10 by default because of "Ergebnis". */
  longestLabelSize = 10;

  /* Index of hovered tornado line. */
  hoverBarIndex = -1;
  /* Index of hovered white circle. */
  hoverWhiteCircleIndex = -1;

  /* Width of the chart as percentage of the chart canvas. */
  readonly chartWidthPercentage = 85;
  /* Padding around the chart in px (all directions). */
  readonly chartPadding = 30;
  /* Height of a tornado line in px. */
  readonly tornadoLineHeight = 10;
  /* Spacing between tornado lines in px. */
  readonly tornadoLineSpacing = 25;
  /* Spacing between the last tornado line and the x axis in px. */
  readonly scaleSpacing = 30;
  /* Thickness of vertical dashed lines in px. */
  readonly dashedLineThickness = 2;
  /* Number of significant digits when rounding numbers. */
  readonly significantDigits = 5;
  /* Spacing between tornado line and number in px. */
  readonly numberSpacing = 13;
  /* Pixel per character ratio. */
  readonly pixelPerCharacter = 10;

  constructor(private elem: ElementRef, private cdRef: ChangeDetectorRef) {}

  ngOnInit() {
    // get indicator scale min/max
    if (!this.objective.indicatorData.useCustomAggregation) {
      // additive min/max
      this.indicatorScaleMin = this.objective.indicatorData.defaultAggregationWorst;
      this.indicatorScaleMax = this.objective.indicatorData.defaultAggregationBest;
      this.indicatorFunction = getIndicatorAggregationFunction(this.objective.indicatorData.indicators, {
        worst: this.objective.indicatorData.defaultAggregationWorst,
        best: this.objective.indicatorData.defaultAggregationBest,
      });
    } else {
      // custom min/max
      this.indicatorFunction = getIndicatorAggregationFunction(
        this.objective.indicatorData.indicators,
        this.objective.indicatorData.customAggregationFormula
      );
      this.indicatorScaleMin = this.indicatorFunction(this.objective.indicatorData.indicators.map(ind => [ind.min]));
      this.indicatorScaleMax = this.indicatorFunction(this.objective.indicatorData.indicators.map(ind => [ind.max]));
    }
    // calculate longest label (indicator name + unit)
    for (const indicator of this.objective.indicatorData.indicators) {
      this.longestLabelSize = Math.max(this.longestLabelSize, indicator.name.length + indicator.unit.length);
    }
    // draw diagram
    if (this.validInput()) {
      this.createTornadoDiagram();
    }
  }

  createTornadoDiagram() {
    this.calculateTornadoValues();
    this.orderTornadoLines();
    this.orderTornadoValues();
    this.calculateScaleLimits();
    this.updateCurrentScaleLimits();
  }

  /* Convert outcome.values into tornado values. */
  private calculateTornadoValues() {
    this.tornado = [];
    if (!this.isProbabilisticVariant) {
      for (let stateIdx = 0; stateIdx < this.outcome.values.length; stateIdx++) {
        for (let indicatorIdx = 0; indicatorIdx < this.outcome.values[stateIdx].length; indicatorIdx++) {
          this.setTornadoIndicatorValue(stateIdx, indicatorIdx);
          const indicatorInputValues = cloneDeep(this.outcome.values[1]); // clone median vector
          indicatorInputValues[indicatorIdx] = this.outcome.values[stateIdx][indicatorIdx]; // insert current indicator value
          this.tornado[indicatorIdx].tornadoValues[stateIdx].outcomeValue = this.indicatorFunction(indicatorInputValues);
        }
      }
      /* The 3 center values are equal in simple variant. */
      this.totalExpectedValue = this.tornado[0].tornadoValues[1].outcomeValue;
    } else {
      /* Reset total expected value. */
      this.totalExpectedValue = 0;
      /* Generate combinations. */
      const cartesian: (PredefinedScenario & { indicatorFunctionResult?: number })[] = generatePredefinedScenarios(
        this.outcome.values,
        (this.outcome.influenceFactor as PredefinedInfluenceFactor).states.map(state => state.probability / 100)
      );
      /* Calculate indicator value for each combination. */
      for (const combination of cartesian) {
        combination.indicatorFunctionResult = this.indicatorFunction(combination.value);
      }
      for (let stateIdx = 0; stateIdx < this.outcome.values.length; stateIdx++) {
        for (let indicatorIdx = 0; indicatorIdx < this.outcome.values[stateIdx].length; indicatorIdx++) {
          let expectedValue = 0;
          for (const combination of cartesian) {
            if (combination.stateIndices[indicatorIdx] === stateIdx) {
              let probabilityMultiplier = 1;
              for (const indicatorIndex of combination.stateIndices) {
                probabilityMultiplier *= this.outcome.influenceFactor.states[indicatorIndex].probability / 100;
              }
              probabilityMultiplier *= 100 / this.outcome.influenceFactor.states[stateIdx].probability;
              expectedValue += combination.indicatorFunctionResult * probabilityMultiplier;
            }
          }
          this.setTornadoIndicatorValue(stateIdx, indicatorIdx);
          this.tornado[indicatorIdx].tornadoValues[stateIdx].outcomeValue = expectedValue;
          this.totalExpectedValue += expectedValue;
        }
      }
      this.totalExpectedValue /= this.objective.indicatorData.indicators.length * 3;
    }
  }

  private setTornadoIndicatorValue(stateIdx: number, indicatorIdx: number) {
    if (this.tornado[indicatorIdx] == null) {
      this.tornado[indicatorIdx] = {
        indicatorIndex: indicatorIdx,
        tornadoValues: [
          { indicatorValue: null, outcomeValue: null },
          { indicatorValue: null, outcomeValue: null },
          { indicatorValue: null, outcomeValue: null },
        ],
      };
    }
    this.tornado[indicatorIdx].tornadoValues[stateIdx].indicatorValue = calculateIndicatorValue(
      this.outcome.values[stateIdx][indicatorIdx],
      this.objective.indicatorData.indicators[indicatorIdx]
    );
  }

  /* Order tornado rows, putting the longer lines on top. */
  private orderTornadoLines() {
    this.tornado.sort((a, b) => {
      if (
        Math.abs(a.tornadoValues[2].outcomeValue - a.tornadoValues[0].outcomeValue) >
        Math.abs(b.tornadoValues[2].outcomeValue - b.tornadoValues[0].outcomeValue)
      ) {
        return -1;
      } else {
        return 1;
      }
    });
  }

  /* Order tornado values in increasing or decreasing order, depending on the indicator scale. */
  private orderTornadoValues() {
    const orderDirection = this.indicatorScaleMin < this.indicatorScaleMax ? 1 : -1;
    this.tornado.forEach(tornadoLine => {
      tornadoLine.tornadoValues.sort((a, b) => {
        if (a.outcomeValue > b.outcomeValue) {
          return orderDirection;
        } else {
          return -orderDirection;
        }
      });
    });
  }

  private calculateScaleLimits() {
    const allOutcomeValues = this.tornado.flatMap(t => t.tornadoValues.map(v => v.outcomeValue));
    const scaleLimit1 = this.indicatorFunction(this.outcome.values[0]);
    const scaleLimit2 = this.indicatorFunction(this.outcome.values[2]);
    if (this.indicatorScaleMin < this.indicatorScaleMax) {
      this.fixedScaleMin = Math.min(...allOutcomeValues);
      this.fixedScaleMax = Math.max(...allOutcomeValues);
      this.worstBestScaleMin = Math.min(scaleLimit1, scaleLimit2);
      this.worstBestScaleMax = Math.max(scaleLimit1, scaleLimit2);
    } else {
      this.fixedScaleMin = Math.max(...allOutcomeValues);
      this.fixedScaleMax = Math.min(...allOutcomeValues);
      this.worstBestScaleMin = Math.max(scaleLimit1, scaleLimit2);
      this.worstBestScaleMax = Math.min(scaleLimit1, scaleLimit2);
    }
  }

  updateCurrentScaleLimits() {
    switch (this.scaleVariant) {
      case 'total':
        this.scaleMin = this.indicatorScaleMin;
        this.scaleMax = this.indicatorScaleMax;
        break;
      case 'worst-best':
        this.scaleMin = this.worstBestScaleMin;
        this.scaleMax = this.worstBestScaleMax;
        break;
      case 'fixed':
        this.scaleMin = this.fixedScaleMin;
        this.scaleMax = this.fixedScaleMax;
        break;
    }
    this.cdRef.detectChanges();
  }

  calculateLeftPercentage(value: number) {
    const numerator = this.scaleMax > this.scaleMin ? value - this.scaleMin : this.scaleMin - value;
    return (numerator / Math.abs(this.scaleMin - this.scaleMax)) * this.chartWidthPercentage + (100 - this.chartWidthPercentage) / 2;
  }

  calculateWidthPercentage(tornadoValues: { indicatorValue: number; outcomeValue: number }[]) {
    return (
      (Math.abs(tornadoValues[2].outcomeValue - tornadoValues[0].outcomeValue) / Math.abs(this.scaleMin - this.scaleMax)) *
      this.chartWidthPercentage
    );
  }

  calculateTopPixels(index: number) {
    return index * (this.tornadoLineHeight + this.tornadoLineSpacing) + this.chartPadding;
  }

  areElementsOverlappingHorizontally(currentElement: HTMLElement, elements: HTMLElement[]) {
    const currentRect = currentElement.getBoundingClientRect();
    for (const element of elements) {
      const elementRect = element.getBoundingClientRect();
      if (!(currentRect.right < elementRect.left || currentRect.left > elementRect.right)) {
        return true;
      }
    }
    return false;
  }

  isIndicatorValueVisible(currentElement: HTMLElement, elements: HTMLElement[], index: number, isCenter = false) {
    if (isCenter) {
      /* Center is displayed when the white circle is hovered OR when there's NO overlapping. */
      return !this.areElementsOverlappingHorizontally(currentElement, elements) || this.hoverWhiteCircleIndex === index;
    } else {
      /* Left and right are shown when the white circle is NOT hovered AND when they do NOT overlap with each other. */
      return !this.areElementsOverlappingHorizontally(currentElement, elements) && this.hoverWhiteCircleIndex !== index;
    }
  }

  private validInput() {
    return this.outcome.values.flat(2).every(value => value != null);
  }

  isScaleValueVisible(value: number) {
    return value !== this.scaleMin && value !== this.scaleMax;
  }

  isNumberRounded(value: number) {
    return value > 999999 || value < -999999 || String(value).split('.')[1]?.length > this.significantDigits;
  }
}
