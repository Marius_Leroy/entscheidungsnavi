import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'dt-numerical-input',
  templateUrl: './numerical-input.component.html',
  styleUrls: ['./numerical-input.component.scss'],
})
export class NumericalInputComponent {
  @Input() unit: string;
  @Input() from: number;
  @Input() to: number;
  @Input() value: number;
  @Input() grayedOutText = false;
  @Output() valueChange = new EventEmitter<number>();

  get isValueValid() {
    return (this.value >= this.from && this.value <= this.to) || (this.value >= this.to && this.value <= this.from);
  }
}
