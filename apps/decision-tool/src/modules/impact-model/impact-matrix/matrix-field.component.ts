import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnChanges,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Alternative, Objective, ObjectiveInput, Outcome, PredefinedInfluenceFactor } from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { validateValue } from '@entscheidungsnavi/decision-data/validations';
import { MatDialog } from '@angular/material/dialog';
import { sum, zip } from 'lodash';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PopOverService } from '@entscheidungsnavi/widgets';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, takeUntil } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { MatSelect } from '@angular/material/select';
import { ProjectService } from '../../../app/data/project';
import {
  ForecastOfOutcomesModalComponent,
  ForecastOfOutcomesModalData,
} from './forecast-of-outcomes-modal/forecast-of-outcomes-modal.component';

@Component({
  selector: 'dt-impact-matrix-field',
  templateUrl: './matrix-field.component.html',
  styleUrls: ['matrix-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImpactMatrixFieldComponent implements OnChanges {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() outcome: Outcome;
  @Input() objective: Objective;
  @Input() alternative: Alternative;
  @Input() colorOutcomes = false;
  @Output() matrixFieldChange = new EventEmitter<void>();
  @Output() isValueValidChange = new EventEmitter<boolean>();
  @Output() influenceFactorModified = new EventEmitter<number>(); // Emits the ID of the modified influence factor

  @HostBinding('attr.data-cy')
  validationStatus = 'invalid';

  // Data specific to numerical scales
  numericalScaleMin: number;
  numericalScaleMax: number;

  // True iff the current values are valid and the outcome is processed
  isValueValid: boolean;
  hasTornadoDiagram: boolean;

  // Smallest/biggest value in outcome.values (indicator values are rounded to 2 decimal places)
  // and the corresponding unit
  min: string;
  max: string;
  unit: string;

  backgroundImage: SafeStyle;

  @ViewChild(MatSelect) verbalInput: MatSelect;
  @ViewChild('readonlyDisplay') readonlyDisplay: ElementRef<HTMLElement>;
  @ViewChild('numericalInputPopover') numericalInputPopover: TemplateRef<unknown>;

  get teamTrait() {
    return this.projectService.getTeamTrait();
  }

  get verbalOutcomeValue() {
    return this.outcome.values[0][0][0] !== undefined ? this.objective.verbalData.options[this.outcome.values[0][0][0] - 1] : null;
  }

  constructor(
    protected decisionData: DecisionData,
    private domSanitizer: DomSanitizer,
    private cdRef: ChangeDetectorRef,
    private matDialog: MatDialog,
    protected snackBar: MatSnackBar,
    protected popOverService: PopOverService,
    private projectService: ProjectService
  ) {
    this.isValueValidChange.pipe(takeUntil(this.onDestroy$)).subscribe(isValid => (this.validationStatus = isValid ? 'valid' : 'invalid'));
  }

  ngOnChanges() {
    this.update();
  }

  onChange() {
    this.update();
    this.matrixFieldChange.emit();
  }

  openForecastModal(selectedTabIndex = 0) {
    const dialogRef = this.matDialog.open<ForecastOfOutcomesModalComponent, ForecastOfOutcomesModalData>(ForecastOfOutcomesModalComponent, {
      data: {
        selectedTabIndex,
        alternative: this.alternative,
        objective: this.objective,
        outcome: this.outcome,
      },
    });
    dialogRef.componentInstance.influenceFactorModified.pipe(takeUntil(dialogRef.afterClosed())).subscribe(influenceFactorId => {
      this.influenceFactorModified.emit(influenceFactorId);
    });
    dialogRef.afterClosed().subscribe(() => {
      this.onChange();
    });
  }

  update() {
    this.outcome.checkProcessed();

    const isValueValid = this.outcome.processed && this.outcome.values.every(value => validateValue(value, this.objective)[0]);
    if (isValueValid !== this.isValueValid) {
      this.isValueValid = isValueValid;
      this.isValueValidChange.emit(isValueValid);
    }
    this.hasTornadoDiagram =
      this.isValueValid && this.objective.isIndicator && this.outcome.influenceFactor instanceof PredefinedInfluenceFactor;

    this.numericalScaleMin = Math.min(this.objective.numericalData.from, this.objective.numericalData.to);
    this.numericalScaleMax = Math.max(this.objective.numericalData.from, this.objective.numericalData.to);

    if (this.objective.isNumerical || this.objective.isVerbal) {
      const min = Math.min(...this.outcome.values[0][0]);
      const max = Math.max(...this.outcome.values[0][0]);

      if (this.objective.isVerbal) {
        this.min = this.objective.verbalData.options[min - 1];
        this.max = this.objective.verbalData.options[max - 1];
        this.unit = '';
      } else {
        this.min = '' + min;
        this.max = '' + max;
        this.unit = this.objective.numericalData.unit;
      }
    } else {
      const minNumber = this.indicatorOutcome('min');
      this.min = '' + minNumber.round(2);

      const maxNumber = this.indicatorOutcome('max');
      this.max = '' + maxNumber.round(2);

      this.unit = this.objective.indicatorData.aggregatedUnit;
    }

    this.calcBackgroundImage();
    this.cdRef.markForCheck();
  }

  private indicatorOutcome(get: 'min' | 'max'): number {
    const getMin = get === 'min';
    // We calculate the outcome values for all influence factor states
    const aggregationFunction = this.objective.indicatorData.aggregationFunction;
    const aggregatedValues = this.outcome.values.map(value => aggregationFunction(value));
    // Return the numerically lowest/highest outcome value
    return getMin ? Math.min(...aggregatedValues) : Math.max(...aggregatedValues);
  }

  private calcBackgroundImage() {
    if (!this.isValueValid || !this.colorOutcomes) {
      this.backgroundImage = undefined;
      return;
    }

    if (this.outcome.influenceFactor) {
      const uncertaintyPercentages = this.outcome.influenceFactor.states.map((_, i) => {
        return this.getOutcomePercentage(this.outcome.values[i]);
      });
      const probabilities = this.outcome.influenceFactor.states.map(state => state.probability / 100);
      const expectedPercentage = sum(
        zip(uncertaintyPercentages, probabilities).map(([percentage, probability]) => percentage * probability)
      );
      const percentages = [Math.min(...uncertaintyPercentages), expectedPercentage, Math.max(...uncertaintyPercentages)];

      const colors = percentages.map(p => {
        if (p < 0.5) {
          const alpha = (0.5 - p) / 2.5;
          return `rgba(255,0,0,${alpha})`;
        } else if (p >= 0.5) {
          const alpha = (p - 0.5) / 2.5;
          return `rgba(0,255,0,${alpha})`;
        }
      });

      let middlePercentage = (Math.abs(percentages[1] - percentages[0]) / Math.abs(percentages[0] - percentages[2])) * 100;

      if (Number.isNaN(middlePercentage)) {
        middlePercentage = 50;
      }

      // Scale
      middlePercentage = 8 + 0.84 * middlePercentage;

      this.backgroundImage = this.domSanitizer.bypassSecurityTrustStyle(
        `linear-gradient(to right, ${colors[0]}, ${colors[1]} ${100 - middlePercentage}%, ${colors[2]})`
      );
    } else {
      const percentage = this.getOutcomePercentage(this.outcome.values[0]);

      let staticColor;
      if (percentage < 0.5) {
        const alpha = (0.5 - percentage) / 2.5;
        staticColor = `rgba(255,0,0,${alpha})`;
      } else if (percentage >= 0.5) {
        const alpha = (percentage - 0.5) / 2.5;
        staticColor = `rgba(0,255,0,${alpha})`;
      }

      this.backgroundImage = this.domSanitizer.bypassSecurityTrustStyle(`linear-gradient(to right, ${staticColor},${staticColor})`);
    }
  }

  /**
   * Returns the value normalized to [0, 1].
   *
   * @param value - The outcome value to be normalized
   * @returns The nromalized value
   */
  private getOutcomePercentage(value: ObjectiveInput) {
    const rawValue = this.objective.isIndicator ? this.objective.indicatorData.aggregationFunction(value) : value[0][0];

    return this.objective.getRangeInterval().normalizePoint(rawValue);
  }

  onClick() {
    if (this.objective.isVerbal && !this.outcome.influenceFactor) {
      this.verbalInput.open();
    } else if (this.objective.isNumerical && !this.outcome.influenceFactor) {
      this.popOverService.open(this.numericalInputPopover, this.readonlyDisplay, {
        position: [{ originX: 'center', originY: 'center', overlayX: 'center', overlayY: 'center' }],
      });
    } else {
      this.openForecastModal();
    }
  }

  transferOutcomeToMe(buttonElement: ElementRef<HTMLElement>) {
    this.teamTrait
      .transferObject(this.outcome.uuid)
      .pipe(loadingIndicator(this.snackBar, $localize`Ausprägung wird übernommen…`))
      .subscribe({
        next: () => this.popOverService.whistle(buttonElement, $localize`Ausprägung übernommen!`, 'check'),
        error: error => {
          if (error instanceof HttpErrorResponse && error.status === 409) {
            const message = error.error?.message;

            if (message === 'missing-alternative') {
              this.popOverService.whistle(buttonElement, $localize`In Deinem Projekt fehlt die zugehörige Alternative`, 'error');
              return;
            } else if (message === 'missing-objective') {
              this.popOverService.whistle(buttonElement, $localize`In Deinem Projekt fehlt das zugehörige Ziel`, 'error');
              return;
            }
          }

          this.popOverService.whistle(buttonElement, $localize`Beim Übernehmen der Ausprägung ist ein Fehler aufgetreten.`, 'error');
        },
      });
  }
}
