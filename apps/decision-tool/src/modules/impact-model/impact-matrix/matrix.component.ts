import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { calculateDominanceRelations, DominanceRelation } from '@entscheidungsnavi/decision-data/calculation';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { validateValue } from '@entscheidungsnavi/decision-data/validations';
import { Alternative, InfluenceFactor, Objective, PREDEFINED_INFLUENCE_FACTORS } from '@entscheidungsnavi/decision-data/classes';
import { MatDialog } from '@angular/material/dialog';
import { FullscreenRef, FullscreenService, PopOverService } from '@entscheidungsnavi/widgets';
import { lastValueFrom, map, Observable, pairwise, startWith } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ObjectiveScaleModalComponent } from '../objective-scale';
import { ExplanationService } from '../../shared/decision-quality';
import { ExportImpactModelModalComponent } from '../../../app/excel-impact-model-export/export-modal/export-impact-model-modal.component';
import { PersistentSetting, PersistentSettingParent } from '../../../app/data/persistent-setting';
import { ProjectService } from '../../../app/data/project';
import { ImpactMatrixFieldComponent } from './matrix-field.component';
import { CopyScaleModalComponent } from './copy-scale-modal/copy-scale-modal.component';
import { NameFieldComponent } from './name-field/name-field.component';

type DragData = { objective: number; alternative: number };

@Component({
  selector: 'dt-impact-matrix',
  templateUrl: 'matrix.component.html',
  styleUrls: ['matrix.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
@PersistentSettingParent('ImpactMatrixComponent')
export class ImpactMatrixComponent implements OnInit, AfterViewInit, OnDestroy {
  static doNotAskDragConfirmation = false;

  @Input()
  @HostBinding('class.extendable')
  extendable = false;

  @Input()
  settingsContainerPadding: string;

  @PersistentSetting()
  showDominanceRelationships = true;

  @PersistentSetting()
  colorOutcomes = true;

  @PersistentSetting()
  smallBoxHeight = true;

  @PersistentSetting()
  transposed = false;

  highlightInfluenceFactorId = -1; // -1 means no highlighting

  @ViewChildren(ImpactMatrixFieldComponent) matrixFields: QueryList<ImpactMatrixFieldComponent>;

  // Contains dominated alternatives as keys and the corresponding set of DominanceRelations as value
  dominatedAlternatives = new Map<Alternative, DominanceRelation[]>();

  // We highlight the dominance relations for a specific dominated alternative
  highlightedDominatedAlternative: Alternative;
  // We look at the highlightedDominatedAlternative: For each alternative that dominates
  // it denotes in what type of dominance is present (with or without indicator)
  highlightedDominances: Alternative[] = [];

  dragData: DragData;
  fieldDragCounter = new Map<ImpactMatrixFieldComponent, number>(); // Used to determine when we remove outline class
  dragConfirmation: { from: DragData; to: DragData; toElement: ImpactMatrixFieldComponent }; // used for confirmation modal

  // eslint-disable-next-line @typescript-eslint/naming-convention
  ImpactMatrixComponent = ImpactMatrixComponent;

  influenceFactorOptions: InfluenceFactor[];

  /**
   * True iff every existing outcome is processed and every outcome value is valid.
   */
  private outcomeValid: boolean[][];
  isEverythingValid = false;

  fullscreenRef: FullscreenRef | null = null;

  explanation: string;
  @ViewChild('stepToExplainMenu', { static: true }) stepToExplainMenu: ElementRef;

  highlightObjectives = false;
  highlightAlternatives = false;
  highlightCells = false;

  lazyCallback = -1;
  lazyAlternative = 0;
  lazyObjective = 0;

  readonly transposedTooltip = $localize`Ziele in der Kopfzeile anzeigen`;
  readonly notTransposedTooltip = $localize`Alternativen in der Kopfzeile anzeigen`;
  readonly coloredTooltip = $localize`Färbung deaktivieren`;
  readonly notColoredTooltip = $localize`Färbung aktivieren`;
  readonly compactScalingTooltip = $localize`Ändere Skalierung zu normal`;
  readonly normalScalingTooltip = $localize`Ändere Skalierung zu kompakt`;

  // Change together with corresponding CSS values!
  readonly boxWidth = 200; // in px
  readonly topHeaderNormalHeight = 125; // in px
  readonly topHeaderCompactHeight = 106; // in px
  readonly boxNormalHeight = 89; // in px
  readonly boxCompactHeight = 70; // in px

  narrowRoot = false;

  requestLazyCallback = (window as any).requestIdleCallback
    ? (callback: () => void, options: { timeout: number }) => (window as any).requestIdleCallback(callback, options)
    : (callback: FrameRequestCallback, _: any) => requestAnimationFrame(callback);
  cancelLazyCallback = (window as any).cancelIdleCallback
    ? (callbackID: number) => (window as any).cancelIdleCallback(callbackID)
    : (callbackID: number) => cancelAnimationFrame(callbackID);

  @ViewChild('matrixContainer', { static: true })
  matrixContainerRef: ElementRef<HTMLElement>;

  @ViewChildren('objectiveNameField') objectiveNameInputRefs: QueryList<NameFieldComponent>;
  @ViewChildren('alternativeNameField') alternativeNameInputRefs: QueryList<NameFieldComponent>;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  // True iff there exists at least one indicator objective
  get hasIndicatorObjective() {
    return this.decisionData.objectives.some(obj => obj.isIndicator);
  }

  get showDominance() {
    return this.showDominanceRelationships && this.isEverythingValid;
  }

  get teamTrait() {
    return this.projectService.getTeamTrait();
  }

  constructor(
    protected decisionData: DecisionData,
    private cdRef: ChangeDetectorRef,
    private dialog: MatDialog,
    private explanationService: ExplanationService,
    private myElementRef: ElementRef<HTMLElement>,
    private fullscreenService: FullscreenService,
    private zone: NgZone,
    private route: ActivatedRoute,
    private router: Router,
    protected popOverService: PopOverService,
    protected snackBar: MatSnackBar,
    private projectService: ProjectService
  ) {
    this.advanceLazyLoading = this.advanceLazyLoading.bind(this);
  }

  ngOnInit() {
    this.influenceFactorOptions = [...this.decisionData.influenceFactors, ...Object.values(PREDEFINED_INFLUENCE_FACTORS)];

    this.route.queryParams.pipe(takeUntil(this.onDestroy$)).subscribe(params => {
      this.highlightInfluenceFactorId = 'highlightInfluenceFactor' in params ? +params.highlightInfluenceFactor : -1;
      this.cdRef.detectChanges();
      this.cdRef.markForCheck();
    });

    this.initOutcomes();
    this.onChange();
  }

  changeHighlightedInfluenceFactor(newInfluenceFactorId: number) {
    const queryParams = newInfluenceFactorId >= 0 ? { highlightInfluenceFactor: newInfluenceFactorId } : {};
    this.router.navigate([], { relativeTo: this.route, queryParams, replaceUrl: true });
  }

  getGridTemplateColumns() {
    const numberOfRows = this.transposed ? this.decisionData.alternatives.length : this.decisionData.objectives.length;
    return `repeat(${numberOfRows + 1}, ${this.boxWidth}px)`;
  }

  getGridTemplateRows() {
    const topRowHeight = this.transposed
      ? this.smallBoxHeight
        ? this.boxNormalHeight
        : this.topHeaderCompactHeight
      : this.smallBoxHeight
      ? this.topHeaderCompactHeight
      : this.topHeaderNormalHeight;
    const normalRowHeight = this.smallBoxHeight ? this.boxCompactHeight : this.boxNormalHeight;
    const numberOfRows = this.transposed ? this.decisionData.objectives.length : this.decisionData.alternatives.length;
    return `${topRowHeight}px repeat(${numberOfRows}, ${normalRowHeight}px)`;
  }

  private initOutcomes() {
    this.outcomeValid = this.decisionData.outcomes.map(ocForAlt => ocForAlt.map(() => false));

    this.isEverythingValid = this.decisionData.outcomes.every(ocForAlternative =>
      ocForAlternative.every(
        (outcome, objectiveIdx) =>
          outcome.processed && outcome.values.every(value => validateValue(value, this.decisionData.objectives[objectiveIdx])[0])
      )
    );
  }

  explainImpactModel() {
    this.explanationService.openExplanationModalFor(
      'CLEAR_VALUES',
      this.decisionData.decisionQuality.criteriaValues['CLEAR_VALUES'],
      true,
      false
    );
  }

  toggleFullScreen() {
    if (this.fullscreenRef) {
      this.fullscreenRef.close();
    } else {
      this.fullscreenRef = this.fullscreenService.fullscreenElement(this.myElementRef);
      this.cdRef.detectChanges();

      this.fullscreenRef.onClose.subscribe(() => {
        this.fullscreenRef = null;
        this.cdRef.detectChanges();
      });
    }
  }

  onChange() {
    this.influenceFactorOptions = [...this.decisionData.influenceFactors, ...Object.values(PREDEFINED_INFLUENCE_FACTORS)];
    if (this.showDominance) {
      // On data change we need to rerun the dominance check
      this.performDominanceCheck();
      // If we have a highlighted alternative, we need to check if it is still dominated with the new data.
      // If not: hide the dominance details; If yes: recalculate the dominance details.
      if (this.highlightedDominatedAlternative != null) {
        if (!this.dominatedAlternatives.has(this.highlightedDominatedAlternative)) {
          this.hideDominanceDetails();
        } else {
          this.calculateDominanceDetails();
        }
      }
    } else {
      this.hideDominanceDetails();
    }

    this.cdRef.detectChanges();
  }

  onMeasuringScaleChange() {
    this.matrixFields.forEach(field => field.update());
    this.onChange();
  }

  onOutcomeValidChange(alternativeIdx: number, objectiveIdx: number, isValid: boolean) {
    this.outcomeValid[alternativeIdx][objectiveIdx] = isValid;

    if (this.isEverythingValid && !isValid) {
      this.isEverythingValid = false;
      this.onChange();
    } else if (!this.isEverythingValid && isValid) {
      this.checkIfEverythingIsValid();
      this.onChange();
    }
  }

  checkIfEverythingIsValid() {
    const isEverythingValid = this.outcomeValid.every(ocForAlternative => ocForAlternative.every(valid => valid));
    if (this.isEverythingValid !== isEverythingValid) {
      this.isEverythingValid = isEverythingValid;
    }
  }

  private performDominanceCheck() {
    this.dominatedAlternatives.clear();
    calculateDominanceRelations(this.decisionData.objectives, this.decisionData.alternatives, this.decisionData.outcomes).forEach(
      relation => {
        if (this.dominatedAlternatives.has(relation.dominated)) {
          this.dominatedAlternatives.get(relation.dominated).push(relation);
        } else {
          this.dominatedAlternatives.set(relation.dominated, [relation]);
        }
      }
    );
  }

  addAlternative() {
    this.decisionData.addAlternative({
      alternative: new Alternative(0, $localize`Alternative` + ' ' + (this.decisionData.alternatives.length + 1)),
    });
    this.outcomeValid[this.decisionData.outcomes.length - 1] = this.decisionData.outcomes[this.decisionData.outcomes.length - 1].map(
      _ => false
    );
    this.isEverythingValid = false;
    this.lazyAlternative = Number.MAX_SAFE_INTEGER;
    this.advanceLazyLoading();
  }

  explainAlternativesSteps() {
    this.explanationService.openExplanationModalFor(
      'CREATIVE_DOABLE_ALTERNATIVES',
      this.decisionData.decisionQuality.criteriaValues['CREATIVE_DOABLE_ALTERNATIVES'],
      true,
      false
    );
  }

  showDominanceDetails(alternativeIndex: number) {
    this.changeHighlightedInfluenceFactor(-1);
    this.highlightedDominatedAlternative = this.decisionData.alternatives[alternativeIndex];
    this.calculateDominanceDetails();
  }

  hideDominanceDetails() {
    this.highlightedDominatedAlternative = null;
    this.highlightedDominances = [];
  }

  addObjective() {
    this.decisionData.addObjective(new Objective($localize`Ziel` + ' ' + (this.decisionData.objectives.length + 1)));
    this.outcomeValid = this.decisionData.outcomes.map((ocForAlt, i) =>
      ocForAlt.map((oc, j) => {
        return j === ocForAlt.length - 1 ? false : this.outcomeValid[i][j];
      })
    );
    this.isEverythingValid = false;
    this.onChange();
    this.lazyObjective = Number.MAX_SAFE_INTEGER;
    this.advanceLazyLoading();
  }

  async onDeleteObjective(objectiveIdx: number) {
    this.outcomeValid.forEach(outcomesForAlternative => outcomesForAlternative.splice(objectiveIdx, 1));
    this.checkIfEverythingIsValid();
    this.onChange();
  }

  explainObjectivesStep() {
    this.explanationService.openExplanationModalFor(
      'MEANINGFUL_RELIABLE_INFORMATION',
      this.decisionData.decisionQuality.criteriaValues['MEANINGFUL_RELIABLE_INFORMATION'],
      true,
      false
    );
  }

  dragField(event: DragEvent, objective: number, alternative: number) {
    this.dragData = { objective, alternative };
    event.dataTransfer.effectAllowed = 'copy';

    // Set something for Firefox and Safari
    event.dataTransfer.setData('text', 'dummy');
  }

  dragEnterField(event: DragEvent, field: ImpactMatrixFieldComponent, objective: number, alternative: number) {
    if (this.dragData && this.dragData.objective === objective && this.dragData.alternative !== alternative) {
      event.dataTransfer.dropEffect = 'copy';
      event.preventDefault();

      this.fieldDragCounter.set(field, (this.fieldDragCounter.get(field) ?? 0) + 1);
    }
  }

  dragOverField(event: DragEvent, objective: number, alternative: number) {
    if (this.dragData && this.dragData.objective === objective && this.dragData.alternative !== alternative) {
      const htmlElement = event.currentTarget as HTMLElement;
      htmlElement.classList.add('impact-drag-over');

      event.dataTransfer.dropEffect = 'copy';
      event.preventDefault();
    }
  }

  dropOnField(element: ImpactMatrixFieldComponent, event: DragEvent, objective: number, alternative: number) {
    const htmlElement = event.currentTarget as HTMLElement;
    htmlElement.classList.remove('impact-drag-over');

    if (this.dragData && this.dragData.objective === objective && this.dragData.alternative !== alternative) {
      const targetOutcome = this.decisionData.outcomes[alternative][objective];
      const doesOutcomeRequireConfirmation =
        targetOutcome.processed && targetOutcome.values.every(value => validateValue(value, this.decisionData.objectives[objective])[0]);

      if (ImpactMatrixComponent.doNotAskDragConfirmation || !doesOutcomeRequireConfirmation) {
        this.confirmDragDrop(this.dragData, { objective, alternative }, element);
      } else {
        this.dragConfirmation = {
          from: this.dragData,
          to: { objective, alternative },
          toElement: element,
        };
        this.cdRef.detectChanges(); // Fixes ExpressionChanged Error related to ng-untouched
      }
    }
  }

  dragLeaveField(event: DragEvent, field: ImpactMatrixFieldComponent, objective: number, alternative: number) {
    if (this.dragData && this.dragData.objective === objective && this.dragData.alternative !== alternative) {
      this.fieldDragCounter.set(field, (this.fieldDragCounter.get(field) ?? 0) - 1);

      if (this.fieldDragCounter.get(field) === 0) {
        const htmlElement = event.currentTarget as HTMLElement;
        htmlElement.classList.remove('impact-drag-over');
      }
    }
  }

  clearDragData() {
    this.dragData = undefined;
    this.fieldDragCounter.clear();
  }

  confirmDragDrop(from: DragData, to: DragData, toElement: ImpactMatrixFieldComponent) {
    this.dragConfirmation = undefined;
    const originOutcome = this.decisionData.outcomes[from.alternative][from.objective];
    const targetOutcome = this.decisionData.outcomes[to.alternative][to.objective];

    targetOutcome.copyBack(originOutcome);

    toElement.update();
    this.onChange();
  }

  private calculateDominanceDetails() {
    this.highlightedDominances = [];
    this.dominatedAlternatives.get(this.highlightedDominatedAlternative).forEach(dominanceRelation => {
      this.highlightedDominances.push(dominanceRelation.dominating);
    });
  }

  isDominating(alternative: Alternative) {
    return this.highlightedDominances.includes(alternative);
  }

  isDominated(alternative: Alternative) {
    return (
      this.showDominance &&
      ((this.dominatedAlternatives.has(alternative) && this.highlightedDominatedAlternative == null) ||
        this.highlightedDominatedAlternative === alternative)
    );
  }

  isAlternativeFadedOut(alternativeIndex: number) {
    const alternative = this.decisionData.alternatives[alternativeIndex];

    // We are currently highlighting a dominance relationship, but this alternative is not part of it (neither dominating nor dominated)
    const isIrrelevantForHighlightedDominance =
      this.highlightedDominatedAlternative != null &&
      this.highlightedDominatedAlternative !== alternative &&
      !this.isDominating(alternative);

    return isIrrelevantForHighlightedDominance;
  }

  isOutcomeFadedOut(alternativeIndex: number, objectiveIndex: number) {
    // We are dragging a different objective
    const draggingDifferentObjective = this.dragData && this.dragData.objective !== objectiveIndex;

    // We are highlighting an influence factor that is not used in this outcome
    const irrelevantForHighlightedInfluenceFactor =
      this.highlightInfluenceFactorId !== -1 &&
      this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor?.id !=
        this.influenceFactorOptions[this.highlightInfluenceFactorId].id;

    return this.isAlternativeFadedOut(alternativeIndex) || draggingDifferentObjective || irrelevantForHighlightedInfluenceFactor;
  }

  isOutcomeHighlighted(alternativeIndex: number, objectiveIndex: number) {
    return (
      this.highlightInfluenceFactorId !== -1 &&
      this.decisionData.outcomes[alternativeIndex][objectiveIndex].influenceFactor?.id ==
        this.influenceFactorOptions[this.highlightInfluenceFactorId].id
    );
  }

  // Lazy Loading Cells
  ngAfterViewInit() {
    const focusLastIfExtended = (queryList: QueryList<{ focus: (setCursor: boolean) => void }>) => {
      queryList.changes
        .pipe(
          map(() => queryList.length),
          startWith(queryList.length),
          pairwise(),
          filter(([lengthBefore, lengthAfter]) => lengthBefore < lengthAfter)
        )
        .subscribe(() => {
          queryList.last.focus(true);
        });
    };

    focusLastIfExtended(this.objectiveNameInputRefs);
    focusLastIfExtended(this.alternativeNameInputRefs);

    requestAnimationFrame(() => {
      const matrixContainer = this.matrixContainerRef.nativeElement;

      const rightColumnWidth = matrixContainer.offsetWidth + 200;
      const rightColumnHeight = matrixContainer.offsetHeight + 118;

      this.lazyAlternative = Math.floor(rightColumnHeight / 84) + 2;
      this.lazyObjective = Math.floor(rightColumnWidth / 200) + 2;

      if (this.lazyAlternative < this.decisionData.alternatives.length || this.lazyObjective < this.decisionData.objectives.length) {
        this.lazyCallback = this.requestLazyCallback(this.advanceLazyLoading, { timeout: 500 });
      }

      this.cdRef.detectChanges();
    });
  }

  ngOnDestroy() {
    this.cancelLazyCallback(this.lazyCallback);
  }

  advanceLazyLoading() {
    this.zone.run(() => {
      this.lazyAlternative = Math.min(this.decisionData.alternatives.length, this.lazyAlternative + 1);
      this.lazyObjective = Math.min(this.decisionData.objectives.length, this.lazyObjective + 1);
      this.cdRef.markForCheck();
    });

    if (this.lazyAlternative < this.decisionData.alternatives.length || this.lazyObjective < this.decisionData.objectives.length) {
      this.lazyCallback = this.requestLazyCallback(this.advanceLazyLoading, { timeout: 500 });
    }
  }

  onInfluenceFactorModified(influenceFactorId: number) {
    this.matrixFields.forEach(field => {
      if (field.outcome.influenceFactor?.id === influenceFactorId) field.update();
    });
  }

  excelExport() {
    this.dialog
      .open(ExportImpactModelModalComponent)
      .afterClosed()
      .subscribe(() => {
        while (this.outcomeValid.length < this.decisionData.alternatives.length) {
          this.outcomeValid.push(new Array(this.decisionData.objectives.length).fill(false));
        }
        this.matrixFields.forEach(field => field.update());
        this.checkIfEverythingIsValid();
        this.advanceLazyLoading();
      });
  }

  dominatedAlternativeClicked(alternativeIdx: number) {
    if (this.highlightedDominatedAlternative) {
      this.hideDominanceDetails();
    } else {
      this.showDominanceDetails(alternativeIdx);
    }
  }

  transferMatrixRowOrColumnToMe(whistleElement: HTMLElement, object: Objective | Alternative) {
    this.teamTrait
      .transferObject(object.uuid)
      .pipe(loadingIndicator(this.snackBar, $localize`${object instanceof Objective ? 'Spalte' : 'Zeile'} wird übernommen…`))
      .subscribe({
        next: () =>
          this.popOverService.whistle(whistleElement, $localize`${object instanceof Objective ? 'Spalte' : 'Zeile'} übernommen!`, 'check'),
        error: () => {
          this.popOverService.whistle(
            whistleElement,
            $localize`Beim Übernehmen der ${object instanceof Objective ? 'Spalte' : 'Zeile'} ist ein Fehler aufgetreten.`,
            'error'
          );
        },
      });
  }

  getObjectiveHeaderHeight(): number {
    if (this.transposed) return this.smallBoxHeight ? this.boxCompactHeight : this.boxNormalHeight;
    else return this.smallBoxHeight ? this.topHeaderCompactHeight : this.topHeaderNormalHeight;
  }

  getAlternativeHeaderHeight(): number {
    if (this.transposed) return this.smallBoxHeight ? this.boxNormalHeight : this.topHeaderCompactHeight;
    else return this.smallBoxHeight ? this.boxCompactHeight : this.boxNormalHeight;
  }

  async deleteAlternative(alternativeBox: HTMLElement, alternativeIdx: number) {
    if (this.decisionData.alternatives.length <= 2) return;

    if (
      await this.popOverService.confirm(alternativeBox, $localize`Alternative wirklich löschen?`, {
        position: [{ originX: 'center', originY: 'center', overlayX: 'center', overlayY: 'center' }],
      })
    ) {
      this.decisionData.removeAlternative(alternativeIdx);
      this.outcomeValid.splice(alternativeIdx, 1);
      this.checkIfEverythingIsValid();
      this.onChange();
    }
  }

  async openScaleEditor(objectiveIdx: number) {
    if (await lastValueFrom(this.dialog.open(ObjectiveScaleModalComponent, { data: objectiveIdx }).afterClosed())) {
      this.matrixFields.forEach(field => field.update());
      this.onChange();
    }
  }

  openCopyScale(objectiveIdx: number) {
    this.dialog
      .open(CopyScaleModalComponent, {
        data: {
          objectiveIdx: objectiveIdx,
        },
      })
      .afterClosed()
      .subscribe((copied: boolean) => {
        if (copied) {
          this.matrixFields.forEach(field => field.update());
          this.onChange();
        }
      });
  }

  async deleteObjective(objectiveBox: HTMLElement, objectiveIdx: number) {
    if (this.decisionData.objectives.length <= 1) return;

    if (
      await this.popOverService.confirm(objectiveBox, $localize`Ziel wirklich löschen?`, {
        position: [{ originX: 'center', originY: 'center', overlayX: 'center', overlayY: 'center' }],
      })
    ) {
      this.decisionData.removeObjective(objectiveIdx);
      this.outcomeValid.forEach(outcomesForAlternative => outcomesForAlternative.splice(objectiveIdx, 1));
      this.checkIfEverythingIsValid();
      this.onChange();
    }
  }
}
