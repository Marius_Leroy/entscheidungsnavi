import { Component, EventEmitter, Input, Output } from '@angular/core';
import { EvaluateAndDecideTool, StructureAndEstimateTool, Tool } from '../../pf-main.component';

const VERBALIZED_TOOLS: { [key in Tool]: string } = {
  'sae-overview': $localize`Übersicht`,

  // Tools in "Structure and estimate".
  'sae-assumptions': $localize`Annahmen und Abgrenzungen`,
  'sae-objective-list': $localize`Listenansicht`,
  'sae-alternative-list': $localize`Listenansicht`,
  'sae-hierarchy': $localize`Hierarchie`,
  'sae-checklists': $localize`Beispielziele`,
  'sae-weak-points': $localize`Schwachpunktanalyse`,
  'sae-objective-focused-search': $localize`Zielfokussierte Suche`,
  'sae-lever-method': $localize`Stellhebelmethode`,
  'sae-influence-factors': $localize`Einflussfaktoren`,

  // Tools in "Evaluate and decide".
  'ead-overview': $localize`Übersicht`,
  'ead-utility-functions': $localize`Nutzenfunktionen`,
  'ead-objective-weighting': $localize`Zielgewichtung`,
  'ead-sensitivity-analysis': $localize`Sensitivitätsanalyse`,
  'ead-pros-and-cons': $localize`Pros und Kontras`,
  'ead-robustness-check': $localize`Robustheitstest`,
  'ead-risk-comparison': $localize`Risikovergleich`,
};

@Component({
  selector: 'dt-pf-tool',
  templateUrl: './tool-category.component.html',
  styleUrls: ['./tool-category.component.scss'],
})
export class PfToolComponent {
  @Input()
  tool: Tool;

  @Input()
  activeTool: Tool;

  @Output()
  activeToolChange = new EventEmitter<Tool>();

  @Input()
  categoryName: string;

  @Input()
  toolsInCategory?: Tool[];

  @Output()
  toolSelected = new EventEmitter<Tool>();

  get isSingle() {
    return this.toolsInCategory === undefined;
  }

  get toolInCategorySelected() {
    return !this.isSingle && this.toolsInCategory.includes(this.activeTool);
  }

  verbalizeTool(tool: StructureAndEstimateTool | EvaluateAndDecideTool) {
    return VERBALIZED_TOOLS[tool];
  }
}
