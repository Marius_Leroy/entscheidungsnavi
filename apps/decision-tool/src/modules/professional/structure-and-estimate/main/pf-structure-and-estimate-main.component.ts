import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { PersistentSetting, PersistentSettingParent } from '@entscheidungsnavi/widgets';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';

@Component({
  templateUrl: './pf-structure-and-estimate-main.component.html',
  styleUrls: ['./pf-structure-and-estimate-main.component.scss'],
})
@DisplayAtMaxWidth
@PersistentSettingParent('PfStructureAndEstimateMainComponent')
export class PfStructureAndEstimateMainComponent {
  @PersistentSetting()
  isDecisionStatementHidden = false;

  isHoveringOverDecisionStatement = false;

  constructor(protected decisionData: DecisionData) {}

  updateDecisionStatement(event: any) {
    if (event.target.innerText === '') return;
    this.decisionData.decisionStatement.statement = event.target.innerText;
  }
}
