import { Component } from '@angular/core';

@Component({
  template: `<dt-alternative-hint-3
    [showTitle]="false"
    data-cy="structure-and-estimate-tool-objective-focused-search-alternative-list"
  ></dt-alternative-hint-3>`,
})
export class PfAlternativeObjectiveFocusedSearchComponent {}
