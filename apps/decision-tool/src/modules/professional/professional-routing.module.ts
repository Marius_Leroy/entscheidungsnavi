import { inject, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, RouterModule, Routes } from '@angular/router';
import { ProfessionalInterpolationGuard, ValidateDecisionDataGuard } from '../../app/guards';
import { InterpolationService } from '../../services/interpolation.service';
import { UtilityFunctionOverviewComponent } from '../results/utility-functions/overview/utility-function-overview.component';
import { UtilityFunctionDetailedComponent, utilityFunctionDetailedGuard } from '../results/utility-functions';
import {
  ObjectiveWeightingDetailedComponent,
  ObjectiveWeightingOverviewComponent,
  objectiveWeightingDetailedGuard,
} from '../results/objective-weighting';
import { PfMainComponent, Tool } from './main/pf-main.component';
import { PfStructureAndEstimateMainComponent } from './structure-and-estimate/main/pf-structure-and-estimate-main.component';
import { PfEvaluateAndDecideMainComponent } from './evaluate-and-decide/main/pf-evaluate-and-decide-main.component';
import { PfDecisionstatementAssumptionsComponent } from './structure-and-estimate/pf-decisionstatement-assumptions.component';
import { PfObjectiveListComponent } from './structure-and-estimate/pf-objective-list.component';
import { PfAlternativesComponent } from './structure-and-estimate/pf-alternatives.component';
import { PfObjectiveAspectHierarchyComponent } from './structure-and-estimate/pf-objective-aspect-hierarchy.component';
import { PfObjectiveCheckListsComponent } from './structure-and-estimate/pf-objective-check-lists.component';
import { PfAlternativesWeakPointsComponent } from './structure-and-estimate/pf-alternatives-weak-points.component';
import { PfAlternativeObjectiveFocusedSearchComponent } from './structure-and-estimate/pf-alternative-objective-focused-search.component';
import { PfAlternativesLeverMethodComponent } from './structure-and-estimate/pf-alternatives-lever-method.component';
import { PfUtilityFunctionsMainComponent } from './evaluate-and-decide/pf-utility-functions-main.component';
import { PfSensitivityAnalysisComponent } from './evaluate-and-decide/pf-sensitivity-analysis.component';
import { PfProAndContraComponent } from './evaluate-and-decide/pf-pro-and-contra.component';
import { PfRobustnessCheckComponent } from './evaluate-and-decide/pf-robustness-check.component';
import { PfInfluenceFactorsComponent } from './structure-and-estimate/pf-influence-factors.component';
import { PfObjectiveWeightingMainComponent } from './evaluate-and-decide/pf-objective-weighting-main.component';
import { PfRiskComparisonComponent } from './evaluate-and-decide/pf-risk-comparison.component';

function withTool(tool: Tool) {
  return {
    tool,
  };
}

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'structure-and-estimate',
  },
  {
    path: '',
    component: PfMainComponent,
    children: [
      {
        path: 'structure-and-estimate',
        canActivateChild: [ProfessionalInterpolationGuard],
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: PfStructureAndEstimateMainComponent,
            data: withTool('sae-overview'),
          },
          {
            path: 'assumptions',
            component: PfDecisionstatementAssumptionsComponent,
            data: withTool('sae-assumptions'),
          },
          {
            path: 'objective-list',
            component: PfObjectiveListComponent,
            data: withTool('sae-objective-list'),
          },
          {
            path: 'alternative-list',
            component: PfAlternativesComponent,
            data: withTool('sae-alternative-list'),
          },
          {
            path: 'hierarchy',
            component: PfObjectiveAspectHierarchyComponent,
            data: withTool('sae-hierarchy'),
          },
          {
            path: 'checklists',
            component: PfObjectiveCheckListsComponent,
            data: withTool('sae-checklists'),
          },
          {
            path: 'weak-points',
            component: PfAlternativesWeakPointsComponent,
            data: withTool('sae-weak-points'),
          },
          {
            path: 'objective-focused-search',
            component: PfAlternativeObjectiveFocusedSearchComponent,
            data: withTool('sae-objective-focused-search'),
          },
          {
            path: 'lever-method',
            component: PfAlternativesLeverMethodComponent,
            data: withTool('sae-lever-method'),
          },
          {
            path: 'influence-factors',
            component: PfInfluenceFactorsComponent,
            data: withTool('sae-influence-factors'),
          },
        ],
      },
      {
        path: 'evaluate-and-decide',
        canActivate: [() => inject(ProfessionalInterpolationGuard).canActivate()],
        canActivateChild: [
          (activatedRouteSnapshot: ActivatedRouteSnapshot) =>
            inject(ProfessionalInterpolationGuard).canActivateChild(activatedRouteSnapshot),
          ValidateDecisionDataGuard,
        ],
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: PfEvaluateAndDecideMainComponent,
            data: withTool('ead-overview'),
          },
          {
            path: 'utility-functions',
            component: PfUtilityFunctionsMainComponent,
            data: withTool('ead-utility-functions'),
            children: [
              {
                path: '',
                component: UtilityFunctionOverviewComponent,
              },
              {
                path: 'detailed/:objectiveId',
                canActivate: [utilityFunctionDetailedGuard],
                component: UtilityFunctionDetailedComponent,
              },
            ],
          },
          {
            path: 'objective-weighting',
            component: PfObjectiveWeightingMainComponent,
            data: withTool('ead-objective-weighting'),
            children: [
              {
                path: '',
                pathMatch: 'full',
                component: ObjectiveWeightingOverviewComponent,
                canDeactivate: [
                  () => {
                    inject(InterpolationService).interpolateForSecondProfessionalTab();
                    return true;
                  },
                ],
              },
              {
                path: 'detailed/:objectiveId',
                canActivate: [objectiveWeightingDetailedGuard],
                component: ObjectiveWeightingDetailedComponent,
              },
            ],
          },
          {
            path: 'sensitivity-analysis',
            component: PfSensitivityAnalysisComponent,
            data: withTool('ead-sensitivity-analysis'),
          },
          {
            path: 'pro-contra',
            component: PfProAndContraComponent,
            data: withTool('ead-pros-and-cons'),
          },
          {
            path: 'robustness-check',
            component: PfRobustnessCheckComponent,
            data: withTool('ead-robustness-check'),
          },
          {
            path: 'risk-comparison',
            component: PfRiskComparisonComponent,
            data: withTool('ead-risk-comparison'),
          },
        ],
      },
    ],
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfessionalRoutingModule {}
