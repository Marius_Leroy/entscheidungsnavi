import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Injector,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
} from '@angular/core';

import { ObjectiveElement } from '@entscheidungsnavi/decision-data/steps';
import { Tree } from '@entscheidungsnavi/tools';

import { MatSnackBar } from '@angular/material/snack-bar';
import { toPng } from 'html-to-image';

import { saveAs } from 'file-saver';

import {
  ConfirmModalComponent,
  DEFAULT_COLOR_PRESET,
  getPixelDeltaY,
  KeyBindHandlerDirective,
  ModalComponent,
  PlatformDetectService,
  PopOverService,
} from '@entscheidungsnavi/widgets';
import {
  IndicatorObjectiveData,
  NumericalObjectiveData,
  Objective,
  ObjectiveType,
  VerbalObjectiveData,
} from '@entscheidungsnavi/decision-data/classes';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { clamp, cloneDeep, isEqual, negate, random, shuffle } from 'lodash';
import { firstValueFrom, map } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { loadingIndicator } from '@entscheidungsnavi/widgets/loading-snackbar/loading-snackbar.component';
import { DebugService } from '../../../app/debug/debug.service';
import { DeleteObjectiveModalComponent } from '../delete-objective-modal/delete-objective-modal.component';
import { ProjectService } from '../../../app/data/project';
import { TreeMirror } from './tree-mirror';
import {
  Action,
  buildGroup,
  buildMultiMove,
  ChangeColor,
  createColor,
  createCompound,
  createDeleteAllPermanent,
  createDeletePermanent,
  createInsert,
  createMove,
  createRename,
  DeleteAllPermanent,
  DeletePermanent,
  Insert,
  Rename,
} from './actions';
import { ObjectiveAspectHierarchyElementComponent } from './element/objective-aspect-hierarchy-element.component';
import { HierarchyElement } from './hierarchy-element';

export type List = 'brainstorming' | 'trash';

export interface TreeLoc {
  location: 'tree';
  locationSequence: number[];
}

export interface ListLoc {
  location: List;
  index: number;
}

export type ObjectiveLocation = TreeLoc | ListLoc;

interface MoveInProgress {
  from: TreeLoc[] | ListLoc;
}

enum ActionResult {
  EXECUTED,
  DENIED,
}

export enum InsertDirection {
  ABOVE = 1,
  LEFT = 2,
  RIGHT = 3,
  BELOW = 4,
}

type DragData = {
  treeId: number;
  treeSegment: Tree<HierarchyElement>;
  isRoot: boolean;
  scales: (NumericalObjectiveData | VerbalObjectiveData | IndicatorObjectiveData)[];
};

type ColorTarget = 'text' | 'background';

/**
 * Grundlegende Informationen:
 * * Position eines Ziel (Knoten) im Baum wird über location sequences definiert.
 * * Diese geben quasi den "Weg" an den man von der Wurzel aus gehen muss um das Element zu erreichen.
 * * [0,2] wäre also das 3. Kind der Wurzel
 * * [0,2,1] das 2. Kind des 3. Kind der Wurzel
 * * Erster Wert ist immer 0 da es nur eine Wurzel gibt.
 *
 * * Der Baum wird ausschließlich über Aktionen verändert die sowohl ausgeführt als auch wieder rückgängig gemacht werden können.
 * * Es gibt 5 unterschiedliche Aktionen:
 * * - Insert: Fügt neues Element an einer Position ein
 * * - Move: Bewegt ein Element von A nach B
 * * - Rename: Benennt ein Element um
 * * - DeleteEmpty: Löscht Element komplett (Damit keine Ziele ohne Namen im Papierkorb landen)
 * * - ChangeColor: Ändert die Text-/Hintergrundfarbe eines Elementes
 * * - Compound: "Liste" von Aktionen die nacheinander als quasi eine Aktion ausgeführt und rückgängig gemacht werden.
 * * Komplexere Aktionen werden aus diesen Grundaktionen zusammengebaut und als Compound Aktion ausgeführt.
 * * Gruppieren ist beispielsweise ein Insert für das neue Element + ein Move für jedes markierte Element hinter das neue Element.
 */
@Component({
  selector: 'dt-objective-aspect-hierarchy',
  templateUrl: './objective-aspect-hierarchy.component.html',
  styleUrls: ['./objective-aspect-hierarchy.component.scss'],
  hostDirectives: [KeyBindHandlerDirective],
})
export class ObjectiveAspectHierarchyComponent implements OnInit, AfterViewInit, OnDestroy {
  // Every pixel in scroll delta we zoom in/out 1 percent.
  static readonly SCROLL_TO_ZOOM_RATIO = 0.01;
  static readonly MINIMAL_SCALE = 0.40187757201;

  // a reference to the element that is being dragged (with children),
  // together with the ID of the tree and the scale (if the element is an objective)
  // used when dragging elements between different components (objective-hirarchies)
  static globalDragData: DragData = null;

  static nextId = 0;
  treeId = ObjectiveAspectHierarchyComponent.nextId++;

  @Input()
  tree: Tree<HierarchyElement>;

  @Input()
  editable = true;

  @Input()
  listOfAspects: ReadonlyArray<ObjectiveElement>;

  @Input()
  listOfDeletedAspects: ReadonlyArray<ObjectiveElement>;

  @Input()
  brainstormingOpen = true;

  @Input()
  trashBinOpen = true;

  @Input()
  allowZoom = true;

  @Input()
  allowFullscreen = true;

  @Input()
  allowSavingImage = true;

  @Input()
  allowInputSelection = true;

  @Input()
  allowDragPanning = true;

  @Input()
  forceHideNoteBtn = false;

  @Input()
  initialZoomDelta = 0;

  @Input()
  showOverlay = true;

  @Input()
  useObjectivesFromTree = false;

  @Input()
  onlyAspects = false; // Styles Fundamental Objectives (The ones in the second column) like aspects.

  @Output()
  dirty = new EventEmitter();

  // Fired every time an objective or a batch of objectives (via shift selection) is focused
  @Output()
  focusObjective = new EventEmitter();

  @Output()
  dblClickedElement = new EventEmitter<[treeElement: ObjectiveElement, treeLocationSequence: number[], htmlElement: HTMLElement]>();

  @Output()
  dropGlobalElement = new EventEmitter<ObjectiveElement>();

  @ViewChild('shortcutExplanationModal')
  private shortcutExplanationModal: ModalComponent;

  @ViewChild('lineSvg', { static: true })
  lineSvg: ElementRef<HTMLElement>;

  @ViewChild('moveContainer', { static: true })
  moveContainer: ElementRef<HTMLDivElement>;

  @ViewChild('mindMap', { static: true })
  mindMap: ElementRef<HTMLDivElement>;

  @ViewChild('me', { static: true })
  myContainer: ElementRef<HTMLDivElement>;

  @ViewChild('mindContainer', { static: true })
  mindContainer: ElementRef<HTMLDivElement>;

  @ViewChildren(ObjectiveAspectHierarchyElementComponent)
  elementComponents: QueryList<ObjectiveAspectHierarchyElementComponent>;

  @ViewChild('dragDialogTemplate', { read: TemplateRef })
  dragDialogTemplate: TemplateRef<any>;

  actionStack: Action[];
  actionStackPosition = -1;

  deletedAspects: ObjectiveElement[] = []; // permanently deleted (saved in case of 'undo')

  horizontalWindows = false;

  collapsedExtras: boolean;

  panning = false;

  modX = 0;
  modY = 0;

  smallHierarchy = false;
  dragWithChildren = false;

  scale = 1.0;

  document = document;

  // tracks the location of the element that is being dragged (used when dragging elements inside the same component)
  activeMove: MoveInProgress;

  selected: ObjectiveAspectHierarchyElementComponent[] = [];

  exportingImage = false;

  navigationBias: TreeMirror<number>;

  // Variables used to automatically pan while dragging element on the edges
  autoMoveHandler: { (event: DragEvent): void; (this: Document, ev: DragEvent): any; (this: Document, ev: DragEvent): any };
  autoMoveInterval: ReturnType<typeof setInterval>;
  autoMoveX = 0;
  autoMoveY = 0;

  // Used to generate single color action after color picking is finished
  preColors: string[];

  alwaysConfirmDeletion = false;

  boundPresetColorFunctionBackground: () => string[];
  boundPresetColorFunctionText: () => string[];

  // toolbar width + space + trash window width + space
  protected readonly MIND_CONTAINER_WIDTH_BREAKPOINT = 612 + 5 + 250 + 5;

  @ViewChild('debug', { static: true })
  debugTemplate: TemplateRef<any>;

  // When set to true, clicks outside the hierarchy don't cause the selection to reset
  private keepSelectionOnClick = false;

  get teamTrait() {
    return this.projectService.getTeamTrait();
  }

  constructor(
    private cdRef: ChangeDetectorRef,
    private zone: NgZone,
    private snackbar: MatSnackBar,
    private popOverService: PopOverService,
    public debugService: DebugService,
    public platformDetectService: PlatformDetectService,
    public injector: Injector,
    private keyBindHandler: KeyBindHandlerDirective,
    private dialog: MatDialog,
    private projectService: ProjectService,
    private snackBar: MatSnackBar,
    private decisionData: DecisionData,
    private hierarchyRef: ElementRef<HTMLElement>
  ) {
    this.actionStack = [];
    this.navigationBias = new TreeMirror<number>();
    this.boundPresetColorFunctionBackground = () => this.getPresetColors('background');
    this.boundPresetColorFunctionText = () => this.getPresetColors('text');
    this.registerKeyBinds();
    this.collapsedExtras = this.decisionData.projectMode === 'educational';
  }

  get isInFullscreen() {
    return (document.fullscreenElement || (document as any).webkitFullscreenElement) === this.myContainer.nativeElement;
  }

  ngOnInit() {
    this.zoom(this.initialZoomDelta);
    this.debugService.registerFleetingTemplate(this, this.debugTemplate);

    // Ensure that the transform is on the element from the start. If not, we get inconsistent behavior
    // with the z-index (having the transform makes the element automatically appear below elements outside).
    this.setupTransform();
  }

  registerKeyBinds() {
    const noInput = (event: KeyboardEvent) => (event.target as HTMLElement).tagName.toLowerCase() !== 'input';
    const somethingSelected = () => this.selected.length > 0;
    const singleSelected = () => this.selected.length === 1;
    const selectedElement = () => this.selected[0];
    const inFullscreen = () => this.isInFullscreen;
    const editable = () => this.editable;
    const editSelected = (event: KeyboardEvent) => {
      const s = selectedElement();
      s.inputElement.nativeElement.focus();
      s.inputElement.nativeElement.setSelectionRange(5000, 5000);
      event.preventDefault();
    };

    // Navigate Tree
    this.keyBindHandler.register({
      key: 'ArrowLeft',
      ctrlKey: false,
      callback: () => this.navigate('left'),
      conditions: [noInput, singleSelected],
    });

    this.keyBindHandler.register({
      key: 'ArrowRight',
      ctrlKey: false,
      callback: () => this.navigate('right'),
      conditions: [noInput, singleSelected],
    });

    this.keyBindHandler.register({
      key: 'ArrowUp',
      callback: () => this.navigate('up'),
      conditions: [singleSelected],
    });

    this.keyBindHandler.register({
      key: 'ArrowDown',
      callback: () => this.navigate('down'),
      conditions: [singleSelected],
    });

    // Fold up / out from element
    this.keyBindHandler.register({
      key: 'ArrowLeft',
      ctrlKey: true,
      callback: () => this.foldTree(selectedElement().locationSequence, 'up'),
      conditions: [singleSelected],
    });

    this.keyBindHandler.register({
      key: 'ArrowRight',
      ctrlKey: true,
      callback: () => this.foldTree(selectedElement().locationSequence, 'out'),
      conditions: [singleSelected],
    });

    // Exit Fullscreen
    this.keyBindHandler.register({
      key: 'Escape',
      callback: () => this.triggerFullScreen(),
      conditions: [inFullscreen, negate(somethingSelected)],
    });

    // Leave Edit Mode
    this.keyBindHandler.register({
      key: 'Escape',
      callback: () => this.clearSelection(),
      conditions: [somethingSelected, noInput],
    });

    this.keyBindHandler.register({
      key: 'Enter',
      callback: () => this.hierarchyRef.nativeElement.focus(),
      conditions: [singleSelected, negate(noInput), editable],
    });

    // Insert
    if (this.platformDetectService.macOS) {
      this.keyBindHandler.register({
        key: 'v',
        ctrlKey: true,
        callback: () => this.insertRightSelected(),
        conditions: [editable, singleSelected],
      });
    } else {
      this.keyBindHandler.register({
        key: 'Insert',
        callback: () => this.insertRightSelected(),
        conditions: [editable, singleSelected],
      });
    }

    this.keyBindHandler.register({
      key: 'Enter',
      shiftKey: true,
      callback: () => this.insertAboveSelected(),
      conditions: [editable, singleSelected, noInput],
    });

    this.keyBindHandler.register({
      key: 'Enter',
      shiftKey: false,
      callback: () => this.insertBelowSelected(),
      conditions: [editable, singleSelected, noInput],
    });

    // Delete
    this.keyBindHandler.register({
      key: 'Delete',
      callback: () => this.deleteSelected(),
      conditions: [editable, singleSelected, noInput],
    });

    // Edit
    this.keyBindHandler.register({
      key: ' ',
      callback: event => editSelected(event),
      conditions: [editable, singleSelected, noInput],
    });

    this.keyBindHandler.register({
      key: 'Spacebar',
      callback: event => editSelected(event),
      conditions: [editable, singleSelected, noInput],
    });

    this.keyBindHandler.register({
      key: 'F2',
      callback: event => editSelected(event),
      conditions: [editable, singleSelected, noInput],
    });

    // Undo / Redo
    this.keyBindHandler.register({
      key: 'z',
      ctrlKey: true,

      callback: event => {
        event.preventDefault();
        this.undoLast();
      },
      conditions: [editable],
    });

    this.keyBindHandler.register({
      key: 'Z',
      ctrlKey: true,
      shiftKey: true,
      callback: event => {
        event.preventDefault();
        this.redo();
      },
      conditions: [editable],
    });
    this.keyBindHandler.register({
      key: 'y',
      ctrlKey: true,
      callback: event => {
        event.preventDefault();
        this.redo();
      },
      conditions: [editable],
    });

    // Group
    this.keyBindHandler.register({
      key: 'Home',
      callback: event => {
        event.preventDefault();
        this.groupSelected();
      },
      conditions: [editable, somethingSelected],
    });
  }

  debug(message: any) {
    if (this.debugService.isDebugEnabled) {
      console.log(message);
    }
  }

  openHelp() {
    this.shortcutExplanationModal.open();
    if (this.isInFullscreen) {
      this.triggerFullScreen();
    }
  }

  openColorPicker(target: ColorTarget) {
    this.keepSelectionOnClick = true;
    if (this.preColors === undefined) {
      this.preColors =
        target === 'text' ? this.selected.map(e => e.tree.value.textColor) : this.selected.map(e => e.tree.value.backgroundColor);
    }
  }

  setFill(target: ColorTarget, color: string) {
    if (target === 'text') {
      this.selected.forEach(e => (e.tree.value.textColor = color));
    } else {
      this.selected.forEach(e => (e.tree.value.backgroundColor = color));
    }
  }

  closeColorPicker(target: ColorTarget) {
    this.keepSelectionOnClick = false;
    this.execute(
      createCompound(
        this.selected.map((s, i) => {
          return createColor(
            { location: 'tree', locationSequence: s.locationSequence.slice() },
            target,
            this.preColors[i],
            target === 'text' ? s.tree.value.textColor : s.tree.value.backgroundColor
          );
        })
      ),
      true,
      false
    );

    this.preColors = undefined;
  }

  exportImage() {
    if (!this.exportingImage) {
      this.exportingImage = true;
      const currentScale = this.scale;
      const [currentModX, currentModY] = [this.modX, this.modY];
      const currentSelected = this.selected;

      this.scale = 1;
      [this.modX, this.modY] = [0, 0];
      this.selected = [];

      this.setupTransform();
      this.cdRef.detectChanges();

      toPng(this.moveContainer.nativeElement)
        .then(dataUrl => {
          saveAs(dataUrl, 'objective-hierarchy.png');

          this.scale = currentScale;
          this.modX = currentModX;
          this.modY = currentModY;
          this.selected = currentSelected;

          this.setupTransform();
          this.exportingImage = false;
        })
        .catch(error => {
          console.error(error);
          this.snackbar.open($localize`Fehler beim Export des Bildes, versuche einen anderen Browser zu verwenden.`, '', {
            duration: 5000,
          });
          this.exportingImage = false;
        });
    }
  }

  foldSelected(direction: 'out' | 'up') {
    if (this.canFoldSelected()) {
      this.foldTree(this.selected[0].locationSequence, direction);
    }
  }

  canFoldSelected() {
    return this.selected.length === 1;
  }

  focus(event: MouseEvent, element: ObjectiveAspectHierarchyElementComponent, locationSequence: number[]) {
    const commandKeyPressed = this.platformDetectService.macOS && event.metaKey;
    if ((!event.ctrlKey && !event.shiftKey && !commandKeyPressed) || !this.editable) {
      this.selected = [];
    }

    if (this.selected.indexOf(element) === -1) {
      const lastSelectedElement = this.selected[this.selected.length - 1];
      const isRangeSelection =
        event.shiftKey && this.selected.length > 0 && this.areSiblings(lastSelectedElement.locationSequence, locationSequence);

      if (isRangeSelection) {
        // Handle range selection:
        // Make sure all elements between the last selected element and the current selected element are selected

        const parent = locationSequence.slice(0, -1);
        const fromSibling = lastSelectedElement.locationSequence[lastSelectedElement.locationSequence.length - 1];
        const toSibling = locationSequence[locationSequence.length - 1];

        for (let sibling = Math.min(fromSibling, toSibling); sibling <= Math.max(fromSibling, toSibling); sibling++) {
          if (sibling === fromSibling) continue; // Already selected.

          const siblingComponent = this.findComponentFromLocationSequence([...parent, sibling]);
          // Avoid double-pushing.
          if (!this.selected.includes(siblingComponent)) {
            this.selected.push(siblingComponent);
          }
        }
      } else {
        // Handle single selection.
        this.selected.push(element);
      }

      this.focusObjective.emit();
    } else {
      let shouldDeselect = false;
      if (commandKeyPressed) {
        // On macOS we can deselect with the command key.
        shouldDeselect = true;
      } else if (event.ctrlKey) {
        // On Windows we can deselect with the control key.
        // Is also a fallback for all other operating systems.
        shouldDeselect = true;
      }

      if (shouldDeselect) this.selected = this.selected.filter(selectedElement => selectedElement !== element);
    }
  }

  // Action Logic
  getTreeFromLoc(treeLoc: TreeLoc | number[]) {
    let sequence: number[];
    if (Array.isArray(treeLoc)) {
      sequence = treeLoc;
    } else {
      sequence = treeLoc.locationSequence;
    }
    sequence = sequence.slice();

    if (sequence.length === 1) {
      return [undefined, this.tree];
    } else {
      sequence.splice(0, 1);
    }

    let node = this.tree;
    let before;

    for (const index of sequence) {
      before = node;

      if (node.children === undefined) {
        node.children = [];
      }

      node = node.children[index];

      if (node === undefined) {
        return [before, undefined];
      }
    }

    return [before, node];
  }

  getLocsOfChildren(element: ObjectiveAspectHierarchyElementComponent): number[][] {
    return element.tree.children.map((_, i) => [...element.locationSequence, i]);
  }

  // List Operations
  spliceList(list: List, index: number, deleteCount: number, toInsert?: ObjectiveElement) {
    const mutableList = this.getList(list) as ObjectiveElement[];

    return toInsert ? mutableList.splice(index, deleteCount, toInsert) : mutableList.splice(index, deleteCount);
  }

  isElement(value: Tree<HierarchyElement> | ObjectiveElement): value is ObjectiveElement {
    return (value as ObjectiveElement).name !== undefined;
  }

  insertLoc(loc: ObjectiveLocation, value: Tree<HierarchyElement> | ObjectiveElement) {
    switch (loc.location) {
      case 'tree': {
        const node = this.getTreeFromLoc(loc);
        const treeValue = this.isElement(value) ? new Tree(value) : value;

        return node[0].children.splice(loc.locationSequence[loc.locationSequence.length - 1], 0, treeValue)[0];
      }
      case 'trash':
      case 'brainstorming': {
        const elementValue = this.isElement(value) ? value : value.value;

        return this.spliceList(loc.location, loc.index, 0, elementValue)[0];
      }
    }
  }

  removeLoc(loc: ObjectiveLocation) {
    switch (loc.location) {
      case 'tree': {
        const node = this.getTreeFromLoc(loc);

        return node[0].children.splice(node[0].children.indexOf(node[1]), 1)[0];
      }
      case 'trash':
      case 'brainstorming':
        return this.spliceList(loc.location, loc.index, 1)[0];
    }
  }

  areSiblings<T extends TreeLoc | number[]>(loc1: T, loc2: T): boolean {
    const locSequence1 = Array.isArray(loc1) ? loc1 : loc1.locationSequence;
    const locSequence2 = Array.isArray(loc2) ? loc2 : loc2.locationSequence;

    let sameTree = false;

    if (locSequence1.length === locSequence2.length) {
      sameTree = true;
      for (let i = 0; i < locSequence1.length - 1; i++) {
        if (locSequence1[i] !== locSequence2[i]) {
          sameTree = false;
          break;
        }
      }
    }

    return sameTree;
  }

  clearSelection() {
    this.selected = [];
  }

  fixDeletedSelection(locationSequence: number[]) {
    const newSelectionSequence = locationSequence.slice();
    let trees: Array<Tree<HierarchyElement>> = this.getTreeFromLoc(newSelectionSequence);

    if (trees[0]) {
      const wasLast = trees[0].children.length === 0;

      if (wasLast) {
        newSelectionSequence.pop();
      } else if (!trees[1]) {
        newSelectionSequence.pop();
        newSelectionSequence.push(trees[0].children.length - 1);
      }
    } else {
      newSelectionSequence.pop();
      newSelectionSequence.pop();

      while (!(trees = this.getTreeFromLoc(newSelectionSequence))[1]) {
        newSelectionSequence.pop();
      }
    }

    this.selectAndFrame(newSelectionSequence);
  }

  selectAndFrame(locationSequence: number[]) {
    const newSelectedComponent = this.findComponentFromLocationSequence(locationSequence);

    if (newSelectedComponent) {
      this.selected = [newSelectedComponent];
      this.makeVisible(newSelectedComponent);
    }
  }

  moveLoc(from: ObjectiveLocation, to: ObjectiveLocation, reconstruct?: Tree<HierarchyElement>) {
    // Move Sub Tree Inside of Hierarchy
    if (from.location === 'tree' && to.location === 'tree') {
      const fromIArea = this.getTreeFromLoc(from);

      if (this.areSiblings(from, to)) {
        const lastFrom = from.locationSequence[from.locationSequence.length - 1];
        let lastTo = to.locationSequence[to.locationSequence.length - 1];

        const toMove = this.removeLoc(from);

        if (lastFrom < lastTo) {
          lastTo--;
        }

        const finalTo = to.locationSequence.slice();
        finalTo[finalTo.length - 1] = lastTo;

        this.insertLoc({ location: 'tree', locationSequence: finalTo }, toMove);
      } else {
        const tree = fromIArea[1];
        this.insertLoc(to, tree);

        fromIArea[0].children.splice(fromIArea[0].children.indexOf(tree), 1);
      }
    } else if (from.location !== 'tree' && from.location === to.location) {
      // Change Order in List
      const list = this.getList(from.location);

      const objective = list[from.index];

      if (from.index < to.index) {
        this.insertLoc(to, objective);
        this.removeLoc(from);
      } else {
        this.removeLoc(from);
        this.insertLoc(to, objective);
      }
    } else if (from.location !== 'tree') {
      const list = this.getList(from.location);

      let counter = 0;
      if (reconstruct) {
        const toCheck = [reconstruct];

        while (toCheck.length > 0) {
          const next = toCheck.pop();

          if (next.value.name !== 'x') {
            next.value = list[from.index + counter++];
          } else {
            next.value.name = '';
          }

          if (next.children !== undefined) {
            toCheck.push(...next.children);
          }
        }

        for (let i = 0; i < counter; i++) {
          this.removeLoc(from);
        }

        this.insertLoc(to, reconstruct);
      } else {
        const objective = list[from.index];
        objective.isAspect = true; // mark node as created from brainstorming

        this.insertLoc(to, objective);
        this.removeLoc(from);
      }
    } else if (to.location !== 'tree') {
      const tree = this.removeLoc(from) as Tree<HierarchyElement>;

      const toCheck = [tree];
      const objectives = [];

      while (toCheck.length > 0) {
        const next = toCheck.pop();

        if (next.value.name !== '') {
          objectives.push(next.value);
        }

        if (next.children !== undefined) {
          toCheck.push(...next.children);
        }
      }

      for (let i = objectives.length - 1; i >= 0; i--) {
        this.insertLoc(to, objectives[i]);
      }
    }
  }

  @HostListener('window:mousedown', ['$event'])
  mouseDown(event: MouseEvent) {
    if (this.keepSelectionOnClick) {
      return;
    }

    if (!this.isInsideMe(event.target as HTMLElement)) {
      this.selected = [];
    }
  }

  rootSelected() {
    return this.selected.some(e => e.depth === 0);
  }

  async deleteSelected() {
    if (this.canDeleteSelected()) {
      if (this.selected.length === 1) {
        const selectedElement = this.selected[0];
        const sequence = selectedElement.locationSequence.slice();

        return await this.multiMoveTree([selectedElement.locationSequence], 'trash').then(result => {
          if (result === ActionResult.EXECUTED) {
            this.cdRef.detectChanges();
            this.fixDeletedSelection(sequence);
          }

          return result;
        });
      } else {
        return await this.multiMoveTree(
          this.selected.map(e => e.locationSequence.slice()),
          'trash'
        );
      }
    } else {
      return ActionResult.DENIED;
    }
  }

  async groupSelected() {
    return this.group(this.selected.map(element => element.locationSequence));
  }

  async group(locationSequences: number[][]) {
    if (this.canGroup(locationSequences)) {
      const compareSequences = (a: number[], b: number[]) => a[a.length - 1] - b[b.length - 1];
      const sortedSequences = locationSequences.sort(compareSequences);
      const firstSequence = sortedSequences[0].slice();

      return this.execute(createCompound(buildGroup(locationSequences.map(locationSequence => locationSequence.slice())))).then(result => {
        if (result === ActionResult.EXECUTED) {
          this.cdRef.detectChanges();

          this.selectAndFrame(firstSequence);

          if (this.selected[0]) {
            this.selected[0].inputElement.nativeElement.focus();
          }
        }
      });
    } else {
      return ActionResult.DENIED;
    }
  }

  insertBelowSelected() {
    this.insertBelow(this.selected[0]);
  }

  insertBelow(element: ObjectiveAspectHierarchyElementComponent) {
    if (this.canInsertBelow(element.locationSequence)) {
      this.addElement(element.locationSequence.slice(), 1);
    }
  }

  insertAboveSelected() {
    this.insertAbove(this.selected[0]);
  }

  insertAbove(element: ObjectiveAspectHierarchyElementComponent) {
    if (this.canInsertAbove(element.locationSequence)) {
      this.addElement(element.locationSequence.slice(), 0);
    }
  }

  insertRightSelected() {
    this.insertRight(this.selected[0]);
  }

  insertRight(element: ObjectiveAspectHierarchyElementComponent) {
    if (this.canInsertRight(element.locationSequence)) {
      const index = element.tree.children ? element.tree.children.length : 0;
      this.addElement(element.locationSequence.slice(), 2, undefined, true, index);
    }
  }

  async insertFromAddButtons(element: ObjectiveAspectHierarchyElementComponent, direction: InsertDirection) {
    switch (direction) {
      case InsertDirection.RIGHT:
        if (element.tree.children.length === 0 || element.locationSequence.length === 1) {
          this.insertRight(element);
        } else {
          await this.group(this.getLocsOfChildren(element));
        }
        break;
      case InsertDirection.LEFT:
        await this.group([element.locationSequence]);
        break;
      case InsertDirection.ABOVE:
      case InsertDirection.BELOW:
        this.insert(element, direction);
    }
  }

  insert(element: ObjectiveAspectHierarchyElementComponent, direction: InsertDirection) {
    switch (direction) {
      case InsertDirection.ABOVE:
        this.insertAbove(element);
        break;
      case InsertDirection.RIGHT:
        this.insertRight(element);
        break;
      case InsertDirection.BELOW:
        this.insertBelow(element);
        break;
    }
  }

  canRedo() {
    return this.actionStackPosition < this.actionStack.length - 1;
  }

  async redo() {
    if (this.canRedo()) {
      const next = this.actionStack[this.actionStackPosition + 1];

      const result = await this.execute(next, false);

      if (result === ActionResult.EXECUTED) {
        this.actionStackPosition++;
      }

      return result;
    } else {
      return ActionResult.EXECUTED;
    }
  }

  canUndo() {
    return this.actionStackPosition >= 0;
  }

  async undoLast() {
    if (this.canUndo()) {
      const last = this.actionStack[this.actionStackPosition];

      const result = await this.undo(last);

      if (result === ActionResult.EXECUTED) {
        this.actionStackPosition--;
      }

      return result;
    } else {
      return ActionResult.EXECUTED;
    }
  }

  async undo(action: Action) {
    this.debug(`[UNDO] ${action.type}`);
    switch (action.type) {
      case 'deleteAllPermanent': {
        const elementsToRestore = this.deletedAspects.slice(
          this.deletedAspects.length - action.numberOfDeletedElements,
          this.deletedAspects.length
        );
        this.decisionData.objectiveAspects.listOfDeletedAspects.push(...elementsToRestore);
        this.deletedAspects.splice(this.deletedAspects.length - action.numberOfDeletedElements, action.numberOfDeletedElements);
        return await this.execute(createDeleteAllPermanent(action.numberOfDeletedElements), false, false);
      }
      case 'deletePermanent': {
        const element = this.deletedAspects.pop();
        this.decisionData.objectiveAspects.listOfDeletedAspects.splice(action.index, 0, element);
        return await this.execute(createDeletePermanent(action.index), false, false);
      }
      case 'rename':
        return await this.execute(createRename(action.location, '', action.before), false);
      case 'color':
        return await this.execute(createColor(action.location, action.target, '', action.before), false);
      case 'move': {
        action = cloneDeep(action);

        const from = action.from;
        const to = action.to;

        if (from.location === 'tree' && to.location === 'tree') {
          const seqFrom = from.locationSequence;
          const seqTo = to.locationSequence;

          if (this.areSiblings(from, to)) {
            if (seqFrom[seqFrom.length - 1] > seqTo[seqTo.length - 1]) {
              seqFrom[seqFrom.length - 1]++;
            } else {
              seqTo[seqTo.length - 1]--;
            }
          } else {
            for (let lI = 0; lI < seqFrom.length; lI++) {
              if (lI === seqFrom.length - 1) {
                const otherValue = seqTo[lI];
                if (seqFrom[lI] < otherValue) {
                  seqTo[lI]--;
                }
              } else if (seqFrom[lI] !== seqTo[lI] || lI >= seqTo.length) {
                break;
              }
            }

            for (let lI = 0; lI < seqTo.length; lI++) {
              if (lI === seqTo.length - 1) {
                const otherValue = seqFrom[lI];
                if (seqTo[lI] <= otherValue) {
                  seqFrom[lI]++;
                }
              } else if (seqTo[lI] !== seqFrom[lI] || lI >= seqFrom.length) {
                break;
              }
            }
          }
        }

        if (from.location !== 'tree' && from.location === to.location) {
          if (from.index < to.index) {
            to.index--;
          } else {
            from.index++;
          }
        }

        return await this.execute({ type: 'move', from: to, to: from, reconstruct: action.reconstruct }, false);
      }
      case 'deleteEmpty':
        return await this.execute(
          {
            type: 'insert',
            location: action.location,
            value: new ObjectiveElement(''),
          },
          false
        );
      case 'insert':
        return await this.execute({ type: 'deleteEmpty', location: action.location }, false);
      case 'compound':
        for (let i = action.actions.length - 1; i >= 0; i--) {
          if ((await this.undo(action.actions[i])) === ActionResult.EXECUTED) {
            continue;
          } else {
            const toUndo = action.actions.length - 1 - i;

            if (toUndo > 0) {
              this.alwaysConfirmDeletion = true;
              for (let a = i + 1; a < action.actions.length; a++) {
                await this.execute(action.actions[a], false);
              }
              this.alwaysConfirmDeletion = false;
            }

            return ActionResult.DENIED;
          }
        }
        return ActionResult.EXECUTED;
    }
  }

  executeMove(from: ObjectiveLocation, to: ObjectiveLocation) {
    return this.execute(createMove(from, to));
  }

  createReconstruct(tree: Tree<HierarchyElement>) {
    const copy = cloneDeep(tree);

    const toCheck = [copy];

    while (toCheck.length > 0) {
      const next = toCheck.pop();

      if (next.value.name === '') {
        next.value.name = 'x';
      } else {
        next.value.name = '';
      }

      if (next.children !== undefined) {
        toCheck.push(...next.children);
      }
    }

    return copy;
  }

  async execute(action: Action, putOnStack = true, execute = true) {
    let result: ActionResult = ActionResult.EXECUTED;

    if (execute) {
      switch (action.type) {
        case 'deleteAllPermanent': {
          this.deletedAspects = this.deletedAspects.concat(this.decisionData.objectiveAspects.listOfDeletedAspects);
          this.decisionData.objectiveAspects.listOfDeletedAspects.splice(0, this.decisionData.objectiveAspects.listOfDeletedAspects.length);
          break;
        }
        case 'deletePermanent': {
          this.deletedAspects.push(this.decisionData.objectiveAspects.listOfDeletedAspects[action.index]);
          this.decisionData.objectiveAspects.listOfDeletedAspects.splice(action.index, 1);
          break;
        }
        case 'rename':
          {
            const location = action.location;
            const node = this.getTreeFromLoc(location);

            if (location.locationSequence.length === 2 && this.decisionData.objectives[location.locationSequence[1]] != null) {
              this.decisionData.objectives[location.locationSequence[1]].name = action.after;
            }

            node[1].value.name = action.after;

            const component = this.elementComponents.find(c => c.tree === node[1]);
            if (component) {
              component.calcInputWidth();
            }
          }
          break;
        case 'color':
          {
            const location = action.location;
            const node = this.getTreeFromLoc(location);

            if (action.target === 'text') {
              node[1].value.textColor = action.after;
            } else {
              node[1].value.backgroundColor = action.after;
            }
          }
          break;
        case 'move': {
          const from = action.from;
          const to = action.to;

          if (
            from.location === 'tree' &&
            to.location === 'tree' &&
            from.locationSequence.length === 2 &&
            to.locationSequence.length === 2
          ) {
            // Change Objective Order
            const fromIndex = from.locationSequence[1];
            let toIndex = to.locationSequence[1];

            if (fromIndex < toIndex) {
              toIndex--;
            }

            this.decisionData.moveObjective(fromIndex, toIndex);
          } else if (to.location === 'tree' && to.locationSequence.length === 2) {
            // Add Objective
            let objectiveName: string;

            if (from.location === 'tree') {
              objectiveName = this.getTreeFromLoc(from)[1].value.name;
            } else if (action.reconstruct) {
              objectiveName = action.reconstruct.value.name === 'x' ? '' : this.getList(from.location)[from.index].name;
            } else {
              objectiveName = this.getList(from.location)[from.index].name;
            }

            const toAdd = new Objective(objectiveName);
            this.decisionData.addObjective(toAdd, to.locationSequence[1]);

            if (from.location === 'tree') {
              toAdd.aspects = this.getTreeFromLoc(from)[1];
            }
          } else if (
            from.location === 'tree' &&
            from.locationSequence.length === 2 &&
            (to.location !== 'tree' || to.locationSequence.length > 2)
          ) {
            // Delete Objective
            const confirmed = await this.requestDeletionConfirmation(from.locationSequence[1]);
            if (!confirmed) {
              result = ActionResult.DENIED;
              break;
            }
            this.decisionData.removeObjective(from.locationSequence[1]);
          }

          let reconstruct = action.reconstruct;
          if (from.location === 'tree' && to.location !== 'tree') {
            reconstruct = this.createReconstruct(this.getTreeFromLoc(from)[1]);

            action.reconstruct = reconstruct;
          }

          if (from.location === 'tree') {
            this.clearSelection();
          }

          this.moveLoc(from, to, reconstruct);

          if (from.location !== 'tree' && to.location === 'tree' && to.locationSequence.length === 2) {
            this.decisionData.objectives[to.locationSequence[1]].aspects = this.getTreeFromLoc(to)[1];
          }
          break;
        }
        case 'deleteEmpty':
          if (action.location.locationSequence.length === 2) {
            const confirmed = await this.requestDeletionConfirmation(action.location.locationSequence[1]);

            if (confirmed) {
              this.decisionData.removeObjective(action.location.locationSequence[1]);
            } else {
              result = ActionResult.DENIED;
              break;
            }
          }

          this.clearSelection();
          this.removeLoc(action.location);
          break;
        case 'insert': {
          let valueClone = this.isElement(action.value)
            ? new ObjectiveElement(action.value.name, action.value.createdInSubStep, action.value.backgroundColor)
            : cloneDeep(action.value);

          if (!this.dragWithChildren) {
            if (this.isElement(valueClone)) {
              valueClone.comment = '';
            } else {
              valueClone.value.comment = '';
              // expand children by default
              valueClone.value.collapsed = false;
            }
          }

          if (action.location.location === 'tree' && action.location.locationSequence.length === 2) {
            const name = this.isElement(valueClone) ? valueClone.name : valueClone.value.name;
            const toAdd = new Objective(name);
            if (this.dragWithChildren) {
              toAdd.comment = this.isElement(valueClone) ? valueClone.comment : valueClone.value.comment;
              if (action.scale instanceof NumericalObjectiveData) {
                toAdd.numericalData = action.scale;
                toAdd.objectiveType = ObjectiveType.Numerical;
              } else if (action.scale instanceof VerbalObjectiveData) {
                toAdd.verbalData = action.scale;
                toAdd.objectiveType = ObjectiveType.Verbal;
              } else if (action.scale instanceof IndicatorObjectiveData) {
                toAdd.indicatorData = action.scale;
                toAdd.objectiveType = ObjectiveType.Indicator;
              }
            }
            this.decisionData.addObjective(toAdd, action.location.locationSequence[1]);

            if (this.isElement(valueClone)) {
              valueClone = new Tree(valueClone);
            }

            toAdd.aspects = valueClone;
          }

          this.insertLoc(action.location, valueClone);
          break;
        }
        case 'compound':
          for (let i = 0; i < action.actions.length; i++) {
            const toExecute = action.actions[i];

            if ((await this.execute(toExecute, false)) === ActionResult.EXECUTED) {
              continue;
            } else {
              if (i > 0) {
                this.alwaysConfirmDeletion = true;
                for (let a = i - 1; a >= 0; a--) {
                  await this.undo(action.actions[a]);
                }
                this.alwaysConfirmDeletion = false;
              }

              result = ActionResult.DENIED;
              break;
            }
          }
          break;
      }
    }

    if (result === ActionResult.EXECUTED) {
      this.dirty.emit();

      if (putOnStack) {
        this.actionStack = this.actionStack.slice(0, this.actionStackPosition + 1);
        this.actionStack.push(action);
        this.actionStackPosition = this.actionStack.length - 1;
      }
    }

    this.debug(`[EXECUTE] Action ${action.type} was ${result === ActionResult.EXECUTED ? 'executed' : 'denied'}`);

    if (this.testObjectiveSync() !== 0) {
      this.snackbar.open($localize`Synchronisationsfehler, wechsle zurück zur Listendarstellung um Probleme zu vermeiden.`);
    }

    return result;
  }

  testObjectiveSync() {
    for (let i = 0; i < this.tree.children.length; i++) {
      const c = this.tree.children[i];

      const objective = this.decisionData.objectives[i];

      if (!objective) {
        console.log(`[SYNC] Missing objective at index ${i}`);
        return 1;
      } else if (objective.name !== c.value.name) {
        console.log(`[SYNC] Differing Objective name at index ${i} (M: '${c.value.name}' T: '${objective.name}')`);
        return 2;
      } else if (
        objective.aspects.children.length !== c.children.length ||
        objective.aspects.children.some((t, iT) => !this.areTreesEqual(t, c.children[iT]))
      ) {
        console.log(`[SYNC] Differing Aspect Tree at index ${i}`);
        console.log(c.children);
        console.log(objective.aspects.children);
        return 3;
      }
    }

    return 0;
  }

  rename(location: TreeLoc, to: string) {
    const node = this.getTreeFromLoc(location)[1];

    if (node.value.name !== to) {
      const action: Rename = createRename(location, node.value.name, to);
      this.execute(action);
    }
  }

  // View
  setupTransform() {
    window.requestAnimationFrame(() => {
      const moveContainerOriginalWidth = this.moveContainer.nativeElement.offsetWidth; // width not affected by scale
      /* Fixes moving tree, when expanding/collapsing elements with a scale level different from 1. */
      const transformXCorrection = (moveContainerOriginalWidth * this.scale - moveContainerOriginalWidth) / 2;
      this.moveContainer.nativeElement.style.transform = `translate(${this.modX + transformXCorrection}px, ${this.modY}px) scale(${
        this.scale
      })`;
    });
  }

  makeVisible(element: ObjectiveAspectHierarchyElementComponent) {
    const nodeContainer = element.nodeElement.nativeElement;
    const limitContainer = this.mindContainer.nativeElement;

    const nodeRect = nodeContainer.getBoundingClientRect();
    const limitRect = limitContainer.getBoundingClientRect();

    // Move Vertical
    if (nodeRect.top < limitRect.top) {
      this.modY += limitRect.top - nodeRect.top + 5;
    } else if (nodeRect.bottom > limitRect.bottom) {
      this.modY -= nodeRect.bottom - limitRect.bottom + 5;
    }

    // Move Horizontal
    if (nodeRect.left < limitRect.left) {
      this.modX += limitRect.left - nodeRect.left + 5;
    } else if (nodeRect.right > limitRect.right) {
      this.modX -= nodeRect.right - limitRect.right + 5;
    }

    this.setupTransform();
  }

  triggerFullScreen() {
    const dummyPromise = Promise.resolve();
    if (this.isInFullscreen) {
      const doc = document as any;
      let promise: Promise<void>;
      if (doc.exitFullscreen) {
        promise = document.exitFullscreen();
      } else if (doc.webkitExitFullscreen) {
        promise = doc.webkitExitFullscreen();
      }

      (promise ?? dummyPromise).then(() => this.cdRef.detectChanges());
    } else {
      const element = this.myContainer.nativeElement as any;
      let promise: Promise<void>;
      if (element.requestFullscreen) {
        promise = element.requestFullscreen();
      } else if (element.webkitRequestFullscreen) {
        promise = element.webkitRequestFullscreen();
      } else if (element.webkitRequestFullScreen) {
        promise = element.webkitRequestFullScreen();
      }

      (promise ?? dummyPromise).then(() => this.cdRef.detectChanges());
    }
  }

  collapseTree(event: MouseEvent, tree: Tree<HierarchyElement>, treeSequence: number[]) {
    tree.value.collapsed = !tree.value.collapsed;

    this.unselectFoldedElements(treeSequence);
    this.setupTransform();
  }

  foldTree(origin: number[], direction: 'up' | 'out') {
    const tree = this.getTreeFromLoc(origin)[1];

    if (tree) {
      let min = -1;

      let nextLevel = [tree];

      const levels = [];

      let currentLevel = 0;

      while (nextLevel.length > 0) {
        levels.push([...nextLevel]);

        const toDo = nextLevel.slice();
        nextLevel = [];

        for (const checkTree of toDo) {
          const collapseTree = checkTree;
          if (collapseTree.value.collapsed && min === -1) {
            min = currentLevel;
          }

          if (!collapseTree.value.collapsed) {
            nextLevel.push(...collapseTree.children);
          }
        }
        currentLevel++;
      }

      if (direction === 'up') {
        const collapseLevel = currentLevel - 2;
        const isRoot = origin.length < 2;
        if (collapseLevel >= 0 && collapseLevel < levels.length && (!isRoot || collapseLevel > 0)) {
          levels[collapseLevel].forEach(t => {
            t.value.collapsed = true;
          });
        }
      } else if (min > -1) {
        levels[min].forEach(t => {
          t.value.collapsed = false;
        });
      }
      this.setupTransform();
    }
  }

  private unselectFoldedElements(foldedElementSequence: number[]) {
    this.selected = this.selected.filter(s => {
      return isEqual(foldedElementSequence, s.locationSequence) || !isChildOf(foldedElementSequence, s.locationSequence);
    });
  }

  zoom(delta: number) {
    const scaleDelta = clamp(delta * ObjectiveAspectHierarchyComponent.SCROLL_TO_ZOOM_RATIO, -0.25, 0.25);
    const oldScale = this.scale;
    this.scale += scaleDelta;
    this.scale = Math.max(this.scale, ObjectiveAspectHierarchyComponent.MINIMAL_SCALE);
    // center on current box center (required because of the transformXCorrection in setupTransform())
    if (delta > 0) {
      this.modX -= (this.moveContainer.nativeElement.offsetWidth / 2) * Math.abs(this.scale - oldScale);
    } else {
      this.modX += (this.moveContainer.nativeElement.offsetWidth / 2) * Math.abs(this.scale - oldScale);
    }
    this.setupTransform();
  }

  zoomIn() {
    this.zoom(+25);
  }

  zoomOut() {
    this.zoom(-25);
  }

  resetView() {
    this.scale = 1.0 + this.initialZoomDelta / 100;
    this.modX = 0;
    this.modY = 0;

    this.setupTransform();
  }

  addElement(locationSequence: number[], direction: number, newElement?: Tree<HierarchyElement>, autoFocus = true, index = 0) {
    if (newElement === undefined) {
      newElement = new Tree<HierarchyElement>(new ObjectiveElement(''));
    }

    const tree = this.getTreeFromLoc(locationSequence)[1];

    tree.value.collapsed = false;

    if (direction === 2) {
      locationSequence.push(index);
    } else {
      locationSequence[locationSequence.length - 1] += direction;
    }

    if (autoFocus) {
      ObjectiveAspectHierarchyElementComponent.FOCUS = locationSequence;
    }

    const action: Insert = createInsert({ location: 'tree', locationSequence }, newElement);

    this.execute(action).then(_1 => {
      this.cdRef.detectChanges();
    });
  }

  getFlex(list: List) {
    return list === 'brainstorming' ? this.listOfAspects.length : this.listOfDeletedAspects.length;
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this.mindMap.nativeElement.addEventListener('wheel', event => {
        event.preventDefault();
        event.stopImmediatePropagation();
        const pixelDeltaY = getPixelDeltaY(event, 25, 500);

        if (event.ctrlKey) {
          this.zoom(-pixelDeltaY);
        } else {
          if (event.shiftKey) {
            this.modX += clamp(pixelDeltaY, -50, 50) * -1;
          } else {
            this.modY += clamp(pixelDeltaY, -50, 50) * -1;
          }

          this.setupTransform();
        }
      });

      this.mindMap.nativeElement.addEventListener('mousedown', (event: MouseEvent) => {
        const target = event.target as HTMLElement;

        let shouldPan = false;
        // Purpose: If the user clicks in the white space of the hierarchy we want to clear the selection.
        // If the click happened in another node however we are not responsible for handling the selection clearing.
        // If the click happens outside the hierarchy we are also not responsible for handling that.
        let shouldClearSelection = false;

        if (this.isInsideMindMap(target)) {
          const isInNode = target.closest('dt-objective-aspect-hierarchy-element') !== null;
          const isMiddleClick = event.button === 1;

          if (!isInNode || isMiddleClick) {
            shouldPan = true;
          }

          if (!isInNode) {
            shouldClearSelection = true;
          }
        }

        if (shouldPan) {
          this.panning = true;

          this.cdRef.detectChanges();
        }

        if (shouldClearSelection) {
          this.clearSelection();
        }
      });

      this.mindMap.nativeElement.addEventListener('mouseup', () => (this.panning = false));
      this.mindMap.nativeElement.addEventListener('mouseleave', () => (this.panning = false));

      {
        // Mouse tracking for event.movement[X/Y] polyfill.
        let mx: number = null;
        let my: number = null;
        this.mindMap.nativeElement.addEventListener('mousemove', (event: MouseEvent) => {
          const difX = event.screenX - (mx ?? event.screenX);
          const difY = event.screenY - (my ?? event.screenY);

          mx = event.screenX;
          my = event.screenY;

          if (this.panning) {
            event.preventDefault();
            event.stopImmediatePropagation();

            this.modX += difX;
            this.modY += difY;

            this.setupTransform();
          }
        });
      }

      if (this.allowDragPanning) {
        this.autoMoveHandler = (event: DragEvent) => this.dragOverGlobal(event);
        this.document.addEventListener('dragover', this.autoMoveHandler);
      }

      // Touch Moving
      let movingTouch: { ident: number; x: number; y: number } = null;
      this.mindMap.nativeElement.addEventListener('touchstart', (event: TouchEvent) => {
        this.panning = true;

        movingTouch = {
          ident: event.changedTouches[0].identifier,
          x: event.changedTouches[0].pageX,
          y: event.changedTouches[0].pageY,
        };

        this.cdRef.detectChanges();
      });

      this.mindMap.nativeElement.addEventListener('touchmove', (event: TouchEvent) => {
        for (const changedTouch of Array.from(event.changedTouches)) {
          if (changedTouch.identifier === movingTouch?.ident) {
            const difX = changedTouch.pageX - movingTouch.x;
            const difY = changedTouch.pageY - movingTouch.y;

            movingTouch.x = changedTouch.pageX;
            movingTouch.y = changedTouch.pageY;

            this.modX += difX;
            this.modY += difY;

            this.setupTransform();
            event.preventDefault();
          }
        }
      });

      const cancelTouch = (event: TouchEvent) => {
        for (const changedTouch of Array.from(event.changedTouches)) {
          if (changedTouch.identifier === movingTouch?.ident) {
            movingTouch = null;
            this.panning = false;
            break;
          }
        }
      };

      this.mindMap.nativeElement.addEventListener('touchend', cancelTouch);
      this.mindMap.nativeElement.addEventListener('touchcancel', cancelTouch);
    });

    if (this.decisionData.objectives.length === 1 && this.decisionData.objectives[0].name === '' && this.editable) {
      this.selected = [this.findComponentFromLocationSequence([0, 0])];
      this.selected[0].inputElement.nativeElement.focus();
      this.cdRef.detectChanges();
    }
  }

  isInsideMindMap(node: HTMLElement) {
    return this.mindMap.nativeElement.contains(node);
  }

  isInsideMe(node: HTMLElement) {
    return this.myContainer.nativeElement.contains(node);
  }

  pinchZoom(change: number) {
    this.scale = Math.max(this.scale + change, 0.40187757201);
    this.setupTransform();
  }

  ngOnDestroy() {
    this.document.removeEventListener('dragover', this.autoMoveHandler);
    this.debugService.removeFleetingTemplate(this);
  }

  dragOverGlobal(event: DragEvent) {
    const move = (x: number, y: number) => {
      this.modX += x;
      this.modY += y;

      this.setupTransform();
    };

    if (this.activeMove) {
      const margin = 20;

      const rect = this.mindMap.nativeElement.getBoundingClientRect();

      const relativeX = event.clientX - rect.left;
      const relativeY = event.clientY - rect.top;

      this.autoMoveX = this.autoMoveY = 0;

      if (relativeX < margin) {
        this.autoMoveX = 4;
      } else if (this.mindMap.nativeElement.offsetWidth - relativeX < margin) {
        this.autoMoveX = -4;
      }

      if (relativeY < margin) {
        this.autoMoveY = 4;
      } else if (this.mindMap.nativeElement.offsetHeight - relativeY < margin) {
        this.autoMoveY = -4;
      }

      if (this.autoMoveInterval === undefined && (this.autoMoveX !== 0 || this.autoMoveY !== 0)) {
        this.autoMoveInterval = setInterval(() => {
          if ((this.autoMoveX === 0 && this.autoMoveY === 0) || !this.activeMove) {
            clearInterval(this.autoMoveInterval);
            this.autoMoveInterval = undefined;
          }
          move(this.autoMoveX, this.autoMoveY);
        }, 15);
      }
    }
  }

  // Drag and Drop Logic

  // Drag Raw String Aspect from somewhere
  dragAspect(event: DragEvent, index: number, origin: List) {
    this.activeMove = { from: { location: origin, index } };
  }

  dragTreeNode(
    event: DragEvent,
    draggedTree: Tree<HierarchyElement>,
    depth: number,
    component: ObjectiveAspectHierarchyElementComponent,
    locationSequence: number[]
  ) {
    const scales = [];
    if (depth === 0) {
      // dragging Root, collect the scale of each objective
      for (const rootChild of draggedTree.children) {
        if (rootChild.value.scale instanceof NumericalObjectiveData) {
          scales.push(rootChild.value.scale);
        } else if (rootChild.value.scale instanceof VerbalObjectiveData) {
          scales.push(rootChild.value.scale);
        } else if (rootChild.value.scale instanceof IndicatorObjectiveData) {
          scales.push(rootChild.value.scale);
        }
      }
    } else if (depth === 1) {
      // dragging a single objective, collect its scale
      if (draggedTree.value.scale instanceof NumericalObjectiveData) {
        scales.push(draggedTree.value.scale);
      } else if (draggedTree.value.scale instanceof VerbalObjectiveData) {
        scales.push(draggedTree.value.scale);
      } else if (draggedTree.value.scale instanceof IndicatorObjectiveData) {
        scales.push(draggedTree.value.scale);
      }
    }
    ObjectiveAspectHierarchyComponent.globalDragData = {
      treeId: this.treeId,
      treeSegment: draggedTree,
      isRoot: depth === 0,
      scales: scales,
    };

    if (depth !== 0) {
      // multi selection disabled for Standardzielsysteme
      if (event.ctrlKey && this.selected.indexOf(component) === -1 && this.editable) {
        this.selected.push(component);
      }

      this.sortSelected();

      if (this.selected.indexOf(component) !== -1) {
        if (this.rootSelected()) {
          event.preventDefault();
          event.stopPropagation();
          return false;
        }

        this.activeMove = {
          from: this.selected.map(e => {
            return { location: 'tree', locationSequence: e.locationSequence.slice() };
          }),
        };
      } else {
        this.selected = [component];
        this.activeMove = { from: [{ location: 'tree', locationSequence: locationSequence.slice() }] };
      }

      const dragImage = this.generateDragElement(
        this.selected.map(s => s.tree.value.name),
        depth
      );
      this.document.body.appendChild(dragImage);

      event.dataTransfer.setDragImage(dragImage, event.offsetX, event.offsetY);
    } else {
      return false;
    }
  }

  generateDragElement(objectives: string[], depth: number) {
    const containerElement = document.createElement('div');
    containerElement.classList.add('fake-drag-image');
    // Safari needs background attribute. Otherwise, the drag image has a black background.
    if (depth > 1) {
      containerElement.style.background = 'rgba(255, 255, 255, .8)';
      containerElement.style.color = 'black';
    } else {
      containerElement.style.background = 'rgba(61, 75, 91, .3)';
      containerElement.style.color = 'white';
    }
    containerElement.style.padding = '5px';
    containerElement.style.position = 'absolute';
    containerElement.style.top = '-500px';

    containerElement.style.maxWidth = '260px';
    containerElement.style.whiteSpace = 'nowrap';

    containerElement.style.display = 'flex';
    containerElement.style.flexDirection = 'column';

    for (const objective of objectives) {
      if (objective !== '') {
        const spanElement = document.createElement('span');
        spanElement.innerText = objective;

        spanElement.style.textOverflow = 'ellipsis';
        spanElement.style.overflow = 'hidden';

        containerElement.appendChild(spanElement);
      }
    }

    return containerElement;
  }

  async dropOnElement(
    event: DragEvent,
    element: ObjectiveAspectHierarchyElementComponent,
    tree: Tree<HierarchyElement>,
    parentList: Array<Tree<HierarchyElement>>,
    treeIndex: number,
    locationSequence: number[]
  ) {
    event.preventDefault();

    element.drop(event);

    if (!this.activeMove && ObjectiveAspectHierarchyComponent.globalDragData.treeSegment == null) {
      return;
    }

    const direction = this.getDropDirection(event, element);

    if (direction !== 'none') {
      if (direction === 'right') {
        locationSequence.push(0);
      } else {
        locationSequence[locationSequence.length - 1] += direction === 'up' ? 0 : 1;
      }

      if (this.activeMove) {
        if (Array.isArray(this.activeMove.from)) {
          await this.multiMoveTree(
            this.activeMove.from.map(loc => loc.locationSequence),
            locationSequence
          );
        } else {
          await this.execute(createMove(this.activeMove.from, { location: 'tree', locationSequence }));
        }
      } else {
        if (ObjectiveAspectHierarchyComponent.globalDragData.isRoot) {
          // dragged element is root
          this.dialog
            .open(ConfirmModalComponent, {
              data: {
                title: $localize`Zielhierarchie überschreiben`,
                prompt: $localize`Bist Du sicher, dass Du Deine Zielhierarchie komplett überschreiben willst?
                Du kannst diese Aktion nicht rückgängig machen!`,
                buttonConfirm: $localize`Ja, überschreiben`,
              },
            })
            .afterClosed()
            .subscribe(async isConfirmed => {
              if (!isConfirmed) return;

              this.dropGlobalElement.emit(ObjectiveAspectHierarchyComponent.globalDragData.treeSegment.value);

              // replace the left hierarchy with the right
              /* Update displayed data (tree). */
              this.tree = cloneDeep(ObjectiveAspectHierarchyComponent.globalDragData.treeSegment);
              /* Update DecisionData. */
              this.decisionData.objectiveHierarchyMainElement.name = this.tree.value.name;

              while (this.decisionData.objectives.length > 0) {
                this.decisionData.removeObjective(0);
              }

              for (let i = 0; i < this.tree.children.length; i++) {
                const copiedObjective = new Objective(this.tree.children[i].value.name);
                copiedObjective.aspects = this.getTreeFromLoc([0, i])[1];
                // copy comment
                copiedObjective.comment = this.tree.children[i].value.comment;
                // copy scale
                const correspondingScale = cloneDeep(ObjectiveAspectHierarchyComponent.globalDragData.scales[i]);
                if (correspondingScale instanceof NumericalObjectiveData) {
                  copiedObjective.numericalData = correspondingScale;
                  copiedObjective.objectiveType = ObjectiveType.Numerical;
                } else if (correspondingScale instanceof VerbalObjectiveData) {
                  copiedObjective.verbalData = correspondingScale;
                  copiedObjective.objectiveType = ObjectiveType.Verbal;
                } else if (correspondingScale instanceof IndicatorObjectiveData) {
                  copiedObjective.indicatorData = correspondingScale;
                  copiedObjective.objectiveType = ObjectiveType.Indicator;
                }
                this.decisionData.addObjective(copiedObjective);
              }
              // clear undo/redo stack
              this.actionStack = [];
              this.actionStackPosition = -1;
              /* Reset drag data AFTER confirmation. */
              this.resetGlobalDragData();
            });
        } else {
          this.dropGlobalElement.emit(ObjectiveAspectHierarchyComponent.globalDragData.treeSegment.value);

          if (ObjectiveAspectHierarchyComponent.globalDragData.treeSegment.children.length === 0) {
            // insert directly
            await this.execute(
              createInsert({ location: 'tree', locationSequence }, ObjectiveAspectHierarchyComponent.globalDragData.treeSegment)
            );
            this.resetGlobalDragData();
          } else {
            // open dialog (copy individually vs. copy with entire branch + comments + scale)
            this.popOverService.open(this.dragDialogTemplate, event.target as HTMLElement, {
              position: [{ originX: 'center', originY: 'bottom', overlayX: 'center', overlayY: 'top' }],
              closeCallback: async () => {
                // check if element is dropped as an objective (depth 1)
                const isNewLocationOfDepth1 =
                  (locationSequence.length === 2 && locationSequence[1] === 0) || direction === 'down' || direction === 'up';
                let treeSegmentClone: Tree<HierarchyElement>;
                if (!this.dragWithChildren) {
                  // clone and remove children
                  treeSegmentClone = cloneDeep(ObjectiveAspectHierarchyComponent.globalDragData.treeSegment);
                  treeSegmentClone.children = [];
                } else {
                  // copy reference
                  treeSegmentClone = ObjectiveAspectHierarchyComponent.globalDragData.treeSegment;
                }
                await this.execute(
                  createInsert(
                    { location: 'tree', locationSequence },
                    treeSegmentClone,
                    isNewLocationOfDepth1 && ObjectiveAspectHierarchyComponent.globalDragData.scales.length > 0
                      ? ObjectiveAspectHierarchyComponent.globalDragData.scales[0]
                      : null
                  )
                );
                this.dragWithChildren = false;
                this.resetGlobalDragData();
              },
            });
          }
        }
      }
    }

    this.dragEnd();
  }

  multiMoveTree(from: number[][], to: number[] | List, toIndex: number = 0) {
    from.forEach(s => {
      const component = this.findComponentFromLocationSequence(s);
      const index = this.selected.indexOf(component);

      component.inputElement.nativeElement.blur();

      if (index !== -1) {
        this.selected.splice(index, 1);
      }
    });

    if (Array.isArray(to)) {
      return this.execute(createCompound(buildMultiMove(from.slice(), to.slice())));
    } else {
      return this.execute(createCompound(buildMultiMove(from.slice(), to, this.getDeleteArray(from.slice()), toIndex)));
    }
  }

  getDeleteArray(sequences: number[][]) {
    return sequences.map(s => {
      const tree = this.getTreeFromLoc(s)[1];
      return (!tree.children || tree.children.length === 0) && tree.value.name === '';
    });
  }

  getList(origin: List) {
    return origin === 'brainstorming' ? this.listOfAspects : this.listOfDeletedAspects;
  }

  resetGlobalDragData() {
    ObjectiveAspectHierarchyComponent.globalDragData = null;
  }

  dragEnd() {
    this.activeMove = undefined;
    this.removeDragImages();
  }

  removeDragImages() {
    document.body.querySelectorAll('.fake-drag-image').forEach(e => {
      document.body.removeChild(e);
    });
  }

  async dropOnList(event: DragEvent, destination: List) {
    if (this.activeMove) {
      if (Array.isArray(this.activeMove.from)) {
        const sequences = this.activeMove.from.map(t => t.locationSequence);
        await this.multiMoveTree(sequences, destination);
      } else {
        await this.execute(createMove(this.activeMove.from, { location: destination, index: 0 }));
      }
      this.dragEnd();
    }
  }

  dragOverList(event: DragEvent, list: List) {
    if (this.activeMove) {
      if (!Array.isArray(this.activeMove.from) && this.activeMove.from.location === list) {
        return;
      }
      event.preventDefault();
      event.dataTransfer.dropEffect = 'move';
    }
  }

  async dropOnListElement(event: DragEvent, listElement: HTMLLIElement, index: number, list: List) {
    if (this.activeMove && !Array.isArray(this.activeMove.from) && this.activeMove.from.location === list) {
      event.stopPropagation();

      const y = event.offsetY;
      const height = listElement.offsetHeight;

      const offset = y > height / 2 ? 1 : 0;

      listElement.removeAttribute('drag');

      if (Array.isArray(this.activeMove.from)) {
        const sequences = this.activeMove.from.map(t => t.locationSequence);
        await this.multiMoveTree(sequences, list, index + offset);
      } else {
        await this.execute(createMove(this.activeMove.from, { location: list, index: index + offset }));
      }
      this.dragEnd();
    }
  }

  dragOverListElement(event: DragEvent, listElement: HTMLLIElement, list: List) {
    if (this.activeMove && !Array.isArray(this.activeMove.from) && this.activeMove.from.location === list) {
      const y = event.offsetY;
      const height = listElement.offsetHeight;

      listElement.setAttribute('drag', y > height / 2 ? 'down' : 'up');

      event.preventDefault();
      event.dataTransfer.dropEffect = 'move';
    }
  }

  dragLeaveListElement(event: DragEvent, listElement: HTMLLIElement, _list: List) {
    listElement.removeAttribute('drag');
  }

  dragOverElement(event: DragEvent, element: ObjectiveAspectHierarchyElementComponent) {
    if (this.editable) {
      if (ObjectiveAspectHierarchyComponent.globalDragData?.treeId !== this.treeId && this.activeMove == null) {
        /* Receive element from different tree. */
        this.acceptDraggedElement(event, element);
      } else if (this.activeMove && this.editable) {
        /* Receive element from current tree. */
        if (Array.isArray(this.activeMove.from)) {
          const targetSequence = element.locationSequence;

          const anyChild = this.activeMove.from.map(loc => loc.locationSequence).some(s => isChildOf(s, targetSequence));

          if (anyChild) {
            return;
          }
        }

        this.acceptDraggedElement(event, element);
      }
    }
  }

  acceptDraggedElement(event: DragEvent, element: ObjectiveAspectHierarchyElementComponent) {
    const dropDirection = this.getDropDirection(event, element);

    if (dropDirection === 'none') {
      return;
    } else {
      const node = element.nodeElement.nativeElement;
      node.setAttribute('drag', dropDirection);
      event.preventDefault();
      event.dataTransfer.dropEffect = 'move';
    }
  }

  canInsertBelowSelected() {
    if (this.selected.length !== 1) {
      return false;
    } else {
      return this.canInsertBelow(this.selected[0].locationSequence);
    }
  }

  canInsertBelow(loc: TreeLoc | number[]) {
    const sequence = (Array.isArray(loc) ? loc : loc.locationSequence).slice();
    sequence.push(sequence.pop() + 1);

    return this.canCreate(sequence);
  }

  canInsertAboveSelected() {
    if (this.selected.length !== 1) {
      return false;
    } else {
      return this.canInsertAbove(this.selected[0].locationSequence);
    }
  }

  canInsertAbove(loc: TreeLoc | number[]) {
    const sequence = (Array.isArray(loc) ? loc : loc.locationSequence).slice();

    return this.canCreate(sequence);
  }

  canInsertRight(loc: TreeLoc | number[]) {
    const sequence = (Array.isArray(loc) ? loc : loc.locationSequence).slice();

    sequence.push(0);

    return this.canCreate(sequence);
  }

  canInsertRightSelected() {
    if (this.selected.length !== 1) {
      return false;
    } else {
      return this.canInsertRight(this.selected[0].locationSequence);
    }
  }

  canInsertRightFromAddButtons(element: ObjectiveAspectHierarchyElementComponent) {
    if (element.locationSequence.length === 1) {
      return this.canInsertRight(element.locationSequence);
    } else {
      const hasChildren = element.tree.children.length > 0;

      if (hasChildren) {
        return this.canGroup(this.getLocsOfChildren(element));
      } else {
        return this.canInsertRight(element.locationSequence);
      }
    }
  }

  canDelete(loc: TreeLoc | number[]) {
    const sequence = Array.isArray(loc) ? loc : loc.locationSequence;

    if (sequence.length === 1) {
      return false; // Can't delete root
    }

    return true;
  }

  canDeleteSelected() {
    if (this.selected.length === 0) {
      return false;
    }

    for (const selectedElement of this.selected) {
      if (!this.canDelete(selectedElement.locationSequence)) {
        return false;
      }
    }

    return true;
  }

  canGroupSelected() {
    return this.canGroup(this.selected.map(elements => elements.locationSequence));
  }

  canGroup(locationSequences: number[][]) {
    if (locationSequences.length === 0) {
      return false;
    }

    const firstLocationSequence = locationSequences[0];

    return !locationSequences.some(secondLocationSequence => {
      return (
        firstLocationSequence.length === 1 ||
        !this.areSiblings(
          { location: 'tree', locationSequence: firstLocationSequence },
          { location: 'tree', locationSequence: secondLocationSequence }
        )
      );
    });
  }

  canCreate(loc: TreeLoc | number[]) {
    const sequence = Array.isArray(loc) ? loc : loc.locationSequence;

    if (sequence.length === 1) {
      return false; // Can't create additional elements on root level
    }

    return true;
  }

  canClearFormatting() {
    return this.selected.length > 0;
  }

  clearFormatting() {
    if (this.canClearFormatting()) {
      const actions: ChangeColor[] = [];

      this.selected.forEach(s => {
        for (const target of ['text', 'background']) {
          actions.push(
            createColor(
              { location: 'tree', locationSequence: s.locationSequence.slice() },
              target as ColorTarget,
              target === 'text' ? s.tree.value.textColor : s.tree.value.backgroundColor,
              undefined
            )
          );
        }
      });

      this.execute(createCompound(actions));
    }
  }

  getDropDirection(event: DragEvent, element: ObjectiveAspectHierarchyElementComponent): 'up' | 'down' | 'right' | 'none' {
    const target = event.target as HTMLElement;

    const x = event.offsetX;
    const y = event.offsetY;
    const height = target.offsetHeight;
    const width = target.offsetWidth;

    const distanceCenterMod = Math.max(0, 0.15 - (0.15 / 0.5) * Math.abs(y / height - 0.5));

    if (target.classList.contains('additional-drop-zone') || x / width > 0.8 - distanceCenterMod || element.depth === 0) {
      return 'right';
    } else {
      return y > height / 2 ? 'down' : 'up';
    }
  }

  // Interface
  selectedColor(target: ColorTarget) {
    if (this.selected.length !== 0) {
      const getColor =
        target === 'text'
          ? (c: ObjectiveAspectHierarchyElementComponent) => c.displayTextColor
          : (c: ObjectiveAspectHierarchyElementComponent) => c.displayBackgroundColor;
      const first = getColor(this.selected[0]);

      if (!this.selected.some(e => getColor(e) !== first) && first !== null && first !== undefined) {
        return first;
      }
    }
    return '#FFFFFF';
  }

  getPresetColors(colorTarget: ColorTarget) {
    const preset = DEFAULT_COLOR_PRESET.slice();

    const toCheck = [this.tree];

    const treeColors = ['#D3D3D3', '#5D666F'];

    const colorGetter =
      colorTarget === 'background' ? (value: ObjectiveElement) => value.backgroundColor : (value: ObjectiveElement) => value.textColor;

    while (toCheck.length > 0) {
      const next = toCheck.pop();

      const color = colorGetter(next.value);

      if (color && treeColors.indexOf(color.toUpperCase()) < 0) {
        treeColors.push(color.toUpperCase());
      }

      if (next.children !== undefined) {
        toCheck.push(...next.children);
      }
    }

    const finalPreset = treeColors;

    while (finalPreset.length < 16 && preset.length > 0) {
      const presetColor = preset.shift();

      if (finalPreset.indexOf(presetColor) < 0) {
        finalPreset.push(presetColor);
      }
    }

    return finalPreset;
  }

  navigate(direction: 'up' | 'down' | 'left' | 'right') {
    if (this.selected.length === 1) {
      const selectedElement = this.selected[0];
      const selectedTree = selectedElement.tree;
      const parentTree = selectedElement.parentTree;

      selectedElement.inputElement.nativeElement.blur();

      const sequence = selectedElement.locationSequence;
      const parentSequence = sequence.slice();
      parentSequence.pop();

      let newTree: Tree<HierarchyElement>;

      switch (direction) {
        case 'left':
          if (parentTree) {
            const index = parentTree.children.indexOf(selectedTree);
            newTree = parentTree;
            this.navigationBias.set(parentSequence, index);
          }
          break;
        case 'right':
          if (selectedTree.children && selectedTree.children.length > 0 && !selectedTree.value.collapsed) {
            const bias = this.navigationBias.get(sequence);
            let childIndex = 0;
            if (bias !== undefined) {
              if (bias < selectedTree.children.length) {
                childIndex = bias;
              }
              this.navigationBias.clear(sequence);
            }
            newTree = selectedTree.children[childIndex];
          }
          break;
        case 'up':
          if (parentTree) {
            const index = parentTree.children.indexOf(selectedTree);
            if (index > 0) {
              newTree = parentTree.children[index - 1];
            } else {
              newTree = parentTree;
            }
          }
          break;
        case 'down':
          if (parentTree) {
            const index = parentTree.children.indexOf(selectedTree);
            if (index < parentTree.children.length - 1) {
              newTree = parentTree.children[index + 1];
            } else {
              newTree = parentTree;
            }
          }
          break;
      }

      if (newTree) {
        const newElement = this.findComponentFromTree(newTree);
        this.makeVisible(newElement);
        this.selected.pop();
        this.selected.push(newElement);
      }

      event.preventDefault();
    }
  }

  findComponentFromTree(tree: Tree<HierarchyElement>) {
    return this.elementComponents.find(e => e.tree === tree);
  }

  findComponentFromLocationSequence(sequence: number[]) {
    return this.elementComponents.find(e => areLocationSequencesEqual(e.locationSequence, sequence));
  }

  sortSelected() {
    this.selected = this.selected.sort((a, b) => {
      let i = 0;
      while (i < a.locationSequence.length && i < b.locationSequence.length) {
        const vA = a.locationSequence[i];
        const vB = b.locationSequence[i];

        if (vA !== vB) {
          return vA - vB;
        }

        i++;
      }

      return a.locationSequence.length - b.locationSequence.length;
    });
  }

  // Removal Confirmation
  requestDeletionConfirmation(objectiveIndex: number): Promise<boolean> {
    const attachedData = this.decisionData.getAttachedObjectiveData(objectiveIndex);
    if (this.alwaysConfirmDeletion || attachedData.length === 0) {
      return Promise.resolve(true);
    }

    const result$ = this.dialog
      .open(DeleteObjectiveModalComponent, {
        data: objectiveIndex,
      })
      .afterClosed()
      .pipe(map(value => value ?? false));

    return firstValueFrom(result$);
  }

  closeWhenBothWindowsOpen(whichToCloseWhenBothOpen: 'brainstorming' | 'trashBin' = 'trashBin') {
    if (this.horizontalWindows && this.brainstormingOpen && this.trashBinOpen) {
      if (whichToCloseWhenBothOpen === 'brainstorming') {
        this.brainstormingOpen = false;
      } else {
        this.trashBinOpen = false;
      }
    }
  }

  async test(maxOperations = 200) {
    const startTree = cloneDeep(this.tree);

    this.alwaysConfirmDeletion = true;

    console.profile('Objective Aspect Hierarchy');
    let operations = 0;
    while (operations < maxOperations && this.testObjectiveSync() === 0) {
      const elementArray = this.elementComponents.toArray();

      const action = random(11);

      if (action < 5 && elementArray.length > 1) {
        const from = elementArray[random(this.elementComponents.length - 1)].locationSequence.slice();
        const to = elementArray[random(this.elementComponents.length - 1)].locationSequence.slice();

        if (Math.random() < 0.3) {
          to.push(0);
        } else if (Math.random() < 0.5) {
          to.push(to.pop() + 1);
        }

        if (from.length > 1 && to.length > 1 && !isChildOf(from, to) && !areLocationSequencesEqual(from, to)) {
          console.log(`Moving ${from} to ${to}`);
          operations++;

          const treeBefore = cloneDeep(this.tree);

          await this.multiMoveTree([from.slice()], to.slice());
          this.cdRef.detectChanges();

          const treeAfter = cloneDeep(this.tree);

          await this.undoLast();
          this.cdRef.detectChanges();

          const treeAfterUndo = cloneDeep(this.tree);

          if (!this.areTreesEqual(treeBefore, treeAfterUndo)) {
            console.log('Undo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          await this.redo();

          if (!this.areTreesEqual(treeAfter, this.tree)) {
            console.log('Redo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          this.cdRef.detectChanges();
        }
      } else if (action === 5 && elementArray.length > 1) {
        // Group
        const grouping = elementArray.filter(e => e.depth > 0)[random(elementArray.length - 2)];
        let allGroup = [grouping];

        if (grouping.parentTree.children.length > 1) {
          allGroup = shuffle(grouping.parentTree.children.slice())
            .slice(0, random(1, grouping.parentTree.children.length))
            .map(tree => this.findComponentFromTree(tree));
        }

        operations++;
        console.log(`Grouping ${allGroup.length} elements`);

        const treeBefore = cloneDeep(this.tree);

        await this.execute(createCompound(buildGroup(allGroup.map(s => s.locationSequence.slice()))));
        this.cdRef.detectChanges();

        const treeAfter = cloneDeep(this.tree);

        await this.undoLast();
        this.cdRef.detectChanges();

        const treeAfterUndo = cloneDeep(this.tree);

        if (!this.areTreesEqual(treeBefore, treeAfterUndo)) {
          console.log('Undo failed');

          this.tree = treeBefore;
          this.cdRef.detectChanges();
          break;
        }

        await this.redo();

        if (!this.areTreesEqual(treeAfter, this.tree)) {
          console.log('Redo failed');

          this.tree = treeBefore;
          this.cdRef.detectChanges();
          break;
        }

        this.cdRef.detectChanges();
      } else if (action === 6 && elementArray.length > 1) {
        // Multi Move
        const toMovecount = Math.min(elementArray.length - 1, random(1, 6));

        const to = elementArray[random(this.elementComponents.length - 1)].locationSequence.slice();

        if (Math.random() < 0.3) {
          to.push(0);
        } else if (Math.random() < 0.5) {
          to.push(to.pop() + 1);
        }

        const toMove = elementArray
          .slice()
          .sort(() => Math.random() - Math.random())
          .filter(e => e.depth > 0 && !areLocationSequencesEqual(e.locationSequence, to) && !isChildOf(e.locationSequence, to))
          .slice(0, toMovecount);

        if (toMove.length > 0 && to.length > 1) {
          operations++;
          console.log(`Multi Moving ${toMove.length} elements to ${to}`);
          console.log(toMove);
          const treeBefore = cloneDeep(this.tree);

          await this.multiMoveTree(
            toMove.slice().map(e => e.locationSequence.slice()),
            to.slice()
          );
          this.cdRef.detectChanges();

          const treeAfter = cloneDeep(this.tree);

          await this.undoLast();
          this.cdRef.detectChanges();

          const treeAfterUndo = cloneDeep(this.tree);

          if (!this.areTreesEqual(treeBefore, treeAfterUndo)) {
            console.log('Undo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          await this.redo();

          if (!this.areTreesEqual(treeAfter, this.tree)) {
            console.log('Redo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }
          this.cdRef.detectChanges();
        }
      } else if (action === 7) {
        const target = elementArray[random(elementArray.length - 1)].locationSequence.slice();
        target.push(0);

        operations++;
        console.log('Inserting new Element');
        const treeBefore = cloneDeep(this.tree);

        const value = Math.random().toString(36).substring(7);
        await this.execute({
          type: 'insert',
          location: { location: 'tree', locationSequence: target.slice() },
          value: new ObjectiveElement(value),
        });
        this.cdRef.detectChanges();

        const treeAfter = cloneDeep(this.tree);

        await this.undoLast();
        this.cdRef.detectChanges();

        const treeAfterUndo = cloneDeep(this.tree);

        if (!this.areTreesEqual(treeBefore, treeAfterUndo)) {
          console.log('Undo failed');

          this.tree = treeBefore;
          this.cdRef.detectChanges();
          break;
        }

        await this.redo();

        if (!this.areTreesEqual(treeAfter, this.tree)) {
          console.log('Redo failed');

          this.tree = treeBefore;
          this.cdRef.detectChanges();
          break;
        }
        this.cdRef.detectChanges();
      } else if (action === 8 && elementArray.length > 1) {
        const toMoveCount = Math.min(elementArray.length - 1, random(1, 3));

        const toMove = elementArray
          .slice()
          .sort(() => Math.random() - Math.random())
          .filter(e => e.depth > 0)
          .slice(0, toMoveCount);

        const toList: List = 'trash';

        if (toMove.length > 1) {
          operations++;
          console.log(`Moving ${toMoveCount} elements to ${toList}`);
          toMove.forEach(e => {
            console.log(' - ' + e.locationSequence.toString());
          });

          const treeBefore = cloneDeep(this.tree);

          const sequences = toMove.map(e => e.locationSequence.slice());

          const newSequences = () => sequences.map(s => s.slice());

          await this.multiMoveTree(newSequences(), toList);
          this.cdRef.detectChanges();

          const treeAfter = cloneDeep(this.tree);

          await this.undoLast();
          this.cdRef.detectChanges();

          const treeAfterUndo = cloneDeep(this.tree);

          if (!this.areTreesEqual(treeBefore, treeAfterUndo)) {
            console.log('Undo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          await this.redo();

          if (!this.areTreesEqual(treeAfter, this.tree)) {
            console.log('Redo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }
          this.cdRef.detectChanges();
        }
      } else if (action === 9 && elementArray.length > 1) {
        const target = elementArray.filter(e => e.depth > 0)[random(elementArray.length - 2)].locationSequence.slice();

        operations++;
        console.log('Renaming Element');
        const treeBefore = cloneDeep(this.tree);

        const after = Math.random().toString(36).substring(7);

        const before = this.getTreeFromLoc(target.slice())[1].value.name;

        await this.execute({
          type: 'rename',
          location: { location: 'tree', locationSequence: target.slice() },
          before,
          after,
        });
        this.cdRef.detectChanges();

        const treeAfter = cloneDeep(this.tree);

        await this.undoLast();
        this.cdRef.detectChanges();

        const treeAfterUndo = cloneDeep(this.tree);

        if (!this.areTreesEqual(treeBefore, treeAfterUndo)) {
          console.log('Undo failed');

          this.tree = treeBefore;
          this.cdRef.detectChanges();
          break;
        }

        await this.redo();

        if (!this.areTreesEqual(treeAfter, this.tree)) {
          console.log('Redo failed');

          this.tree = treeBefore;
          this.cdRef.detectChanges();
          break;
        }
        this.cdRef.detectChanges();
      } else if (action === 10 && elementArray.length > 1) {
        // Color
        const target = elementArray.filter(e => e.depth > 0)[random(elementArray.length - 2)].locationSequence.slice();

        operations++;
        const treeBefore = cloneDeep(this.tree);

        const colorTarget = Math.random() < 0.5 ? 'text' : 'background';

        const after = getRandomColor();

        const before =
          colorTarget === 'text'
            ? this.getTreeFromLoc(target.slice())[1].value.textColor
            : this.getTreeFromLoc(target.slice())[1].value.backgroundColor;

        await this.execute({
          type: 'color',
          target: colorTarget,
          location: { location: 'tree', locationSequence: target.slice() },
          before,
          after,
        });
        this.cdRef.detectChanges();

        const treeAfter = cloneDeep(this.tree);

        await this.undoLast();
        this.cdRef.detectChanges();

        const treeAfterUndo = cloneDeep(this.tree);

        if (!this.areTreesEqual(treeBefore, treeAfterUndo)) {
          console.log('Undo failed');

          this.tree = treeBefore;
          this.cdRef.detectChanges();
          break;
        }

        await this.redo();

        if (!this.areTreesEqual(treeAfter, this.tree)) {
          console.log('Redo failed');

          this.tree = treeBefore;
          this.cdRef.detectChanges();
          break;
        }
        this.cdRef.detectChanges();
      } else if (action > 9) {
        // Move Trash to Tree
        const to = elementArray[random(this.elementComponents.length - 1)].locationSequence.slice();

        if (to.length === 1) {
          to.push(0);
        }

        const array = this.getList('trash');

        const takeIndex = random(array.length - 1);

        if (array.length > 0 && to.length > 1) {
          operations++;
          console.log(`Deleting ${takeIndex} from Trash`);
          const treeBefore = cloneDeep(this.tree);

          await this.execute(createDeletePermanent(takeIndex));
          this.cdRef.detectChanges();

          const treeAfterSingleDeletion = cloneDeep(this.tree);

          await this.undoLast();
          this.cdRef.detectChanges();

          const treeAfterSingleDeletionUndo = cloneDeep(this.tree);

          if (!this.areTreesEqual(treeBefore, treeAfterSingleDeletionUndo)) {
            console.log('Undo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          await this.redo();

          if (!this.areTreesEqual(treeAfterSingleDeletion, this.tree)) {
            console.log('Redo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          await this.undoLast();

          operations++;
          console.log(`Deleting all aspects from Trash`);

          await this.execute({
            type: 'deleteAllPermanent',
            numberOfDeletedElements: array.length,
          });
          this.cdRef.detectChanges();

          const treeAfterAllDeletion = cloneDeep(this.tree);

          await this.undoLast();
          this.cdRef.detectChanges();

          const treeAfterAllDeletionUndo = cloneDeep(this.tree);

          if (!this.areTreesEqual(treeBefore, treeAfterAllDeletionUndo)) {
            console.log('Undo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          await this.redo();

          if (!this.areTreesEqual(treeAfterAllDeletion, this.tree)) {
            console.log('Redo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          await this.undoLast();
          this.cdRef.detectChanges();

          operations++;
          console.log(`Moving ${takeIndex} (Trash) to ${to}`);

          await this.executeMove({ location: 'trash', index: takeIndex }, { location: 'tree', locationSequence: to.slice() });
          this.cdRef.detectChanges();

          const treeAfter = cloneDeep(this.tree);

          await this.undoLast();
          this.cdRef.detectChanges();

          const treeAfterUndo = cloneDeep(this.tree);

          if (!this.areTreesEqual(treeBefore, treeAfterUndo)) {
            console.log('Undo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }

          await this.redo();

          if (!this.areTreesEqual(treeAfter, this.tree)) {
            console.log('Redo failed');

            this.tree = treeBefore;
            this.cdRef.detectChanges();
            break;
          }
          this.cdRef.detectChanges();
        }
      }
    }

    if (operations === maxOperations) {
      console.log('Execute-Undo-Redo Test succeeded');

      const treeAfterAll = cloneDeep(this.tree);

      console.log('Batch Undo...');
      let count = this.actionStack.length;
      console.log(' - Undoing ' + count);
      // Undo Everything
      for (let i = 0; i < count; i++) {
        await this.undoLast();
      }

      this.cdRef.detectChanges();

      if (!this.areTreesEqual(startTree, this.tree)) {
        console.log('Batch Undo failed');
      } else {
        console.log('Batch Undo succeeded');

        console.log('Batch Redo...');
        count = this.actionStack.length;
        console.log(' - Redoing ' + count);
        // Undo Everything
        for (let i = 0; i < count; i++) {
          await this.redo();
        }

        if (!this.areTreesEqual(treeAfterAll, this.tree)) {
          console.log('Batch Redo failed');
        } else {
          console.log('Batch Redo succeeded');

          console.log('Batch Undo [2]...');
          count = this.actionStack.length;
          console.log(' - Undoing ' + count);
          // Undo Everything
          for (let i = 0; i < count; i++) {
            await this.undoLast();
          }

          this.cdRef.detectChanges();

          if (!this.areTreesEqual(startTree, this.tree)) {
            console.log('Final Batch Undo failed');
          } else {
            console.log('Final Batch Undo succeeded, Test SUCCESS');
          }

          this.cdRef.detectChanges();
        }
      }
    } else {
      console.log('Execute-Undo Test failed');
    }

    console.profileEnd('Objective Aspect Hierarchy');

    this.alwaysConfirmDeletion = false;
  }

  areTreesEqual(tree1: Tree<HierarchyElement>, tree2: Tree<HierarchyElement>): boolean {
    if (!this.areObjectiveElementsEqual(tree1.value, tree2.value)) {
      this.debug(`[areTreesEqual] Differing Elements ${tree1.value.name} | ${tree2.value.name}`);
      return false;
    }
    if (tree1.children.length !== tree2.children.length) {
      this.debug(`[areTreesEqual] Differing children ${tree1.children.length} | ${tree2.children.length}`);
      return false;
    }

    return !tree1.children.some((c, i) => {
      return !this.areTreesEqual(c, tree2.children[i]);
    });
  }

  areObjectiveElementsEqual(o1: ObjectiveElement, o2: ObjectiveElement) {
    return o1.name === o2.name && o1.backgroundColor === o2.backgroundColor && o1.textColor === o2.textColor;
  }

  deletePermanent(index: number) {
    const action: DeletePermanent = createDeletePermanent(index);
    this.execute(action);
  }

  deleteAllPermanent() {
    const action: DeleteAllPermanent = createDeleteAllPermanent(this.decisionData.objectiveAspects.listOfDeletedAspects.length);
    this.execute(action);
  }

  shouldHideTrashButton(trashIndex: number) {
    return (
      this.activeMove &&
      !Array.isArray(this.activeMove.from) &&
      this.activeMove.from.location === 'trash' &&
      this.activeMove.from.index === trashIndex
    );
  }

  get canTransferSelection() {
    return this.selected.length === 1 && this.selected[0].depth === 1;
  }

  transferSelection() {
    if (!this.canTransferSelection) {
      return;
    }

    const nodeElement = this.selected[0].nodeElement;
    const objectiveIndex = this.selected[0].locationSequence[1];
    const objective = this.decisionData.objectives[objectiveIndex];

    this.teamTrait
      .transferObject(objective.uuid)
      .pipe(loadingIndicator(this.snackBar, $localize`Ziel wird übernommen…`))
      .subscribe({
        next: () => this.popOverService.whistle(nodeElement, $localize`Ziel übernommen!`, 'check'),
        error: () => this.popOverService.whistle(nodeElement, $localize`Beim Übernehmen des Ziels ist ein Fehler aufgetreten.`, 'error'),
      });
  }
}

export function areLocationSequencesEqual(sequence1: number[], sequence2: number[]) {
  if (sequence1.length !== sequence2.length) {
    return false;
  }
  for (let i = 0; i < sequence1.length; i++) {
    if (sequence1[i] !== sequence2[i]) {
      return false;
    }
  }

  return true;
}

export function isChildOf(potentialParent: number[], potentialChild: number[]) {
  for (let i = 0; i < potentialParent.length; i++) {
    if (i < potentialChild.length) {
      if (potentialParent[i] !== potentialChild[i]) {
        return false;
      }
    } else {
      return false;
    }
  }

  return true;
}

function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
