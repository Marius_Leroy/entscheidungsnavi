import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  CollapsibleTinyComponent,
  KeyBindHandlerDirective,
  NoteBtnComponent,
  NoteBtnPresetPipe,
  PinchZoomDirective,
  RichTextEmptyPipe,
  WidgetsModule,
  WidthTriggerDirective,
} from '@entscheidungsnavi/widgets';
import { SharedModule } from '../../shared/shared.module';
import { ObjectiveAspectHierarchyShortcutsComponent } from '../help';
import { ObjectiveAspectHierarchyComponent } from './objective-aspect-hierarchy.component';
import { ObjectiveAspectHierarchyElementComponent } from './element/objective-aspect-hierarchy-element.component';

@NgModule({
  imports: [
    CommonModule,
    CollapsibleTinyComponent,
    SharedModule,
    WidgetsModule,
    NoteBtnPresetPipe,
    RichTextEmptyPipe,
    NoteBtnComponent,
    WidthTriggerDirective,
    PinchZoomDirective,
    KeyBindHandlerDirective,
  ],
  declarations: [ObjectiveAspectHierarchyComponent, ObjectiveAspectHierarchyElementComponent, ObjectiveAspectHierarchyShortcutsComponent],
  exports: [ObjectiveAspectHierarchyComponent, ObjectiveAspectHierarchyShortcutsComponent],
})
export class ObjectiveAspectHierarchyModule {}
