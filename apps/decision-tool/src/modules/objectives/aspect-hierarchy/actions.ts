import { ObjectiveElement } from '@entscheidungsnavi/decision-data/steps';
import { IndicatorObjectiveData, NumericalObjectiveData, VerbalObjectiveData } from '@entscheidungsnavi/decision-data/classes';
import { Tree } from '@entscheidungsnavi/tools';
import { isChildOf, List, ObjectiveLocation, TreeLoc } from './objective-aspect-hierarchy.component';
import { HierarchyElement } from './hierarchy-element';

export interface ChangeColor {
  type: 'color';
  location: TreeLoc;
  target: 'text' | 'background';
  before: string;
  after: string;
}
export interface Insert {
  type: 'insert';
  location: TreeLoc;
  value: Tree<HierarchyElement> | ObjectiveElement;
  scale?: NumericalObjectiveData | VerbalObjectiveData | IndicatorObjectiveData;
}
export interface Move {
  type: 'move';
  from: ObjectiveLocation;
  to: ObjectiveLocation;
  reconstruct?: Tree<HierarchyElement>;
}
export interface Rename {
  type: 'rename';
  location: TreeLoc;
  before: string;
  after: string;
}
export interface DeleteEmpty {
  type: 'deleteEmpty';
  location: TreeLoc;
}
export interface DeletePermanent {
  type: 'deletePermanent';
  index: number;
}
export interface DeleteAllPermanent {
  type: 'deleteAllPermanent';
  numberOfDeletedElements: number;
}
export interface CompoundAction {
  type: 'compound';
  actions: Action[];
}
export type Action = Move | Rename | Insert | DeleteEmpty | DeletePermanent | DeleteAllPermanent | ChangeColor | CompoundAction;

export function createInsert(
  location: TreeLoc,
  value: Tree<HierarchyElement> | ObjectiveElement,
  scale?: NumericalObjectiveData | VerbalObjectiveData | IndicatorObjectiveData
): Insert {
  return { type: 'insert', location, value, scale };
}

export function createMove(from: ObjectiveLocation, to: ObjectiveLocation, reconstruct?: Tree<HierarchyElement>): Move {
  return { type: 'move', from, to, reconstruct };
}

export function createRename(location: TreeLoc, before: string, after: string): Rename {
  return { type: 'rename', location, before, after };
}

export function createDeleteEmpty(location: TreeLoc): DeleteEmpty {
  return { type: 'deleteEmpty', location };
}

export function createDeletePermanent(index: number): DeletePermanent {
  return { type: 'deletePermanent', index };
}

export function createDeleteAllPermanent(numberOfDeletedElements: number): DeleteAllPermanent {
  return { type: 'deleteAllPermanent', numberOfDeletedElements };
}

export function createColor(location: TreeLoc, target: 'text' | 'background', before: string, after: string): ChangeColor {
  return { type: 'color', location, target, before, after };
}

export function createCompound(actions: Action[]): CompoundAction {
  return { type: 'compound', actions };
}

export function buildMultiMove(from: number[][], to: number[] | List, deleteArray: boolean[] = [], toIndex = 0) {
  const copy = from.slice();

  if (deleteArray !== undefined) {
    deleteArray = deleteArray.filter((_1, index) => !copy.some(other => other !== from[index] && isChildOf(other, from[index])));
  }

  const finalToMove = from.filter(ls => !copy.some(other => other !== ls && isChildOf(other, ls)));
  const actions: Array<Move | DeleteEmpty> = [];

  let offset = 0;

  for (let i = 0; i < finalToMove.length; i++) {
    const sequence = finalToMove[i];

    if (Array.isArray(to)) {
      const finalTo = to.slice();
      finalTo.push(finalTo.pop() + offset++);

      actions.push({
        type: 'move',
        from: { location: 'tree', locationSequence: sequence },
        to: { location: 'tree', locationSequence: finalTo },
      });

      // Fix To
      for (let lI = 0; lI < sequence.length; lI++) {
        if (lI === sequence.length - 1) {
          const otherValue = to[lI];
          if (sequence[lI] < otherValue) {
            to[lI]--;
          }
        } else if (sequence[lI] !== to[lI] || lI >= to.length) {
          break;
        }
      }
    } else if (deleteArray[i]) {
      actions.push({
        type: 'deleteEmpty',
        location: { location: 'tree', locationSequence: sequence },
      });
    } else {
      actions.push({
        type: 'move',
        from: { location: 'tree', locationSequence: sequence },
        to: { location: to, index: toIndex + offset++ },
      });
    }

    for (let a = i + 1; a < finalToMove.length; a++) {
      const otherSequence = finalToMove[a];

      // Fix From Because missing
      for (let lI = 0; lI < sequence.length; lI++) {
        if (lI === sequence.length - 1) {
          const otherValue = otherSequence[lI];
          if (sequence[lI] < otherValue) {
            otherSequence[lI]--;
          }
        } else if (sequence[lI] !== otherSequence[lI] || lI >= otherSequence.length) {
          break;
        }
      }

      // Fix From Because Addition
      if (Array.isArray(to)) {
        for (let lI = 0; lI < to.length; lI++) {
          if (lI === to.length - 1) {
            const otherValue = otherSequence[lI];
            if (to[lI] <= otherValue) {
              otherSequence[lI]++;
            }
          } else if (to[lI] !== otherSequence[lI] || lI >= otherSequence.length) {
            break;
          }
        }
      }
    }
  }

  return actions;
}

export function buildGroup(elements: number[][]) {
  const sorted = elements.slice().sort((a, b) => {
    return a[a.length - 1] - b[b.length - 1];
  });

  const actions: Action[] = [];

  const to = sorted[0].slice();
  to.push(0);

  actions.push(createInsert({ location: 'tree', locationSequence: sorted[0].slice() }, new ObjectiveElement('')));

  const moveActions = buildMultiMove(
    sorted.map(e => {
      const sequence = e.slice();
      sequence.push(sequence.pop() + 1);

      return sequence;
    }),
    to
  );

  actions.push(...moveActions);

  return actions;
}
