import { Component } from '@angular/core';

@Component({
  templateUrl: 'explanation.component.html',
  styleUrls: ['explanation.component.scss', '../../hints.scss'],
})
export class ZieleExplanationComponent {}
