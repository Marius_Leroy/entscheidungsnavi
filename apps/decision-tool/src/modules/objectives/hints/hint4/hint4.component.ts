import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { QuickstartService } from '@entscheidungsnavi/api-client';
import { OnDestroyObservable, Tree } from '@entscheidungsnavi/tools';
import { map, switchMap, takeUntil, tap, Observable, Subject, catchError, EMPTY } from 'rxjs';
import { LocalizedString, QuickstartHierarchy, QuickstartHierarchyList } from '@entscheidungsnavi/api-types';
import { Tag, TemplateHierarchyElement, TemplateHierarchyService } from '@entscheidungsnavi/widgets';
import { ObjectiveElement } from '@entscheidungsnavi/decision-data/steps';
import { AbstractObjectiveHintComponent } from '../objective-hint.component';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { ObjectiveAspectHierarchyComponent } from '../../aspect-hierarchy';
import { Help1Component } from './help1/help1.component';

@Component({
  selector: 'dt-objective-hint-4',
  templateUrl: './hint4.component.html',
  styleUrls: ['./hint4.component.scss'],
})
@DisplayAtMaxWidth
export class ZieleHint4Component extends AbstractObjectiveHintComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @ViewChild('rightSideHierarchy') rightSideHierarchy: ObjectiveAspectHierarchyComponent;

  @Input()
  showTitle = true;

  inSelectionMode = true;
  smallScreen = false;

  tags: Tag[];

  hierarchies: QuickstartHierarchyList;

  selectedHierarchy: {
    id: string;
    name: LocalizedString;
    tree: Tree<ObjectiveElement>;
  };
  hoveredHierarchy: {
    id: string;
    name: LocalizedString;
    tree: Tree<ObjectiveElement>;
  };

  get rightHierarchy() {
    return this.hoveredHierarchy ?? this.selectedHierarchy;
  }

  nameFilter = '';
  tagIdFilter: string[] = [];

  isLoading = false;
  hasError = false;

  private readonly updateList$ = new Subject<'load-more' | 'filter-change'>();

  readonly helpMenu = {
    educational: [
      helpPage()
        .name($localize`So funktioniert's`)
        .component(Help1Component)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 2`)
        .component(HelpBackgroundComponent)
        .build(),
    ],
  };

  constructor(
    injector: Injector,
    private quickstartService: QuickstartService,
    private templateHierarchyService: TemplateHierarchyService
  ) {
    super(injector);
    this.pageKey = 4;

    this.quickstartService.getTags().subscribe({
      next: tags => {
        this.tags = tags;

        if (this.hierarchies) {
          this.updateActiveTags();
        }
      },
      error: () => (this.hasError = true),
    });

    this.updateList$
      .pipe(
        tap(() => (this.isLoading = true)),
        switchMap(command => {
          const offset = command === 'load-more' ? this.hierarchies.items.length : 0;
          return this.quickstartService
            .getHierarchies({ nameQuery: this.nameFilter, tags: this.tagIdFilter }, { limit: 50, offset })
            .pipe(map(list => [command, list] as const));
        }),

        takeUntil(this.onDestroy$)
      )
      .subscribe({
        next: ([command, list]) => {
          if (command === 'load-more') {
            this.hierarchies.items.push(...list.items);
            this.hierarchies.count = list.count;
          } else {
            this.hierarchies = list;

            if (this.tags) {
              this.updateActiveTags();
            }
          }
          this.isLoading = false;
        },
        error: () => (this.hasError = true),
      });
    this.onFilterChange();
  }

  onFilterChange() {
    this.updateList$.next('filter-change');
  }

  loadMore() {
    this.updateList$.next('load-more');
  }

  hoverHierarchy(hierarchy: QuickstartHierarchy) {
    if (!this.inSelectionMode) return;

    this.hoveredHierarchy = {
      id: hierarchy.id,
      name: hierarchy.name,
      tree: this.templateHierarchyService.extractHierarchy(hierarchy.tree),
    };
  }

  selectHierarchy() {
    if (!this.inSelectionMode) return;

    this.selectedHierarchy = this.hoveredHierarchy;
    this.inSelectionMode = false;
  }

  trackElementDrop(element: ObjectiveElement) {
    if (element instanceof TemplateHierarchyElement) {
      this.quickstartService
        .trackObjective([element.id])
        .pipe(catchError(() => EMPTY))
        .subscribe();
    }
  }

  exitFullScreen() {
    if (!this.rightSideHierarchy.isInFullscreen) return;
    this.rightSideHierarchy.triggerFullScreen();
  }

  updateActiveTags() {
    this.tags.forEach(tag => (tag.isProductive = this.hierarchies.activeTags.includes(tag.id)));
    // Notify the input of the changes
    this.tags = [...this.tags];
  }
}
