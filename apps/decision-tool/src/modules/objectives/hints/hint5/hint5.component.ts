import { Component, Injector } from '@angular/core';
import { HelpService } from '../../../../app/help/help.service';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractObjectiveHintComponent } from '../objective-hint.component';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  templateUrl: './hint5.component.html',
  styleUrls: ['../hint3-5.component.scss'],
})
@DisplayAtMaxWidth
export class ZieleHint5Component extends AbstractObjectiveHintComponent {
  readonly displayAtMaxWidth = true;

  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Weitere Hinweise`)
          .component(Help2Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 2`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }

  constructor(injector: Injector, public helpService: HelpService) {
    super(injector);
    this.pageKey = 5;
  }
}
