import { Directive, Injector, OnInit } from '@angular/core';
import { NaviSubStep, ObjectiveElement, OBJECTIVES_STEPS } from '@entscheidungsnavi/decision-data/steps';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Tree } from '@entscheidungsnavi/tools';
import { CurrentProgressService } from '../../../app/data/current-progress.service';
import { ContextualHelpMenu, HelpMenu, HelpMenuProvider } from '../../../app/help/help';
import { HierarchyElement } from '../aspect-hierarchy/hierarchy-element';
import { NavLine, navLineElement, Navigation } from '../../shared/navline';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractObjectiveHintComponent implements OnInit, Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [
      navLineElement()
        .back(() => (this.pageKey === 1 ? '/decisionstatement' : `/objectives/steps/${this.pageKey - 1}`))
        .build(),
    ],
    right: [
      navLineElement()
        .continue(() => (this.pageKey === 5 ? '/objectives' : `/objectives/steps/${this.pageKey + 1}`))
        .build(),
    ],
  });

  abstract helpMenu: ContextualHelpMenu | HelpMenu;
  pageKey: number; // 1-based
  decisionData: DecisionData;

  protected currentProgressService: CurrentProgressService;

  aspects: string[];
  listOfAspects: ObjectiveElement[];
  listOfDeletedAspects: ObjectiveElement[];
  aspectsFromTree: ObjectiveElement[];
  tree: Tree<HierarchyElement>;

  createdAspects = false;

  get naviSubStep(): NaviSubStep {
    return { step: 'objectives', subStepIndex: this.pageKey - 1 };
  }

  protected constructor(injector: Injector) {
    this.currentProgressService = injector.get(CurrentProgressService);
    this.decisionData = injector.get(DecisionData);
  }

  ngOnInit() {
    if (this.pageKey > OBJECTIVES_STEPS.indexOf(this.decisionData.objectiveAspects.subStepProgression) + 1) {
      this.decisionData.objectiveAspects.subStepProgression = OBJECTIVES_STEPS[this.pageKey - 1];
    }

    this.currentProgressService.update();

    if (
      this.decisionData.objectiveAspects.listOfAspects.length === 0 &&
      this.decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`).children.length === 0 &&
      this.pageKey === 1
    ) {
      this.decisionData.objectiveAspects.generateInitialAspects();
      this.createdAspects = true;
    }

    this.listOfAspects = this.decisionData.objectiveAspects.listOfAspects;
    this.listOfDeletedAspects = this.decisionData.objectiveAspects.listOfDeletedAspects;
    this.tree = this.decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`);

    this.aspectsFromTree = [];
    for (const child of this.tree.children) {
      this.getAspectsFromTree(child);
    }
  }

  getAspectsFromTree(tree: Tree<HierarchyElement>) {
    this.aspectsFromTree.push(tree.value);
    for (const child of tree.children) {
      this.getAspectsFromTree(child);
    }
  }
}
