import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EducationalModeGuard } from '../../app/guards';
import { ZieleHint1Component, ZieleHint2Component, ZieleHint3Component, ZieleHint4Component, ZieleHint5Component } from './hints';
import { ObjectivesComponent } from './main';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ObjectivesComponent,
  },
  {
    path: 'hints',
    redirectTo: 'steps',
  },
  {
    path: 'steps',
    canActivate: [EducationalModeGuard],
    data: {
      educationalModeRedirect: '/objectives',
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '1',
      },
      {
        path: '1',
        component: ZieleHint1Component,
      },
      {
        path: '2',
        component: ZieleHint2Component,
      },
      {
        path: '3',
        component: ZieleHint3Component,
      },
      {
        path: '4',
        component: ZieleHint4Component,
      },
      {
        path: '5',
        component: ZieleHint5Component,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ObjectivesRoutingModule {}
