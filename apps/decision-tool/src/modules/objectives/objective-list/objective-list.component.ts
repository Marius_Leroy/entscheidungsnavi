import { Component, EventEmitter, Output } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { MatDialog } from '@angular/material/dialog';
import { lastValueFrom } from 'rxjs';
import { AppSettingsService } from '../../../app/data/app-settings.service';
import { DeleteObjectiveModalComponent } from '../delete-objective-modal/delete-objective-modal.component';

@Component({
  selector: 'dt-objective-list',
  templateUrl: './objective-list.component.html',
  styleUrls: ['./objective-list.component.scss'],
})
export class ObjectiveListComponent {
  objectiveExpanded: boolean[];
  @Output() newExpansionStatus = new EventEmitter<{ allExpanded: boolean; allCollapsed: boolean }>();

  get showSendButton() {
    return this.appSettings.showSendButtons;
  }

  constructor(protected decisionData: DecisionData, private appSettings: AppSettingsService, private dialog: MatDialog) {
    this.objectiveExpanded = this.decisionData.objectives.map(() => true);
  }

  move(from: number, to: number) {
    this.decisionData.moveObjective(from, to);
    this.objectiveExpanded.splice(to, 0, ...this.objectiveExpanded.splice(from, 1));
  }

  add() {
    this.decisionData.addObjective();
    this.objectiveExpanded.push(true);
  }

  async delete(objectiveIndex: number) {
    const attachedData = this.decisionData.getAttachedObjectiveData(objectiveIndex);

    const getConfirmation = () => {
      const dialogResult$ = this.dialog
        .open(DeleteObjectiveModalComponent, {
          data: objectiveIndex,
        })
        .afterClosed();
      return lastValueFrom(dialogResult$);
    };

    if (attachedData.length === 0 || (await getConfirmation())) {
      this.decisionData.removeObjective(objectiveIndex);
    }
  }

  confirmDelete(i: number) {
    this.decisionData.removeObjective(i);
    this.objectiveExpanded.splice(i, 1);
  }

  expandAll() {
    this.objectiveExpanded.fill(true);
    this.updateExpansionStatus();
  }

  collapseAll() {
    this.objectiveExpanded.fill(false);
    this.updateExpansionStatus();
  }

  get allExpanded() {
    return this.objectiveExpanded.every(x => x === true);
  }
  get allCollapsed() {
    return this.objectiveExpanded.every(x => x === false);
  }

  updateExpansionStatus() {
    this.newExpansionStatus.emit({ allCollapsed: this.allCollapsed, allExpanded: this.allExpanded });
  }
}
