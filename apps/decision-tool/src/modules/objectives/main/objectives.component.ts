import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NAVI_STEP_ORDER, ObjectiveElement, OBJECTIVES_STEPS } from '@entscheidungsnavi/decision-data/steps';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective } from '@entscheidungsnavi/decision-data/classes';
import { OnDestroyObservable, Tree } from '@entscheidungsnavi/tools';
import { Observable, takeUntil } from 'rxjs';
import { ExplanationService } from '../../shared/decision-quality';
import { CurrentProgressService } from '../../../app/data/current-progress.service';
import { HelpService } from '../../../app/help/help.service';
import { HelpMenuProvider, helpPage } from '../../../app/help/help';
import { getExplanationPage, getShortcutsPage } from '../help';
import { TransferService } from '../../../app/transfer';
import { PersistentSetting } from '../../../app/data/persistent-setting';
import { HelpBackgroundComponent } from '../hints/help-background/help-background.component';
import { DisplayAtMaxWidth } from '../../../app/interfaces/display-at-max-width';
import { ObjectiveListComponent } from '../objective-list/objective-list.component';
import { HierarchyElement } from '../aspect-hierarchy/hierarchy-element';
import { NavLine, navLineElement, Navigation } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';

@Component({
  templateUrl: './objectives.component.html',
  styleUrls: ['./objectives.component.scss', '../../hints.scss'],
})
@DisplayAtMaxWidth
export class ObjectivesComponent implements OnInit, Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [navLineElement().back('/objectives/steps/5').build()],
    middle: [this.explanationService.generateAssessmentButton('MEANINGFUL_RELIABLE_INFORMATION')],
    right: [navLineElement().continue('/alternatives/steps/1').build()],
  });

  helpMenu = {
    educational: [
      helpPage()
        .name($localize`Ergebnisseite`)
        .template(() => this.helpTemplate)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 2`)
        .component(HelpBackgroundComponent)
        .build(),
    ],
    starter: [
      helpPage()
        .name($localize`Schritt ${NAVI_STEP_ORDER.indexOf('objectives') + 1}\: ${this.languageService.steps.objectives.name}`)
        .template(() => this.starterModeHelp)
        .build(),
      getShortcutsPage(),
      getExplanationPage('starter'),
    ],
  };

  @PersistentSetting('project')
  displayVariant: 'list' | 'tree' = 'tree';

  allExpanded = true;
  allCollapsed = false;

  listOfAspects: ObjectiveElement[];
  listOfDeletedAspects: ObjectiveElement[];

  tree: Tree<HierarchyElement>;

  @ViewChild('starterModeHelp') starterModeHelp: TemplateRef<any>;
  @ViewChild('helpTemplate') helpTemplate: TemplateRef<any>;
  @ViewChild(ObjectiveListComponent) objectiveList: ObjectiveListComponent;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  constructor(
    private decisionData: DecisionData,
    private languageService: LanguageService,
    private currentProgressService: CurrentProgressService,
    public helpService: HelpService,
    private explanationService: ExplanationService,
    transferService: TransferService
  ) {
    transferService.onReceived$.pipe(takeUntil(this.onDestroy$)).subscribe(receivedObject => {
      if (receivedObject instanceof Objective) {
        transferService.openNotification(receivedObject);
      }
    });
  }

  ngOnInit() {
    this.decisionData.objectiveAspects.subStepProgression = OBJECTIVES_STEPS[OBJECTIVES_STEPS.length - 1];

    this.currentProgressService.update();

    this.listOfAspects = this.decisionData.objectiveAspects.listOfAspects;
    this.listOfDeletedAspects = this.decisionData.objectiveAspects.listOfDeletedAspects;

    if (this.decisionData.objectives.length === 0) {
      this.decisionData.addObjective();
    }

    this.createTree();
  }

  setDisplay(newDisplay: 'list' | 'tree') {
    if (this.displayVariant === 'list' && newDisplay === 'tree') {
      this.createTree();
    }

    this.displayVariant = newDisplay;
  }

  private createTree() {
    this.tree = this.decisionData.objectiveAspects.getAspectTree($localize`Zielhierarchie`);
  }

  expandAll() {
    this.objectiveList.expandAll();
  }

  collapseAll() {
    this.objectiveList.collapseAll();
  }

  updateExpansionStatus(expansionstatus: { allExpanded: boolean; allCollapsed: boolean }) {
    this.allExpanded = expansionstatus.allExpanded;
    this.allCollapsed = expansionstatus.allCollapsed;
  }
}
