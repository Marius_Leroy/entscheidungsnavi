import { Directive, Injector, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { DECISION_STATEMENT_STEPS } from '@entscheidungsnavi/decision-data/classes';
import { NaviSubStep } from '@entscheidungsnavi/decision-data/steps';
import { CurrentProgressService } from '../../../app/data/current-progress.service';
import { ContextualHelpMenu, HelpMenu, HelpMenuProvider } from '../../../app/help/help';
import { NavLine, navLineElement, Navigation } from '../../shared/navline';

@Directive()
// eslint-disable-next-line @angular-eslint/directive-class-suffix
export abstract class AbstractDecisionStatementHint implements OnInit, Navigation, HelpMenuProvider {
  navLine = new NavLine({
    left: [
      navLineElement()
        .back(() => (this.page === 1 ? '/decisionstatement/steps/1' : `/decisionstatement/steps/${this.page - 1}`))
        .disabled(() => this.page === 1)
        .build(),
    ],
    right: [
      navLineElement()
        .continue(() => `/decisionstatement/steps/${this.page + 1}`)
        .build(),
    ],
  });

  abstract helpMenu: ContextualHelpMenu | HelpMenu;
  page: number; // 1-based
  decisionData: DecisionData;

  protected currentProgressService: CurrentProgressService;

  protected dialog: MatDialog;

  get naviSubStep(): NaviSubStep {
    return { step: 'decisionStatement', subStepIndex: this.page - 1 };
  }

  protected constructor(injector: Injector) {
    this.currentProgressService = injector.get(CurrentProgressService);
    this.decisionData = injector.get(DecisionData);
    this.dialog = injector.get(MatDialog);
  }

  ngOnInit() {
    if (this.page > DECISION_STATEMENT_STEPS.indexOf(this.decisionData.decisionStatement.subStepProgression) + 1) {
      this.decisionData.decisionStatement.subStepProgression = DECISION_STATEMENT_STEPS[this.page - 1];
    }

    this.currentProgressService.update();
  }
}
