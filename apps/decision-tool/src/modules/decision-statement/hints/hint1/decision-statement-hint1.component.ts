import { Component, Injector, OnInit } from '@angular/core';
import { AbstractDecisionStatementHint } from '../decision-statement-hint';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  templateUrl: './decision-statement-hint1.component.html',
  styleUrls: ['./decision-statement-hint1.component.scss'],
})
export class DecisionStatementHint1Component extends AbstractDecisionStatementHint implements OnInit {
  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Weitere Hinweise`)
          .component(Help2Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 1`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }

  constructor(injector: Injector) {
    super(injector);
    this.page = 1;
  }

  override ngOnInit() {
    super.ngOnInit();

    if (this.decisionData.decisionStatement.statement_attempt == null) {
      this.decisionData.decisionStatement.statement_attempt = '';
    }
  }

  track(index: number) {
    return index;
  }

  addNote(index: number, notes: 'pre' | 'after') {
    this.getArray(notes).splice(index, 0, '');
  }

  removeNote(index: number, notes: 'pre' | 'after') {
    this.getArray(notes).splice(index, 1);
  }

  getArray(name: 'pre' | 'after') {
    return name === 'pre' ? this.decisionData.decisionStatement.preNotes : this.decisionData.decisionStatement.afterNotes;
  }
}
