import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { DECISION_STATEMENT_STEPS } from '@entscheidungsnavi/decision-data/classes';
import { ProjectService } from '../../../app/data/project';
import { TemplateType } from '../../../app/data/templates.service';
import { CurrentProgressService } from '../../../app/data/current-progress.service';
import { HelpMenuProvider, helpPage } from '../../../app/help/help';
import { getExplanationPage } from '../help';
import { ExplanationService } from '../../shared/decision-quality';
import { HelpBackgroundComponent } from '../hints/help-background/help-background.component';
import { NavLine, navLineElement, Navigation } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';

@Component({
  selector: 'dt-decision-statement',
  templateUrl: './decision-statement.component.html',
  styleUrls: ['./decision-statement.component.scss'],
})
export class DecisionStatementComponent implements OnInit, HelpMenuProvider, Navigation {
  navLine = new NavLine({
    left: [
      navLineElement()
        .back(() => '/decisionstatement/steps/4')
        .build(),
    ],
    middle: [this.explanationService.generateAssessmentButton('APPROPRIATE_FRAME')],
    right: [navLineElement().continue('/objectives/steps/1').build()],
  });

  @Input()
  showTitle = true;

  helpMenu = {
    educational: [
      helpPage()
        .name($localize`Ergebnisseite`)
        .template(() => this.helpTemplate)
        .build(),
      helpPage()
        .name($localize`Hintergrundwissen zum Schritt 1`)
        .component(HelpBackgroundComponent)
        .build(),
    ],
    starter: [
      helpPage()
        .name(
          $localize`Schritt
          ${NAVI_STEP_ORDER.indexOf('decisionStatement') + 1}\: ${this.languageService.steps.decisionStatement.name}`
        )
        .template(() => this.starterModeHelp)
        .build(),
      getExplanationPage('starter'),
    ],
  };

  showTemplateHint = false;
  projectTemplate: TemplateType | 'blank';

  @ViewChild('helpTemplate') helpTemplate: TemplateRef<any>;
  @ViewChild('starterModeHelp') starterModeHelp: TemplateRef<any>;

  constructor(
    protected decisionData: DecisionData,
    private languageService: LanguageService,
    private currentProgressService: CurrentProgressService,
    private projectService: ProjectService,
    private explanationService: ExplanationService
  ) {}

  ngOnInit() {
    this.decisionData.decisionStatement.subStepProgression = DECISION_STATEMENT_STEPS[DECISION_STATEMENT_STEPS.length - 1];
    this.currentProgressService.update();

    if (
      !this.decisionData.decisionStatement.statement &&
      this.decisionData.alternatives.length === 0 &&
      this.decisionData.objectives.length === 0
    ) {
      this.showTemplateHint = true;
    }

    if (this.decisionData.decisionStatement.statement == null) {
      this.decisionData.decisionStatement.statement = this.decisionData.decisionStatement.statement_attempt_2 ?? '';
      this.currentProgressService.update();
    }

    if (this.decisionData.decisionStatement.preNotes_2.length > 0 && this.decisionData.decisionStatement.preNotesFinal.length === 0) {
      this.decisionData.decisionStatement.preNotesFinal = this.decisionData.decisionStatement.preNotes_2.filter(note => note.length > 0);
    }

    if (this.decisionData.decisionStatement.afterNotes_2.length > 0 && this.decisionData.decisionStatement.afterNotesFinal.length === 0) {
      this.decisionData.decisionStatement.afterNotesFinal = this.decisionData.decisionStatement.afterNotes_2.filter(
        note => note.length > 0
      );
    }
  }

  loadTemplate() {
    if (this.projectTemplate != null && this.projectTemplate !== 'blank') {
      this.projectService.loadTemplate(this.projectTemplate);
    }
  }
}
