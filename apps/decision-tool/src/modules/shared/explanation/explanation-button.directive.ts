import { Directive, ElementRef, HostBinding, Input } from '@angular/core';
import { DomPortal } from '@angular/cdk/portal';

@Directive({
  selector: '[dtExplanationTrigger]',
  exportAs: 'explanationTrigger',
})
export class ExplanationButtonDirective {
  @HostBinding('style.display')
  private _display: 'none' | 'block';

  @Input()
  set visible(visible: boolean) {
    this._display = visible ? 'block' : 'none';
  }

  private readonly explanationButtonPortal: DomPortal;

  constructor(public elementRef: ElementRef) {
    this.explanationButtonPortal = new DomPortal(elementRef);
  }

  getExplanationButtonPortal(): DomPortal {
    return this.explanationButtonPortal;
  }
}
