import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'dt-explanation-simple-open-button',
  template: `
    <button (click)="$event.stopPropagation(); explain.emit()" mat-icon-button color="accent">
      <mat-icon>help</mat-icon>
    </button>
  `,
  styles: [
    `
      mat-icon {
        font-size: 20px;
        background: radial-gradient(white 7px, transparent 5px);
      }
    `,
  ],
})
export class SimpleExplanationOpenButtonComponent {
  @Output()
  explain = new EventEmitter<void>();
}
