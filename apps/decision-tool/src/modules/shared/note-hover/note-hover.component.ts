import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { RichTextEditorComponent } from '@entscheidungsnavi/widgets';

@Component({
  selector: 'dt-note-hover',
  templateUrl: './note-hover.component.html',
  styleUrls: ['./note-hover.component.scss'],
  imports: [RichTextEditorComponent],
  standalone: true,
})
export class NoteHoverComponent implements AfterViewInit {
  @ViewChild('content') content: ElementRef;

  contentString: string;

  constructor(private cdRef: ChangeDetectorRef) {}

  ngAfterViewInit() {
    this.contentString = this.content.nativeElement.innerHTML;
    this.cdRef.detectChanges();
  }
}
