import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { TeamMember } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, debounceTime, filter, finalize, Observable, of, Subject, switchMap, takeUntil, tap } from 'rxjs';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ProjectService } from '../../../../app/data/project';
import {
  TeamUnreadCommentsModalComponent,
  TeamUnreadCommentsModalData,
} from '../team-unread-comments-modal/team-unread-comments-modal.component';
import { TeamTrait } from '../../../../app/data/project/team-trait';

@Component({
  templateUrl: './team-modal.component.html',
  styleUrls: ['./team-modal.component.scss'],
})
export class TeamModalComponent implements OnInit {
  protected teamTrait: TeamTrait;

  protected inviteForm = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
  });

  protected settingsForm = new FormGroup({
    teamName: new FormControl('', Validators.required),
  });

  protected whiteboardChange = new Subject<string>();

  @OnDestroyObservable()
  protected onDestroy$: Observable<never>;

  protected allProjects$: Observable<DecisionData[]>;

  constructor(
    private dialogRef: MatDialogRef<TeamModalComponent>,
    private projectService: ProjectService,
    private teamAPIService: TeamsService,
    private cdRef: ChangeDetectorRef,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.projectService.teamTrait$.pipe(takeUntil(this.onDestroy$)).subscribe(teamTrait => {
      if (teamTrait == null) {
        this.dialogRef.close();
      } else {
        this.teamTrait = teamTrait;
      }
    });
  }

  get team() {
    return this.teamTrait.info;
  }

  get userMember() {
    return this.teamTrait.userMember;
  }

  get isUserTeamOwner() {
    return this.teamTrait.userMember.id === this.team.owner;
  }

  get isUsersProjectLoaded() {
    return this.teamTrait.userMember === this.teamTrait.activeMember;
  }

  get isUserEditor() {
    return this.team.editor$.value === this.teamTrait.userMember.id;
  }

  get invites() {
    return this.team.pendingInvites;
  }

  get usersUnreadComments() {
    return this.teamTrait.getUsersUnreadComments();
  }

  ngOnInit() {
    this.allProjects$ = this.teamTrait.allProjects$.pipe(
      catchError(() => {
        this.snackBar.open($localize`Beim Laden der Auswertung ist ein Fehler aufgetreten.`, $localize`Ok`);
        return of(null);
      })
    );

    this.teamTrait.updateTeamData().subscribe({
      error: () => {
        this.snackBar.open($localize`Beim Abrufen der Teaminformationen ist ein Fehler aufgetreten.`, $localize`Ok`);
      },
    });

    this.whiteboardChange
      .pipe(
        debounceTime(2000),
        filter(() => this.isUserEditor),
        switchMap(newWhiteboardText =>
          this.teamAPIService.updateTeam(this.team.id, { whiteboard: newWhiteboardText }).pipe(
            tap(() => (this.teamTrait.info.whiteboard = newWhiteboardText)),
            catchError(error => {
              if (error instanceof HttpErrorResponse && error.status === 403) {
                this.teamTrait.info.editor$.next(error.error.message);
                this.snackBar.open($localize`Du hast nicht länger das Recht das Whiteboard zu bearbeiten.`, 'Ok');
              } else {
                this.snackBar.open($localize`Beim Aktualisieren des Whiteboards ist ein Fehler aufgetreten.`, 'Ok');
              }

              return of();
            })
          )
        ),
        takeUntil(this.onDestroy$)
      )
      .subscribe();

    this.settingsForm.controls.teamName.setValue(this.team.name);

    this.settingsForm.controls.teamName.valueChanges
      .pipe(
        filter(_ => this.settingsForm.controls.teamName.valid),
        debounceTime(2000),
        filter(() => this.isUserTeamOwner),
        switchMap(newTeamName =>
          this.teamAPIService.updateTeam(this.team.id, { name: newTeamName }).pipe(
            catchError(_ => {
              this.snackBar.open($localize`Beim Aktualisieren des Teamnamens ist ein Fehler aufgetreten.`, 'Ok');
              return of();
            })
          )
        ),
        takeUntil(this.onDestroy$)
      )
      .subscribe(team => (this.teamTrait.info.name = team.name));
  }

  openUnreadCommentsIn(member: TeamMember) {
    this.dialog.open<TeamUnreadCommentsModalComponent, TeamUnreadCommentsModalData>(TeamUnreadCommentsModalComponent, {
      data: {
        team: this.team,
        teamMember: member,
        unreadComments: this.usersUnreadComments.get(member),
      },
    });
  }

  changeEditorTo(memberId: string) {
    this.teamTrait.changeEditorTo(memberId).subscribe();
  }

  takeMainProject() {
    this.teamTrait.takeMainProject().subscribe();
  }

  isProjectLoaded(member: TeamMember) {
    return this.teamTrait.activeMember.id === member.id;
  }

  isUser(member: TeamMember) {
    return this.teamTrait.userMember.id === member.id;
  }

  createInvite() {
    if (this.inviteForm.valid) {
      const email = this.inviteForm.controls.email.value;

      this.teamTrait.invite(email).subscribe({
        complete: () => this.inviteForm.controls.email.setValue(''),
        error: () => this.snackBar.open($localize`Beim Erstellen der Einladung ist ein Fehler aufgetreten.`, $localize`Ok`),
      });
    }
  }

  loadMemberProject(member: TeamMember) {
    this.cdRef.detach();
    this.projectService
      .loadTeamProject(this.team.id, member.id)
      .pipe(finalize(() => this.cdRef.reattach()))
      .subscribe({
        error: () => this.snackBar.open($localize`Beim Laden des Projekts ist ein Fehler aufgetreten.`, $localize`Ok`),
      });
  }

  close() {
    this.dialogRef.close();
  }
}
