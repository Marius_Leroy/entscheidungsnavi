import { Component, Input, OnInit } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { NAVI_STEP_ORDER } from '@entscheidungsnavi/decision-data/steps';
import { TeamMember } from '@entscheidungsnavi/api-types';
import { ValidateDecisionDataGuard } from '../../../../app/guards';
import { LanguageService } from '../../../../app/data/language.service';

@Component({
  selector: 'dt-team-project-progress',
  templateUrl: './team-project-progress.component.html',
  styleUrls: ['./team-project-progress.component.scss'],
})
export class TeamProjectProgressComponent implements OnInit {
  @Input()
  projects: DecisionData[];

  @Input()
  members: readonly TeamMember[];

  protected progress: number[];

  protected stepIcons: string[];

  constructor(languageService: LanguageService) {
    this.stepIcons = NAVI_STEP_ORDER.map(stepId => languageService.steps[stepId].icon);
  }

  ngOnInit() {
    this.calcData();
  }

  private calcData() {
    this.progress = new Array(this.projects.length);

    for (let i = 0; i < this.projects.length; i++) {
      const project = this.projects[i];

      const [firstStepWithError] = ValidateDecisionDataGuard.getFirstErrorStepWithErrors(project);

      this.progress[i] = firstStepWithError ? NAVI_STEP_ORDER.indexOf(firstStepWithError) : NAVI_STEP_ORDER.length;
    }
  }
}
