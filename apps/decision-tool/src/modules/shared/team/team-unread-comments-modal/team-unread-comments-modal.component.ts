import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TeamComment, TeamMember } from '@entscheidungsnavi/api-types';
import { TeamsService } from '@entscheidungsnavi/api-client';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Alternative, Objective, Outcome, TeamUUIDs, UserdefinedInfluenceFactor } from '@entscheidungsnavi/decision-data/classes';
import { NaviStep } from '@entscheidungsnavi/decision-data/steps';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert_unreachable';
import { groupBy } from 'lodash';
import { LoadedTeam } from '../../../../app/data/project/team-trait';
import { LanguageService } from '../../../../app/data/language.service';

export type TeamUnreadCommentsModalData = { team: LoadedTeam; teamMember: TeamMember; unreadComments: TeamComment[] };

type CommentEntry = { id: string; comment: TeamComment; objectId: string } & (
  | { type: 'alternative'; object: Alternative }
  | { type: 'objective'; object: Objective }
  | { type: 'influence-factor'; object: UserdefinedInfluenceFactor }
  | { type: 'outcome'; object: Outcome }
  | { type: 'step'; object: NaviStep }
  | { type: 'whiteboard'; object: null }
);

@Component({
  selector: 'dt-team-unread-comments',
  templateUrl: './team-unread-comments-modal.component.html',
  styleUrls: ['./team-unread-comments-modal.component.scss'],
})
export class TeamUnreadCommentsModalComponent {
  protected team: LoadedTeam;
  protected teamMember: TeamMember;
  protected unreadComments: TeamComment[];
  protected unreadCommentEntries: Record<string, CommentEntry[]>;

  protected project: DecisionData;

  constructor(
    private dialogRef: MatDialogRef<TeamUnreadCommentsModalComponent>,
    private languageService: LanguageService,
    @Inject(MAT_DIALOG_DATA) { team, teamMember, unreadComments }: TeamUnreadCommentsModalData,
    snackBar: MatSnackBar,
    private teamAPIService: TeamsService
  ) {
    this.team = team;
    this.teamMember = teamMember;
    this.unreadComments = unreadComments;
    this.teamAPIService.getTeamProject(this.team.id, this.teamMember.id).subscribe({
      next: project => {
        this.project = readText(project.data);
        this.findObjectsForComments();
      },
      error: _ => {
        snackBar.open($localize`Fehler beim Laden der ungelesenen Kommentare`, $localize`Ok`);
        this.close();
      },
    });
  }

  close() {
    this.dialogRef.close();
  }

  private findObjectsForComments() {
    const commentEntries: CommentEntry[] = [];
    for (let i = 0; i < this.unreadComments.length; i++) {
      const comment = this.unreadComments[i];
      const objectId = comment.objectId;

      if (objectId === 'whiteboard') {
        commentEntries.push({
          id: comment.id,
          objectId,
          comment,
          object: null,
          type: 'whiteboard',
        });
      } else if (TeamUUIDs.STEPS_INVERTED[objectId]) {
        commentEntries.push({
          id: comment.id,
          comment,
          objectId,
          object: TeamUUIDs.STEPS_INVERTED[objectId],
          type: 'step',
        });
      } else {
        const { object: decisionDataObject } = this.project.findObject(objectId);
        if (decisionDataObject) {
          if (decisionDataObject instanceof Alternative) {
            commentEntries.push({
              id: comment.id,
              objectId,
              comment,
              object: decisionDataObject,
              type: 'alternative',
            });
          } else if (decisionDataObject instanceof Objective) {
            commentEntries.push({
              id: comment.id,
              objectId,
              comment,
              object: decisionDataObject,
              type: 'objective',
            });
          } else if (decisionDataObject instanceof UserdefinedInfluenceFactor) {
            commentEntries.push({
              id: comment.id,
              objectId,
              comment,
              object: decisionDataObject,
              type: 'influence-factor',
            });
          } else if (decisionDataObject instanceof Outcome) {
            commentEntries.push({
              id: comment.id,
              objectId,
              comment,
              object: decisionDataObject,
              type: 'outcome',
            });
          } else {
            assertUnreachable(decisionDataObject);
          }
        }
      }
    }

    this.unreadCommentEntries = groupBy(commentEntries, entry => entry.objectId);
  }

  protected getLabelForObject(object: CommentEntry['object']) {
    if (object instanceof Alternative) {
      return $localize`Alternative „ ${object.name}“`;
    } else if (object instanceof Objective) {
      return $localize`Ziel „ ${object.name}“`;
    } else if (object instanceof UserdefinedInfluenceFactor) {
      return $localize`Einflussfaktor „ ${object.name}“`;
    } else if (object instanceof Outcome) {
      return $localize`Ausprägung`;
    } else if (object == null) {
      return $localize`Whiteboard`;
    } else {
      return $localize`Schritt „${this.languageService.steps[object].name}“`;
    }
  }
}
