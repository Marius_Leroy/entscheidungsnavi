import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  templateUrl: './json-error-modal.component.html',
  styleUrls: ['./json-error-modal.component.scss'],
})
export class JsonErrorModalComponent {
  constructor(public dialogRef: MatDialogRef<JsonErrorModalComponent>) {}
}
