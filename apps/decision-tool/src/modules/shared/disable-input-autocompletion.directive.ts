import { Directive, ElementRef } from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'input:not([type="email"])',
})
export class DisableInputAutocompletionDirective {
  constructor(private input: ElementRef) {
    this.input.nativeElement.setAttribute('autocomplete', 'off');
  }
}
