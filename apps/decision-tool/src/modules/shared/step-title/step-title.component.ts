import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NAVI_STEP_ORDER, NaviStep } from '@entscheidungsnavi/decision-data/steps';

@Component({
  selector: 'dt-step-title',
  templateUrl: './step-title.component.html',
  styleUrls: ['./step-title.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StepTitleComponent {
  @Input() showStepNumber = true;
  @Input() step: NaviStep;
  @Input() subTitle?: string;

  get stepNumber() {
    return NAVI_STEP_ORDER.indexOf(this.step) + 1;
  }
}
