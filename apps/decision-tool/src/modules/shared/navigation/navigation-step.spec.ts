import { naviSubStepToUrl, urlToNaviSubStep } from './navigation-step';

describe('navigation-step', () => {
  describe('urlToNaviSubStep', () => {
    it('works for main steps', () => {
      expect(urlToNaviSubStep('/decisionstatement')).toEqual({ step: 'decisionStatement' });
      expect(urlToNaviSubStep('/impactmodel')).toEqual({ step: 'impactModel' });
      expect(urlToNaviSubStep('/impactmodel/uncertaintyfactors')).toEqual({ step: 'impactModel' });
      expect(urlToNaviSubStep('/results')).toEqual({ step: 'results' });
    });

    it('works for substeps', () => {
      expect(urlToNaviSubStep('/decisionstatement/steps/1')).toEqual({ step: 'decisionStatement', subStepIndex: 0 });
      expect(urlToNaviSubStep('/decisionstatement/steps/3')).toEqual({ step: 'decisionStatement', subStepIndex: 2 });
      expect(urlToNaviSubStep('/alternatives/steps/4')).toEqual({ step: 'alternatives', subStepIndex: 3 });
      expect(urlToNaviSubStep('/results/steps/2')).toEqual({ step: 'results', subStepIndex: 1 });
    });
  });

  describe('naviSubStepToUrl', () => {
    it('works for main steps', () => {
      expect(naviSubStepToUrl({ step: 'decisionStatement' })).toBe('/decisionstatement');
      expect(naviSubStepToUrl({ step: 'impactModel' })).toBe('/impactmodel');
      expect(naviSubStepToUrl({ step: 'results' })).toBe('/results');
    });

    it('works for substeps', () => {
      expect(naviSubStepToUrl({ step: 'decisionStatement', subStepIndex: 0 })).toBe('/decisionstatement/steps/1');
      expect(naviSubStepToUrl({ step: 'decisionStatement', subStepIndex: 2 })).toBe('/decisionstatement/steps/3');
      expect(naviSubStepToUrl({ step: 'alternatives', subStepIndex: 3 })).toBe('/alternatives/steps/4');
      expect(naviSubStepToUrl({ step: 'results', subStepIndex: 1 })).toBe('/results/steps/2');
    });
  });
});
