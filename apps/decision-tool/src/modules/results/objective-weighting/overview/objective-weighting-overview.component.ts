import { DecimalPipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective, Weights } from '@entscheidungsnavi/decision-data/classes';
import { filter, identity } from 'lodash';
import { CurrentProgressService } from '../../../../app/data/current-progress.service';
import { getDefaultTradeoffObjective, getPercentages } from '../objective-weighting-util';
import { ReferenceObjectiveSelectionComponent } from '../reference-objective-selection/reference-objective-selection.component';

@Component({
  selector: 'dt-objective-weighting-overview',
  templateUrl: './objective-weighting-overview.component.html',
  styleUrls: ['./objective-weighting-overview.component.scss'],
})
export class ObjectiveWeightingOverviewComponent implements OnInit {
  maxInputValue: number; // max value of the inputs

  isManualTradeoffActive: boolean[];
  percentages: Array<[number, number]>;

  @Input() showResetButton = true;
  @Input() tradeoffUrl = 'detailed';
  @Input() sliderLength = 220;

  @Output() weightsChange = new EventEmitter<void>();

  @ViewChild('notification', { static: true }) private notificationRef: TemplateRef<any>;

  constructor(
    protected decisionData: DecisionData,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private numberPipe: DecimalPipe,
    private currentProgressService: CurrentProgressService
  ) {}

  get objectiveWeights() {
    return this.decisionData.weights.getWeights();
  }

  get hasDefaultMaxWeight() {
    return this.maxInputValue === Weights.NORMALIZATION_TARGET_WEIGHT;
  }

  ngOnInit() {
    if ((this.decisionData.resultSubstepProgress ?? 0) < 1) {
      this.decisionData.resultSubstepProgress = 1;
      this.currentProgressService.update();
    }

    this.updateShownMax();
    this.updatePercentages();

    this.isManualTradeoffActive = this.decisionData.objectives.map((_, index) => this.decisionData.weights.isManualTradeoffActive(index));
  }

  isTradeoffVerified(objectiveIndex: number) {
    return (
      this.decisionData.weights.tradeoffWeights[objectiveIndex] != null && !this.decisionData.weights.unverifiedWeights[objectiveIndex]
    );
  }

  resetWeights() {
    this.decisionData.weights.resetWeights();
    this.maxInputValue = Weights.NORMALIZATION_TARGET_WEIGHT;
    this.weightsChange.emit();
  }

  updateShownMax() {
    const values = this.objectiveWeights.map(weight => (weight.value ?? 0) + weight.precision);
    if (this.decisionData.weights.tradeoffWeights.some(w => w != null)) {
      this.maxInputValue = Math.max(...values);
    } else {
      // if no tradeoff is set, shownMax should be at least 250
      this.maxInputValue = Math.max(Weights.NORMALIZATION_TARGET_WEIGHT, ...filter(values, isFinite));
    }
    this.maxInputValue = Math.min(Weights.MAX_WEIGHT, this.maxInputValue);
  }

  /**
   * update the weight in the decisionData object. Show the warning modal if it does interfere with existing data
   */
  changeWeight(idx: number, value: number, normalize: boolean) {
    const precisionRemoved = this.decisionData.weights.changePreliminaryWeightValue(idx, value, normalize);
    this.onValueChange(idx, normalize, precisionRemoved);
  }

  adjustWeight(idx: number, amount: number) {
    const normalize = this.isNormalized();

    const precisionRemoved = this.decisionData.weights.adjustPreliminaryWeightValue(idx, amount, normalize);
    this.onValueChange(idx, normalize, precisionRemoved);
  }

  setWeightToMax(idx: number) {
    this.changeWeight(idx, this.maxInputValue, false);
  }

  private onValueChange(idx: number, normalize: boolean, precisionRemoved = false) {
    if (normalize) {
      this.updateShownMax();
    }

    if (precisionRemoved) {
      this.showPrecisionNotification();
    }
    if (idx === this.decisionData.weights.tradeoffObjectiveIdx && this.isManualTradeoffActive.some(identity)) {
      this.isManualTradeoffActive = this.isManualTradeoffActive.map(() => false);
      this.snackBar.open($localize`Explizit eingegebene Trade-offs sind nicht mehr aktiv`, 'Ok');
    } else if (this.isManualTradeoffActive[idx]) {
      this.isManualTradeoffActive[idx] = false;
      this.snackBar.open($localize`Explizit eingegebener Trade-off ist nicht mehr aktiv`, 'Ok');
    }

    this.updatePercentages();
    this.checkDefaultTradeoffObjective();
    this.weightsChange.emit();
  }

  checkDefaultTradeoffObjective() {
    if (
      (this.decisionData.weights.tradeoffObjectiveIdx == null ||
        this.decisionData.weights.tradeoffObjectiveIdx >= this.decisionData.objectives.length ||
        this.decisionData.weights.getWeight(this.decisionData.weights.tradeoffObjectiveIdx).value === 0) &&
      this.isNormalized()
    ) {
      this.decisionData.weights.tradeoffObjectiveIdx = getDefaultTradeoffObjective(this.decisionData);
    }
  }

  normalize() {
    this.decisionData.weights.normalizePreliminaryWeights();
    this.checkDefaultTradeoffObjective();
    this.updatePercentages();
    this.updateShownMax();
    this.weightsChange.emit();
  }

  updatePercentages() {
    this.percentages = getPercentages(this.objectiveWeights);
  }

  isFinished() {
    return this.decisionData.weights.preliminaryWeights.every(weight => weight.value !== undefined);
  }

  isNormalized() {
    return (
      this.decisionData.weights
        .getWeights()
        .map(weight => weight.value + weight.precision)
        .some(value => value === this.maxInputValue || value > Weights.MAX_WEIGHT) &&
      this.decisionData.weights.getWeights().every(w => 0 <= w.value && w.value <= this.maxInputValue)
    );
  }

  /* --- string functions --- */
  getMin(ziel: Objective) {
    if (ziel.isNumerical) {
      return this.numberPipe.transform(ziel.numericalData.from) + ' ' + ziel.numericalData.unit;
    } else if (ziel.isVerbal) {
      return ziel.verbalData.options[0];
    } else if (ziel.isIndicator) {
      return this.numberPipe.transform(ziel.indicatorData.defaultAggregationWorst) + ' ' + ziel.indicatorData.aggregatedUnit;
    }
  }

  getMax(ziel: Objective) {
    if (ziel.isNumerical) {
      return this.numberPipe.transform(ziel.numericalData.to) + ' ' + ziel.numericalData.unit;
    } else if (ziel.isVerbal) {
      return ziel.verbalData.options[ziel.verbalData.options.length - 1];
    } else if (ziel.isIndicator) {
      return this.numberPipe.transform(ziel.indicatorData.defaultAggregationBest) + ' ' + ziel.indicatorData.aggregatedUnit;
    }
  }

  weightPre(idx: number): string {
    return '\xB1' + this.numberPipe.transform(this.percentages[idx][1]) + '%';
  }

  getIndicatorScaleInfo(objective: Objective) {
    if (objective.isIndicator) {
      return objective.indicatorData.indicators.map(indicator => [
        indicator.name,
        this.numberPipe.transform(indicator.min),
        this.numberPipe.transform(indicator.max),
        indicator.unit,
      ]);
    } else {
      return [];
    }
  }

  openReferenceObjectiveSelection() {
    this.dialog
      .open(ReferenceObjectiveSelectionComponent)
      .afterClosed()
      .subscribe((newReferenceObjectiveIdx?: number) => {
        if (
          newReferenceObjectiveIdx != null &&
          this.decisionData.weights.changeTradeoffObjective(newReferenceObjectiveIdx, this.decisionData.objectives)
        ) {
          this.snackBar.open($localize`Alle explizit eingegebenen Trade-offs wurden entfernt`, 'Ok');
        }
      });
  }

  showPrecisionNotification() {
    this.snackBar.openFromTemplate(this.notificationRef, { duration: 3000 });
  }
}
