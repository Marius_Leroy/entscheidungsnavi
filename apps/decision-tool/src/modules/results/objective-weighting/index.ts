export * from './detailed';
export * from './main/objective-weighting-main.component';
export * from './main/objective-weighting-navigation.component';
export * from './reference-objective-selection/reference-objective-selection.component';
export * from './overview/objective-weighting-overview.component';
