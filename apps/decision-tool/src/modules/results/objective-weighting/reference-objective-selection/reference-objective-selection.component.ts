import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective } from '@entscheidungsnavi/decision-data/classes';
import { sortBy } from 'lodash';
import { getDefaultTradeoffObjective, getPercentages } from '../objective-weighting-util';

@Component({
  templateUrl: './reference-objective-selection.component.html',
  styleUrls: ['./reference-objective-selection.component.scss'],
})
export class ReferenceObjectiveSelectionComponent implements OnInit {
  sortedObjectives: Array<[Objective, number, number]>;
  percentages: number[];

  currentReferenceObjectiveIdx: number;
  referenceObjectiveIdx: number;

  constructor(private decisionData: DecisionData, public dialogRef: MatDialogRef<ReferenceObjectiveSelectionComponent>) {}

  ngOnInit() {
    if (
      this.decisionData.weights.tradeoffObjectiveIdx === null ||
      this.decisionData.weights.tradeoffObjectiveIdx >= this.decisionData.objectives.length
    ) {
      this.decisionData.weights.tradeoffObjectiveIdx = getDefaultTradeoffObjective(this.decisionData);
    }

    this.percentages = getPercentages(this.decisionData.weights.getWeights()).map(a => a[0]);

    this.sortedObjectives = sortBy(
      this.decisionData.objectives.map((o, index): [Objective, number, number] => [o, index, this.percentages[index]]),
      obj => {
        const o = obj[0];
        const index = obj[1];
        const weight = this.decisionData.weights.preliminaryWeights[index];

        if (!weight) {
          return 0;
        }
        return -weight.value * (o.isNumerical ? 1.5 : 1);
      }
    ).map(t => [t[0], t[1], t[2]]);

    this.currentReferenceObjectiveIdx = this.referenceObjectiveIdx = this.decisionData.weights.tradeoffObjectiveIdx;
  }

  submit() {
    if (this.referenceObjectiveIdx !== this.currentReferenceObjectiveIdx) {
      this.dialogRef.close(this.referenceObjectiveIdx);
    }
  }
}
