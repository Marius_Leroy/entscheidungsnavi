import { Component } from '@angular/core';
import { LanguageService } from '../../../../app/data/language.service';

@Component({
  template: `
    <dt-extendable-header i18n-headerName variation="large" headerName="Robustheitstest mit Präzisionsintervallen" [initialOpening]="true">
      <p i18n
        >Außerhalb des Robustheitstest werden Präzisionsintervalle bei der Berechnung der Nutzenwerte nicht berücksichtigt. Das
        Gesamtergebnis zeigt immer die Bewertung, die sich ergibt, wenn Du diese Präzisionsintervalle gar nicht gesetzt und mit dem exakten
        Wert gerechnet hättest. Im Robustheitstest hast Du aber die Möglichkeit zu überprüfen, ob diese Präzisionsintervalle einen Einfluss
        auf das Ergebnis haben können.</p
      >
      <p i18n
        >Im Robustheitstest werden dafür rein zufällig Werte aus den von Dir angegebenen Präzisionsintervallen ermittelt und auf dieser
        Grundlage ein Ergebnis berechnet. Eine solche zufällige Auswahl wird in jedem Simulationsschritt unternommen, so dass Du bei
        z.&nbsp;B. 10.000 Simulationsschritten 10.000 unterschiedliche Ergebnisse erhältst. Hierbei können sich nicht nur die berechneten
        Nutzenwerte der Alternativen ändern, sondern auch deren Rangfolgen untereinander.</p
      >
      <p i18n
        >In der Auswertung des Robustheitstests siehst Du insbesondere, wie stabil Deine beste Alternative auf der ersten Rangposition ist.
        Wenn in allen Simulationsschritten Deine beste Alternative immer auf die erste Rangposition gelangt, dann hat die fehlende Präzision
        keinerlei Auswirkungen auf Deine Entscheidung für diese Alternative. Du kannst Dir also sicher sein, dass diese Alternative die
        beste ist. Dies erkennst Du daran, dass oben in der Matrix 100 % bei Rangplatz 1 vermerkt ist.</p
      >
      <p i18n
        >Wenn oben links jedoch eine kleinere Prozentzahl als 100&nbsp;% steht, so gibt es Konstellationen mit den von Dir gemachten
        Angaben, in denen eine andere Alternative besser wäre. In diesem Fall ist es nicht sicher, dass Deine eigentlich beste Alternative
        auch wirklich immer die beste ist. Dann bietet es sich an, sich die unpräzisen Angaben noch einmal anzuschauen und zu reflektieren,
        ob diese richtig angegeben sind oder ggf. die Präzisionsintervalle kleiner gewählt werden können. Zugleich kann es sinnvoll sein,
        sich die Auswirkungen dieser kritischen Angaben in der Sensitivitätsanalyse anzuschauen, um ein besseres Gefühl für die Auswirkungen
        von Ungenauigkeiten bei den Angaben zu erhalten.</p
      >
    </dt-extendable-header>
    <dt-extendable-header i18n-headerName variation="large" headerName="Robustheitstest mit Einflussfaktoren">
      <p i18n
        >Mit dem Robustheitstest kannst Du analysieren, wie sich die in den Einflussfaktoren abgebildeten Unsicherheiten auf Deine
        Entscheidung auswirken können. Außerhalb des Robustheitstests wird für jede unsichere Wirkungsprognose nämlich nur mit einem
        Nutzenerwartungswert über alle Zustände des definierten Einflussfaktors gerechnet. Du erhältst also immer nur ein „gemitteltes“
        Ergebnis. Es ist im Ergebnis nicht erkennbar, wie unterschiedlich die Bewertungen in Abhängigkeit der unsicheren
        <span class="hover-text" [dtHoverPopover]="scenariosPopover" dtHoverPopoverPosition="left">Szenarien</span> ausfallen.</p
      >
      <p i18n
        >Im Robustheitstest wird hingegen in jedem Simulationsschritt ein solches Szenario, also eine Kombination von Zuständen aller
        Einflussfaktoren, unterstellt. Hierzu wird in jedem Einflussfaktor zunächst genau ein Zustand ausgewählt und ein Szenario gebildet.
        Im Anschluss wird der Nutzenwert der Alternativen und deren Rangfolgen unter der Annahme berechnet, dass diese ausgewählten Zustände
        des Szenarios mit Sicherheit eintreten. Bei 10.000 Simulationsschritten gibt es z.&nbsp;B. 10.000 verschiedene Ergebnisse, die
        zugleich zu unterschiedlichen Rangfolgen führen können. Wie häufig ein Szenario in der Simulation vorkommt, ergibt sich aus den
        angegebenen Wahrscheinlichkeiten der einzelnen Zustände, die dieses Szenario bilden. Zustände mit höheren Wahrscheinlichkeiten
        kommen dementsprechend häufiger in den Szenarien bzw. den Simulationsschritten vor.</p
      >
      <p i18n
        >Im Ergebnis über alle Simulationsschritte lässt sich ablesen, wie häufig die Alternativen die verschiedenen Rangpositionen belegen.
        Besonders interessant ist hier, wie häufig Deine beste Alternative auf Rang 1 gelangt. Ist dies in 100 % der Simulationsschritte der
        Fall, ist die Alternative in allen betrachteten Szenarien immer die beste. Egal, was passiert, machst Du mit dieser Alternative
        keinen Fehler. Sollte dies in weniger als 100 % der Simulationsschritte der Fall sein, gibt es Szenarien, in denen eine andere
        Alternative besser gewesen wäre. Hier wäre es beim Eintreffen dieser Szenarien besser gewesen, diese andere Alternative zu wählen.
        Zwar ist man nachher immer schlauer, aber trotzdem kann es nützlich sein, diese Zusammenhänge vorher schon reflektiert zu haben.</p
      >
      <p i18n
        >Wenn Du mit der Maus über die Balken fährst, kannst Du zusätzlich etwas über die Szenarien erfahren, in denen die jeweils gezeigte
        Rangposition abgeleitet wurde. Da dies durchaus sehr verschiedene Szenarien sein können und eine Darstellung aller Szenarien zu
        unübersichtlich wäre, werden lediglich die Zustände angezeigt, die ausnahmslos in allen Szenarien auftreten, die zu der Rangposition
        geführt haben.</p
      >
      <p i18n
        >Besonders charakterisierend für das Auftreten einer Rangposition sind diejenigen Zustände, die von Dir mit einer geringen
        Wahrscheinlichkeit angegeben wurden, aber trotzdem in vielen Szenarien vorkommen. Wenn die Eintrittswahrscheinlichkeit eines
        Zustands ohnehin sehr hoch ist, dann ist das Auftreten dieses Zustands in einem Szenario weniger charakterisierend für das Erreichen
        der Rangposition. Deshalb wird in der Darstellung nach aufsteigender Wahrscheinlichkeit sortiert, und die besonders
        charakterisierenden Zustände finden sich deshalb in den ersten Zeilen.</p
      >
      <p i18n
        >Wenn über alle Simulationsschritte hinweg bei einer Rangposition mehrere, aber nicht alle, Zustände eines Einflussfaktors
        auftreten, werden diese auch mit in die Analyse aufgenommen und zusammengefasst dargestellt (<span
          [dtHoverPopover]="examplePopover"
          dtHoverPopoverPosition="left"
          class="hover-text"
          >Beispiel</span
        >). Dies bedeutet in anderen Worten, dass in allen Szenarien stets genau einer dieser Zustände auftritt. Die angegebene
        Wahrscheinlichkeit entspricht in diesem Fall der Summe der entsprechenden Zustandswahrscheinlichkeiten.</p
      >
    </dt-extendable-header>

    <ng-template #scenariosPopover>
      <dt-note-hover>
        <p i18n>Angenommen Du hast die folgenden zwei Einflussfaktoren definiert:</p>
        <img src="assets/assistant/robustness-check_{{ languageService.isEnglish ? 'en' : 'de' }}.png" />
        <p i18n
          >Ein mögliches Szenario wäre in diesem Fall, dass die Zustände „Erfolg des Start-ups: erfolgreich“ und „Betriebsklima: schlecht“
          zusammen eintreten. Insgesamt gibt es in diesem Beispiel durch die Kombination aller Zustände insgesamt neun Szenarien.</p
        >
      </dt-note-hover>
    </ng-template>

    <ng-template #examplePopover>
      <dt-note-hover>
        <p i18n
          >Angenommen Du hast den Einflussfaktor „Erfolg des Start-ups“ mit den möglichen Zuständen „Scheitert“, „Mittel“, „Erfolgreich“ und
          „Sehr erfolgreich“ und eine Alternative „Start-up gründen“ angelegt. Wenn es nun der Fall ist, dass die Alternative auf dem ersten
          Platz landet, wenn die Zustände „Erfolgreich“ und „Sehr erfolgreich“ eintreten, dann werden diese Zustände aggregiert
          angezeigt.</p
        >
      </dt-note-hover>
    </ng-template>
  `,
  styleUrls: ['../../../hints.scss'],
})
export class RobustnessCheckHintsComponent {
  constructor(protected languageService: LanguageService) {}
}
