import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges, TrackByFunction } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { RobustnessWorkerResult } from '../../../../worker/robustness-check.service';

@Component({
  selector: 'dt-robustness-check-chart',
  templateUrl: './robustness-check-chart.component.html',
  styleUrls: ['./robustness-check-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RobustnessCheckChartComponent implements OnChanges {
  readonly barHeight = 40;
  readonly barSpace = 20;
  readonly chartHeightPercent = 100;

  @Input() result: RobustnessWorkerResult;
  @Input() selectedAlternatives: number[];
  @Input() expectedUtilityValues: number[]; // for ALL alternatives, not only the selected ones

  rankedResultIDs: number[];
  positions: number[][];
  scores: number[];
  resValues: number[][];

  // height of a row in the alternatives chart
  get totalBarHeight() {
    return this.barHeight + this.barSpace;
  }

  get resBotPadding() {
    return (this.totalBarHeight * (100 / this.chartHeightPercent - 1) * this.rankedResultIDs.length) / 2;
  }

  constructor(protected decisionData: DecisionData) {}

  ngOnChanges(_changes: SimpleChanges) {
    if (this.result && this.selectedAlternatives && this.expectedUtilityValues) {
      this.updateResult();
    }
  }

  private updateResult() {
    // calculate score (average position)
    this.scores = this.result.alternatives.map(row => row.positionDistribution.reduce((sum, v, i) => sum + v * (i + 1)));

    // calculate ranked ID mapping (displayed position -> calculationIDs idx)
    this.rankedResultIDs = this.scores
      .map((score, i) => [score, i])
      .sort((a, b) => a[0] - b[0])
      .map(v => v[1]);

    this.resValues = this.rankedResultIDs.map(cIdx => [
      this.expectedUtilityValues[this.selectedAlternatives[cIdx]],
      this.result.alternatives[cIdx].minUtility * 100,
      this.result.alternatives[cIdx].maxUtility * 100,
    ]);
    this.positions = this.rankedResultIDs.map(idx => this.result.alternatives[idx].positionDistribution);
  }

  readonly trackByIndex: TrackByFunction<any> = position => {
    return position;
  };
}
