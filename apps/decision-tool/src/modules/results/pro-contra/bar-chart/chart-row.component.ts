import { Component, Input } from '@angular/core';

import { Objective, ObjectiveType, Outcome } from '@entscheidungsnavi/decision-data/classes';
import { CellColor } from './chart-cell.component';

/**
 * This component shows one row of the bar-chart. Therefore, it shows the values
 * of one alternative in all the objectives.
 */
@Component({
  selector: 'dt-chart-row',
  styleUrls: ['./chart-row.component.css'],
  templateUrl: './chart-row.component.html',
})
export class ChartRowComponent {
  @Input() rowOfValues: number[];
  @Input() rowOfOutcomes: Outcome[];
  @Input() objectives: Objective[];
  @Input() maxValue: number; // limit of y-axis

  getOutcomeValueExtrema(extrema: 'min' | 'max', objective: Objective, outcome: Outcome): string {
    if (objective.objectiveType === ObjectiveType.Indicator) {
      const aggregationFunction = objective.indicatorData.aggregationFunction;
      const aggregatedValues = outcome.values.map(value => aggregationFunction(value));
      const minOrMaxIndicatorOutcomeValue = extrema === 'min' ? Math.min(...aggregatedValues) : Math.max(...aggregatedValues);
      return '' + minOrMaxIndicatorOutcomeValue.round(2);
    } else {
      const minOrMaxOutcomeValue = extrema === 'min' ? Math.min(...outcome.values[0][0]) : Math.max(...outcome.values[0][0]);

      if (objective.objectiveType === ObjectiveType.Verbal) {
        return objective.verbalData.options[minOrMaxOutcomeValue - 1];
      } else {
        return '' + minOrMaxOutcomeValue;
      }
    }
  }

  getCellText(objectiveIndex: number): string {
    const objective = this.objectives[objectiveIndex];
    const outcome = this.rowOfOutcomes[objectiveIndex];

    let outcomeText = '';

    if (outcome.processed) {
      const minValue = this.getOutcomeValueExtrema('min', objective, outcome);
      const maxValue = this.getOutcomeValueExtrema('max', objective, outcome);

      outcomeText += minValue;
      if (outcome.influenceFactor && maxValue !== minValue) {
        outcomeText += ' ' + $localize`bis` + ' ' + maxValue;
      }
    } else {
      outcomeText = '?';
    }

    // Append unit if possible.
    if (objective.objectiveType === ObjectiveType.Numerical && objective.numericalData.unit) {
      outcomeText += ' ' + objective.numericalData.unit;
    } else if (objective.objectiveType === ObjectiveType.Indicator) {
      outcomeText += ' ' + objective.indicatorData.aggregatedUnit;
    }

    return outcomeText;
  }

  getCellColor(objectiveIndex: number): CellColor {
    return this.rowOfValues[objectiveIndex] < 0 ? 'red' : 'green';
  }

  getRelativeValue(objectiveIndex: number) {
    return Math.abs(this.rowOfValues[objectiveIndex] / this.maxValue);
  }
}
