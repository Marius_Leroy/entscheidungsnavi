import { Component, Input } from '@angular/core';

export type CellColor = 'red' | 'green';

@Component({
  selector: 'dt-chart-cell',
  templateUrl: './chart-cell.component.html',
  styleUrls: ['./chart-cell.component.scss'],
})
export class ChartCellComponent {
  /**
   * A value between 0 and 1 that denotes how big the box is.
   */
  @Input() weightedUtilityValue: number;
  /**
   * The text to display below the box.
   */
  @Input() text: string;
  /**
   * Color of the box.
   */
  @Input() color: CellColor;

  get inverted(): boolean {
    return this.color === 'red';
  }
}
