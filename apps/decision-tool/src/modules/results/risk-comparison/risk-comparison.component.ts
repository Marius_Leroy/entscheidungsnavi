import { ChangeDetectorRef, Component, Input, OnInit, inject } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { range, sum } from 'lodash';
import { Point, RiskProfileChartSeries } from '@entscheidungsnavi/widgets';
import { Observable, takeUntil, throttleTime } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { NavLine, Navigation, navLineElement } from '../../shared/navline';
import { HelpMenuProvider } from '../../../app/help/help';
import { LanguageService } from '../../../app/data/language.service';
import { getHelpMenu } from '../help';
import { RobustnessCheckService, RobustnessWorkerResult } from '../../../worker/robustness-check.service';
import { ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT } from '../../../worker/robustness-check-messages';

@Component({
  selector: 'dt-risk-comparison',
  templateUrl: './risk-comparison.component.html',
  styleUrls: ['./risk-comparison.component.scss'],
  providers: [RobustnessCheckService],
})
export class RiskComparisonComponent implements Navigation, HelpMenuProvider, OnInit {
  // When the maximum change in any value of the position distribution per iteration drops
  // below this threshold, we consider the calculation "done".
  private readonly changeThreshold = 1e-7;

  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() showTitle = true;

  private languageService = inject(LanguageService);
  protected decisionData = inject(DecisionData);
  private robustnessCheckService = inject(RobustnessCheckService);

  private cdRef = inject(ChangeDetectorRef);

  readonly navLine = new NavLine({ left: [navLineElement().back('/results').build()] });
  readonly helpMenu = getHelpMenu(this.languageService.steps);

  currentResult: RobustnessWorkerResult;
  dataSeries: RiskProfileChartSeries[] = [];
  isPaused: boolean;
  isDone: boolean;
  private lastUpdateTime: number;
  elapsedTime: number;

  xLabels = range(9).map(index => ((index / 8) * 100).toString());
  selectedAlternativeIndices = range(this.decisionData.alternatives.length);

  get state(): 'done' | 'calculating' | 'paused' {
    if (!this.isPaused) return 'calculating';
    else if (this.isDone) return 'done';
    else return 'paused';
  }

  ngOnInit() {
    this.robustnessCheckService.onUpdate$
      .pipe(throttleTime(1_000, undefined, { leading: true, trailing: true }), takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.updateResults();
        this.cdRef.detectChanges();
      });

    this.isPaused = false;
    this.isDone = false;
    this.lastUpdateTime = performance.now();
    this.elapsedTime = 0;

    // Include all uncertainties and all alternatives
    this.robustnessCheckService.startWorker({
      selectedAlternatives: range(this.decisionData.alternatives.length),
      parameters: {
        utilityFunctions: true,
        probabilities: true,
        objectiveWeights: true,
        influenceFactorScenarios: {
          predefined: true,
          userdefinedIds: range(this.decisionData.influenceFactors.length),
        },
      },
    });
  }

  pauseCalculation() {
    if (this.currentResult.iterationCount === 0) return;

    this.robustnessCheckService.pauseWorker();
    this.isPaused = true;
    this.updateResults();
  }

  resumeCalculation() {
    this.robustnessCheckService.resumeWorker();
    this.lastUpdateTime = performance.now();
    this.isPaused = false;
  }

  updateResults() {
    const newResult = this.robustnessCheckService.getCurrentResult();
    const newIterations = newResult.iterationCount - (this.currentResult?.iterationCount ?? 0);

    // Compute the change per iteration and pause if required
    if (newIterations > 0 && this.currentResult != null && !this.isDone) {
      const maxChange = Math.max(
        ...newResult.alternatives.flatMap((alternativeResult, alternativeIndex) =>
          alternativeResult.utilityHistogram.map((value, valueIndex) =>
            Math.abs(
              value / newResult.iterationCount -
                this.currentResult.alternatives[alternativeIndex].utilityHistogram[valueIndex] / this.currentResult.iterationCount
            )
          )
        )
      );

      if (maxChange / newIterations < this.changeThreshold) {
        this.isDone = true;
        this.pauseCalculation();
        return;
      }
    }

    this.currentResult = newResult;
    this.dataSeries = [];

    for (const alternativeIndex of this.selectedAlternativeIndices) {
      const histogram = newResult.alternatives[alternativeIndex].utilityHistogram;
      const totalCount = sum(histogram);

      const pointSeries: Point[] = [{ x: 0, y: 1 }];

      for (let binIndex = 0; binIndex < histogram.length; binIndex++) {
        if (histogram[binIndex] === 0) continue;

        pointSeries.push({
          // We take the x value at the midpoint of the bin
          x: (binIndex + 0.5) / ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT,
          y: pointSeries.at(-1).y - histogram[binIndex] / totalCount,
        });
      }

      pointSeries.push({ x: 1, y: 0 });

      this.dataSeries.push({
        seriesLabel: this.decisionData.alternatives[alternativeIndex].name,
        dataPoints: pointSeries,
      });
    }

    const currentUpdateTime = performance.now();
    this.elapsedTime += currentUpdateTime - this.lastUpdateTime;
    this.lastUpdateTime = currentUpdateTime;
  }
}
