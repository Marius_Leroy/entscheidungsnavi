import { inject } from '@angular/core';
import { CanActivateFn, createUrlTreeFromSnapshot } from '@angular/router';
import { inRange } from 'lodash';
import { DecisionData } from '@entscheidungsnavi/decision-data';

export const utilityFunctionDetailedGuard: CanActivateFn = (route, _state) => {
  const decisionData = inject(DecisionData);
  const activeIdx = +route.params['objectiveId'];

  if (isNaN(activeIdx) || !inRange(activeIdx, 0, decisionData.objectives.length)) {
    // Go back to the overview page
    return createUrlTreeFromSnapshot(route, ['../..']);
  }

  return true;
};
