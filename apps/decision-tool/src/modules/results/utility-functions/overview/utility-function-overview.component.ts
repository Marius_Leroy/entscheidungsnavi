import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { ObjectiveType, Objective, UtilityFunction } from '@entscheidungsnavi/decision-data/classes';
import { isEqual } from 'lodash';
import { CurrentProgressService } from '../../../../app/data/current-progress.service';
import { UtilityFunctionElementNumericalOrIndicatorComponent, UtilityFunctionElementVerbalComponent } from '../main/elements';

@Component({
  templateUrl: './utility-function-overview.component.html',
  styleUrls: ['./utility-function-overview.component.scss'],
})
export class UtilityFunctionOverviewComponent implements OnInit {
  constructor(protected decisionData: DecisionData, private progressService: CurrentProgressService) {}

  private backupUtility: Array<[number, number] | [number, number, boolean, number[]]>;

  @ViewChildren(UtilityFunctionElementNumericalOrIndicatorComponent)
  numericalElements: QueryList<UtilityFunctionElementNumericalOrIndicatorComponent>;

  @ViewChildren(UtilityFunctionElementVerbalComponent)
  verbalElements: QueryList<UtilityFunctionElementVerbalComponent>;

  ngOnInit() {
    if (this.decisionData.resultSubstepProgress == null) {
      this.decisionData.resultSubstepProgress = 0;
      this.progressService.update();
    }
    this.saveBackup();
  }

  saveBackup() {
    this.backupUtility = [];

    this.decisionData.objectives.forEach(objective => {
      if (objective.isVerbal) {
        this.backupUtility.push([
          objective.verbalData.c,
          objective.verbalData.precision,
          objective.verbalData.hasCustomUtilityValues,
          objective.verbalData.utilities.slice(),
        ]);
      } else {
        const utilityFunction = this.getUtilityFunction(objective);

        this.backupUtility.push([utilityFunction.c, utilityFunction.precision]);
      }
    });
  }

  hasChanged(objectiveIndex: number) {
    const objective = this.decisionData.objectives[objectiveIndex];

    if (objective.isVerbal) {
      const bC = this.backupUtility[objectiveIndex][0];
      const bAcc = this.backupUtility[objectiveIndex][1];
      const bCustom = this.backupUtility[objectiveIndex][2];
      const bUtility = this.backupUtility[objectiveIndex][3];

      return (
        bC !== objective.verbalData.c ||
        bAcc !== objective.verbalData.precision ||
        objective.verbalData.hasCustomUtilityValues !== bCustom ||
        !isEqual(bUtility, objective.verbalData.utilities)
      );
    } else {
      const bC = this.backupUtility[objectiveIndex][0];
      const bAcc = this.backupUtility[objectiveIndex][1];

      const utilityFunction = this.getUtilityFunction(objective);

      return bC !== utilityFunction.c || bAcc !== utilityFunction.precision;
    }
  }

  restore(event: MouseEvent, objectiveIndex: number) {
    event.stopImmediatePropagation();
    const objective = this.decisionData.objectives[objectiveIndex];

    if (objective.isVerbal) {
      objective.verbalData.c = this.backupUtility[objectiveIndex][0];
      objective.verbalData.precision = this.backupUtility[objectiveIndex][1];
      objective.verbalData.hasCustomUtilityValues = this.backupUtility[objectiveIndex][2];

      objective.verbalData.utilities = this.backupUtility[objectiveIndex][3].slice();

      if (!objective.verbalData.hasCustomUtilityValues) {
        objective.verbalData.adjustUtilityToFunction();
      }
    } else {
      const utilityFunction = this.getUtilityFunction(objective);

      utilityFunction.c = this.backupUtility[objectiveIndex][0];
      utilityFunction.precision = this.backupUtility[objectiveIndex][1];

      this.numericalElements.find(item => item.objective === objective).onChange();
    }
  }

  getUtilityFunction(ziel: Objective): UtilityFunction {
    switch (ziel.objectiveType) {
      case ObjectiveType.Numerical:
        return ziel.numericalData.utilityfunction;
      case ObjectiveType.Indicator:
        return ziel.indicatorData.utilityfunction;
      default:
        throw new Error(`Unsupported objective type: ${ziel.objectiveType}`);
    }
  }

  getExplanation(objective: Objective): string {
    switch (objective.objectiveType) {
      case ObjectiveType.Numerical:
        return objective.numericalData.utilityfunction.explanation;
      case ObjectiveType.Verbal:
        return objective.verbalData.utilityFunctionExplanation;
      case ObjectiveType.Indicator:
        return objective.indicatorData.utilityfunction.explanation;
    }
  }

  setExplanation(objective: Objective, explanation: string) {
    switch (objective.objectiveType) {
      case ObjectiveType.Numerical:
        objective.numericalData.utilityfunction.explanation = explanation;
        break;
      case ObjectiveType.Verbal:
        objective.verbalData.utilityFunctionExplanation = explanation;
        break;
      case ObjectiveType.Indicator:
        objective.indicatorData.utilityfunction.explanation = explanation;
        break;
    }
  }
}
