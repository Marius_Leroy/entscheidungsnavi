import { cloneDeep } from 'lodash';
import { ObjectiveInput } from '@entscheidungsnavi/decision-data/classes';
import { InfluenceFactorSA } from './influence-factor-sa';

export class OutcomeSA {
  // SA = Sensitivity Analysis
  public values: ObjectiveInput[];
  public initValues: ObjectiveInput[]; // constant value in sensitivity analysis
  public influenceFactor: InfluenceFactorSA;

  constructor(values: ObjectiveInput[], influenceFactor?: InfluenceFactorSA) {
    this.values = cloneDeep(values);
    this.initValues = cloneDeep(values);
    this.influenceFactor = influenceFactor;
  }

  public resetValue() {
    this.values = cloneDeep(this.initValues);
  }

  public valueChanged(): boolean {
    return !this.values.every((objectiveInput, i) => {
      if (Array.isArray(objectiveInput)) {
        return objectiveInput.every((numberValue, j) => numberValue === this.initValues[i][j]);
      }
      return objectiveInput === this.initValues[i];
    });
  }
}
