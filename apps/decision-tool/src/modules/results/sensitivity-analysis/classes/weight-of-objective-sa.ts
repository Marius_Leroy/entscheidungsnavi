import { Interval } from '@entscheidungsnavi/decision-data/classes';
import * as calculation from './../calculation/sensitivity-analysis';

export class WeightOfObjectiveSA {
  // SA = Sensitivity Analysis
  public valueRangeLimits = new Interval(0, 100);
  public value: number;
  public shownValue: number;
  public initValue: number; // constant value in sensitivity analysis
  public changedValue: number;
  public precision: number; // constant value in sensitivity analysis
  public precisionInterval: Interval; // constant value in sensitivity analysis

  constructor(value: number, precision: number, shownValue?: number) {
    this.value = value;
    this.shownValue = shownValue;
    this.initValue = value;
    this.precision = precision;
    this.precisionInterval = calculation.getPrecisionInterval(this.valueRangeLimits, value, precision);
  }

  public resetValue() {
    this.value = this.initValue;
    this.changedValue = null;
  }

  public valueChanged(): boolean {
    return this.value !== this.initValue;
  }
}
