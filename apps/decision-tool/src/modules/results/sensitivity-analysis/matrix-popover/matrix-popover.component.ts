import { Component, Input } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Alternative, Objective, Outcome } from '@entscheidungsnavi/decision-data/classes';

@Component({
  selector: 'dt-matrix-popover',
  templateUrl: './matrix-popover.component.html',
  styleUrls: ['./matrix-popover.component.scss'],
})
export class MatrixPopoverComponent {
  @Input() highlightOutcomes: Outcome[];

  objectives: Objective[];
  alternatives: Alternative[];
  outcomes: Outcome[][];

  constructor(decisionData: DecisionData) {
    this.objectives = decisionData.objectives;
    this.alternatives = decisionData.alternatives;
    this.outcomes = decisionData.outcomes;
  }
}
