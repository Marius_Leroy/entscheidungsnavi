import { Component, Input, OnInit } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import '@entscheidungsnavi/tools/number_rounding';
import { getAlternativeUtilities } from '@entscheidungsnavi/decision-data/calculation';
import { CurrentProgressService } from '../../../app/data/current-progress.service';
import { HelpMenuProvider } from '../../../app/help/help';
import { ExplanationService } from '../../shared/decision-quality';
import { getHelpMenu } from '../help';
import { NavLine, navLineElement, Navigation } from '../../shared/navline';
import { LanguageService } from '../../../app/data/language.service';

@Component({
  selector: 'dt-results-main',
  templateUrl: './results-overview.component.html',
  styleUrls: ['./results-overview.component.scss', '../../hints.scss'],
})
export class ResultsOverviewComponent implements OnInit, Navigation, HelpMenuProvider {
  @Input()
  showStepNumber = true;

  navLine = new NavLine({
    left: [navLineElement().back('/bewertung/zielgewichtung').build()],
    middle: [
      navLineElement()
        .label($localize`Sensitivitätsanalyse`)
        .link('/ergebnis/auswertung-sensitivitaetsanalyse')
        .cypressId('sensitivity-analysis')
        .build(),
      navLineElement()
        .label($localize`Pros und Kontras`)
        .link('/ergebnis/auswertung-pro-contra')
        .cypressId('pro-contra')
        .build(),
      navLineElement()
        .label($localize`Robustheitstest`)
        .condition(() => this.decisionData.projectMode === 'educational')
        .link(() => (this.robustnessCheckEnabled ? '/results/robustness-check' : null))
        .disabled(() => !this.robustnessCheckEnabled)
        .tooltip(() =>
          this.robustnessCheckEnabled
            ? undefined
            : $localize`Der Robustheitstest funktioniert nur, wenn Präzisionsintervalle eingestellt oder Einflussfaktoren verwendet wurden`
        )
        .cypressId('robustness-check')
        .build(),
      navLineElement()
        .label($localize`Risikovergleich`)
        .condition(() => this.decisionData.projectMode === 'educational')
        .link('/results/risk-comparison')
        .cypressId('risk-comparison')
        .build(),
      this.explanationService.generateAssessmentButton('LOGICALLY_CORRECT_REASONING'),
    ],
    right: [
      navLineElement()
        .continue('/finishproject')
        .tooltip($localize`Projekt abschließen`)
        .cypressId('finish-project')
        .build(),
    ],
  });

  utilityValues: number[];
  weightedUtilityMatrix: number[][];

  helpMenu = getHelpMenu(this.languageService.steps);

  robustnessCheckEnabled = false;

  constructor(
    private currentProgressService: CurrentProgressService,
    private languageService: LanguageService,
    private decisionData: DecisionData,
    private explanationService: ExplanationService
  ) {}

  ngOnInit() {
    if ((this.decisionData.resultSubstepProgress ?? 0) < 2) {
      this.decisionData.resultSubstepProgress = 2;
      this.currentProgressService.update();
    }
    this.updateAlternativenRanking();

    this.robustnessCheckEnabled = Object.values(this.decisionData.getInaccuracies()).some(isInaccurate => isInaccurate);
  }

  updateAlternativenRanking() {
    this.weightedUtilityMatrix = this.decisionData.getWeightedUtilityMatrix();
    this.utilityValues = getAlternativeUtilities(this.weightedUtilityMatrix);
  }
}
