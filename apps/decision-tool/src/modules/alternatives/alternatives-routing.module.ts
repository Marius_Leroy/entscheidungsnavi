import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EducationalModeGuard } from '../../app/guards';
import { AlternativesComponent } from './main/alternatives.component';
import {
  AlternativeHint1Component,
  AlternativeHint2Component,
  AlternativeHint3Component,
  AlternativeHint4Component,
  AlternativeHint5Component,
  AlternativeHint6Component,
  AlternativeHint7Component,
} from './hints';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: AlternativesComponent,
  },
  {
    path: 'hints',
    redirectTo: 'steps',
  },
  {
    path: 'steps',
    canActivate: [EducationalModeGuard],
    data: {
      educationalModeRedirect: '/alternatives',
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '1',
      },
      {
        path: '1',
        component: AlternativeHint1Component,
      },
      {
        path: '2',
        component: AlternativeHint2Component,
      },
      {
        path: '3',
        component: AlternativeHint3Component,
      },
      {
        path: '4',
        component: AlternativeHint4Component,
      },
      {
        path: '5',
        component: AlternativeHint5Component,
      },
      {
        path: '6',
        component: AlternativeHint6Component,
      },
      {
        path: '7',
        component: AlternativeHint7Component,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlternativesRoutingModule {}
