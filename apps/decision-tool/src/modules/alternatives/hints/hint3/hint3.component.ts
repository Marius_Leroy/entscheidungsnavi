import { Component, Injector, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Alternative, NotePage } from '@entscheidungsnavi/decision-data/classes';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractAlternativeHintComponent } from '../alternative-hint.component';
import { AlternativeListComponent } from '../../alternative-list/alternative-list.component';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  selector: 'dt-alternative-hint-3',
  templateUrl: './hint3.component.html',
  styleUrls: ['./hint3.component.scss'],
})
@DisplayAtMaxWidth
export class AlternativeHint3Component extends AbstractAlternativeHintComponent implements OnInit {
  @Input()
  showTitle = true;

  public newAlternative = '';

  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Weitere Hinweise`)
          .component(Help2Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 3`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }

  @ViewChild('notify', { static: true }) private notifyRef: TemplateRef<any>;
  @ViewChild(AlternativeListComponent) alternativeList: AlternativeListComponent;

  aspects: Array<ReadonlyArray<string>>;

  constructor(injector: Injector, private snackBar: MatSnackBar) {
    super(injector);
    this.pageKey = 3;
  }

  override ngOnInit() {
    super.ngOnInit();

    this.aspects = this.decisionData.objectives.map(o => o.getAspectsAsArray());

    if (!this.decisionData.hintAlternatives.ideas) {
      this.decisionData.hintAlternatives.initIdeas();
    } else {
      while (this.decisionData.hintAlternatives.ideas.noteGroups.length < this.decisionData.objectives.length) {
        console.log('adding missing group...');
        this.decisionData.hintAlternatives.ideas.addNoteGroup();
      }
    }
  }

  get ideas(): NotePage {
    return this.decisionData.hintAlternatives.ideas;
  }

  addAlternative() {
    if (this.newAlternative) {
      this.decisionData.addAlternative({ alternative: new Alternative(this.pageKey, this.newAlternative) });
      this.newAlternative = '';
      this.alternativeList.updateAlternatives();

      this.snackBar.openFromTemplate(this.notifyRef, { duration: 3000 });
    }
  }
}
