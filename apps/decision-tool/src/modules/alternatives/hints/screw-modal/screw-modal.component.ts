import { Component, Inject, OnInit, TrackByFunction } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Screw, ScrewStateOp } from '@entscheidungsnavi/decision-data/steps';
import { ConfirmModalComponent } from '@entscheidungsnavi/widgets';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { filter } from 'rxjs';

export const SCREW_DEFAULT_NAME = $localize`Unbenannter Stellhebel`;
export const SCREW_STATE_DEFAULT_NAMES = [$localize`Unbenannter Zustand`, $localize`Unbenannter Zustand`] as const;

export type ScrewModalData = {
  screwIndex: number;
  allowScrewDeletion: boolean;
};

export type ScrewModalResultData =
  | {
      shouldScrewBeDeleted: false;
      editedScrew: Screw;
      executedOperationsOnScrew: ScrewStateOp[];
    }
  | { shouldScrewBeDeleted: true };

@Component({
  templateUrl: './screw-modal.component.html',
  styleUrls: ['./screw-modal.component.scss'],
})
export class ScrewModalComponent implements OnInit {
  readonly minNumberOfStates = 2;
  readonly maxNumberOfStates = 8;

  private executedOperationsOnScrew: ScrewStateOp[] = [];

  readonly screwIndex: number;
  readonly screwDeletionAllowed: boolean;

  editedScrew: Screw;

  readonly screwDefaultName = SCREW_DEFAULT_NAME;

  constructor(
    protected decisionData: DecisionData,
    public matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) data: ScrewModalData,
    public dialogRef: MatDialogRef<ScrewModalComponent, ScrewModalResultData>,
    private dialog: MatDialog
  ) {
    this.screwIndex = data.screwIndex;
    this.screwDeletionAllowed = data.allowScrewDeletion;
  }

  ngOnInit() {
    const currentScrew = this.decisionData.hintAlternatives.screws[this.screwIndex];
    this.editedScrew = currentScrew.clone();
  }

  addState(stateIndex: number) {
    this.editedScrew.states.splice(stateIndex, 0, '');
    this.executedOperationsOnScrew.push({
      opCode: 'addition',
      stateIndex,
    });
    this.focusField(stateIndex);
  }

  stateDrop(event: CdkDragDrop<string[]>) {
    this.moveStates(event.previousIndex, event.currentIndex);
  }

  moveStates(fromStateIndex: number, toStateIndex: number) {
    moveItemInArray(this.editedScrew.states, fromStateIndex, toStateIndex);
    this.executedOperationsOnScrew.push({
      opCode: 'move',
      fromStateIndex,
      toStateIndex,
    });
  }

  deleteState(stateIndex: number) {
    this.editedScrew.states.splice(stateIndex, 1);
    this.executedOperationsOnScrew.push({
      opCode: 'deletion',
      stateIndex,
    });
  }

  deleteScrew() {
    this.closeScrewModal(true, true);
  }

  saveScrew() {
    if (this.isEditedScrewValid) {
      this.cleanEmptyFields();
      this.closeScrewModal(true);
    }
  }

  cleanEmptyFields() {
    for (let stateIndex = 0; stateIndex < this.editedScrew.states.length; stateIndex++) {
      if (!this.editedScrew.states[stateIndex]) {
        this.deleteState(stateIndex);
        stateIndex--;
      }
    }
  }

  get numberOfNonEmptyStates() {
    return this.editedScrew.states.filter(Boolean).length;
  }

  get isEditedScrewValid() {
    return (
      this.editedScrew.name.length > 0 &&
      this.numberOfNonEmptyStates >= this.minNumberOfStates &&
      this.editedScrew.states.length <= this.maxNumberOfStates
    );
  }

  tryCloseScrewModal() {
    const originalScrew = this.decisionData.hintAlternatives.screws[this.screwIndex];
    if (this.editedScrew.isEqual(originalScrew)) {
      this.closeScrewModal(false);
    } else {
      this.dialog
        .open(ConfirmModalComponent, {
          data: {
            title: $localize`Ungespeicherte Änderungen`,
            prompt: $localize`Bist Du sicher, dass Du die ungespeicherten Änderungen verwerfen willst?`,
            buttonConfirm: $localize`Verwerfen`,
            buttonDeny: $localize`Abbrechen`,
          },
        })
        .afterClosed()
        .pipe(filter(Boolean))
        .subscribe(() => this.closeScrewModal(false));
    }
  }

  closeScrewModal(shouldApplyChanges: boolean, shouldScrewBeDeleted = false) {
    if (shouldScrewBeDeleted) {
      this.dialogRef.close({
        shouldScrewBeDeleted: true,
      });
    } else {
      this.dialogRef.close({
        shouldScrewBeDeleted: false,
        editedScrew: shouldApplyChanges ? this.editedScrew : this.decisionData.hintAlternatives.screws[this.screwIndex],
        executedOperationsOnScrew: shouldApplyChanges ? this.executedOperationsOnScrew : [],
      });
    }
  }

  focusField(stateIndex: number) {
    setTimeout(() => {
      const inputElements = document.querySelectorAll('.screw-state');
      (inputElements[stateIndex] as HTMLElement)?.focus();
    }, 50);
  }

  readonly trackByIndex: TrackByFunction<any> = position => {
    return position;
  };
}
