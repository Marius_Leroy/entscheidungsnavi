import { ChangeDetectorRef, Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { Alternative } from '@entscheidungsnavi/decision-data/classes';
import { HintAlternatives, Screw } from '@entscheidungsnavi/decision-data/steps';
import { MatDialog } from '@angular/material/dialog';
import { ScrewConfiguration } from '@entscheidungsnavi/decision-data/interfaces';
import { helpPage } from '../../../../app/help/help';
import { HelpBackgroundComponent } from '../help-background/help-background.component';
import { AbstractAlternativeHintComponent } from '../alternative-hint.component';
import { AlternativeListComponent } from '../../alternative-list/alternative-list.component';
import { DisplayAtMaxWidth } from '../../../../app/interfaces/display-at-max-width';
import {
  SCREW_DEFAULT_NAME,
  SCREW_STATE_DEFAULT_NAMES,
  ScrewModalComponent,
  ScrewModalData,
  ScrewModalResultData,
} from '../screw-modal/screw-modal.component';
import { Help1Component } from './help1/help1.component';
import { Help2Component } from './help2/help2.component';

@Component({
  selector: ' dt-alternative-hint-5',
  templateUrl: './hint5.component.html',
  styleUrls: ['./hint5.component.scss'],
})
@DisplayAtMaxWidth
export class AlternativeHint5Component extends AbstractAlternativeHintComponent implements OnInit {
  get helpMenu() {
    return {
      educational: [
        helpPage()
          .name($localize`So funktioniert's`)
          .component(Help1Component)
          .build(),
        helpPage()
          .name($localize`Weitere Hinweise`)
          .component(Help2Component)
          .build(),
        helpPage()
          .name($localize`Hintergrundwissen zum Schritt 3`)
          .component(HelpBackgroundComponent)
          .build(),
      ],
    };
  }
  @ViewChild(AlternativeListComponent) alternativeList: AlternativeListComponent;

  @Input()
  showTitle = true;

  screws: Screw[];

  screwConfigurationForNewAlternative: ScrewConfiguration;

  nameForNewAlternative = '';

  screwEditModalOpened = false;

  constructor(private cdr: ChangeDetectorRef, injector: Injector, public matDialog: MatDialog) {
    super(injector);
    this.pageKey = 5;
  }

  override ngOnInit() {
    super.ngOnInit();

    this.screws = this.decisionData.hintAlternatives.screws;
    while (this.screws.length < 2) {
      this.decisionData.hintAlternatives.appendScrew(SCREW_DEFAULT_NAME, SCREW_STATE_DEFAULT_NAMES);
      this.decisionData.hintAlternatives.appendScrew(SCREW_DEFAULT_NAME, SCREW_STATE_DEFAULT_NAMES);
    }

    // Projects that used the older screws do not have screw names. Patch that.
    this.screws.filter(screw => !screw.name).forEach(screw => (screw.name = SCREW_DEFAULT_NAME));

    this.screwConfigurationForNewAlternative = new Array(this.decisionData.hintAlternatives.screws.length).fill(null);
  }

  appendScrew() {
    this.decisionData.hintAlternatives.appendScrew(SCREW_DEFAULT_NAME, SCREW_STATE_DEFAULT_NAMES);
    HintAlternatives.adjustScrewConfigurationOnAppendedScrew(this.screwConfigurationForNewAlternative);
  }

  moveScrew(screwIndex: number, destinationScrewIndex: number) {
    this.decisionData.hintAlternatives.moveScrew(screwIndex, destinationScrewIndex);
    HintAlternatives.adjustScrewConfigurationOnScrewMove(this.screwConfigurationForNewAlternative, screwIndex, destinationScrewIndex);

    this.cdr.detectChanges();
  }

  editOrDeleteScrew(screwIndex: number) {
    this.screwEditModalOpened = true;

    const screwModalRef = this.matDialog.open<ScrewModalComponent, ScrewModalData, ScrewModalResultData>(ScrewModalComponent, {
      data: {
        screwIndex,
        allowScrewDeletion: this.screws.length > 2,
      },
    });

    screwModalRef.afterClosed().subscribe(screwEditResult => {
      if (screwEditResult.shouldScrewBeDeleted === true) {
        this.decisionData.hintAlternatives.deleteScrew(screwIndex);
        HintAlternatives.adjustScrewConfigurationOnScrewDeletion(this.screwConfigurationForNewAlternative, screwIndex);
      } else {
        this.decisionData.hintAlternatives.updateScrew(screwIndex, screwEditResult.editedScrew, screwEditResult.executedOperationsOnScrew);
        HintAlternatives.adjustScrewConfigurationOnUpdatedScrew(
          this.screwConfigurationForNewAlternative,
          screwIndex,
          screwEditResult.executedOperationsOnScrew,
          this.decisionData.hintAlternatives.screws[screwIndex].states.length
        );
      }

      this.screwEditModalOpened = false;
    });
  }

  addAlternative() {
    if (this.nameForNewAlternative) {
      this.decisionData.addAlternative({
        alternative: new Alternative(this.pageKey, this.nameForNewAlternative, '', [], this.screwConfigurationForNewAlternative),
      });

      this.nameForNewAlternative = '';
      this.alternativeList.updateAlternatives();
    }
  }
}
