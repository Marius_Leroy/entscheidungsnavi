import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Alternative } from '@entscheidungsnavi/decision-data/classes';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmModalComponent, ConfirmModalData } from '@entscheidungsnavi/widgets';
import { filter, of } from 'rxjs';
import { PersistentSetting, PersistentSettingParent } from '../../../app/data/persistent-setting';

interface AlternativeMetadata {
  expanded: boolean;
  parentOfVisibleChild: boolean; // Whether one of the children is visible
  hidden: boolean; // Whether the alternative itself is hidden
  shown: boolean; // Whether the alternative is shown in the objective-list anyway (either not hidden or parent of visible child)
  childrenLocked: boolean[];
}

@Component({
  selector: 'dt-alternative-list',
  templateUrl: './alternative-list.component.html',
  styleUrls: ['./alternative-list.component.scss'],
})
@PersistentSettingParent('AlternativeList')
export class AlternativeListComponent implements OnInit {
  @Input() pageKey = 0;
  @Input() collapsedByDefault = false;
  @Input() showAddButton = true;
  @Input() showSendButton = false;
  @Input() readonly = false;

  @PersistentSetting()
  filter: 'all' | 'onlyCurrent' | 'currentAndPrevious' = 'all';

  alternativeList: Alternative[];
  alternativeMetadata = new Map<Alternative, AlternativeMetadata>();

  @Output() dirty = new EventEmitter(); // Emit for any changes that aren't detected by HTML change event

  @Output() notifyReadonly = new EventEmitter(); // Emit when the user tries to modify the readonly content

  constructor(private dialog: MatDialog, protected decisionData: DecisionData) {}

  ngOnInit() {
    this.updateAlternatives();
  }

  updateAlternatives() {
    this.decisionData.alternatives.forEach(alternative => {
      const expanded = this.alternativeMetadata.get(alternative)?.expanded ?? !this.collapsedByDefault;

      const parentOfVisibleChild = this.isParentOfVisibleChild(alternative);
      const hidden = this.isHidden(alternative);

      this.alternativeMetadata.set(alternative, {
        expanded,
        parentOfVisibleChild,
        hidden,
        shown: !hidden || parentOfVisibleChild,
        childrenLocked: alternative.children.map(child => this.isHidden(child)),
      });
    });

    this.alternativeList = this.decisionData.alternatives.filter(alternative => this.alternativeMetadata.get(alternative).shown);
  }

  add(alternative: Alternative = new Alternative(this.pageKey), position: number = this.alternativeList.length) {
    position = this.calculateRealPosition(position); // needed when filtering out hidden alternatives

    this.dirty.emit();

    this.decisionData.addAlternative({ alternative, position });

    this.updateAlternatives();
  }

  move(from: number, to: number) {
    from = this.calculateRealPosition(from); // needed when filtering out hidden alternatives
    to = this.calculateRealPosition(to); // needed when filtering out hidden alternatives

    this.dirty.emit();

    this.decisionData.moveAlternative(from, to);

    this.updateAlternatives();
  }

  delete(position: number) {
    const alternative = this.alternativeList[position];
    position = this.calculateRealPosition(position); // needed when filtering out hidden alternatives

    this.dirty.emit();

    this.decisionData.removeAlternative(position);

    this.alternativeMetadata.delete(alternative);
    this.updateAlternatives();
  }

  private calculateRealPosition(position: number) {
    let visibleCounter = 0;
    for (let i = 0; i < this.decisionData.alternatives.length; i++) {
      if (this.alternativeMetadata.get(this.decisionData.alternatives[i]).shown) {
        visibleCounter++;
      }
      if (visibleCounter === position + 1) {
        return i;
      }
    }
    return undefined;
  }

  group(i1: number, i2: number) {
    // Check whether the alternative that is being grouped in has any outcomes associated with it
    const groupedAlternativeRealPosition = this.calculateRealPosition(i1);
    const hasNonEmptyOutcome = this.decisionData.outcomes[groupedAlternativeRealPosition].some(outcome => !outcome.isEmpty);

    (hasNonEmptyOutcome
      ? this.dialog
          .open(ConfirmModalComponent, {
            data: {
              title: $localize`Gruppieren bestätigen`,
              prompt: $localize`Durch Eingruppieren der Alternative „${this.alternativeList[i1].name}“ werden
              etwaige Einträge im Wirkungsmodell für diese Alternative unwiderruflich verloren gehen.<br>
              Bist Du sicher, dass Du diese Alternative eingruppieren möchtest?`,
              buttonConfirm: $localize`Ja, eingruppieren`,
              buttonDeny: $localize`Nein, abbrechen`,
            } as ConfirmModalData,
          })
          .afterClosed()
      : of(true)
    )
      .pipe(filter(Boolean))
      .subscribe(() => {
        this.dirty.emit();

        const a1 = this.alternativeList[i1];
        const a2 = this.alternativeList[i2];

        a2.addChild(a1);
        this.delete(i1);

        this.alternativeMetadata.get(a2).expanded = true;
        this.updateAlternatives();
      });
  }

  /**
   * Move a child between alternatives. It stays a child.
   */
  moveChild(from: number, fromChild: number, to: number) {
    this.dirty.emit();

    const parent1 = this.alternativeList[from];
    const parent2 = this.alternativeList[to];
    const child = parent1.children[fromChild];

    parent1.removeChild(fromChild);
    parent2.addChild(child);

    if (!parent1.isGroup() && !parent1.name) {
      // delete the empty alternative
      this.delete(from);

      if (from < to) {
        to--;
      }
    }

    this.alternativeMetadata.get(parent2).expanded = true;
    this.updateAlternatives();
  }

  /**
   * Extract the child from its parent and make it into its own alternative.
   */
  extractChild(from: number, fromChild: number, to: number) {
    this.dirty.emit();

    const parent1 = this.alternativeList[from];
    const child = parent1.children[fromChild];
    parent1.removeChild(fromChild);

    this.add(child, to);
  }

  getNamePlaceholder(index: number) {
    if (this.decisionData.isStarter() && index === 0) {
      return $localize`Meine erste Handlungsalternative`;
    } else if (this.decisionData.isStarter() && index === 1) {
      return $localize`Eine weitere Handlungsalternative`;
    } else {
      return $localize`Name der Alternative`;
    }
  }

  getNotePlaceholder(index: number) {
    if (this.decisionData.isStarter() && index === 0) {
      // eslint-disable-next-line max-len
      return $localize`Ersetze „Meine erste Handlungsalternative“ links durch eine Handlungsoption, für die Du Dich entscheiden kannst und die Dir als Erstes einfällt. Dieses weiße Feld kannst Du für Erläuterungen verwenden, was Du genau unter der Handlungsalternative verstehst.`;
    } else if (this.decisionData.isStarter() && index === 1) {
      // eslint-disable-next-line max-len
      return $localize`Überlege, welche Option für Dich sonst noch in Frage kommt und trage diese links ein.\n\nDu kannst anschließend gerne weitere Alternativen hinzufügen.`;
    } else {
      return $localize`Platz für Erläuterungen`;
    }
  }

  private isParentOfVisibleChild(alternative: Alternative) {
    // check if alternative should be visible because of a child that is not hidden
    for (const child of alternative.children) {
      if (!this.isHidden(child)) {
        return true;
      }
    }
    return false;
  }

  private isHidden(alternative: Alternative) {
    if (this.pageKey === 0 || this.decisionData.isProfessional()) {
      return false; // all alternatives visible at RESULT
    }
    switch (this.filter) {
      case 'all':
        return false; // all alternatives, neither is locked
      case 'onlyCurrent': {
        const fromThisStep = alternative.createdInSubStep === this.pageKey;
        return !fromThisStep;
      }
      case 'currentAndPrevious': {
        if (alternative.createdInSubStep === 0) {
          return true;
        }

        const fromPreviousOrThisStep = alternative.createdInSubStep <= this.pageKey;
        return !fromPreviousOrThisStep;
      }
    }
  }

  changeFilter(newFilter: 'all' | 'onlyCurrent' | 'currentAndPrevious') {
    this.filter = newFilter;
    this.updateAlternatives();
  }

  expandAll() {
    this.alternativeList.forEach(alternative => (this.alternativeMetadata.get(alternative).expanded = true));
  }

  collapseAll() {
    this.alternativeList.forEach(alternative => (this.alternativeMetadata.get(alternative).expanded = false));
  }

  get allExpanded() {
    return this.alternativeList.every(alternative => this.alternativeMetadata.get(alternative).expanded);
  }
  get allCollapsed() {
    return this.alternativeList.every(alternative => !this.alternativeMetadata.get(alternative).expanded);
  }
}
