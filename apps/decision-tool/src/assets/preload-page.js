// Inline scripts are forbidden due to the Content-Security-Policy.
// This script only enabled the preloadPage animation.
const preloadPage = document.getElementById('preload-page');

setTimeout(() => {
  preloadPage.classList.add('transition');
}, 1000);
