import { randomizedUtilityGenerator, UtilityGeneratorResult } from '@entscheidungsnavi/decision-data/calculation';
import { PREDEFINED_INFLUENCE_FACTORS } from '@entscheidungsnavi/decision-data/classes';
import { readText } from '@entscheidungsnavi/decision-data/export';
import { InfluenceFactorStateMap } from '@entscheidungsnavi/decision-data/tools';
import { range } from 'lodash';
import { ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT, StartMessage, UpdateMessage } from './robustness-check-messages';

const STEP_DURATION = 500; // the time length of one step in ms

addEventListener('message', (e: MessageEvent<StartMessage>) => {
  const decisionData = readText(e.data.decisionData);
  const selectedOutcomes = decisionData.outcomes.filter((_val, idx) => e.data.selectedAlternatives.includes(idx));

  const generator = randomizedUtilityGenerator(
    decisionData.objectives,
    selectedOutcomes,
    decisionData.weights.getWeights(),
    e.data.parameters
  );

  // Result arrays
  const sum: number[] = new Array(selectedOutcomes.length);
  const min: number[] = new Array(selectedOutcomes.length);
  const max: number[] = new Array(selectedOutcomes.length);
  const utilityHistograms: number[][] = selectedOutcomes.map(() => new Array(ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT));
  // matrix with count of positions for each alternative [alternative][positionCount]
  const positions: number[][] = range(selectedOutcomes.length).map(() => new Array(selectedOutcomes.length));
  // we count how often each influence factor state occurred for each position [alternative][positionCount]
  const stateCounts = selectedOutcomes.map(() => selectedOutcomes.map(() => new InfluenceFactorStateMap<number[]>()));

  // eslint-disable-next-line no-constant-condition
  while (true) {
    // Reset all our values
    sum.fill(0);
    min.fill(Number.MAX_SAFE_INTEGER);
    max.fill(Number.MIN_SAFE_INTEGER);
    for (const histogram of utilityHistograms) {
      histogram.fill(0);
    }
    for (const positionsForAlternative of positions) {
      positionsForAlternative.fill(0);
    }
    for (const i of stateCounts) {
      for (const j of i) {
        for (const [, array] of j.entries()) {
          array.fill(0);
        }
      }
    }

    // Do all our iterations for this step
    const startTime = performance.now();
    let iterationCount = 0;
    while (performance.now() - startTime < STEP_DURATION) {
      // We know that this generator never terminates
      const currResult = generator.next().value as UtilityGeneratorResult;
      const currUtilities = currResult.alternativeUtilities;

      for (let alternativeID = 0; alternativeID < sum.length; alternativeID++) {
        sum[alternativeID] += currUtilities[alternativeID];
        min[alternativeID] = Math.min(min[alternativeID], currUtilities[alternativeID]);
        max[alternativeID] = Math.max(max[alternativeID], currUtilities[alternativeID]);

        const histogramBinIndex = Math.min(
          Math.floor(currUtilities[alternativeID] * ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT),
          ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT - 1
        );
        utilityHistograms[alternativeID][histogramBinIndex]++;
      }

      // Alternative IDs sorted by their utility
      const sortedNews = currUtilities.map((val: number, idx: number) => ({ val, idx })).sort((a, b) => b.val - a.val);

      sortedNews.forEach((alternative, rankIdx) => {
        positions[alternative.idx][rankIdx] += 1;

        // Increment the respective influence factor states
        const stateCount = stateCounts[alternative.idx][rankIdx];
        for (const [key, state] of currResult.influenceFactorStates.entries()) {
          let countArray = stateCount.get(key);

          if (countArray == null) {
            // Initialize the array if we have not done so yet
            countArray = new Array(
              typeof key === 'number'
                ? decisionData.influenceFactors[key].states.length
                : PREDEFINED_INFLUENCE_FACTORS[key.id].states.length
            ).fill(0);
            stateCount.set(key, countArray);
          }

          countArray[state]++;
        }
      });

      iterationCount++;
    }

    // Report the results for this step
    const updateMessage: UpdateMessage = {
      iterationCount,
      result: {
        sum,
        min,
        max,
        positions,
        stateCounts: stateCounts.map(outer => outer.map(inner => inner.innerMap())),
        histograms: utilityHistograms,
      },
    };
    postMessage(updateMessage);
  }
});
