import { RandomizationParameters } from '@entscheidungsnavi/decision-data/calculation';
import { InfluenceFactorStateMap } from '@entscheidungsnavi/decision-data/tools';

export interface StartMessage {
  decisionData: string;
  selectedAlternatives: number[]; // The indices of every alternative that we want to consider
  parameters: RandomizationParameters;
}

export interface UpdateMessage {
  iterationCount: number;
  // The results FOR THIS STEP. Not for the runtime of the worker, but only for this one step
  // consisting of iterationCount iterations.
  result: {
    sum: number[];
    min: number[];
    max: number[];
    positions: number[][];
    histograms: number[][]; // A utility histogram for each alternative
    stateCounts: ReturnType<InfluenceFactorStateMap<number[]>['innerMap']>[][]; // A state count for each position
  };
}

export const ROBUSTNESS_CHECK_HISTOGRAM_BIN_COUNT = 200; // the number of bins in the alternative utility histogram
