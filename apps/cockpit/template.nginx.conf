# We cache as follows:
# - Files with hashes in their names may be cached indefinitely (hashes are basically 16 digit hexadecimal strings)
# - Images may be cached for up to 1 day
# - Everything else (ngsw.json, assets/env.js, ...) is not cached
map $uri $expires {
  default                           -1;
  "~.+\.[a-f0-9]{16}\.[\d\w]+$"     max;
  ~\.(jpg|png|gif|svg)$             1d;
}

# route prefix primarily based on language cookie, then language header
map $http_accept_language $default_language_uri {
  default    en;
  ~de        de;
}

map $cookie_language $index_redirect_uri {
  default    $default_language_uri;
  de         de;
  en         en;
}

server {
  listen       80;
  listen  [::]:80;

  root   /usr/share/nginx/html;

  server_tokens off;
  absolute_redirect off;

  gzip on;
  gzip_types text/plain text/html text/css application/javascript image/x-icon;
  gzip_proxied any;

  expires $expires;

  add_header X-Frame-Options "SAMEORIGIN" always;
  add_header X-Content-Type-Options "nosniff" always;
  add_header Referrer-Policy "strict-origin-when-cross-origin" always;
  # Angular defaults + Google Fonts + Matomo
  add_header Content-Security-Policy "default-src 'self'; frame-src <DECISION_TOOL_ORIGIN>; style-src 'self' 'unsafe-inline'; script-src 'self'; connect-src 'self'; font-src 'self'; frame-ancestors 'self'; form-action 'self';" always;
  # Generated using https://www.permissionspolicy.com/
  add_header Permissions-Policy "accelerometer=(), ambient-light-sensor=(), autoplay=(), camera=(), display-capture=(), document-domain=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), midi=(), payment=(), publickey-credentials-get=(), screen-wake-lock=(), usb=()" always;

  # URL starts with either /de or /en -> serve decision-tool
  location ~ ^/(de|en) {
    try_files $uri $uri/ /$1/index.html;
  }

  # If a URL does not start with /de or /en, we redirect it
  location / {
    return 302 /${index_redirect_uri}${request_uri};
  }
}
