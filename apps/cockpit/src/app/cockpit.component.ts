import { Component, HostListener } from '@angular/core';
import { Angulartics2Matomo } from 'angulartics2';

@Component({
  selector: 'dt-root',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.scss'],
})
export class CockpitComponent {
  changeDetectionTrackingOpen = false;

  constructor(angulartics: Angulartics2Matomo) {
    angulartics.startTracking();
  }

  @HostListener('window:keydown', ['$event'])
  keydown(event: KeyboardEvent) {
    if (event.key === 'a' && event.ctrlKey && event.altKey) {
      this.changeDetectionTrackingOpen = !this.changeDetectionTrackingOpen;
    }
  }
}
