import { Component } from '@angular/core';

@Component({
  template: `<mat-error class="dt-error" i18n
    >Beim Laden der Daten ist ein Fehler aufgetreten. Bitte versuche es später erneut.</mat-error
  >`,
  styles: [
    `
      :host {
        margin: 0 auto;
        padding: 16px;
        max-width: max-content;
        display: block;
      }
    `,
  ],
})
export class DataLoadErrorComponent {}
