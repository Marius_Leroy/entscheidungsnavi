import { Component, inject, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, filter, finalize, forkJoin, tap } from 'rxjs';
import { EmbeddedDecisionToolModalComponent, EmbeddedDecisionToolModalData } from '@entscheidungsnavi/embedded-decision-tool';
import { QuickstartProject } from '@entscheidungsnavi/api-types';
import { QuickstartProjectDto, QuickstartService, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { Sort } from '@angular/material/sort';
import { FileExport, Tag } from '@entscheidungsnavi/widgets';
import sanitize from 'sanitize-filename';
import { omit, without } from 'lodash';
import { logError } from '@entscheidungsnavi/tools';
import { DECISION_TOOL_ORIGIN } from '../cockpit.module';
import { QuickstartProjectDetailsModalComponent } from './details-modal/quickstart-project-details-modal.component';

@Component({
  templateUrl: './quickstart-projects.component.html',
  styleUrls: ['./quickstart-projects.component.scss'],
})
export class QuickstartProjectsComponent implements OnInit {
  displayedColumns = ['name', 'visibility', 'tags', 'updatedAt', 'actions'];

  hasError = false;

  protected projects: QuickstartProjectDto[];
  displayedProjects: QuickstartProjectDto[];

  tags: Tag[];
  tagsById: { [id: string]: QuickstartTagDto };

  tagIdFilter: string[] = [];

  nameFilter = '';
  activeSort: Sort;

  private _pendingActions = 0;

  atLeastOneActionPending$ = new BehaviorSubject<boolean>(false);

  private decisionToolOrigin = inject(DECISION_TOOL_ORIGIN);

  private incrementPendingActions() {
    if (++this._pendingActions === 1) {
      this.atLeastOneActionPending$.next(true);
    }
  }

  private decrementPendingActions() {
    if (--this._pendingActions === 0) {
      this.atLeastOneActionPending$.next(false);
    }
  }

  constructor(private quickstartService: QuickstartService, private snackBar: MatSnackBar, private dialog: MatDialog) {}

  ngOnInit() {
    forkJoin({ projects: this.quickstartService.getProjects(true), tags: this.quickstartService.getTags() }).subscribe({
      next: result => {
        this.projects = result.projects.list;
        this.tags = result.tags;
        this.tagsById = result.tags.reduce((prev, curr) => {
          prev[curr.id] = curr;
          return prev;
        }, {});
        this.updateDisplayedProjectsAndTagProductivity();
      },
      error: () => (this.hasError = true),
    });
  }

  updateDisplayedProjectsAndTagProductivity() {
    let projects = this.nameFilter
      ? this.projects.filter(project => project.name.toLowerCase().includes(this.nameFilter.toLowerCase()))
      : this.projects.slice();

    if (this.tagIdFilter) {
      projects = projects.filter(project => this.tagIdFilter.every(tagId => project.tags.includes(tagId)));
    }

    if (this.activeSort?.direction) {
      projects = projects.sort((a, b) => {
        let result = 0;
        if (this.activeSort.active === 'name') {
          result = a.name.localeCompare(b.name);
        } else if (this.activeSort.active === 'updatedAt') {
          result = a.updatedAt.getTime() - b.updatedAt.getTime();
        } else if (this.activeSort.active === 'visibility') {
          result = +a.visible - +b.visible;
        }

        return this.activeSort.direction === 'asc' ? result : -result;
      });
    }

    this.displayedProjects = projects;

    this.tags.forEach(
      tag => (tag.isProductive = this.displayedProjects.some(p => p instanceof QuickstartProjectDto && p.tags.includes(tag.id)))
    );
  }

  addProject() {
    this.dialog
      .open(QuickstartProjectDetailsModalComponent, { data: { tags: this.tags.map(tag => omit(tag, 'isProductive')) } })
      .afterClosed()
      .subscribe(project => {
        if (project) {
          this.projects.push(project);
          this.updateDisplayedProjectsAndTagProductivity();
        }
      });
  }

  showDetails(project: QuickstartProjectDto) {
    this.dialog
      .open(QuickstartProjectDetailsModalComponent, { data: { project, tags: this.tags.map(tag => omit(tag, 'isProductive')) } })
      .afterClosed()
      .pipe(filter(result => result))
      .subscribe((result: QuickstartProjectDto | 'deleted') => {
        const index = this.projects.findIndex(element => element.id === project.id);

        if (result === 'deleted') {
          this.projects.splice(index, 1);
        } else {
          this.projects[index] = result;
        }

        this.updateDisplayedProjectsAndTagProductivity();
      });
  }

  onSortChange(sort: Sort) {
    this.activeSort = sort;
    this.updateDisplayedProjectsAndTagProductivity();
  }

  downloadClick(event: Event, project: QuickstartProjectDto) {
    event.preventDefault();

    this.incrementPendingActions();
    this.quickstartService
      .getProject(project.id)
      .pipe(
        logError(),
        finalize(() => this.decrementPendingActions())
      )
      .subscribe({
        next: data => {
          const name = (sanitize(project.name) || 'project') + '.json';
          FileExport.download(name, data.data);
        },
        error: () => this.snackBar.open($localize`Download fehlgeschlagen`, 'Ok'),
      });
  }

  toggleVisibility(project: QuickstartProjectDto) {
    this.incrementPendingActions();
    this.quickstartService
      .updateProject(project.id, { visible: !project.visible })
      .pipe(
        logError(),
        finalize(() => this.decrementPendingActions())
      )
      .subscribe({
        next: (updatedProject: QuickstartProjectDto) => {
          const index = this.projects.findIndex(element => element.id === project.id);
          this.projects[index].visible = updatedProject.visible;
          this.projects[index].updatedAt = updatedProject.updatedAt;
          this.updateDisplayedProjectsAndTagProductivity();
        },
        error: () => this.snackBar.open($localize`Unbekannter Fehler beim Ändern der Sichtbarkeit des Projekts`, 'Ok'),
      });
  }

  tagClick(event: Event, tagId: string) {
    event.stopImmediatePropagation();

    if (!this.tagSelected(tagId)) {
      this.tagIdFilter.push(tagId);
    } else {
      this.tagIdFilter = without(this.tagIdFilter, tagId);
    }

    this.updateDisplayedProjectsAndTagProductivity();
  }

  tagSelected(tagId: string) {
    return this.tagIdFilter.includes(tagId);
  }

  edit(project: QuickstartProjectDto) {
    this.quickstartService.getProject(project.id).subscribe(projectWithData => {
      this.dialog.open<EmbeddedDecisionToolModalComponent, EmbeddedDecisionToolModalData>(EmbeddedDecisionToolModalComponent, {
        data: {
          project: projectWithData,
          decisionToolOrigin: this.decisionToolOrigin,
          persist: updatedProject => {
            const updatedQuickstartProject = { ...updatedProject, visible: project.visible };

            return this.quickstartService.updateProject(project.id, updatedQuickstartProject).pipe(
              tap((project: QuickstartProject) => {
                const index = this.projects.findIndex(element => element.id === project.id);
                this.projects[index] = project;
                this.updateDisplayedProjectsAndTagProductivity();
              })
            );
          },
        },
      });
    });
  }
}
