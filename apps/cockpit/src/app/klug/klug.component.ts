import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Sort } from '@angular/material/sort';
import { KlugProjectFilter, KlugProjectSort, ROLES } from '@entscheidungsnavi/api-types';
import { KlugManagerService, KlugProjectDto } from '@entscheidungsnavi/api-client';
import { finalize, Observable, startWith, Subject, switchMap, takeUntil, tap } from 'rxjs';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { PageEvent } from '@angular/material/paginator';
import { KlugProjectModalComponent, KlugProjectModalData } from './klug-project-modal/klug-project-modal.component';

@Component({
  templateUrl: './klug.component.html',
  styleUrls: ['./klug.component.scss'],
})
export class KlugComponent implements OnInit {
  readonly displayedColumns = ['token', 'finished', 'isOfficial', 'readonly', 'expiresAt', 'createdAt'];

  isLoading = false;
  hasError = false;
  items: KlugProjectDto[];
  itemCount: number;

  exportingExcel = false;

  filterForm = new FormGroup({
    isOfficial: new FormControl(true),
    finished: new FormControl(true),
    dateRange: new FormGroup({
      start: new FormControl<Date>(null),
      end: new FormControl<Date>(null),
    }),
  });
  page: { pageIndex: number; pageSize: number } = { pageIndex: 0, pageSize: 100 };
  sort: KlugProjectSort = { sortBy: 'createdAt', sortDirection: 'desc' };
  triggerLoadSubject = new Subject<void>();

  roles = ROLES;

  @OnDestroyObservable()
  protected onDestroy$: Observable<void>;

  constructor(private klugManagerService: KlugManagerService, private snackBar: MatSnackBar, private dialog: MatDialog) {}

  ngOnInit() {
    this.triggerLoadSubject
      .pipe(
        startWith(null),
        tap(() => (this.isLoading = true)),
        switchMap(() =>
          this.klugManagerService.getKlugProjects(this.formToFilter(), this.sort, {
            limit: this.page.pageSize,
            offset: this.page.pageIndex * this.page.pageSize,
          })
        ),
        takeUntil(this.onDestroy$)
      )
      .subscribe({
        next: data => {
          this.isLoading = false;
          this.items = data.items;
          this.itemCount = data.count;
        },
        error: () => (this.hasError = true),
      });

    this.filterForm.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(() => this.triggerLoadSubject.next());
  }

  onPageChange(event: PageEvent) {
    this.page = event;
    this.triggerLoadSubject.next();
  }

  onSortChange(sort: Sort) {
    this.sort = { sortDirection: sort.direction, sortBy: sort.active } as KlugProjectSort;
    this.triggerLoadSubject.next();
  }

  addProject() {
    this.dialog
      .open<KlugProjectModalComponent, KlugProjectModalData, boolean>(KlugProjectModalComponent)
      .afterClosed()
      .subscribe(shouldRefreshProjectList => {
        if (shouldRefreshProjectList) {
          this.triggerLoadSubject.next();
        }
      });
  }

  editProject(project: KlugProjectDto) {
    this.dialog
      .open<KlugProjectModalComponent, KlugProjectModalData, boolean>(KlugProjectModalComponent, { data: { project } })
      .afterClosed()
      .subscribe(shouldRefreshProjectList => {
        if (shouldRefreshProjectList) {
          this.triggerLoadSubject.next();
        }
      });
  }

  generateExcelExport() {
    this.exportingExcel = true;
    const filter = this.formToFilter();

    this.klugManagerService
      .generateExcelExport(filter)
      .pipe(
        finalize(() => {
          this.exportingExcel = false;
        })
      )
      .subscribe({
        next: response => {
          const dataType = response.type;
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([response], { type: dataType }));
          downloadLink.setAttribute('download', `event-export-${Date.now()}.xlsx`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        error: () => this.snackBar.open($localize`Fehler beim Export`, 'Ok'),
      });
  }

  private formToFilter() {
    const filter: KlugProjectFilter = {};
    if (this.filterForm.controls.isOfficial.value != null) {
      filter.isOfficial = this.filterForm.controls.isOfficial.value;
    }
    if (this.filterForm.controls.finished.value != null) {
      filter.finished = this.filterForm.controls.finished.value;
    }
    if (this.filterForm.controls.dateRange.controls.start.value != null) {
      filter.startDate = this.filterForm.controls.dateRange.controls.start.value;
    }
    if (this.filterForm.controls.dateRange.controls.end.value != null) {
      filter.endDate = this.filterForm.controls.dateRange.controls.end.value;
    }

    return filter;
  }
}
