import { Component, Input } from '@angular/core';

@Component({
  selector: 'dt-table-footer',
  template: `<ng-container i18n
    >Angezeigte Elemente: <ng-container *ngIf="visibleCount != null"> {{ visibleCount }} von</ng-container> {{ totalCount }}</ng-container
  >`,
  styleUrls: [`./table-footer.component.scss`],
})
export class TableFooterComponent {
  @Input() visibleCount?: number;
  @Input() totalCount: number;
}
