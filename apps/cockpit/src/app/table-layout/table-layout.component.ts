import { Component, Input } from '@angular/core';

@Component({
  selector: 'dt-table-layout',
  templateUrl: './table-layout.component.html',
  styleUrls: ['./table-layout.component.scss'],
})
export class TableLayoutComponent {
  @Input() isLoading = false;
}
