import { Component, OnInit } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { QuickstartService, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import {
  QuickstartHierarchy,
  QuickstartHierarchyFilter,
  QuickstartHierarchySort,
  QuickstartHierarchyWithStats,
} from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { catchError, EMPTY, Observable, startWith, Subject, switchMap, takeUntil } from 'rxjs';
import { Tag } from '@entscheidungsnavi/widgets';
import { MatDialog } from '@angular/material/dialog';
import { Sort } from '@angular/material/sort';
import { without } from 'lodash';
import { QuickstartHierarchyDetailsModalComponent } from '../details/quickstart-hierarchy-details-modal.component';

@Component({
  selector: 'dt-quickstart-hierarchies',
  templateUrl: './quickstart-hierarchies.component.html',
  styleUrls: ['./quickstart-hierarchies.component.scss'],
})
export class QuickstartHierarchiesComponent implements OnInit {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  readonly displayedColumns = ['nameDe', 'nameEn', 'totalAccumulatedScore', 'tags', 'createdAt'];

  isLoading = false;
  hasError = false;

  items: QuickstartHierarchyWithStats[];
  itemCount: number;

  tags: Tag[];
  tagsById: { [id: string]: QuickstartTagDto };

  filterForm = this.fb.group({
    searchQuery: '',
    tags: [[] as string[]],
  });
  page: { pageIndex: number; pageSize: number } = { pageIndex: 0, pageSize: 100 };
  sort: QuickstartHierarchySort = { sortBy: 'totalAccumulatedScore', sortDirection: 'desc' };
  triggerLoadSubject = new Subject<void>();

  constructor(private quickstartService: QuickstartService, private fb: NonNullableFormBuilder, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.quickstartService.getTags().subscribe({
      next: tags => {
        this.tags = tags;
        this.tagsById = tags.reduce((prev, curr) => {
          prev[curr.id] = curr;
          return prev;
        }, {});
      },
      error: () => (this.hasError = true),
    });

    this.triggerLoadSubject
      .pipe(
        startWith(null),
        switchMap(() => {
          this.isLoading = true;

          const filter: QuickstartHierarchyFilter = {};
          if (this.filterForm.value.searchQuery) {
            filter.nameQuery = this.filterForm.value.searchQuery;
          }
          if (this.filterForm.value.tags.length > 0) {
            filter.tags = this.filterForm.value.tags;
          }

          return this.quickstartService
            .getHierarchies(
              filter,
              {
                limit: this.page.pageSize,
                offset: this.page.pageIndex * this.page.pageSize,
              },
              true,
              this.sort
            )
            .pipe(catchError(() => EMPTY));
        }),
        takeUntil(this.onDestroy$)
      )
      .subscribe({
        next: data => {
          this.isLoading = false;
          this.items = data.items;
          this.itemCount = data.count;
        },
        error: () => (this.hasError = true),
      });

    this.filterForm.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(() => this.triggerLoadSubject.next());
  }

  onPageChange(event: PageEvent) {
    this.page = event;
    this.triggerLoadSubject.next();
  }

  onSortChange(sort: Sort) {
    this.sort = { sortDirection: sort.direction, sortBy: sort.active } as QuickstartHierarchySort;
    this.triggerLoadSubject.next();
  }

  tagClick(event: Event, tagId: string) {
    event.stopImmediatePropagation();

    if (!this.filterForm.value.tags.includes(tagId)) {
      this.filterForm.patchValue({ tags: [...this.filterForm.value.tags, tagId] });
    } else {
      this.filterForm.patchValue({ tags: without(this.filterForm.value.tags, tagId) });
    }
  }

  openModal(hierarchy?: QuickstartHierarchy) {
    this.dialog
      .open(QuickstartHierarchyDetailsModalComponent, { data: { hierarchy, tags: this.tags } })
      .afterClosed()
      .subscribe(result => {
        if (result) this.triggerLoadSubject.next();
      });
  }
}
