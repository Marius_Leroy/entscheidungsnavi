import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Sort } from '@angular/material/sort';
import { QuickstartService, QuickstartTagDto } from '@entscheidungsnavi/api-client';
import { filter } from 'rxjs';
import { QuickstartTagDetailsModalComponent } from './details-modal/quickstart-tag-details-modal.component';

@Component({
  templateUrl: './quickstart-tags.component.html',
  styleUrls: ['./quickstart-tags.component.scss'],
})
export class QuickstartTagsComponent implements OnInit {
  displayedColumns = ['nameDe', 'nameEn', 'weight', 'updatedAt'];

  protected tags: QuickstartTagDto[];
  displayedTags: QuickstartTagDto[];

  hasError = false;
  nameFilter = '';
  activeSort: Sort = { active: 'nameDe', direction: 'asc' };

  constructor(private quickstartService: QuickstartService, private dialog: MatDialog) {}

  ngOnInit() {
    this.quickstartService.getTags().subscribe({
      next: tags => {
        this.tags = tags;
        this.updateDisplayedTags();
      },
      error: () => (this.hasError = true),
    });
  }

  updateDisplayedTags() {
    const filter = this.nameFilter.toLowerCase();
    const tags = this.nameFilter
      ? this.tags.filter(tag => tag.name.de?.toLowerCase().includes(filter) || tag.name.en.toLowerCase().includes(filter))
      : this.tags.slice();

    tags.sort((a, b) => {
      let result = 0;
      if (this.activeSort.active === 'nameDe') {
        result = (a.name.de ?? '').localeCompare(b.name.de ?? '');
      } else if (this.activeSort.active === 'nameEn') {
        result = a.name.en.localeCompare(b.name.en);
      } else if (this.activeSort.active === 'updatedAt') {
        result = a.updatedAt.getTime() - b.updatedAt.getTime();
      } else if (this.activeSort.active === 'weight') {
        result = (a.weight ?? -Infinity) - (b.weight ?? -Infinity);
      }

      return this.activeSort.direction === 'asc' ? result : -result;
    });

    this.displayedTags = tags;
  }

  showDetails(tag: QuickstartTagDto) {
    this.dialog
      .open(QuickstartTagDetailsModalComponent, { data: tag })
      .afterClosed()
      .pipe(filter(result => result))
      .subscribe((result: QuickstartTagDto | 'deleted') => {
        const index = this.tags.findIndex(element => element.id === tag.id);

        if (result === 'deleted') {
          this.tags.splice(index, 1);
        } else {
          this.tags[index] = result;
        }

        this.updateDisplayedTags();
      });
  }

  addTag() {
    this.dialog
      .open(QuickstartTagDetailsModalComponent)
      .afterClosed()
      .subscribe((newTag: QuickstartTagDto) => {
        if (newTag) {
          this.tags.push(newTag);
          this.updateDisplayedTags();
        }
      });
  }

  onSortChange(sort: Sort) {
    this.activeSort = sort;
    this.updateDisplayedTags();
  }
}
