import { QuestionnaireEntry, QuestionnaireEntryType } from '@entscheidungsnavi/api-types';
import { assertUnreachable } from '@entscheidungsnavi/tools/assert_unreachable';

export function getEmptyQuestionnaireEntry(type: QuestionnaireEntryType): QuestionnaireEntry {
  switch (type) {
    case 'textBlock':
      return { entryType: type, text: '', type: 'body' };
    case 'numberQuestion':
    case 'textQuestion':
      return { entryType: type, question: '', label: '' };
    case 'optionsQuestion':
      return { entryType: type, displayType: 'radio', options: [''], label: '', question: '' };
    case 'tableQuestion':
      return { entryType: type, baseQuestion: '', subQuestions: [''], options: [''] };
    default:
      assertUnreachable(type);
  }
}
