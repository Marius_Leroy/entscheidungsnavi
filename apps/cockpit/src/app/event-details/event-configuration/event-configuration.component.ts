import { Component, OnInit } from '@angular/core';
import { ControlContainer, FormControl } from '@angular/forms';
import { FORWARD_CONTROL_CONTAINER } from '@entscheidungsnavi/widgets';
import { EventConfigurationForm } from './event-configuration-form.service';

@Component({
  selector: 'dt-event-configuration',
  templateUrl: './event-configuration.component.html',
  styleUrls: ['./event-configuration.component.scss'],
  viewProviders: [FORWARD_CONTROL_CONTAINER],
})
export class EventConfigurationComponent implements OnInit {
  eventForm: EventConfigurationForm;

  constructor(private controlContainer: ControlContainer) {}

  ngOnInit(): void {
    this.eventForm = this.controlContainer.control as EventConfigurationForm;
  }

  onDatetimeFieldBlur(event: FocusEvent, control: FormControl) {
    // Only the browser knows when this field is partially filled, not the form control. Thus, we cannot use
    // a normal form validator.
    if ((event.target as HTMLInputElement).validity.badInput) {
      control.setErrors({ incomplete: true });
    }
  }

  clearDateField(control: FormControl) {
    control.markAsDirty();
    control.markAsTouched();
    control.setValue(null);
  }
}
