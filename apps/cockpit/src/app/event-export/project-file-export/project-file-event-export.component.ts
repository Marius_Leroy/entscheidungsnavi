import { Component, Input } from '@angular/core';
import { NonNullableFormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventManagementService } from '@entscheidungsnavi/api-client';
import { finalize } from 'rxjs';

@Component({
  selector: 'dt-project-file-event-export',
  templateUrl: './project-file-event-export.component.html',
  styleUrls: ['../export-form.scss'],
})
export class ProjectFileEventExportComponent {
  @Input() selectedEvents: string[];

  protected isLoading = false;

  constructor(private eventService: EventManagementService, private snackBar: MatSnackBar, protected fb: NonNullableFormBuilder) {}

  createExportProjectFiles() {
    if (this.isLoading) return;

    this.isLoading = true;
    this.eventService
      .generateProjectsExport({ eventIds: this.selectedEvents })
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          const dataType = response.type;
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([response], { type: dataType }));
          downloadLink.setAttribute('download', `event-export-${Date.now()}.zip`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        error: () => this.snackBar.open($localize`Fehler beim Export`, 'Ok'),
      });
  }
}
