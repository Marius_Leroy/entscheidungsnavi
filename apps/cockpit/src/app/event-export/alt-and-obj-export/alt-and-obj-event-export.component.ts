import { Component, Input } from '@angular/core';
import { FormGroup, NonNullableFormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventManagementService } from '@entscheidungsnavi/api-client';
import {
  EVENT_ALT_AND_OBJ_EXPORT_ALTERNATIVES_ATTRIBUTES,
  EVENT_ALT_AND_OBJ_EXPORT_OBJECTIVES_ATTRIBUTES,
} from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { createControlDependency } from '@entscheidungsnavi/widgets';
import { finalize, Observable } from 'rxjs';

@Component({
  selector: 'dt-alt-and-obj-event-export',
  templateUrl: './alt-and-obj-event-export.component.html',
  styleUrls: ['../export-form.scss'],
})
export class AltAndObjEventExportComponent {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() selectedEvents: string[];

  protected parametersForm = this.fb.group({
    alternativesSheet: [true],
    defaultAnalysis: [true],
    linearNumericalAnalysis: [true],
    linearVerbalAnalysis: [true],
    linearBothAnalysis: [true],
    linearMostImportantAnalyses: [true],
    linearMostImportantAccumulatedAnalyses: [true],
    linearLeastImportantAnalyses: [true],
    linearLeastImportantAccumulatedAnalyses: [true],

    objectivesSheet: [true],
    scaleInfo: [true],
    weightInfo: [true],
    utilityInfo: [true],
  });

  protected formGroup = new FormGroup({
    parameters: this.parametersForm,
  });

  protected isLoading = false;

  constructor(private eventService: EventManagementService, private snackBar: MatSnackBar, protected fb: NonNullableFormBuilder) {
    const pi = this.parametersForm.controls;

    createControlDependency(pi.alternativesSheet, pi.defaultAnalysis, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearNumericalAnalysis, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearVerbalAnalysis, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearBothAnalysis, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearMostImportantAnalyses, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearMostImportantAccumulatedAnalyses, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearLeastImportantAnalyses, this.onDestroy$);
    createControlDependency(pi.alternativesSheet, pi.linearLeastImportantAccumulatedAnalyses, this.onDestroy$);

    createControlDependency(pi.objectivesSheet, pi.scaleInfo, this.onDestroy$);
    createControlDependency(pi.objectivesSheet, pi.weightInfo, this.onDestroy$);
    createControlDependency(pi.objectivesSheet, pi.utilityInfo, this.onDestroy$);
  }

  createExportAltAndObj() {
    if (!this.areAltAndObjParametersValid()) return;

    const val = this.parametersForm.value;
    const alternativeAttributes = EVENT_ALT_AND_OBJ_EXPORT_ALTERNATIVES_ATTRIBUTES.filter(attribute => val[attribute]);
    const objectiveAttributes = EVENT_ALT_AND_OBJ_EXPORT_OBJECTIVES_ATTRIBUTES.filter(attribute => val[attribute]);
    this.isLoading = true;

    this.eventService
      .generateAltAndObjExport({ eventIds: this.selectedEvents, alternativeAttributes, objectiveAttributes })
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          const dataType = response.type;
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([response], { type: dataType }));
          downloadLink.setAttribute('download', `event-export-${Date.now()}.xlsx`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        error: () => this.snackBar.open($localize`Fehler beim Export`, 'Ok'),
      });
  }

  selectFullExport() {
    this.parametersForm.patchValue({
      alternativesSheet: true,
      defaultAnalysis: true,
      linearNumericalAnalysis: true,
      linearVerbalAnalysis: true,
      linearBothAnalysis: true,
      linearMostImportantAnalyses: true,
      linearMostImportantAccumulatedAnalyses: true,
      linearLeastImportantAnalyses: true,
      linearLeastImportantAccumulatedAnalyses: true,
      objectivesSheet: true,
      scaleInfo: true,
      weightInfo: true,
      utilityInfo: true,
    });
  }

  areAltAndObjParametersValid() {
    return this.formGroup.value.parameters.alternativesSheet || this.formGroup.value.parameters.objectivesSheet;
  }

  selectMinimalExport() {
    this.parametersForm.patchValue({
      alternativesSheet: true,
      defaultAnalysis: false,
      linearNumericalAnalysis: false,
      linearVerbalAnalysis: false,
      linearBothAnalysis: false,
      linearMostImportantAnalyses: false,
      linearMostImportantAccumulatedAnalyses: false,
      linearLeastImportantAnalyses: false,
      linearLeastImportantAccumulatedAnalyses: false,
      objectivesSheet: true,
      scaleInfo: false,
      weightInfo: false,
      utilityInfo: false,
    });
  }
}
