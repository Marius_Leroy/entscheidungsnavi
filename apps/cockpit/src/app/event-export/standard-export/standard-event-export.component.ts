import { Component, Input, OnChanges } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { EventManagementService } from '@entscheidungsnavi/api-client';
import { EVENT_DEFAULT_EXPORT_ATTRIBUTES, EventDefaultExportAttribute } from '@entscheidungsnavi/api-types';
import { OnDestroyObservable } from '@entscheidungsnavi/tools';
import { createControlDependency } from '@entscheidungsnavi/widgets';
import { finalize, Observable } from 'rxjs';

@Component({
  selector: 'dt-standard-event-export',
  templateUrl: './standard-event-export.component.html',
  styleUrls: ['../export-form.scss'],
})
export class StandardEventExportComponent implements OnChanges {
  @OnDestroyObservable()
  private onDestroy$: Observable<void>;

  @Input() selectedEvents: string[];

  private attributesForm: Record<EventDefaultExportAttribute, FormControl<boolean>> = {
    projectInformation: new FormControl(true, { nonNullable: true }),
    modelParameterCounts: new FormControl(true, { nonNullable: true }),
    processStatus: new FormControl(false, { nonNullable: true }),

    stepExplanations: new FormControl(false, { nonNullable: true }),
    objectiveExplanations: new FormControl(false, { nonNullable: true }),
    alternativeExplanations: new FormControl(false, { nonNullable: true }),
    influenceFactorExplanations: new FormControl(false, { nonNullable: true }),

    decisionQuality: new FormControl(false, { nonNullable: true }),
    timeRecording: new FormControl(false, { nonNullable: true }),

    submissionQuestionnaire: new FormControl(false, { nonNullable: true }),
    submissionFreeText: new FormControl(false, { nonNullable: true }),
  };

  protected parametersForm = new FormGroup({
    includeObjectives: new FormControl(true, { nonNullable: true }),
    limitObjectiveCount: new FormControl(false, { nonNullable: true }),
    objectiveCountLimit: new FormControl<number>(10, { validators: [Validators.required, Validators.min(1)], nonNullable: true }),

    includeAlternatives: new FormControl(true, { nonNullable: true }),
    limitAlternativeCount: new FormControl(false, { nonNullable: true }),
    alternativeCountLimit: new FormControl<number>(10, { validators: [Validators.required, Validators.min(1)], nonNullable: true }),

    includeInfluenceFactors: new FormControl(false, { nonNullable: true }),
    limitInfluenceFactorCount: new FormControl(false, { nonNullable: true }),
    influenceFactorCountLimit: new FormControl<number>(10, { validators: [Validators.required, Validators.min(1)], nonNullable: true }),

    ...this.attributesForm,
  });

  protected formGroup = new FormGroup({
    parameters: this.parametersForm,
  });

  protected isLoading = false;

  constructor(private eventService: EventManagementService, private snackBar: MatSnackBar, protected fb: NonNullableFormBuilder) {
    const ps = this.parametersForm.controls;

    createControlDependency(ps.includeObjectives, ps.objectiveExplanations, this.onDestroy$);
    createControlDependency(ps.includeObjectives, ps.limitObjectiveCount, this.onDestroy$);
    createControlDependency(ps.limitObjectiveCount, ps.objectiveCountLimit, this.onDestroy$);

    createControlDependency(ps.includeAlternatives, ps.alternativeExplanations, this.onDestroy$);
    createControlDependency(ps.includeAlternatives, ps.limitAlternativeCount, this.onDestroy$);
    createControlDependency(ps.limitAlternativeCount, ps.alternativeCountLimit, this.onDestroy$);

    createControlDependency(ps.includeInfluenceFactors, ps.influenceFactorExplanations, this.onDestroy$);
    createControlDependency(ps.includeInfluenceFactors, ps.limitInfluenceFactorCount, this.onDestroy$);
    createControlDependency(ps.limitInfluenceFactorCount, ps.influenceFactorCountLimit, this.onDestroy$);
  }

  ngOnChanges() {
    if (this.selectedEvents.length > 1) {
      this.attributesForm.submissionQuestionnaire.disable();
    } else {
      this.attributesForm.submissionQuestionnaire.enable();
    }
  }

  createExportStandard() {
    if (this.formGroup.invalid || this.isLoading) return;

    const val = this.parametersForm.value;
    const enabledAttributes = EVENT_DEFAULT_EXPORT_ATTRIBUTES.filter(attribute => val[attribute]);
    const objectiveLimit = !val.includeObjectives ? 0 : !val.limitObjectiveCount ? undefined : val.objectiveCountLimit;
    const alternativeLimit = !val.includeAlternatives ? 0 : !val.limitAlternativeCount ? undefined : val.alternativeCountLimit;
    const influenceFactorLimit = !val.includeInfluenceFactors
      ? 0
      : !val.limitInfluenceFactorCount
      ? undefined
      : val.influenceFactorCountLimit;
    this.isLoading = true;

    this.eventService
      .generateDefaultExport({
        eventIds: this.selectedEvents,
        attributes: enabledAttributes,
        objectiveLimit,
        alternativeLimit,
        influenceFactorLimit,
      })
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe({
        next: response => {
          const dataType = response.type;
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([response], { type: dataType }));
          downloadLink.setAttribute('download', `event-export-${Date.now()}.xlsx`);
          document.body.appendChild(downloadLink);
          downloadLink.click();
        },
        error: () => this.snackBar.open($localize`Fehler beim Export`, 'Ok'),
      });
  }

  selectFullExport() {
    const attributeValues = Object.fromEntries(EVENT_DEFAULT_EXPORT_ATTRIBUTES.map(attribute => [attribute, true])) as Record<
      EventDefaultExportAttribute,
      boolean
    >;

    this.parametersForm.patchValue({
      includeObjectives: true,
      limitObjectiveCount: false,
      includeAlternatives: true,
      limitAlternativeCount: false,
      includeInfluenceFactors: true,
      limitInfluenceFactorCount: false,
      ...attributeValues,
    });
  }

  selectDefaultExport() {
    this.parametersForm.reset();
  }

  selectMinimalExport() {
    const attributeValues = Object.fromEntries(EVENT_DEFAULT_EXPORT_ATTRIBUTES.map(attribute => [attribute, false])) as Record<
      EventDefaultExportAttribute,
      boolean
    >;

    this.parametersForm.patchValue({
      includeObjectives: false,
      includeAlternatives: false,
      includeInfluenceFactors: false,
      ...attributeValues,
    });
  }
}
