import { Component } from '@angular/core';
import { EventManagementService } from '@entscheidungsnavi/api-client';
import { EventWithStats } from '@entscheidungsnavi/api-types';
import { catchError, map, Observable, of } from 'rxjs';
import { sum } from 'lodash';

@Component({
  templateUrl: './event-export.component.html',
  styleUrls: ['./event-export.component.scss'],
})
export class EventExportComponent {
  events: EventWithStats[];
  protected events$: Observable<EventWithStats[]>;
  protected hasError = false;
  selectedIds: string[] = [];

  exportType: 'standard' | 'alternatives-and-objectives' | 'project-files' = 'standard';

  constructor(eventService: EventManagementService) {
    this.events$ = eventService.events$.pipe(
      map(events => events.list),
      catchError(() => {
        this.hasError = true;
        return of();
      })
    );
    this.events$.subscribe(events => (this.events = events));
  }

  get selectedSubmissionCount() {
    return sum(this.events?.filter(event => this.selectedIds.includes(event.id)).map(event => event.submissionCount));
  }
}
