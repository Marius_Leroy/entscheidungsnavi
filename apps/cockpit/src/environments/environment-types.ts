// eslint-disable-next-line @typescript-eslint/no-var-requires
const { version } = require('../../../../package.json');

export const ENVIRONMENT_TYPES = ['development', 'preview', 'nightly', 'production'] as const;
export type EnvironmentType = (typeof ENVIRONMENT_TYPES)[number];

export interface EnvironmentConfig {
  type: EnvironmentType;
  sentryDsn?: string;
  decisionToolOrigin?: string;
}

export function createEnvironment(config: EnvironmentConfig) {
  return {
    ...config,
    production: config.type !== 'development',
    version,
  };
}
