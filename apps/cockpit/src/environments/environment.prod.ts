import { createEnvironment, ENVIRONMENT_TYPES, EnvironmentType } from './environment-types';

function getType(): EnvironmentType {
  if (ENVIRONMENT_TYPES.includes(window.cpEnv?.environmentType as EnvironmentType)) {
    return window.cpEnv?.environmentType as EnvironmentType;
  }

  return 'production';
}

export const ENVIRONMENT = createEnvironment({
  type: getType(),
  sentryDsn: window.cpEnv?.sentryDsn || null,
  decisionToolOrigin: window.cpEnv?.decisionToolOrigin || null,
});
