import { Component, TemplateRef, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Angulartics2Matomo } from 'angulartics2';
import { HelpComponent } from './help/help.component';
import { AutoSaveService } from './services/auto-save.service';
import { PairComparisonService } from './services/pair-comparisons.service';
import { StateService } from './services/state.service';
import { wantsToBeDisplayedAtMaximumWidth } from './shared/display-at-max-width';
import { AbstractStepComponent } from './steps/step-container.component';

@Component({
  selector: 'klug-root',
  templateUrl: './klug-tool.component.html',
  styleUrls: ['./klug-tool.component.scss'],
})
export class KlugToolComponent {
  title = 'KLUGentscheiden-Tool';

  @ViewChild(HelpComponent)
  help: HelpComponent;

  limitComponentWidth = true;

  protected helpTemplate: TemplateRef<unknown>;

  readonly isHelpOpen$ = this.stateService.helpOpen$;

  constructor(
    private stateService: StateService,
    private _autoSaveService: AutoSaveService,
    private _pairComparisonService: PairComparisonService,
    titleService: Title,
    angulartics: Angulartics2Matomo
  ) {
    angulartics.startTracking();
    titleService.setTitle(this.title);
  }

  onActivateRoute(componentInstance: unknown) {
    if (componentInstance instanceof AbstractStepComponent) {
      this.helpTemplate = componentInstance.helpTemplate;
    } else {
      this.helpTemplate = null;
    }

    this.help.helpTemplate = this.helpTemplate;

    this.limitComponentWidth = !wantsToBeDisplayedAtMaximumWidth(componentInstance);
  }
}
