import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  public helpOpen$ = new BehaviorSubject(true);

  toggleHelp() {
    this.helpOpen$.next(!this.helpOpen$.value);
  }
}
