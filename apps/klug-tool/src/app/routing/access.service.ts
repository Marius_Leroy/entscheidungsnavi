import { Injectable } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { validateDecisionStatement, validateOutcomes } from '@entscheidungsnavi/decision-data/validations';
import { RouterStep, ROUTER_STEPS } from '../navigation/navigation.component';
import { PairComparisonService } from '../services/pair-comparisons.service';
import { Constants } from '../shared/constants';

@Injectable({
  providedIn: 'root',
})
export class AccessService {
  private readonly routeChecks: Record<RouterStep, () => boolean> = {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    'decision-statement': () => true,
    objectives: () => validateDecisionStatement(this.decisionData.decisionStatement)[0],
    options: () =>
      this.decisionData.objectives.length >= this.constants.minObjectiveCount &&
      this.decisionData.objectives.every(objective => objective.name),
    assessment: () =>
      this.decisionData.alternatives.length >= this.constants.minAlternativeCount &&
      this.decisionData.alternatives.every(alternative => alternative.name),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    'assessment/comparison': () => validateOutcomes(this.decisionData)[0],
    evaluation: () =>
      this.pairComparisonService
        .getComparisonTable()
        .every((row, rowIndex) => row.every((comparison, columnIndex) => columnIndex <= rowIndex || comparison !== 'missing')),
  };

  constructor(private decisionData: DecisionData, private pairComparisonService: PairComparisonService, private constants: Constants) {}

  canAccess(step: RouterStep) {
    return ROUTER_STEPS.slice(0, ROUTER_STEPS.indexOf(step) + 1).every(stepToCheck => this.routeChecks[stepToCheck]());
  }
}
