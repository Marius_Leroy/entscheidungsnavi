import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'klug-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss'],
})
export class HelpComponent {
  @Input()
  helpTemplate: TemplateRef<unknown>;
}
