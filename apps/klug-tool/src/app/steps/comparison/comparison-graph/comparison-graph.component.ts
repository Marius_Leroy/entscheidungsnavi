import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { Objective } from '@entscheidungsnavi/decision-data/classes';
import { KlugCompareResult } from '@entscheidungsnavi/decision-data/klug';
import { PairComparisonService } from '../../../services/pair-comparisons.service';

@Component({
  selector: 'klug-comparison-graph',
  templateUrl: './comparison-graph.component.html',
  styleUrls: ['./comparison-graph.component.scss'],
})
export class ComparisonGraphComponent {
  protected objectives: Objective[];

  protected pairComparisons: KlugCompareResult[][];

  @ViewChild('comparisonModal', { static: true })
  comparisonModalTemplate: TemplateRef<unknown>;

  activeComparison = 0;
  pendingComparisons: [objectiveIndexRow: number, objectiveIndexColumn: number][];

  constructor(private dialog: MatDialog, decisionData: DecisionData, pairComparisonService: PairComparisonService) {
    this.objectives = decisionData.objectives;

    this.pairComparisons = pairComparisonService.getComparisonTable();
  }

  get comparisonGridTransform() {
    return `translateY(-${this.activeComparison * 100}%)`;
  }

  openComparisonModal(objectiveIndexRow: number, objectiveIndexColumn: number) {
    this.pendingComparisons = [[objectiveIndexRow, objectiveIndexColumn]];
    this.activeComparison = 0;

    this.pairComparisons.forEach((row, rowIndex) =>
      row.forEach((comparison, columnIndex) => {
        if (
          comparison === 'missing' &&
          columnIndex > rowIndex &&
          (columnIndex !== objectiveIndexColumn || rowIndex !== objectiveIndexRow)
        ) {
          this.pendingComparisons.push([rowIndex, columnIndex]);
        }
      })
    );

    this.dialog.open(this.comparisonModalTemplate);
  }

  setComparisonResult(result: KlugCompareResult) {
    if (result) {
      const activeComparison = this.pendingComparisons[this.activeComparison];
      this.pairComparisons[activeComparison[0]][activeComparison[1]] = result;
    }

    if (this.activeComparison < this.pendingComparisons.length - 1) {
      this.activeComparison++;

      return false;
    } else {
      return true;
    }
  }

  get gridColumns() {
    return `repeat(${this.objectives.length + 1}, 150px)`;
  }
}
