import { Component, Input } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';
import { getAlternativeUtilities } from '@entscheidungsnavi/decision-data/calculation';
import { Alternative, Objective } from '@entscheidungsnavi/decision-data/classes';

@Component({
  selector: 'klug-evaluation-graph',
  templateUrl: './evaluation-graph.component.html',
  styleUrls: ['./evaluation-graph.component.scss'],
})
export class EvaluationGraphComponent {
  @Input() showKey = false;

  protected objectives: Objective[];

  protected sortedAlternatives: Alternative[];

  protected sortedUtilityValues: number[][];
  protected sortedDisplayValues: number[][];

  protected hoveredObjectiveIndex = -1;

  constructor(private decisionData: DecisionData) {
    this.objectives = decisionData.objectives;

    const weightedMatrix = decisionData.getWeightedUtilityMatrix();
    const alternativeUtilities = getAlternativeUtilities(weightedMatrix);

    const sortedAlternativeIndices = decisionData.alternatives.map((_, index) => index);
    sortedAlternativeIndices.sort(
      (alternativeIndexA, alternativeIndexB) => alternativeUtilities[alternativeIndexB] - alternativeUtilities[alternativeIndexA]
    );

    this.sortedAlternatives = sortedAlternativeIndices.map(index => this.decisionData.alternatives[index]);

    this.sortedUtilityValues = sortedAlternativeIndices.map(alternativeIndex => weightedMatrix[alternativeIndex]);

    this.sortedDisplayValues = this.sortedUtilityValues.map(alternative => alternative.map(value => Math.round(value * 10 * 10)));
  }

  get gridColumns() {
    return `repeat(${this.sortedAlternatives.length}, 1fr)`;
  }

  get gridRows() {
    if (this.showKey) {
      return 'max-content 1fr min-content';
    } else {
      return '1fr min-content';
    }
  }
}
