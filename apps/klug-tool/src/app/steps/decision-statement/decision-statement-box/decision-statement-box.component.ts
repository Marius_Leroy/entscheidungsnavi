import { Component } from '@angular/core';
import { DecisionData } from '@entscheidungsnavi/decision-data';

@Component({
  selector: 'klug-decision-statement-box',
  templateUrl: './decision-statement-box.component.html',
  styleUrls: ['./decision-statement-box.component.scss'],
})
export class DecisionStatementBoxComponent {
  constructor(protected decisionData: DecisionData) {}
}
