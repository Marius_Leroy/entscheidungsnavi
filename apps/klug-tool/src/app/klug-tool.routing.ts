import { inject, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessService } from './routing/access.service';
import { HasNoTokenGuard } from './routing/has-no-token-guard';
import { HasTokenGuard } from './routing/has-token-guard';
import { AlternativesComponent } from './steps/alternatives/alternatives.component';
import { AssessmentComponent } from './steps/assessment/assessment.component';
import { CodeInputComponent } from './steps/code-input/code-input.component';
import { ComparisonComponent } from './steps/comparison/comparison.component';
import { DecisionStatementComponent } from './steps/decision-statement/decision-statement.component';
import { EvaluationComponent } from './steps/evaluation/evaluation.component';
import { ObjectivesComponent } from './steps/objectives/objectives.component';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'code',
  },
  {
    path: 'code',
    component: CodeInputComponent,
    canActivate: [HasNoTokenGuard],
  },
  {
    path: '',
    canActivateChild: [HasTokenGuard],
    children: [
      {
        path: 'decision-statement',
        component: DecisionStatementComponent,
        canActivate: [() => inject(AccessService).canAccess('decision-statement')],
      },
      {
        path: 'objectives',
        component: ObjectivesComponent,
        canActivate: [() => inject(AccessService).canAccess('objectives')],
      },
      {
        path: 'options',
        component: AlternativesComponent,
        canActivate: [() => inject(AccessService).canAccess('options')],
      },
      {
        path: 'assessment',
        children: [
          {
            path: '',
            component: AssessmentComponent,
            canActivate: [() => inject(AccessService).canAccess('assessment')],
          },
          {
            path: 'comparison',
            component: ComparisonComponent,
            canActivate: [() => inject(AccessService).canAccess('assessment/comparison')],
          },
        ],
      },
      {
        path: 'evaluation',
        component: EvaluationComponent,
        canActivate: [() => inject(AccessService).canAccess('evaluation')],
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      initialNavigation: 'enabledBlocking',
    }),
  ],
  exports: [RouterModule],
})
export class KlugRoutingModule {}
