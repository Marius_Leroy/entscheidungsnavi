describe('Clickthrough Percy Test', () => {
  it('goes through all steps', () => {
    cy.visit('/');
    cy.get('[data-cy=start_container]').should('exist');
    cy.percySnapshot('Start Page');
    cy.loadAlex();

    // Decision Statement
    cy.getBySel(`step-decisionStatement`).click();
    cy.getBySel(`substep-0`).click();
    cy.percySnapshot('Entscheidungsfrage - Erste Formulierung');

    cy.getBySel(`substep-1`).click();
    cy.percySnapshot('Entscheidungsfrage - Grundlegende Werte');

    cy.getBySel(`substep-2`).click();
    cy.percySnapshot('Entscheidungsfrage - Impulsfragen');

    cy.getBySel(`substep-3`).click();
    cy.percySnapshot('Entscheidungsfrage - Überprüfung');

    cy.getBySel(`result-step`).click();
    cy.percySnapshot('Entscheidungsfrage - Ergebnis');

    // Objectives
    cy.getBySel(`step-objectives`).click();
    cy.getBySel(`substep-0`).click();
    cy.percySnapshot('Fundamentalziele - Erstes Brainstorming');

    cy.getBySel(`substep-1`).click();
    cy.percySnapshot('Fundamentalziele - Weitere Überlegungen');

    cy.getBySel(`substep-2`).click();
    cy.percySnapshot('Fundamentalziele - Beispielziele');

    cy.getBySel(`substep-3`).click();
    cy.percySnapshot('Fundamentalziele - Erste Zielhiearchie');

    cy.getBySel(`substep-4`).click();
    cy.percySnapshot('Fundamentalziele - Überprüfung');

    cy.getBySel(`result-step`).click();
    cy.percySnapshot('Fundamentalziele - Ergebnis');

    // Alternatives
    cy.getBySel(`step-alternatives`).click();
    cy.getBySel(`substep-0`).click();
    cy.percySnapshot('Alternativen - Bekannte Alternativen');

    cy.getBySel(`substep-1`).click();
    cy.percySnapshot('Alternativen - Finden von Schwachpunkten');

    cy.getBySel(`substep-2`).click();
    cy.percySnapshot('Alternativen - Zielfokussierte Suche');

    cy.getBySel(`substep-3`).click();
    cy.percySnapshot('Alternativen - Befragen anderer Leute');

    cy.getBySel(`substep-4`).click();
    cy.percySnapshot('Alternativen - Wichtige Stellhebel');

    cy.getBySel(`substep-5`).click();
    cy.percySnapshot('Alternativen - Sinnvolles Zusammenfassen');

    cy.getBySel(`substep-6`).click();
    cy.percySnapshot('Alternativen - Intuitives Ordnen');

    cy.getBySel(`result-step`).click();
    cy.percySnapshot('Alternativen - Ergebnis');

    // Wirkungsmodell
    cy.getBySel(`step-impactModel`).click();
    cy.percySnapshot('Wirkungsmodell');

    // Evaluation
    cy.getBySel(`step-results`).click();

    cy.getBySel(`substep-0`).click();
    cy.percySnapshot('Evaluation - Nutzenfunktionen');

    cy.getBySel(`substep-1`).click();
    cy.percySnapshot('Evaluation - Zielgewichtung');

    cy.getBySel(`result-step`).click();
    cy.percySnapshot('Evaluation - Ergebnis');

    cy.getBySel(`navline-element-sensitivity-analysis`).click();
    cy.percySnapshot('Evaluation - Sensitivitätsanalyse');

    cy.getBySel(`result-step`).click();

    cy.getBySel(`navline-element-pro-contra`).click();
    cy.percySnapshot('Evaluation - Pros und Kontras - Netzdiagramm');
    cy.selectOptionFromDtSelect('Netzdiagramm (absolut)', 'Balkendiagramm (relativ)');
    cy.percySnapshot('Evaluation - Pros und Kontras - Balkendiagramm - Standard');
    cy.selectOptionFromDtSelect('Standard', 'Zielgewichte');

    cy.percySnapshot('Evaluation - Pros und Kontras - Balkendiagramm - Zielgewichte');

    cy.getBySel(`result-step`).click();

    cy.getBySel(`navline-element-robustness-check`).click();
    // Disable all uncertainties to make sure the result is always identical
    cy.getBySel(`influence-factor-box`).find('mat-slide-toggle button').click({ force: true, multiple: true });
    cy.getBySel(`imprecision-box`).find('mat-slide-toggle button').click({ force: true, multiple: true });
    cy.getBySel(`robustness-recompute-button`).click();
    // Wait for the computation to complete
    cy.getBySel('robustness-resume-button').should('exist');
    // Hide the step count and runtime table
    cy.percySnapshot('Evaluation - Robustheitstest', { percyCSS: 'dt-robustness-check .steps-box-content table { display: none; }' });

    cy.getBySel(`result-step`).click();

    cy.getBySel(`navline-element-risk-comparison`).click();
    cy.getBySel('resume-button').should('exist');
    // Hide the step count and runtime table
    cy.percySnapshot('Evaluation - Risikovergleich', { percyCSS: 'dt-risk-comparison .calculation-info-box table { display: none; }' });

    cy.getBySel(`result-step`).click();

    // Abschlussbetrachtung
    cy.getBySel(`navline-element-finish-project`).click();
    cy.percySnapshot('Abschlussbetrachtung');
  });
});
